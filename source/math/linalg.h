#ifndef LINALG_H_
#define LINALG_H_

#include "bunkcore.h"
#include "linalgbezier.h"
namespace linalg
{
	fl32 distance(v2 &point1, v2 &point2);
	fl32 distance(v3 &point1, v3 &point2);

	// Bilinear interpolation using 4 points and 2 weights
	template<class v>
	v blerp(v p00, v p10, v p01, v p11, fl32 w1, fl32 w2)
	{
		fl32 dw1 = (1.f-w1);
		fl32 dw2 = (1.f-w2);

		return (p00*dw1*dw2) + (p10*w1*dw2) + (p01*dw1*w2) + (p11*w1*w2);
	}

	namespace lefthand
	{
		const v3 up = v3(0.f, 1.f, 0.f);
		const v3 look = v3(0.f, 0.f, 1.f);

		m4 lookAt(v3 position, v3 look, v3 up);
		m4 orthographic(fl32 viewWidth, fl32 viewHeight, fl32 nearZ, fl32 farZ);
		m4 perspective(fl32 fovY, fl32 aspectRatio, fl32 nearZ, fl32 farZ);
	}

	namespace rotation
	{
		m3 yawPitchRoll(fl32 y, fl32 p, fl32 r);
	}


}



#endif