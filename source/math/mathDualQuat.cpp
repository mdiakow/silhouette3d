#include "math/mathDualQuat.h"

math::dualQuaternion::dualQuaternion(quat rotation, v3 translation)
{
	this->rotation = rotation.normalized();
	quat q = (quat(0.f, translation.x() * 0.5f, translation.y() * 0.5f, translation.z() * 0.5f) * this->rotation);
	this->translation = q;
}

math::dualQuaternion::dualQuaternion(quat rotation, quat translation)
{
	this->rotation = rotation;
	this->translation = translation;
}

math::dualQuaternion::dualQuaternion()
{
	this->rotation = quat::Identity();
	this->translation = quat::Identity();
}

math::dualQuaternion math::dualQuaternion::operator+(dualQuaternion & rhs)
{
	quat translation = quat(this->translation.w() + rhs.translation.w(), this->translation.x() + rhs.translation.x(), this->translation.y() + rhs.translation.y(), this->translation.z() + rhs.translation.z());
	quat rotation = quat(this->rotation.w() + rhs.rotation.w(), this->rotation.x() + rhs.rotation.x(), this->rotation.y() + rhs.rotation.y(), this->rotation.z() + rhs.rotation.z());
	return dualQuaternion(rotation, translation);
}

math::dualQuaternion math::dualQuaternion::operator*(dualQuaternion rhs)
{
	//quat t1 = rhs.translation * this->rotation;
	//quat t2 = rhs.rotation * this->translation;
	//quat t3 = quat(t1.w()+t2.w(), t1.x()+t2.x(), t1.y()+t2.y(), t1.z()+t2.z());
	//return dualQuaternion(rhs.rotation * this->rotation, t3);
	quat t1 = this->rotation * rhs.translation;
	quat t2 = this->translation * rhs.rotation;
	quat t3 = quat(t1.w()+t2.w(), t1.x()+t2.x(), t1.y()+t2.y(), t1.z()+t2.z());
	return dualQuaternion(this->rotation * rhs.rotation, t3);
}

math::dualQuaternion math::dualQuaternion::conjugate()
{
	return dualQuaternion(this->rotation.conjugate(), this->translation.conjugate());
}

v3 math::dualQuaternion::transform(v3 point)
{
	quat qPoint = quat(0.f, point.x(), point.y(), point.z());
	quat qRotOut = (rotation*qPoint)*rotation.conjugate();
	v3 vOut = getTranslation() + v3(qRotOut.x(), qRotOut.y(), qRotOut.z());
	return vOut;
}

v3 math::dualQuaternion::getTranslation()
{
	//quat t = this->translation;
	//quat o = quat(t.w()*2.f, t.x()*2.f, t.y()*2.f, t.z()*2.f)*this->rotation.conjugate();

	dualQuaternion dq = dualQuaternion(this->rotation, this->translation);
	dq.normalize();
	
	quat t = dq.translation;
	quat o = quat(t.w()*2.f, t.x()*2.f, t.y()*2.f, t.z()*2.f)*this->rotation.conjugate();

	return v3(o.x(), o.y(), o.z());
}

math::dualQuaternion math::dualQuaternion::normalized()
{
	float m = this->rotation.norm();
	float im = 1.f/m;
	quat r = this->rotation;
	quat t = this->translation;
	return dualQuaternion(quat(r.w()*im, r.x()*im, r.y()*im, r.z()*im), quat(t.w()*im, t.x()*im, t.y()*im, t.z()*im));
}

void math::dualQuaternion::normalize()
{
	float m = this->rotation.norm();
	float im = 1.f/m;
	auto r = this->rotation;
	auto t = this->translation;

	this->rotation = quat(r.w()*im, r.x()*im, r.y()*im, r.z()*im);
	this->translation = quat(t.w()*im, t.x()*im, t.y()*im, t.z()*im);
	
}
