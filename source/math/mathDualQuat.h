#ifndef MATH_DQ_H_
#define MATH_DQ_H_

#include <Eigen/Core>
#include <Eigen/Dense>

using quat = Eigen::Quaternionf;
using v3 = Eigen::Vector3f;

namespace math
{
	struct dualQuaternion
	{
		// Makes a dual quaternion from a rotation quaternion and a translation vector.
		dualQuaternion(quat rotation, v3 translation);
		dualQuaternion(quat rotation, quat translation);
		dualQuaternion();
		
		// The rotation component in quaternion form.
		quat rotation;
		// The translation component in dual quaternion form.
		quat translation;
		// IE: dq = rotation + epsilon*translation

		dualQuaternion operator + (dualQuaternion &rhs);
		dualQuaternion operator * (dualQuaternion rhs);

		dualQuaternion normalized();
		dualQuaternion conjugate();

		// Take the point and transform it using this dual quaternion
		v3 transform(v3 point);

		v3 getTranslation();
		
		void normalize();
	};
}


#endif