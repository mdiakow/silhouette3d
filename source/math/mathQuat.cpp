#include "Math/mathQuat.h"

quat math::quaternion::fromEuler(fl32 yaw, fl32 pitch, fl32 roll)
{
	Eigen::AngleAxisf ya(yaw, v3::UnitY());
	Eigen::AngleAxisf pa(pitch, v3::UnitX());
	Eigen::AngleAxisf ra(roll, v3::UnitZ());

	//return quat((ra * ya * pa));
	//return quat((ya * pa * ra));
	return quat((pa*ra*ya));
}
