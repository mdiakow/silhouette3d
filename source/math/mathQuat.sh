struct dualQuaternion
{
	float4 rotation; // In quaternions
	float4 translation; 
};

float4 quatMul(float4 q1, float4 q2)
{
	return float4(q1.w*q2.xyz + q2.w*q1.xyz + cross(q1.xyz, q2.xyz), q1.w*q2.w - dot(q1.xyz, q2.xyz));
}

float4 quatConj(float4 q)
{
	return float4(-q.xyz, q.w);
}

float4 quatIden() {
	return float4(0, 0, 0, 1);
}

float4 quatFromEuler(float yaw, float pitch, float roll) 
{
	float4 q1 = float4(0, cos(yaw*0.5), 0, sin(yaw*0.5));
	float4 q2 = float4(cos(pitch*0.5), 0, 0, sin(pitch*0.5));
	float4 q3 = float4(0, 0, cos(roll*0.5), sin(roll*0.5));

	return quatMul(q1, quatMul(q2, q3));
}
// Get the translation component back from the dual quaternion
float4 dqTranslation(dualQuaternion dq)
{
	float4 output;

	output = quatMul((dq.translation * 2), quatConj(dq.rotation));
	//output.w = 1;

	return output;
}

float4 dqRotation(dualQuaternion dq) 
{
	return dq.rotation;
}

// Apply the transform on the point using dq
float4 dqTransform(dualQuaternion dq, float4 pt)
{
	float4 t = dqTranslation(dq);
	pt.w = 0;
	return t + quatMul(quatMul(dq.rotation, pt), quatConj(dq.rotation));
}

dualQuaternion dqCreate(float4 rotation, float4 translation)
{
	dualQuaternion output;
	output.rotation = rotation;
	translation.w = 0;
	output.translation = quatMul((translation * 0.5), rotation);
	return output;
}

dualQuaternion dqAdd(dualQuaternion lhs, dualQuaternion rhs)
{
	dualQuaternion dqOut;
	dqOut.rotation = lhs.rotation + rhs.rotation;
	dqOut.translation = lhs.translation + rhs.translation;
	return dqOut;
}

dualQuaternion dqMul(dualQuaternion lhs, dualQuaternion rhs)
{
	dualQuaternion dqOut;
	float4 q1 = quatMul(lhs.rotation, rhs.translation);
	float4 q2 = quatMul(lhs.translation, rhs.rotation);
	dqOut.rotation = quatMul(lhs.rotation, rhs.rotation);
	dqOut.translation = q1 + q2;
	return dqOut;
}

