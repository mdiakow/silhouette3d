#include "math/linalg.h"

#include "debugger/bunkdebug.h"

namespace linalg
{
	fl32 distance(v2 &p1, v2 &p2)
	{
		fl32 xd = p2(0) - p1(0);
		fl32 yd = p2(1) - p1(1);

		return sqrtf(xd*xd + yd*yd);
	}

	fl32 distance(v3 &point1, v3 &point2)
	{
		v3 vld = point2 - point1;

		return sqrtf(vld(0)*vld(0) + vld(1)*vld(1) + vld(2)*vld(2));
	}

	

}

m3 linalg::rotation::yawPitchRoll(fl32 y, fl32 p, fl32 r)
{
	Eigen::AngleAxisf ya(y, v3::UnitY());
	Eigen::AngleAxisf pa(p, v3::UnitX());
	Eigen::AngleAxisf ra(r, v3::UnitZ());

	quat qr = ra * ya * pa;
	return qr.matrix();
}

m4 linalg::lefthand::lookAt(v3 position, v3 look, v3 up)
{
	v3 z = (look - position).normalized();	
	v3 x = up.cross(z).normalized();
	v3 y = z.cross(x);

	m4 mat = m4::Zero();

	mat.row(0) << x(0), y(0), z(0), 0.f;
	mat.row(1) << x(1), y(1), z(1), 0.f;
	mat.row(2) << x(2), y(2), z(2), 0.f;
	mat.row(3) << -(x.dot(position)), -(y.dot(position)), -(z.dot(position)), 1.0f;

	return mat;
}

m4 linalg::lefthand::orthographic(fl32 viewWidth, fl32 viewHeight, fl32 nearZ, fl32 farZ)
{
	fl32 normalRange = 1.0f / (farZ - nearZ);

	m4 mat;
	mat.row(0) << 2.0f / viewWidth, 0.f, 0.f, 0.f;
	mat.row(1) << 0.f, 2.0f / viewHeight, 0.f, 0.f;
	mat.row(2) << 0.f, 0.f, normalRange, 0.f;
	mat.row(3) << 0.f, 0.f, -normalRange*nearZ, 1.f;

	return mat;
}

m4 linalg::lefthand::perspective(fl32 fovY, fl32 aspectRatio, fl32 nearZ, fl32 farZ)
{
	fl32 sn = sinf(fovY * 0.5f);
	fl32 cs = cosf(fovY * 0.5f);

	fl32 height = cs/sn;
	fl32 width = height / aspectRatio;

	fl32 normalRange = farZ / (farZ - nearZ);

	m4 mat;
	mat.row(0) << width, 0.f, 0.f, 0.f;
	mat.row(1) << 0.f, height, 0.f, 0.f;
	mat.row(2) << 0.f, 0.f, normalRange, 1.f;
	mat.row(3) << 0.f, 0.f, -normalRange*nearZ, 0.f;

	return mat;
}
