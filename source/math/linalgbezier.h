#ifndef LINALG_BEZIER_H_
#define LINALG_BEZIER_H_

#include "bunkcore.h"

namespace linalg
{
	template<class T> 
	struct bezier
	{
	public:
		// Calculate the bezier curve at t.
		T calculate(fl32 t);
		// Calculate the first derivative at t.
		T differentiate1(fl32 t);
		// Calculate the second derivative at t.
		T differentiate2(fl32 t);

		T start;
		T startControl;
		T endControl;
		T end;
	};

	template<class T>
	inline T bezier<T>::calculate(fl32 t)
	{
		fl32 td = 1.f-t;

		return (td*td*td)*start + (3.f*td*td*t)*startControl + (3.f*t*t*td)*endControl + (t*t*t)*end;
	}

	template<class T>
	inline T bezier<T>::differentiate1(fl32 t)
	{
		fl32 td = 1.f-t;

		return 3.f*(td*td)*(startControl - start) + 6.f*td*t*(endControl - startControl) + 3.f*t*t*(end - endControl);
	}

	template<class T>
	inline T bezier<T>::differentiate2(fl32 t)
	{
		fl32 td = 1.f-t;

		return 6.f*td*(endControl - 2.f*startControl + start) + 6.f*t*(end - 2.f*endControl + startControl);
	}
}


#endif