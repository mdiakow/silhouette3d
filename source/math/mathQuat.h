#ifndef MATH_QUATERNION_H_
#define MATH_QUATERNION_H_

#include "bunkcore.h"

namespace math
{
	namespace quaternion
	{
		quat fromEuler(fl32 yaw, fl32 pitch, fl32 roll);
	}


}

#endif