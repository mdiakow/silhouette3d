#ifndef SYSTEM_H_
#define SYSTEM_H_

#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//#include "input.h"
//#include "graphics.h"


namespace bunkcore
{
	void run();

	static LRESULT CALLBACK wndProc(HWND, UINT, WPARAM, LPARAM);

	void initialize();
	void cleanup();

	// Function to shutdown the program from other modules
	void shutdown();
}

#endif