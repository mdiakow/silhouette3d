#ifndef RANDOM_FILTER_H_
#define RANDOM_FILTER_H_

#include <vector>

#include "bunkcore.h"

namespace random
{
	// Takes a list of points and randomly returns a subset the size of min(points.size(), maxSize).
	std::vector<v3> points(std::vector<v3> points, ui32 maxSize);
}

#endif