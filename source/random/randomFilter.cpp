#include "random/randomFilter.h"

#include <random>
#include <unordered_set>
#include <algorithm>

std::vector<v3> random::points(std::vector<v3> points, ui32 maxSize)
{
	std::default_random_engine engine;
	std::uniform_int_distribution<ui32> rng(0, (ui32)points.size()-1);
	
	maxSize = std::min<ui32>(maxSize, (ui32)points.size());

	std::unordered_set<ui32> set(maxSize);
	// Generate maxSize values from the set.
	while (set.size() < maxSize) {
		set.insert(rng(engine));
	}

	std::vector<v3> out(maxSize);
	ui32 currentPoint = 0;
	for (auto &number : set) {
		out[currentPoint] = points[number];
		currentPoint++;
	}

	return out;
}
