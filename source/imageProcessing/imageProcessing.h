#ifndef IMAGE_PROCESSING_H_
#define IMAGE_PROCESSING_H_

#include "imageProcessing/ipKernel.h"
#include "imageProcessing/ipColourIsolate.h"
#include "imageProcessing/ipMeanShift.h"
#include "imageProcessing/marchingSquares/ipMarchingSquares.h"

#endif