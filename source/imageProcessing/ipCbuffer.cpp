#include "ipCbuffer.h"

namespace cbuffer
{
	D3D11_BUFFER_DESC edge::description()
	{
		D3D11_BUFFER_DESC d;

		d.Usage = D3D11_USAGE_DYNAMIC;
		d.ByteWidth = sizeof(cbuffer::edge);
		d.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		d.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		d.MiscFlags = 0;
		d.StructureByteStride = 0;

		return d;
	}
	D3D11_BUFFER_DESC colourIsolate::description()
	{
		D3D11_BUFFER_DESC d;

		d.Usage = D3D11_USAGE_DYNAMIC;
		d.ByteWidth = sizeof(cbuffer::edge);
		d.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		d.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		d.MiscFlags = 0;
		d.StructureByteStride = 0;

		return d;
	}
}