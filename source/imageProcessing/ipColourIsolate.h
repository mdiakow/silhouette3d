#ifndef IP_ISOLATE_COLOUR_H_
#define IP_ISOLATE_COLOUR_H_

#include "d3d.h"

namespace cbuffer
{
	struct isolateColour
	{
		//ui32 targetColour;
		//ui32 padding[3];
		v4 targetColour;
	};
}

namespace imageProcessing
{
	void isolateColour(ID3D11ShaderResourceView *inputView, ID3D11UnorderedAccessView *outputView, cbuffer::isolateColour cbuff);
}


#endif