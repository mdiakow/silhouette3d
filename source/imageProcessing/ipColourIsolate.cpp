#include "ImageProcessing/ipColourIsolate.h"
#include "render/shader/shader.h"
#include "buffer/constant/bufferConstant.h"
void imageProcessing::isolateColour(ID3D11ShaderResourceView *inputView, ID3D11UnorderedAccessView *outputView, cbuffer::isolateColour cbuff)
{
	auto context = d3d::getDeviceContext();
	auto device = d3d::getDevice();
	//HRESULT result;

	ID3D11ComputeShader *shader = shader::compute::load(L"../Engine_Bunk/imageProcessing/ipColourIsolate.cms");

	//D3D11Buffer *cbuffer = shader::cbuffer::create(cbuff);
	auto cbuffer = buffer::constant<cbuffer::isolateColour>(cbuff);
	context->CSSetShader(shader, nullptr, 0);
	context->CSSetConstantBuffers(0, 1, cbuffer.data.GetAddressOf());
	context->CSSetShaderResources(0, 1, &inputView);
	context->CSSetUnorderedAccessViews(0, 1, &outputView, nullptr);

	context->Dispatch(32, 32, 1);

	shader->Release();
	//cbuffer->Release();
}
