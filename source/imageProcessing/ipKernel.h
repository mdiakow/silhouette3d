#ifndef IP_KERNEL_H_
#define IP_KERNEL_H_

#include "d3d.h"
//#include "canvas/canvasData.h"

namespace imageProcessing
{
	struct kernel
	{
		kernel(ui32 width, ui32 height, fl32* weights);
		~kernel();

		static kernel *edge3x3();

		ID3D11Texture2D *texture2d; // The weights of the kernel.
		ID3D11ShaderResourceView *textureView;
		
		//void apply(canvas::data *cnvs);
		void apply(ID3D11ShaderResourceView *inputView, ID3D11UnorderedAccessView *outputView);
	};
	
}


#endif