#ifndef IP_MEAN_SHIFT_H_
#define IP_MEAN_SHIFT_H_

#include "d3d.h"
//#include "canvas/canvasData.h"
#include "imageProcessing/ipKernel.h"

namespace cbuffer
{
	struct meanShift
	{
		v2 center;
		v2 windowOffset;
		v4 padding;
	};
}

namespace imageProcessing
{
	void meanShift(ID3D11ShaderResourceView *inputView, ID3D11UnorderedAccessView *outputView, cbuffer::meanShift input);
}

#endif