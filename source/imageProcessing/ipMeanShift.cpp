#include "imageProcessing/ipMeanShift.h"
#include "render/shader/shader.h"
#include "buffer/constant/bufferConstant.h"
void imageProcessing::meanShift(ID3D11ShaderResourceView *inputView, ID3D11UnorderedAccessView *outputView, cbuffer::meanShift input)
{
	auto context = d3d::getDeviceContext();
	auto device = d3d::getDevice();
	//HRESULT result;

	ID3D11ComputeShader *shader = shader::compute::load(L"../Engine_Bunk/imageProcessing/meanShift.cms");

	//ID3D11Buffer *cbuffer = shader::cbuffer::create(input);
	auto cbuffer = buffer::constant<cbuffer::meanShift>(input);

	context->CSSetShader(shader, nullptr, 0);
	context->CSSetConstantBuffers(0, 1, cbuffer.data.GetAddressOf());
	context->CSSetShaderResources(0, 1, &inputView);
	context->CSSetUnorderedAccessViews(0, 1, &outputView, nullptr);

	context->Dispatch(1, 1, 1);

	shader->Release();
}
