#ifndef IP_MARCHING_SQUARES_H_
#define IP_MARCHING_SQUARES_H_

#include "D3D.h"
#include "math/linalgbezier.h"

namespace imageProcessing
{
	namespace marchingSquares
	{
		// Returns the cell index from the marching squares algorithm. There are 16 different values packed
		// in to the RGBA channels.
		void cellIndex(ID3D11ShaderResourceView *inputView, ID3D11UnorderedAccessView *outputView);

		// Take the indexed cell output and convert it in to bezier curves.
		void indexToBezier(ID3D11ShaderResourceView *inputView, ID3D11UnorderedAccessView *outputView);


	}
	
	
}


#endif