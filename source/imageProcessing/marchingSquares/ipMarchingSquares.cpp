#include "imageProcessing/marchingSquares/ipMarchingSquares.h"
#include "render/shader/shader.h"

void imageProcessing::marchingSquares::cellIndex(ID3D11ShaderResourceView * inputView, ID3D11UnorderedAccessView * outputView)
{
	auto context = d3d::getDeviceContext();

	ID3D11ComputeShader *shader = shader::compute::load(L"../Engine_Bunk/imageProcessing/marchingSquares/ipMsCellIndex.cms");

	context->CSSetShader(shader, nullptr, 0);
	context->CSSetShaderResources(0, 1, &inputView);
	context->CSSetUnorderedAccessViews(0, 1, &outputView, nullptr);

	context->Dispatch(16, 16, 1);

	shader->Release();
}

void imageProcessing::marchingSquares::indexToBezier(ID3D11ShaderResourceView * inputView, ID3D11UnorderedAccessView * outputView)
{
	auto context = d3d::getDeviceContext();

	
}
