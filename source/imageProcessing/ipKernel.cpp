#include "ipKernel.h"
#include "render/render.h"

#include <d3dcompiler.h>
#include <fstream>

#include "render/shader/shader.h"

namespace imageProcessing
{
	kernel::kernel(ui32 width, ui32 height, fl32* weights)
	{
		HRESULT result;
		auto device = d3d::getDevice();
		
		// Set up and create the texture2d.
		D3D11_TEXTURE2D_DESC texDesc;
		texDesc.Height = height;
		texDesc.Width = width;
		texDesc.MipLevels = 1;
		texDesc.ArraySize = 1;
		texDesc.Format = DXGI_FORMAT_R32_FLOAT;
		texDesc.SampleDesc.Count = 1;
		texDesc.SampleDesc.Quality = 0;
		texDesc.Usage = D3D11_USAGE_DEFAULT;
		texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		texDesc.CPUAccessFlags = 0;
		texDesc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA initialTexData;
		initialTexData.pSysMem = weights;
		initialTexData.SysMemPitch = width*4;
		initialTexData.SysMemSlicePitch = 0;

		result = device->CreateTexture2D(&texDesc, &initialTexData, &this->texture2d);

		// Create the view.
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = texDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = 1;

		result = device->CreateShaderResourceView(this->texture2d, &srvDesc, &this->textureView);
	}

	kernel::~kernel()
	{
		if(texture2d) texture2d->Release();
		if(textureView) textureView->Release();
	}

	kernel *kernel::edge3x3()
	{
		fl32 texData[9];
		for (ui32 i = 0; i < 9; i++) {
			texData[i] = (-1.f/8.f);
		}
		texData[4] = 1.f;
		auto knl = new kernel(3, 3, texData);

		return knl;
	}

	//void kernel::apply(canvas::data *cnvs)
	//{
	//	auto context = d3d::getDeviceContext();
	//	auto device = d3d::getDevice();
	//	HRESULT result;

	//	ID3D11ComputeShader *shader = shader::compute::load(L"../Engine_Bunk/imageProcessing/ipKernel.cms");

	//	// Create an additional texture of same size as a tile for compute shader input
	//	D3D11_TEXTURE2D_DESC textureDesc;
	//	cnvs->tiles[0].texture2d->GetDesc(&textureDesc);

	//	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	//	cnvs->tiles[0].textureView->GetDesc(&srvDesc);

	//	ID3D11Texture2D *inputTexture;
	//	result = device->CreateTexture2D(&textureDesc, nullptr, &inputTexture);

	//	ID3D11ShaderResourceView *inputView;
	//	result = device->CreateShaderResourceView(inputTexture, &srvDesc, &inputView);
	//	
	//	context->CSSetShader(shader, nullptr, 0);
	//	context->CSSetShaderResources(0, 1, &inputView);
	//	context->CSSetShaderResources(1, 1, &this->textureView);
	//	context->CSSetUnorderedAccessViews(0, 1, &cnvs->outputView, nullptr);

	//	for (ui32 i = 0; i < cnvs->size(); i++) {
	//		context->CopyResource(inputTexture, cnvs->tiles[i].texture2d);
	//		context->CopyResource(cnvs->outputTexture, cnvs->tiles[i].texture2d);
	//					
	//		context->Dispatch(32,32,1);

	//		context->CopyResource(cnvs->tiles[i].texture2d, cnvs->outputTexture);
	//	}

	//	inputView->Release();
	//	inputView = nullptr;

	//	inputTexture->Release();
	//	inputTexture = nullptr;

	//	shader->Release();
	//}

	void kernel::apply(ID3D11ShaderResourceView *inputView, ID3D11UnorderedAccessView *outputView)
	{
		auto context = d3d::getDeviceContext();
		auto device = d3d::getDevice();
		//HRESULT result;

		ID3D11ComputeShader *shader = shader::compute::load(L"../Engine_Bunk/imageProcessing/applyKernel.cms");

		context->CSSetShader(shader, nullptr, 0);
		context->CSSetShaderResources(0, 1, &inputView);
		context->CSSetShaderResources(1, 1, &this->textureView);
		context->CSSetUnorderedAccessViews(0, 1, &outputView, nullptr);

		context->Dispatch(32, 32, 1);

		shader->Release();
	}
}