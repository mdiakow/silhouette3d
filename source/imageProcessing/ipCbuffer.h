#ifndef IP_CBUFFER_H_
#define IP_CBUFFER_H_

#include "bunkcore.h"
#include "d3d.h"

namespace cbuffer
{
	struct edge
	{
		ui32 imageWidth;
		ui32 imageHeight;
		ui32 padding[2];

		static D3D11_BUFFER_DESC description();
	};

	struct colourIsolate
	{
		ui32 imageWidth;
		ui32 imageHeight;
		ui32 targetColour;
		ui32 voidColour;

		static D3D11_BUFFER_DESC description();
	};

	// Creates a constant buffer from whichever struct it is given.
	/*
	template<class data>
	ID3D11Buffer *create(data structData)
	{
		D3D11_BUFFER_DESC description;
		description.Usage = D3D11_USAGE_DYNAMIC;
		description.ByteWidth = sizeof(data);
		description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		description.MiscFlags = 0;
		description.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA initData;
		initData.pSysMem = &structData;
		initData.SysMemPitch = 0;
		initData.SysMemSlicePitch = 0;

		ID3D11Buffer *buffer;
		d3d::getDevice()->CreateBuffer(&description, &initData, &buffer);

		return buffer;
	}*/
}



#endif