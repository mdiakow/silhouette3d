// coreInput.h
// This module holds functionality to set the state of the input module.
// It also holds windows specific functionality to handle incoming windows messages related
// to system input.
#ifndef CORE_INPUT_H_
#define CORE_INPUT_H_

#include <windows.h>

#include "bunkcore.h"

namespace input
{
	namespace mouse
	{
		void setPosition(ui16 x, ui16 y);
	}

	namespace key
	{
		void setDown(ui8 key);
		void setUp(ui8 key);
	}

	// Function sends and interprets message from windows.
	void poll();

	// Function to handle incoming messages from windows to set internal input state.
	LRESULT CALLBACK messageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam);

	void initialize();
	void cleanup();
}

#endif