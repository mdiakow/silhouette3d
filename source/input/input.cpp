#include "input/coreInput.h"
#include "input/Input.h"
#include "render/render.h"
#include "debugger/bunkdebug.h"

#include "userInterface/text/uiTextInput.h"



namespace input
{
	MSG msg;

	const ui32 keyCount = 256;

	bool keyIsPressed[keyCount];
	bool keyWasPressed[keyCount];

	ui16 mouseX;
	ui16 mouseY;

	// input.h
	namespace mouse
	{
		v2 getPosition()
		{
			return v2((fl32)mouseX, (fl32)mouseY);
		}
		namespace position
		{
			v2 screen()
			{
				return v2((fl32)mouseX, (fl32)mouseY);
			}

			v2 world()
			{
				v2 halfScreen = render::getScreenSize() * 0.5f;

				v2 mousePosition = input::mouse::getPosition() - halfScreen;
				mousePosition(1) *= -1.f;

				v3 camPos = render::getCameraPosition();

				v2 cameraPosition = v2(camPos(0), camPos(1));

				v2 globalPosition = cameraPosition + mousePosition;

				globalPosition(0) = roundf(globalPosition(0));
				globalPosition(1) = roundf(globalPosition(1));

				return globalPosition;
			}

			v2 ui()
			{
				v2 halfScreen = render::getScreenSize() * 0.5f;

				v2 mousePosition = input::mouse::getPosition() - halfScreen;
				mousePosition(1) *= -1.f;

				return mousePosition;
			}
		}
	}

	namespace key
	{
		bool onPress(ui16 key)
		{
			return (keyIsPressed[key] && !keyWasPressed[key]);
		}

		bool onRelease(ui16 key)
		{
			return (!keyIsPressed[key] && keyWasPressed[key]);
		}

		bool isDown(ui16 key)
		{
			return keyIsPressed[key];
		}

		bool isHeld(ui16 key)
		{
			return (keyIsPressed[key] && keyWasPressed[key]);
		}
	}

	// end input.h

	// coreInput.h
	namespace mouse
	{
		void setPosition(ui16 x, ui16 y)
		{
			mouseX = x;
			mouseY = y;
		}
	}

	namespace key
	{
		void setDown(ui8 key)
		{
			keyIsPressed[key] = true;
		}

		void setUp(ui8 key)
		{
			keyIsPressed[key] = false;
		}
	}

	namespace text
	{
		// NOTE: Probably not thread safe, I wonder if a better solution would be to just pass a
		// pointer to a string and check if it is null.
		str internalBuffer = "";
		
		
		userInterface::text8::input *currentInput = nullptr;
		str buffer()
		{
			str output = internalBuffer;
			
			// Consume the output by resetting the string.
			internalBuffer.clear();
			return output;
		}
		
		void setFocus(userInterface::text8::input *newInput)
		{
			if (newInput) {
				if (currentInput) {
					// Reset old focus, so that it no longer takes the input buffer.
					currentInput->isFocused = false;
				}
				internalBuffer.clear();
				currentInput = newInput;
				currentInput->line.font->cursor.setPosition(currentInput->line.getNextCharacterPosition());
				currentInput->isFocused = true;
			} else {
				internalBuffer.clear();
				currentInput = nullptr;
			}
		}


	}

	void poll()
	{
		for (int i = 0; i < keyCount; i++) {
			keyWasPressed[i] = keyIsPressed[i];
		}

		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	void initialize()
	{
		ZeroMemory(&msg, sizeof(MSG));

		for (int i = 0; i < keyCount; i++) {
			keyIsPressed[i] = false;
			keyWasPressed[i] = false;
		}


	}

	LRESULT CALLBACK messageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
	{
		
		switch (umsg) {
		case WM_CHAR:
			//debug::log((char)wparam);
			text::internalBuffer += (char)wparam;
			//debug::log(text::internalBuffer);
			return 0;
		case WM_KEYDOWN:

			//debug::log();
			input::key::setDown((ui8)wparam);
			return 0;
		case WM_KEYUP:
			input::key::setUp((ui8)wparam);
			return 0;
		case WM_LBUTTONDOWN:
			input::key::setDown((ui8)wparam);
			return 0;
		case WM_LBUTTONUP:
			input::key::setUp(VK_LBUTTON); // Standard wparam wasn't returning the same value it was setting.
			return 0;
		case WM_MOUSEMOVE:
			POINTS current = MAKEPOINTS(lparam);
			input::mouse::setPosition(current.x, current.y);
			return 0;
		default:
			return DefWindowProc(hwnd, umsg, wparam, lparam);
		}
	}

	void cleanup()
	{

	}
	// end coreInput.h
}