#include "inputAxes.h"
#include "input.h"
/*
input::axes::axes(
	std::unique_ptr<ui16[]> forward, ui16 forwardCount,
	std::unique_ptr<ui16[]> back, ui16 backCount,
	std::unique_ptr<ui16[]> up, ui16 upCount,
	std::unique_ptr<ui16[]> down, ui16 downCount,
	std::unique_ptr<ui16[]> right, ui16 rightCount,
	std::unique_ptr<ui16[]> left, ui16 leftCount
) :
	forward(std::move(forward)), forwardCount(forwardCount),
	back(std::move(back)), backCount(backCount),
	up(std::move(up)), upCount(upCount),
	down(std::move(down)), downCount(downCount),
	right(std::move(right)), rightCount(rightCount),
	left(std::move(left)), leftCount(leftCount),
	direction(v3::Zero())
{

}

input::axes::axes(
	ui16 *forward, ui16 forwardCount,
	ui16 *back, ui16 backCount,
	ui16 *up, ui16 upCount,
	ui16 *down, ui16 downCount,
	ui16 *right, ui16 rightCount,
	ui16 *left, ui16 leftCount
) :
	forward(std::unique_ptr<ui16[]>(forward)), forwardCount(forwardCount),
	back(std::unique_ptr<ui16[]>(back)), backCount(backCount),
	up(std::unique_ptr<ui16[]>(up)), upCount(upCount),
	down(std::unique_ptr<ui16[]>(down)), downCount(downCount),
	left(std::unique_ptr<ui16[]>(left)), leftCount(leftCount),
	right(std::unique_ptr<ui16[]>(right)), rightCount(rightCount),
	direction(v3::Zero())
{

}


input::axes::axes(axes && other) :
	forward(std::move(other.forward)), forwardCount(other.forwardCount),
	back(std::move(other.back)), backCount(other.backCount),
	up(std::move(other.up)), upCount(other.upCount),
	down(std::move(other.down)), downCount(other.downCount),
	right(std::move(other.right)), rightCount(other.rightCount),
	left(std::move(other.left)), leftCount(other.leftCount),
	direction(v3::Zero())
{
}*/
/*
input::axes & input::axes::operator=(axes && other)
{
	if (this != &other) {
		forward = std::move(other.forward);
		forwardCount = other.forwardCount;

		back = std::move(other.back);
		backCount = other.backCount;

		forward = std::move(other.forward);
		forwardCount = other.forwardCount;

		forward = std::move(other.forward);
		forwardCount = other.forwardCount;

		forward = std::move(other.forward);
		forwardCount = other.forwardCount;

		forward = std::move(other.forward);
		forwardCount = other.forwardCount;
	}
	return *this;
}
*/

/*
input::axes::axes(
	ui16 *forward, ui16 forwardCount,
	ui16 *back, ui16 backCount,
	ui16 *up, ui16 upCount,
	ui16 *down, ui16 downCount,
	ui16 *right, ui16 rightCount,
	ui16 *left, ui16 leftCount)
{
	this->forward = forward;
	this->forwardCount = forwardCount;

	this->back = back;
	this->backCount = backCount;

	this->up = up;
	this->upCount = upCount;

	this->down = down;
	this->downCount = downCount;

	this->right = right;
	this->rightCount = rightCount;

	this->left = left;
	this->leftCount = leftCount;

	direction = v3::Zero();
}*/

/*
input::axes::axes(const axes & other) :
	forward(other.forwardCount > 0 ? new ui16[other.forwardCount] : nullptr), forwardCount(other.forwardCount),
	up(other.upCount > 0 ? new ui16[other.upCount] : nullptr), upCount(other.upCount),
	back(other.backCount > 0 ? new ui16[other.backCount] : nullptr), backCount(other.backCount),
	down(other.downCount > 0 ? new ui16[other.downCount] : nullptr), downCount(other.downCount),
	right(other.rightCount > 0 ? new ui16[other.rightCount] : nullptr), rightCount(other.rightCount),
	left(other.leftCount > 0 ? new ui16[other.leftCount] : nullptr), leftCount(other.leftCount),
	direction(other.direction)
{


}

input::axes::~axes()
{
	if(up) delete[] up;
	if(down) delete[] down;
	if(forward) delete[] forward;
	if(back) delete[] back;
	if(left) delete[] left;
	if(right) delete[] right;
}*/


input::axes::axes(
	ui16 *forward, ui16 forwardCount,
	ui16 *back, ui16 backCount,
	ui16 *up, ui16 upCount,
	ui16 *down, ui16 downCount,
	ui16 *right, ui16 rightCount,
	ui16 *left, ui16 leftCount
) :
	forward(std::unique_ptr<ui16[]>(forward)), forwardCount(forwardCount),
	back(std::unique_ptr<ui16[]>(back)), backCount(backCount),
	up(std::unique_ptr<ui16[]>(up)), upCount(upCount),
	down(std::unique_ptr<ui16[]>(down)), downCount(downCount),
	right(std::unique_ptr<ui16[]>(right)), rightCount(rightCount),
	left(std::unique_ptr<ui16[]>(left)), leftCount(leftCount)
{

}

v3 input::axes::getAxes()
{
	direction(2) = 0.0f;
	for (ui16 i = 0; i < forwardCount; i++) {
		if (input::key::isDown(forward[i])) {
			direction(2) = 1.0f;
			break;
		}
	}

	for (ui16 i = 0; i < backCount; i++) {
		if (input::key::isDown(back[i])) {
			direction(2) = -1.0f;
			break;
		}
	}

	direction(1) = 0.0f;
	for (ui16 i = 0; i < upCount; i++) {
		if (input::key::isDown(up[i])) {
			direction(1) = 1.0f;
			break;
		}
	}

	for (ui16 i = 0; i < downCount; i++) {
		if (input::key::isDown(down[i])) {
			direction(1) = -1.0f;
			break;
		}
	}

	direction(0) = 0.0f;
	for (ui16 i = 0; i < rightCount; i++) {
		if (input::key::isDown(right[i])) {
			direction(0) = 1.0f;
			break;
		}
	}

	for (ui16 i = 0; i < leftCount; i++) {
		if (input::key::isDown(left[i])) {
			direction(0) = -1.0f;
			break;
		}
	}



	return direction;
}