#include "inputButton.h"
#include "input.h"

input::button::button(ui16 *indices, ui16 indexCount)
{
	this->indices = indices;
	this->indexCount = indexCount;	

}

input::button::~button()
{
	delete[] indices;
}

bool input::button::isDown()
{
	for (ui16 i = 0; i < indexCount; i++) {
		if (input::key::isDown(indices[i])) {
			return true;
		}
	} // otherwise no buttons were pressed
	return false;
}

bool input::button::isHeld()
{
	for (ui16 i = 0; i < indexCount; i++) {
		if (input::key::isHeld(indices[i])) {
			return true;
		}
	} // otherwise no buttons were pressed
	return false;
}

bool input::button::onPress()
{
	for (ui16 i = 0; i < indexCount; i++) {
		if (input::key::onPress(indices[i])) {
			return true;
		}
	} // otherwise no buttons were pressed
	return false;
}

bool input::button::onRelease()
{
	for (ui16 i = 0; i < indexCount; i++) {
		if (input::key::onRelease(indices[i])) {
			return true;
		}
	} // otherwise no buttons were pressed
	return false;
}