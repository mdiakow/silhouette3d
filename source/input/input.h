#ifndef INPUT_H_
#define INPUT_H_

#include "inputCodes.h"

#include "bunkcore.h"
//#include "linalg.h"

//#include "button.h"
//#include "inputButton.h"
//#include "inputAxes.h"
//#include "axes.h"

// Forward declaration
namespace userInterface::text8 { class input; }

namespace input
{
	namespace mouse
	{
		v2 getPosition();
		namespace position
		{
			v2 screen();
			// mouse position in world space from camera
			v2 world();
			// mouse position in UI space
			v2 ui();
		}
	}

	namespace key
	{
		bool onPress(ui16 key);
		bool onRelease(ui16 key);
		bool isDown(ui16 key);
		bool isHeld(ui16 key);
	}

	namespace text
	{
		// Returns and consumes the current text buffer.
		str buffer();
		void setFocus(userInterface::text8::input *newInput);
	}

}

#endif