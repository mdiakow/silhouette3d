#ifndef INPUT_AXES_H_
#define INPUT_AXES_H_

//#include "linalg.h"
#include "bunkcore.h"

namespace input
{
	class axes
	{
	public:
		//axes &operator=(axes&& other);
		
		axes(
			ui16 *forward, ui16 forwardCount,
			ui16 *back, ui16 backCount,
			ui16 *up, ui16 upCount,
			ui16 *down, ui16 downCount,
			ui16 *right, ui16 rightCount,
			ui16 *left, ui16 leftCount
		);

		//axes(axes &&other);
		
		//axes(const axes &other);
		/*
		axes &operator=(const axes &other);
		axes(axes &&other);
		axes &operator=(axes&& other);
		*/


		//~axes();

		v3 getAxes();
	private:
		//ui16 *forward, *back, *up, *down, *right, *left;
		std::unique_ptr<ui16[]> forward, back, up, down, right, left;
		ui16 forwardCount, backCount, upCount, downCount, rightCount, leftCount;
		v3 direction;

		
	};
}

#endif