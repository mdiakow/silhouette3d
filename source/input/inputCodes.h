#ifndef INPUT_CODES_H_
#define INPUT_CODES_H_

#include "bunkcore.h"
#include <Windows.h>

namespace input
{
	namespace mouse
	{
		static ui16 leftButton = VK_LBUTTON;
		static ui16 rightButton = VK_RBUTTON;
		static ui16 middleButton = VK_MBUTTON;
	}

	namespace key
	{
		static const ui8 tilde = 0xC0;
		static const ui8 enter = 0x0D;
		static const ui8 backspace = 0x08;
		static const ui16 up = VK_UP;
		static const ui16 down = VK_DOWN;
		static const ui16 left = VK_LEFT;
		static const ui16 right = VK_RIGHT;
		static const ui16 space = VK_SPACE;
		static const ui16 control = VK_CONTROL;
	}
}


#endif