#ifndef INPUT_BUTTON_H_
#define INPUT_BUTTON_H_

#include "bunkcore.h"

namespace input
{
	class button
	{
	public:
		button(ui16 *indices, ui16 indexCount);
		~button();

		bool isDown();
		bool isHeld();

		bool onPress();
		bool onRelease();
	private:
		ui16 *indices;
		ui16 indexCount;
	};
}


#endif