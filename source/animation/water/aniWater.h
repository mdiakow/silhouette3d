#ifndef ANIMATION_WATER_H_
#define ANIMATION_WATER_H_

#include "bunkcore.h"

#include "buffer/texture/bufTexInOut2d.h"
#include "buffer/texture/bufTex2d.h"
#include "buffer/structured/bufferStructured.h"
#include "buffer/constant/bufferConstant.h"

#include <vector>

namespace vertex
{
	struct water
	{
		v3 position;
	};
}

namespace animation
{
	namespace water
	{
		// One time constant buffer set up.
		struct oneTime
		{
			fl32 scale;
			fl32 constPad[3];
		};
		// Per frame constant buffer setup.
		struct perFrame
		{
			fl32 deltaTime;
			fl32 lifetime;
			fl32 pad[2];
		};

		struct waveTransform
		{
			fl32 amplitude;
			fl32 frequency;
			v2 direction;
			fl32 speed;
		};

		class surface
		{
		public:
			
			surface(ui32 width, ui32 height, fl32 scale, buffer::structured::input<waveTransform> waveTransforms);

		public:
			void update();
			void render();
		public:
			// Height of each vertex along the instanced mesh.
			buffer::texture::inputOutput2d heightMap;

			buffer::texture::input2d heightMapIn;
			buffer::texture::output2d heightMapOut;

			buffer::constant<perFrame> perFrameBuffer;
			buffer::constant<oneTime> oneTimeBuffer;
			
			buffer::structured::input<waveTransform> waveTransforms;
		};
	}
}


#endif