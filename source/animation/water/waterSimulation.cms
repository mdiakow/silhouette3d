///////////////////////////////////
// CONSTANT BUFFER INPUT
///////////////////////////////////

cbuffer constBuffer : register(b0)
{
	float scale;
	float constPad[3];
};

cbuffer perFrame : register(b1)
{
	float deltaTime;
	float lifetime;
	float pfp1;
	float pfp2;
};

///////////////////////////////////
// DATA STRUCTURES
///////////////////////////////////
struct waveTransform
{
	float amplitude;
	float frequency;
	float2 direction;
	float speed;
};

///////////////////////////////////
// INPUT/OUTPUT
///////////////////////////////////

StructuredBuffer<waveTransform> waveTransforms : register(t0);

RWTexture2D<float> heightMap : register(u0);

///////////////////////////////////
// HELPER FUNCTIONS
///////////////////////////////////

static const float pi = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899;

[numthreads(32, 32, 1)]
void csMain(uint3 dt : SV_DispatchThreadID)
{
	uint width, height;
	heightMap.GetDimensions(width, height);

	if (dt.x < width && dt.y < height) {
		float x = (float)dt.x*scale;
		float y = (float)dt.y*scale;
		float speed = 1.f;

		uint waveTransformsCount, junk;
		waveTransforms.GetDimensions(waveTransformsCount, junk);
		
		waveTransform wt;
		float2 position = float2(x,y);		

		float waveHeight = 0.f;
		for (uint i = 0; i < waveTransformsCount; i++) {
			wt = waveTransforms[i];
			
			float currentWave = /*wt.amplitude**/sin(wt.frequency*dot(wt.direction, position) + wt.speed*lifetime);
			currentWave = wt.amplitude*pow(currentWave, 5);
			waveHeight += currentWave;
		}

		heightMap[dt.xy] = waveHeight;
	}
}

// Wave propogation dispersion test : https://en.wikipedia.org/wiki/Dispersion_(water_waves)
//float amplitude = 10.f;
//float wavelength = 1.0f;
//float period = 0.5f;
//
//float theta = 2*pi*(x/wavelength - lifetime/period);

//heightMap[dt.xy] = amplitude*sin(theta);



// Longitudinal wave tests : https://en.wikipedia.org/wiki/Longitudinal_wave
//float amplitude = 10.f;
//float speed = 4.0f;
//float angularFrequency = pi*0.025;

//float dist = 10.f*distance(float2(dt.xy), float2(0,0));
//heightMap[dt.xy] = amplitude*cos( (angularFrequency*(100.f*lifetime - dist/speed)) );