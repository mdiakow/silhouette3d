#include "animation/water/aniWater.h"

#include <filesystem>
#include <array>

#include "render/shader/common/shaderHelper.h"
#include "systemCore/time/bunktime.h"

namespace fp
{
	static std::filesystem::path simulate = "source/animation/water/waterSimulation.cms";
}

namespace initialize
{
	static buffer::structured::input<vertex::water> vertices(ui32 width, ui32 height, fl32 scale);
	static buffer::texture::inputOutput2d heightMap(ui32 width, ui32 height);

	static D3D11_TEXTURE2D_DESC textureDesc(ui32 width, ui32 height);
}

namespace animation::water
{
	surface::surface(ui32 width, ui32 height, fl32 scale, buffer::structured::input<waveTransform> waveTransforms) :
		heightMap{ initialize::heightMap(width, height) },
		perFrameBuffer{ perFrame() },
		oneTimeBuffer{ oneTime() },
		waveTransforms{ waveTransforms },

		heightMapIn{ initialize::textureDesc(width, height) },
		heightMapOut{ initialize::textureDesc(width, height) }
	{
		oneTime ot;
		ot.scale = scale;
		oneTimeBuffer.set(ot);
	}
	// Compute shader constants.
	static fl32 numThreadsX = 32.f;
	static fl32 numThreadsY = 32.f;

	void surface::update()
	{
		auto context = d3d::getDeviceContext();

		auto simulateShader = shader::compute::find(fp::simulate);
		context->CSSetShader(simulateShader.Get(), 0, 0);

		perFrame perFrameData;

		perFrameData.deltaTime = bunktime::delta();
		perFrameData.lifetime = bunktime::total();

		perFrameBuffer.set(perFrameData);

		// Set up shader inputs
		std::array<ID3D11Buffer*, 2> constantBuffers = {
			oneTimeBuffer.data.Get(),
			perFrameBuffer.data.Get()
		};
		context->CSSetConstantBuffers(0, 2, constantBuffers.data());

		context->CSSetUnorderedAccessViews(0, 1, heightMap.uaView.GetAddressOf(), nullptr);

		context->CSSetShaderResources(0, 1, waveTransforms.view.GetAddressOf());

		auto [heightMapWidth, heightMapHeight] = heightMap.dimensions();

		fl32 x = (fl32)heightMapWidth;
		fl32 y = (fl32)heightMapHeight;

		ui32 dispatchX = (ui32)std::ceil(x/numThreadsX);
		ui32 dispatchY = (ui32)std::ceil(y/numThreadsY);

		context->Dispatch(dispatchX, dispatchY, 1);
		
		std::array<ID3D11UnorderedAccessView*, 1> nullViews = {
			nullptr
		};
		context->CSSetUnorderedAccessViews(0, (ui32)nullViews.size(), nullViews.data(), 0);	
	}

	static const ui32 heightMapDivisor = 2;

	void surface::render()
	{
		auto context = d3d::getDeviceContext();

		context->VSSetConstantBuffers(1, 1, oneTimeBuffer.data.GetAddressOf());
		context->VSSetShaderResources(0, 1, heightMap.srView.GetAddressOf());

		auto [heightMapWidth, heightMapHeight] = heightMap.dimensions();

		context->DrawInstanced(6, (heightMapWidth)*(heightMapHeight), 0, 0);
	}
}

namespace initialize
{
	buffer::structured::input<vertex::water> vertices(ui32 width, ui32 height, fl32 scale)
	{
		std::vector<vertex::water> vertices(height*width);
		ui32 index = 0;
		for (ui32 y = 0; y < height; y++) {
			for (ui32 x = 0; x < width; x++) {
				vertices[index].position = v3((fl32)x, 0.f, (fl32)y)*scale;
				index++;
			}
		}

		return buffer::structured::input<vertex::water>(vertices);
	}

	buffer::texture::inputOutput2d heightMap(ui32 width, ui32 height)
	{
		D3D11_TEXTURE2D_DESC textureDesc;

		textureDesc.Height = height;
		textureDesc.Width = width;
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = DXGI_FORMAT_R32_FLOAT;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.SampleDesc.Quality = 0;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;

		return buffer::texture::inputOutput2d(textureDesc);
	}

	static D3D11_TEXTURE2D_DESC textureDesc(ui32 width, ui32 height)
	{
		D3D11_TEXTURE2D_DESC textureDesc;

		textureDesc.Height = height;
		textureDesc.Width = width;
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = DXGI_FORMAT_R32_FLOAT;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.SampleDesc.Quality = 0;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;

		return textureDesc;
	}
}
