#include "animation/field/aniFieldEmitter.h"

#include <vector>
#include "math/bunkmath.h"

#include "buffer/structured/bufferStructured.h"
#include "buffer/constant/bufferConstant.h"

#include "render/shader/common/shaderHelper.h"

namespace animation::field::emitter
{

	namespace fp
	{
		static std::filesystem::path sphere = "source/animation/field/efSphere.cms";
	}

	buffer::structured::input<data> torus(fl32 radius, fl32 distance, ui32 pointsTheta, ui32 pointsPhi)
	{
		ui32 totalPoints = pointsTheta*pointsPhi;
		std::vector<data> points(totalPoints);
		
		v3 tubeMidpoint;

		fl32 pi = math::pi;

		fl32 theta = 0.f;
		fl32 phi = 0.f;

		fl32 stepSizeTheta = 2.f*pi/(fl32)(pointsTheta+1);
		fl32 stepSizePhi = 2.f*pi/(fl32)(pointsPhi+1);

		ui32 index = 0; // Vector index
		for (ui32 i = 0; i < pointsTheta; i++) {
			phi = 0.f;
			for (ui32 j = 0; j < pointsPhi; j++) {
				fl32 d = distance + radius*cos(phi);
				points[index].position.x() = d*cos(theta);
				points[index].position.y() = d*sin(theta);
				points[index].position.z() = radius*sin(phi);

				tubeMidpoint.x() = distance*cos(theta);
				tubeMidpoint.y() = distance*sin(theta);
				tubeMidpoint.z() = radius*sin(phi);
				
				// use surface normal of torus as the direction
				points[index].direction = points[index].position - tubeMidpoint;

				phi += stepSizePhi;	
				index++;
			}
			theta += stepSizeTheta;
		}

		auto output = buffer::structured::input<data>(data(), totalPoints);
		output.set(points);

		return output;
	}
	   
	buffer::structured::input<data> sphere(fl32 radius, fl32 magnitude, ui32 pointsTheta, ui32 pointsPhi)
	{
		struct sphereInput
		{
			v3 position;
			fl32 radius;

			fl32 magnitude;
			ui32 pointsTheta;
			ui32 pointsPhi;
			fl32 thetaStepSize;

			fl32 phiStepSize;
			fl32 pad[3];

		};
		auto pi = math::pi;

		sphereInput sphere;
		sphere.position = v3::Zero();
		sphere.radius = radius;

		sphere.magnitude = magnitude;
		sphere.pointsTheta = pointsTheta;
		sphere.pointsPhi = pointsPhi;
		sphere.thetaStepSize = pi/(fl32)(pointsTheta+1);

		sphere.phiStepSize = 2.f*pi/(fl32)(pointsPhi+1);

		auto shader = shader::compute::find(fp::sphere);

		auto context = d3d::getDeviceContext();

		context->CSSetShader(shader.Get(), nullptr, 0);

		buffer::constant<sphereInput> sphereConstants(sphere);
		context->CSSetConstantBuffers(0, 1, sphereConstants.data.GetAddressOf());

		buffer::structured::output<data> sphereOutput(data(), pointsTheta*pointsPhi);
		context->CSSetUnorderedAccessViews(0, 1, sphereOutput.view.GetAddressOf(), nullptr);

		context->Dispatch(pointsTheta, pointsPhi, 1);

		buffer::structured::input<data> sphereInput(data(), pointsTheta*pointsPhi);

		context->CopyResource(sphereInput.data.Get(), sphereOutput.data.Get());

		return sphereInput;
	}
}