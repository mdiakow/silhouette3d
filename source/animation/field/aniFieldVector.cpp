#include "animation/field/aniFieldVector.h"

#include "render/shader/common/shaderHelper.h"
#include "buffer/constant/bufferConstant.h"

namespace fp
{
	static std::filesystem::path sphereOut = "source/animation/field/vfSphereOut.cms";
}

namespace animation::field::vector
{
	namespace constants
	{
		struct sphereOut
		{
			fl32 radius;
			v3 center;
			fl32 magnitude;
			v3 pad;
		};
	}
	buffer::texture::output3d sphere(fl32 radius, fl32 magnitude)
	{
		ui32 dimension = (ui32)std::ceil(radius*2.f);

		D3D11_TEXTURE3D_DESC textureDesc;
		textureDesc.Height = dimension;
		textureDesc.Width = dimension;
		textureDesc.Depth = dimension;
		textureDesc.MipLevels = 1;
		// NOTE: May be complete overkill, I'm having a big issue with my graphics card not being able to
		// use: R32G32B32. I can set it to some of the smaller float formats, but then writing to the
		// texture3D does nothing, so I'm just going to leave it like this for now since it is working.
		textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;

		buffer::texture::output3d sphere(textureDesc);

		constants::sphereOut initConst;
		initConst.center = v3(radius, radius, radius);
		initConst.radius = radius;
		initConst.magnitude = magnitude;

		buffer::constant<constants::sphereOut> bufferConst(initConst);

		auto shader = shader::compute::find(fp::sphereOut);

		auto context = d3d::getDeviceContext();

		context->CSSetShader(shader.Get(), 0, 0);

		context->CSSetConstantBuffers(0, 1, bufferConst.data.GetAddressOf());
		context->CSSetUnorderedAccessViews(0, 1, sphere.view.GetAddressOf(), nullptr);

		context->Dispatch(dimension/8, dimension/8, dimension/8);		

		return sphere;
	}
}