#ifndef ANI_PAR_VF_H_
#define ANI_PAR_VF_H_

#include "bunkcore.h"

#include "buffer/texture/bufTex3d.h"

namespace animation
{
	namespace field
	{
		namespace vector
		{
			// Generates a sphere where the forces all push out from the centre.
			// You can flip this and increase the intensity by using magnitude.
			buffer::texture::output3d sphere(fl32 radius, fl32 magnitude);
		}		
	}
}


#endif