#ifndef ANI_PAR_EMIT_FIELD_H_
#define ANI_PAR_EMIT_FIELD_H_

#include "bunkcore.h"
#include "buffer/structured/bufferStructured.h"

namespace animation
{
	namespace field
	{
		namespace emitter
		{
			
			struct data
			{
				v3 position;
				v3 direction; 
			};

			// radius is of the tube.
			// distance is center of the tube to center of the torus.
			// pointsPhi/Theta control how many points around the torus you'd like.
			buffer::structured::input<data> torus(fl32 radius, fl32 distance, ui32 pointsTheta, ui32 pointsPhi);
			buffer::structured::input<data> sphere(fl32 radius, fl32 magnitude, ui32 pointsTheta, ui32 pointsPhi);
		}
	}
}


#endif