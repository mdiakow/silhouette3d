#include "aniFoliage.h"

//#include "mesh/meshVertex.h"
#include "colour.h"


#include <random>
#include <vector>

//#include "animation/skeleton/aniJoint.h"
#include "Debugger/bunkdebug.h"

#include "math/mathQuat.h"
#include "math/mathDualQuat.h"

#include "shaders/cpp/shaderInstAniLight.h"
namespace fp
{
	static const std::filesystem::path foliageShader = "../Engine_Bunk/source/animation/foliage/simulateFoliage.cms";
}

#define VERTEXTYPE vertex::animated::light

namespace vertex
{
	struct foliage
	{
		v3 position;
		v2 textureCoordinate;
		v3 normal;
	};
}
/*
namespace instance
{
	struct index
	{
		si16 index;
	};
}*/

animation::foliage::foliage(ui32 instanceCount, v3 boxStart, v3 boxEnd)
{
	this->instanceCount = instanceCount;
	HRESULT result;
	auto device = d3d::getDevice();
	///////////////////////////////////
	// Set up the constant buffer
	///////////////////////////////////
	struct cbuffer::foliage cbuffer;
	cbuffer.jointCount = 5;
	this->jointCount = cbuffer.jointCount;
	
	D3D11_BUFFER_DESC cbd;
	cbd.Usage = D3D11_USAGE_DYNAMIC;
	cbd.ByteWidth = sizeof(cbuffer::foliage);
	cbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cbd.MiscFlags = 0;
	cbd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA initCbd;
	initCbd.pSysMem = &cbuffer;
	initCbd.SysMemPitch = 0;
	initCbd.SysMemSlicePitch = 0;

	d3d::getDevice()->CreateBuffer(&cbd, &initCbd, &this->constantBuffer);

	///////////////////////////////////
	// Set up the vertex buffer, index buffer and skeleton.
	///////////////////////////////////
	joint::foliage *joints = new joint::foliage[jointCount];
	for (si32 i = 0; i < (si32)jointCount; i++) {
		joints[i].children[0] = i+1;
		joints[i].children[1] = -1;
		joints[i].children[2] = -1;
		joints[i].parent = i-1;
	}
	joints[jointCount-1].children[0] = -1;

	this->skeletonGraph = new animation::skeleton::graph<joint::foliage>(joints, jointCount);

	ui32 vertexCount = 8*jointCount;
	ui32 indexCount = 12*jointCount;

	this->vertexCount = vertexCount;
	this->indexCount = indexCount;
	
	auto *vertices = new VERTEXTYPE[vertexCount];
	//vertex::foliage *vertices = new vertex::foliage[vertexCount];
	fl32 quadScale = 5.f; // quad scale

	// CPU side vertex set up (A double sided quad).
	v3 norm = v3(0.f, 0.f, 1.f);
	vertices[0].position = v3(0.f, 0.f, 0.f)*quadScale;
	vertices[0].textureCoordinate = v2(0.f, 0.f);
	vertices[0].normal = norm;
	vertices[0].indices[0] = 0;
	vertices[0].indices[1] = 0;
	vertices[0].weights[0] = 1.f;
	vertices[0].weights[1] = 0.f;

	vertices[1].position = v3(0.f, 1.f, 0.f)*quadScale;
	vertices[1].textureCoordinate = v2(0.f, 1.f);
	vertices[1].normal = norm;
	vertices[1].indices[0] = 0;
	vertices[1].indices[1] = 1;
	vertices[1].weights[0] = 0.5f;
	vertices[1].weights[1] = 0.5f;

	vertices[2].position = v3(1.f, 1.f, 0.f)*quadScale;
	vertices[2].textureCoordinate = v2(1.f, 1.f);
	vertices[2].normal = norm;
	vertices[2].indices[0] = 0;
	vertices[2].indices[1] = 1;
	vertices[2].weights[0] = 0.5f;
	vertices[2].weights[1] = 0.5f;

	vertices[3].position = v3(1.f, 0.f, 0.f)*quadScale;
	vertices[3].textureCoordinate = v2(1.f, 0.f);
	vertices[3].normal = norm;
	vertices[3].indices[0] = 0;
	vertices[3].indices[1] = 0;
	vertices[3].weights[0] = 1.f;
	vertices[3].weights[1] = 0.f;

	vertices[4] = vertices[0];
	vertices[4].normal = -norm;
	vertices[5] = vertices[1];
	vertices[5].normal = -norm;
	vertices[6] = vertices[2];
	vertices[6].normal = -norm;
	vertices[7] = vertices[3];
	vertices[7].normal = -norm;

	ui16 *indices = new ui16[indexCount];

	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;

	indices[3] = 0;
	indices[4] = 2;
	indices[5] = 3;

	indices[6] = 6;
	indices[7] = 5;
	indices[8] = 4;

	indices[9] = 7;
	indices[10] = 6;
	indices[11] = 4;

	// This is test code that generates however many additional quads, relative to the number of bones
	// to make the foliage taller.
	v3 offsetStep = v3(0.f, quadScale, 0.f);
	v3 offsetCurr = offsetStep;
	for (ui32 i = 1; i < jointCount; i++) {
		ui32 qvi = i*8; // quad vertex index
		vertices[qvi] = vertices[0];
		vertices[qvi].position += offsetCurr;
		vertices[qvi].indices[0] = i;
		vertices[qvi].indices[1] = i-1;
		vertices[qvi].weights[0] = 0.5f;
		vertices[qvi].weights[1] = 0.5f;

		vertices[qvi+1] = vertices[1];
		vertices[qvi+1].position += offsetCurr;
		vertices[qvi+1].indices[0] = i;
		vertices[qvi+1].indices[1] = i+1;
		vertices[qvi+1].weights[0] = 0.5f;
		vertices[qvi+1].weights[1] = 0.5f;

		vertices[qvi+2] = vertices[2];
		vertices[qvi+2].position += offsetCurr;
		vertices[qvi+2].indices[0] = i;
		vertices[qvi+2].indices[1] = i+1;
		vertices[qvi+2].weights[0] = 0.5f;
		vertices[qvi+2].weights[1] = 0.5f;

		vertices[qvi+3] = vertices[3];
		vertices[qvi+3].position += offsetCurr;
		vertices[qvi+3].indices[0] = i;
		vertices[qvi+3].indices[1] = i-1;
		vertices[qvi+3].weights[0] = 0.5f;
		vertices[qvi+3].weights[1] = 0.5f;

		vertices[qvi+4] = vertices[4];
		vertices[qvi+4].position += offsetCurr;
		vertices[qvi+4].indices[0] = i;
		vertices[qvi+4].indices[1] = i-1;
		vertices[qvi+4].weights[0] = 0.5f;
		vertices[qvi+4].weights[1] = 0.5f;

		vertices[qvi+5] = vertices[5];
		vertices[qvi+5].position += offsetCurr;
		vertices[qvi+5].indices[0] = i;
		vertices[qvi+5].indices[1] = i+1;
		vertices[qvi+5].weights[0] = 0.5f;
		vertices[qvi+5].weights[1] = 0.5f;

		vertices[qvi+6] = vertices[6];
		vertices[qvi+6].position += offsetCurr;
		vertices[qvi+6].indices[0] = i;
		vertices[qvi+6].indices[1] = i+1;
		vertices[qvi+6].weights[0] = 0.5f;
		vertices[qvi+6].weights[1] = 0.5f;

		vertices[qvi+7] = vertices[7];
		vertices[qvi+7].position += offsetCurr;
		vertices[qvi+7].indices[0] = i;
		vertices[qvi+7].indices[1] = i-1;
		vertices[qvi+7].weights[0] = 0.5f;
		vertices[qvi+7].weights[1] = 0.5f;


		ui32 ii = i*12; // index for indices array.
		indices[ii] = qvi;
		indices[ii+1] = qvi+1;
		indices[ii+2] = qvi+2;

		indices[ii+3] = qvi;
		indices[ii+4] = qvi+2;
		indices[ii+5] = qvi+3;

		indices[ii+6] = qvi+6;
		indices[ii+7] = qvi+5;
		indices[ii+8] = qvi+4;

		indices[ii+9] = qvi+7;
		indices[ii+10] = qvi+6;
		indices[ii+11] = qvi+4;

		offsetCurr += offsetStep;
	}

	vertices[vertexCount-2].indices[0] = jointCount-1;
	vertices[vertexCount-2].indices[1] = jointCount-1;

	vertices[vertexCount-3].indices[0] = jointCount-1;
	vertices[vertexCount-3].indices[1] = jointCount-1;

	vertices[vertexCount-6].indices[0] = jointCount-1;
	vertices[vertexCount-6].indices[1] = jointCount-1;

	vertices[vertexCount-7].indices[0] = jointCount-1;
	vertices[vertexCount-7].indices[1] = jointCount-1;

	// GPU side set up.
	D3D11_BUFFER_DESC vbDesc;
	vbDesc.Usage = D3D11_USAGE_DEFAULT;
	vbDesc.ByteWidth = sizeof(VERTEXTYPE) * vertexCount;
	vbDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbDesc.CPUAccessFlags = 0;
	vbDesc.MiscFlags = 0;
	vbDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vbInit;
	vbInit.pSysMem = vertices;
	vbInit.SysMemPitch = 0;
	vbInit.SysMemSlicePitch = 0;
	
	result = device->CreateBuffer(&vbDesc, &vbInit, &this->vertexBuffer);

	D3D11_BUFFER_DESC indbDesc;
	indbDesc.Usage = D3D11_USAGE_DEFAULT;
	indbDesc.ByteWidth = sizeof(ui16) * indexCount;
	indbDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indbDesc.CPUAccessFlags = 0;
	indbDesc.MiscFlags = 0;
	indbDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indbInit;
	indbInit.pSysMem = indices;
	indbInit.SysMemPitch = 0;
	indbInit.SysMemSlicePitch = 0;

	result = device->CreateBuffer(&indbDesc, &indbInit, &this->indexBuffer);

	///////////////////////////////////
	// Set up the bind pose.
	///////////////////////////////////
	joint::bindPose *bpOrientations = new joint::bindPose[jointCount];

	fl32 bindPoint = quadScale/2.f;
	v3 originBindPose;
	fl32 springRate = 25.f;
	for (ui32 i = 0; i < jointCount; i++) {
		originBindPose = v3(-bindPoint, (fl32)i * -quadScale, 0.f);

		bpOrientations[i].orientation = dq(quat::Identity(), originBindPose);
		bpOrientations[i].springRate = springRate/(fl32)(i+1);
	}

	this->bindPose = new animation::skeleton::orientation<joint::bindPose>(bpOrientations, jointCount);
	
	///////////////////////////////////
	// Set up the skeleton orientation data.
	///////////////////////////////////
	ui32 orientationCount = instanceCount*jointCount;
	
	instance::foliage *orientationData = new instance::foliage[orientationCount];
	std::default_random_engine generator;

	// Distributions for x,y,z, put within bounding box.
	std::uniform_real_distribution<fl32> dx(boxStart[0], boxEnd[0]);
	std::uniform_real_distribution<fl32> dy(boxStart[1], boxEnd[1]);
	std::uniform_real_distribution<fl32> dz(boxStart[2], boxEnd[2]);
	
	for (ui32 i = 0; i < instanceCount; i++) {
		ui32 ci = i*jointCount;

		// Root position is randomly generated.
		fl32 rx = dx(generator);
		fl32 ry = dy(generator);
		fl32 rz = dz(generator);

		//quat rot = quat::Identity() * math::quaternion::fromEuler(0.f, 0.785398f/1.f, 0.f);
		quat rot = quat::Identity() * math::quaternion::fromEuler(0.f, 0.f, 0.f);
		rot.normalize();

		//orientationData[ci].orientation = (dq(rot, v3(rx, ry, rz)) * bpOrientations[0].orientation).normalized();
		orientationData[ci].orientation = dq(rot, v3(rx, ry, rz)).normalized();
		for (ui32 j = 1; j < jointCount; j++) {
			dq parent = orientationData[ci+j-1].orientation;
			v3 bp = -bpOrientations[j].orientation.getTranslation();
			
			quat qt = parent.rotation * quat(0.f, bp.x(), bp.y(), bp.z()) * parent.rotation.conjugate();
			v3 childPosition = v3(qt.x(), qt.y(), qt.z());
			quat childRotation = rot;
			dq current = (dq(parent.rotation * childRotation, parent.getTranslation() + childPosition) * bpOrientations[j].orientation);

			orientationData[ci+j].orientation = (current).normalized();
		}
	}

	skeletonData = new animation::skeleton::orientation<instance::foliage>(orientationData, orientationCount);
	skeletonDebug = new animation::skeleton::debug<instance::foliage>(orientationData, instanceCount, jointCount);

	///////////////////////////////////
	// Set up the instance data.
	///////////////////////////////////
	ui32 instCount = instanceCount;
	ui32 *instances = new ui32[instCount];
	for (ui32 i = 0; i < instCount; i++) {
		instances[i] = i*jointCount;
	}

	D3D11_BUFFER_DESC ibDesc;
	ibDesc.Usage = D3D11_USAGE_DEFAULT;
	ibDesc.ByteWidth = sizeof(ui32) * instCount;
	ibDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	ibDesc.CPUAccessFlags = 0;
	ibDesc.MiscFlags = 0;
	ibDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA ibInit;
	ibInit.pSysMem = instances;
	ibInit.SysMemPitch = 0;
	ibInit.SysMemSlicePitch = 0;

	result = device->CreateBuffer(&ibDesc, &ibInit, &this->instanceBuffer);

	delete[] instances;
	delete[] orientationData;
	delete[] bpOrientations;
	instances = nullptr;
	orientationData = nullptr;
	bpOrientations = nullptr;

	delete[] vertices;
	delete[] indices;
	indices = nullptr;
	vertices = nullptr;

	///////////////////////////////////
	// Set up the texture.
	///////////////////////////////////
	ui32 checkCount = 4;
	ui32 width = 64;
	ui32 height = width;
	ui32 *texData = new ui32[width*height];

	ui32 c1 = colour::rgba32(v4(0.0f, 1.0f, 0.0f, 1.0f));
	ui32 c2 = colour::rgba32(v4(0.0f, 0.5f, 0.0f, 1.0f));

	ui32 currentColour = c2;

	ui32 index = 0;
	for(ui32 a = 0; a < checkCount; a++) {
		for (ui32 b = 0; b < height / checkCount; b++) {
			for (ui32 c = 0; c < checkCount; c++) {
				for (ui32 d = 0; d < width / checkCount; d++) {
					texData[index] = currentColour;
					index++;
				}
				if (currentColour == c2) {
					currentColour = c1;
				} else {
					currentColour = c2;
				}
			}
		}
		if (currentColour == c2) {
			currentColour = c1;
		} else {
			currentColour = c2;
		}
	}

	D3D11_TEXTURE2D_DESC textureDesc;
	textureDesc.Height = height;
	textureDesc.Width = width;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DYNAMIC;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	textureDesc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA textureInit;
	textureInit.pSysMem = texData;
	textureInit.SysMemPitch = 4*width;
	textureInit.SysMemSlicePitch = 0;

	result = device->CreateTexture2D(&textureDesc, &textureInit, &this->texture2d);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = textureDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip = 0;
	srvDesc.Texture2D.MipLevels = 1;

	result = device->CreateShaderResourceView(texture2d, &srvDesc, &this->textureView);
	
	delete[] texData;
	texData = nullptr;

	///////////////////////////////////
	// Set up the compute shader.
	///////////////////////////////////
	computeShader = shader::compute::load(fp::foliageShader.c_str());

	///////////////////////////////////////
	// Set up the buffer for the output orientation data.
	///////////////////////////////////////
	D3D11_BUFFER_DESC bd;
	bd.ByteWidth = orientationCount * sizeof(instance::foliage);
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	bd.StructureByteStride = sizeof(instance::foliage);

	result = device->CreateBuffer(&bd, nullptr, &this->outputOrientations);

	///////////////////////////////////////
	// Set up the view for the output orientation buffer.
	///////////////////////////////////////
	D3D11_UNORDERED_ACCESS_VIEW_DESC uavd;
	uavd.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	uavd.Format = DXGI_FORMAT_UNKNOWN;
	uavd.Buffer.Flags = 0;
	uavd.Buffer.FirstElement = 0;
	uavd.Buffer.NumElements = orientationCount;

	result = device->CreateUnorderedAccessView(this->outputOrientations, &uavd, &this->outputView);

	///////////////////////////////////////
	// Set up the wind buffer for physics simulation.
	///////////////////////////////////////
	this->wind = new animation::wind();
}

animation::foliage::~foliage()
{
	d3d::release(&this->instanceBuffer);
	d3d::release(&this->vertexBuffer);
	d3d::release(&this->indexBuffer);
	d3d::release(&this->texture2d);
	d3d::release(&this->textureView);
}

void animation::foliage::updateOrientations()
{
	wind->update();

	auto context = d3d::getDeviceContext();

	context->CSSetShader(this->computeShader, nullptr, 0);

	context->CSSetConstantBuffers(0, 1, &this->constantBuffer);

	context->CSSetShaderResources(0, 1, &this->skeletonGraph->graphView);
	context->CSSetShaderResources(1, 1, &this->bindPose->orientationView);
	context->CSSetShaderResources(2, 1, &this->skeletonData->orientationView);
	context->CSSetShaderResources(3, 1, &this->wind->dataView);

	context->CSSetUnorderedAccessViews(1, 1, &this->outputView, nullptr);
	
	ui32 xCount = this->instanceCount/16;
	context->Dispatch(xCount, 1, 1);

	context->CopyResource(this->skeletonData->orientationBuffer, this->outputOrientations);
}

void animation::foliage::render()
{
	ui32 strides[2] = {sizeof(VERTEXTYPE), sizeof(ui32)};
	ui32 offsets[2] = {0, 0};
	ID3D11Buffer *buffers[2] = {this->vertexBuffer, this->instanceBuffer};

	auto context = d3d::getDeviceContext();
	context->VSSetShaderResources(0, 1, &this->skeletonData->orientationView);
	context->PSSetShaderResources(0, 1, &this->textureView);

	context->IASetVertexBuffers(0, 2, buffers, strides, offsets);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R16_UINT, 0);

	context->DrawIndexedInstanced(this->indexCount, this->instanceCount, 0, 0, 0);
}
