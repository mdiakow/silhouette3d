#ifndef ANIMATION_FOLIAGE_H_
#define ANIMATION_FOLIAGE_H_

#include "bunkcore.h"
#include "D3D.h"

#include "animation/skeleton/aniSkeleGraph.h"
#include "animation/skeleton/aniSkeleOrientation.h"
#include "animation/skeleton/aniSkeleDebug.h"

#include "animation/wind/aniWind.h"

#include "render/shader/shader.h"

#include "math/mathDualQuat.h"
using dq = math::dualQuaternion;
namespace joint
{
	struct foliage
	{
		si32 parent;
		si32 children[3];
	};

	struct bindPose
	{
		dq orientation;
		fl32 springRate;
		fl32 padding[3];
	};
}

namespace instance
{
	/*
	struct foliage
	{
		v4 position;
		v4 rotation;
	};*/

	struct foliage
	{
		dq orientation;
	};
}

namespace cbuffer
{
	struct foliage
	{
		ui32 jointCount;
		ui32 padding[3];
	};
}



namespace animation 
{
	struct foliage
	{
		// Randomly create instance count of this data within the cube formed from boxStart to boxEnd.
		foliage(ui32 instanceCount, v3 boxStart, v3 boxEnd);
		~foliage();

		void updateOrientations();
		void render();
		
		
		

		// Data for the model itself.
		ID3D11Buffer *vertexBuffer, *indexBuffer;
		ID3D11Texture2D *texture2d;
		ID3D11ShaderResourceView *textureView;

		// Both the graph and bind pose are used to traverse and calculate the orientations of the
		// skeleton data.
		animation::skeleton::graph<joint::foliage> *skeletonGraph;
		animation::skeleton::orientation<joint::bindPose> *bindPose;

		// instance buffer consists of indices in to the orientations buffer.
		ID3D11Buffer *instanceBuffer;

		// Current orientations generated from a compute shader. Used to skin the model.
		animation::skeleton::orientation<instance::foliage> *skeletonData;
		animation::skeleton::debug<instance::foliage> *skeletonDebug;

		// Data for the compute shader
		ID3D11Buffer *constantBuffer;
		ID3D11ComputeShader *computeShader;
		ID3D11UnorderedAccessView *outputView;
		ID3D11Buffer *outputOrientations;

		// Forces applied to the joints to calculate their orientations.
		animation::wind *wind;		
	private:
		ui32 vertexCount, indexCount;
		ui32 instanceCount;
		ui32 jointCount;			   		
	};
}

#endif