
// The underlying buffer representation of the joint graph.
#ifndef ANIMATION_graph_H_
#define ANIMATION_graph_H_

#include "bunkcore.h"

#include <vector>
#include "d3d.h"

namespace animation
{	
	namespace skeleton
	{
		template <class joint>
		struct graph
		{
			graph(joint *joints, ui32 jointCount);
			~graph();

			ID3D11Buffer *graphBuffer;
			ID3D11ShaderResourceView *graphView;
		private:
			ui16 maxChildren;
		};

		template<class joint>
		inline graph<joint>::graph(joint *joints, ui32 jointCount)
		{
			HRESULT result;
			auto device = d3d::getDevice();

			///////////////////////////////////////
			// Set up the buffer of the graph.
			///////////////////////////////////////
			D3D11_BUFFER_DESC sbd;
			sbd.ByteWidth = jointCount * sizeof(joint);
			sbd.Usage = D3D11_USAGE_DEFAULT;
			sbd.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			sbd.CPUAccessFlags = 0;
			sbd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			sbd.StructureByteStride = sizeof(joint);

			D3D11_SUBRESOURCE_DATA sbi;
			sbi.pSysMem = joints;
			sbi.SysMemPitch = 0;
			sbi.SysMemSlicePitch = 0;

			result = device->CreateBuffer(&sbd, &sbi, &this->graphBuffer);

			///////////////////////////////////////
			// Set up the view for the graph.
			///////////////////////////////////////
			D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
			srvd.Format = DXGI_FORMAT_UNKNOWN;
			srvd.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
			srvd.Buffer.FirstElement = 0;
			srvd.Buffer.NumElements = jointCount;

			result = device->CreateShaderResourceView(this->graphBuffer, &srvd, &this->graphView);
		}

		template<class joint>
		inline graph<joint>::~graph()
		{
			d3d::release(&graphBuffer);
			d3d::release(&graphView);
		}
	}
	
}

#endif