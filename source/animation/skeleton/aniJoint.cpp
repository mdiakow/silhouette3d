#include "animation/skeleton/aniJoint.h"

animation::joint::joint(ui32 maxChildren)
{
	children = new ui16[maxChildren];
}

animation::joint::~joint()
{
	delete[] children;
}
