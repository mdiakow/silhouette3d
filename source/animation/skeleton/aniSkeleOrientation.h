// The is the buffer that holds the data for the orientation of some other skeleton.
// It is an array, only used for instancing currently.
#ifndef ANI_SKELE_ORIENTATION_H_
#define ANI_SKELE_ORIENTATION_H_

#include "d3d.h"

namespace animation
{
	namespace skeleton
	{
		template <class data>
		struct orientation
		{
			orientation(data *orientations, ui32 orientationCount);
			~orientation();

			ID3D11Buffer *orientationBuffer;
			ID3D11ShaderResourceView *orientationView;
		};
		template<class data>
		inline orientation<data>::orientation(data *orientations, ui32 orientationCount)
		{
			HRESULT result;
			auto device = d3d::getDevice();

			///////////////////////////////////////
			// Set up the buffer for the orientation data.
			///////////////////////////////////////
			D3D11_BUFFER_DESC bd;
			bd.ByteWidth = orientationCount * sizeof(data);
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			bd.CPUAccessFlags = 0;
			bd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			bd.StructureByteStride = sizeof(data);
			
			D3D11_SUBRESOURCE_DATA srd;
			srd.pSysMem = orientations;
			srd.SysMemPitch = 0;
			srd.SysMemSlicePitch = 0;

			result = device->CreateBuffer(&bd, &srd, &this->orientationBuffer);

			///////////////////////////////////////
			// Set up the view for the orientation buffer.
			///////////////////////////////////////
			D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
			srvd.Format = DXGI_FORMAT_UNKNOWN;
			srvd.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
			srvd.Buffer.FirstElement = 0;
			srvd.Buffer.NumElements = orientationCount;

			result = device->CreateShaderResourceView(this->orientationBuffer, &srvd, &this->orientationView);
		}

		template<class data>
		inline orientation<data>::~orientation()
		{
			d3d::release(&this->orientationBuffer);
			d3d::release(&this->orientationView);
		}
	}
}


#endif