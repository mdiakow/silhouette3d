#ifndef ANI_SKELETON_DEBUG_H_
#define ANI_SKELETON_DEBUG_H_

#include "Mesh/meshLine.h"
#include "Mesh/meshVertex.h"
#include "shaders/cpp/shaderLight.h"
namespace animation
{
	namespace skeleton
	{
		// A debug model that can be used to render the bones.
		template <class data>
		struct debug
		{
			debug(data *orientations, ui32 instanceCount, ui32 jointCount);
			~debug();

			mesh::line<vertex::light> *model;
		};


		template<class data>
		inline debug<data>::debug(data *orientations, ui32 instanceCount, ui32 jointCount)
		{
			fl32 qs = 5.f;
			vertex::light *vertices = new vertex::light[instanceCount*jointCount*2];
			ui32 *indices = new ui32[instanceCount*jointCount*2];
			ui32 cv = 0;
			for (ui32 i = 0; i < instanceCount; i++) {
				ui32 ci = i*jointCount;
				
				vertices[cv].position = orientations[ci].orientation.transform(v3(0.f, 0.f, 0.f));
				vertices[cv+1].position = orientations[ci].orientation.transform(v3(0.f, 5.f, 0.f));
				//vertices[cv].position = orientations[ci].orientation.getTranslation();
				//vertices[cv+1].position = orientations[ci+1].orientation.getTranslation();

				indices[cv] = cv;
				indices[cv+1] = cv+1;

				cv += 2;
				for (ui32 j = 1; j < jointCount; j++) {			
					fl32 pos = qs*(fl32)j;

					vertices[cv].position = orientations[ci+j].orientation.transform(v3(0.f, (fl32)j*qs, 0.f));
					vertices[cv+1].position = orientations[ci+j].orientation.transform(v3(0.f, (fl32)(j+1)*qs, 0.f));

					//vertices[cv].position = orientations[ci+j].orientation.getTranslation();
					//vertices[cv+1].position = orientations[ci+j+1].orientation.getTranslation();

					indices[cv] = cv;
					indices[cv+1] = cv+1;

					cv += 2;
				}

				
			}

			model = new mesh::line<vertex::light>(vertices, indices, instanceCount*jointCount*2, instanceCount*jointCount*2);

			delete[] vertices;
			delete[] indices;
		}

		template<class data>
		inline debug<data>::~debug()
		{
			
		}

	}
}

#endif
