#ifndef ANIMATION_JOINT_H_
#define ANIMATION_JOINT_H_

#include "bunkcore.h"

namespace animation
{
	struct joint
	{
		joint(ui32 maxChildren);
		~joint();

		ui16 *children;		
	};
}

namespace joint
{
	// Test joint used in 
	struct test
	{
		si16 children[3];
	};
}


#endif