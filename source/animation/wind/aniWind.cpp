#include "animation/wind/aniWind.h"
#include "math/linalg.h"
#include "systemCore/time/bunktime.h"



#include "Debugger/bunkdebug.h"

animation::wind::wind()
{
	this->windCount = 10;
	cylinderWind *winds = new cylinderWind[windCount];
	fl32 radius = 50.f;
	v3 currentRotation = v3(10.f, 0.f, 5.f);
	m3 rot = linalg::rotation::yawPitchRoll(0.78539816339f, 0.78539816339f, 0.f);
	for (ui32 i = 0; i < windCount; i++) {
		winds[i].force = currentRotation;
		winds[i].position = v4(50.f, 0.f, 50.f, 0.f) * (fl32)i;
		winds[i].sqrRadius = radius*radius;

		currentRotation = rot * currentRotation;
	}

	D3D11_BUFFER_DESC wd;
	wd.Usage = D3D11_USAGE_DYNAMIC;
	wd.ByteWidth = sizeof(cylinderWind) * windCount;
	wd.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	wd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	wd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	wd.StructureByteStride = sizeof(cylinderWind);

	D3D11_SUBRESOURCE_DATA wdInit;
	wdInit.pSysMem = winds;
	wdInit.SysMemPitch = 0;
	wdInit.SysMemSlicePitch = 0;

	auto device = d3d::getDevice();
	device->CreateBuffer(&wd, &wdInit, &this->data);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvd;
	srvd.Format = DXGI_FORMAT_UNKNOWN;
	srvd.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	srvd.Buffer.FirstElement = 0;
	srvd.Buffer.NumElements = windCount;

	device->CreateShaderResourceView(data, &srvd, &this->dataView);

	delete[] winds;
	fl32 windMax = 0.6f/((fl32)windCount);
	transform.amplitude = windMax;
	transform.frequency = 3.3f;
	transform.verticalShift = windMax + 0.05f;
	transform.phaseShift = 0.0f;

	currentTime = 0.f;

	direction = v3(1.f, 0.f, 1.f).normalized();
	magnitude = 5.f;
}

animation::wind::~wind()
{
	d3d::release(&this->data);
	d3d::release(&this->dataView);
}

void animation::wind::update()
{
	std::uniform_real_distribution<fl32> randomFrequency(2.f, 4.f);

	auto context = d3d::getDeviceContext();
	
	currentTime += bunktime::dt();
	
	// Each time the period of the function ends, randomly calculate a new frequency and amplitude.
	if (currentTime > (6.28318530718f / transform.frequency)) {
		currentTime -= (6.28318530718f / transform.frequency);
		transform.frequency = randomFrequency(generator);
	}

	cylinderWind *winds = new cylinderWind[windCount];
	fl32 sqrRadius = 50.f*50.f;

	v3 currentRotation = direction * magnitude;

	for (ui32 i = 0; i < windCount; i++) {
		fl32 phaseShift = (fl32)i * 6.28318530718f/((fl32)windCount);
		winds[i].force = currentRotation * (transform.amplitude * cos(transform.frequency*currentTime + phaseShift) + transform.verticalShift);
		winds[i].position = v4(50.f, 0.f, 50.f, 0.f) * (fl32)i;
		winds[i].sqrRadius = sqrRadius*10.f;
	}
	
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	
	context->Map(this->data, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);

	memcpy(mappedResource.pData, winds, sizeof(cylinderWind)*windCount);

	context->Unmap(this->data, 0);

	delete[] winds;
}
