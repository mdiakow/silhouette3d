#ifndef ANI_WIND_H_
#define ANI_WIND_H_

#include "bunkcore.h"
#include "D3D.h"

#include <random>

namespace animation
{
	struct ambientWind
	{
		v3 force; // direction + magnitude of wind
		fl32 scale; 
	};

	struct cylinderWind
	{
		v3 force;
		fl32 sqrRadius;
		v4 position;
	};

	struct waveTransform
	{
		fl32 amplitude;
		fl32 frequency;
		fl32 phaseShift;
		fl32 verticalShift;
	};

	class wind
	{
	public:
		wind();
		~wind();
	public:
		void update();
	public:
		ID3D11Buffer *data;
		ID3D11ShaderResourceView *dataView;

		// transforms for cos function as follows:
		// y(t) = amplitude * cos(frequency*t + phaseShift) + verticalShift;
		waveTransform transform;
		ui32 windCount;
		// Force
		v3 direction;
		fl32 magnitude;

		// TODO: This should probably be inside the time module somehow. I need
		// a way to reset it after a certain time.
		fl32 currentTime;

		std::default_random_engine generator;
		
	};
}


#endif