#ifndef ANI_PAR_EMITTER_H_
#define ANI_PAR_EMITTER_H_

#include "bunkcore.h"

#include <wrl/client.h>

#include "mesh/meshInstance.h"
#include "mesh/meshMulti.h"
#include "mesh/meshTriangle.h"

#include "shaders/cpp/shaderParticle.h"
#include "buffer/structured/bufferStructured.h"
#include "buffer/structured/bufferAppendStructured.h"
#include "buffer/texture/bufTex2d.h"
#include "buffer/texture/bufTex3d.h"
#include "buffer/constant/bufferConstant.h"
#include "buffer/indirect/bufferIndirect.h"

#include "animation/field/aniFieldEmitter.h"

namespace animation
{
	namespace particle
	{		
		struct minMax
		{
			fl32 min;
			fl32 max;
		};

		struct particleCreation
		{
			// Point square generation using emitterPosition +- bounds 
			fl32 pad[3];
			fl32 emitterBound;

			// initial vectors of particle
			v3 direction;
			fl32 directionAngle;
			
			minMax force;

			minMax scaleStart;
			minMax scaleEnd;

			fl32 lifetime;
			fl32 pcPad;

			fl32 alphaStart;
			fl32 alphaEnd;

			ui32 createCount; // how many particles to create during one update.
			fl32 createRate; // time in seconds to wait between particle creation.

			v4 colourStart;
			v4 colourEnd;
		};

		// Constants used each frame.
		struct perFrame
		{
			fl32 deltaTime; // time difference from last frame.
			v3 gravity;
			fl32 emitterLifetime; // how long the emitter has existed for.
			v3 emitterPosition;
			// Hoping these random numbers help GPU RNG
			v4 random;
		};

		struct emitterInitializer
		{
			particleCreation particles;
			perFrame perFrame;
		};

		// Transform for each particle
		struct transform
		{
			v3 position;
			fl32 scale;
		};
		// Data for each particle
		struct particle
		{
			v3 velocity;
			fl32 lifetime;
			// using eigen v4 is causing weird alignment issues, not sure how to fix it, the solution with the
			// macro from eigen's docs didn't fix the issue.
			fl32 colour[4];

			fl32 scaleStart;
			fl32 scaleEnd;
		};
		// Tracks indices needed to maintain ring buffer between updates of the compute shaders. Used in
		// aniParUpdateCircularBuffer.cms
		struct ringIndices
		{
			ui32 destroyed; // particles destroyed this frame.
			ui32 created; // particles created this frame.
			ui32 startIndex; // index of the start of data in the ring buffer.
			ui32 endIndex; // index for the end of the data in the ring buffer.

		};

		class emitter
		{
		public:
			emitter(emitterInitializer initializer, 
				buffer::texture::input2d particleTexture,
				buffer::texture::output3d vectorField,
				buffer::structured::input<animation::field::emitter::data> emitterField);
		public:
			void update();
			void render();
		private:
			void createDestroy();
			void updateBuffers();
			void simulate();
			
		private:	
			ui32 particleCount;

			buffer::texture::input2d particleTexture;
			mesh::multi<vertex::particle, instance::particle, mesh::triangle<vertex::particle>, mesh::instance<instance::particle>> particleMesh;

			// Ring buffers that hold the saved particle transforms/data between frames
			buffer::structured::output<transform> ringTransforms;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> ringTransformSRV;

			buffer::structured::output<particle> ringData;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> ringDataSRV;
			
			// Constant buffers that are used inside the creation/simulation shaders
			buffer::constant<particleCreation> creationBuffer;
			buffer::constant<perFrame> perFrameBuffer;

			buffer::indirect dispatchIndirect; // Used to dispatch create/destroy shader

			// Indirect buffers for rendering particles. The remainder is only used when the buffer needs to
			// wrap around on itself. Updated in aniParUpdateCircularBuffers
			buffer::indirect drawIndirect; 
			buffer::indirect drawRemainderIndirect; 

			buffer::structured::output<ringIndices> ringIndices;

			// Vector field influences the particles during simulation, this one is local to the emitter position.
			buffer::texture::output3d vectorField;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> vectorFieldSRV;

			// Emitter field determines where particles can be generated, also local to the emitter position.
			buffer::structured::input<animation::field::emitter::data> emitterField;
			
			fl32 currentCreateTime;
			fl32 createTime;

			v3 position;
			perFrame perFrameData;

		};
	}
}

#endif
