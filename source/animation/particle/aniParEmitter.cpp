#include "animation/particle/aniParEmitter.h"

#include <algorithm>
#include <random>
#include <filesystem>
#include <memory>

#include "render/shader/common/shaderHelper.h"
#include "systemCore/time/bunktime.h"

#include "render/render.h"

namespace helper
{
	static mesh::triangle<vertex::particle> quad(fl32 quadScale);
	static std::vector<animation::particle::particle> particleData(ui32 maxParticles);
}

namespace fp
{
	static std::filesystem::path simulate = "source/animation/particle/aniParSimulate.cms";
	static std::filesystem::path createDestroy = "source/animation/particle/aniParCreateDestroy.cms";
	static std::filesystem::path updateBuffers = "source/animation/particle/aniParUpdateBuffers.cms";
}

namespace initialize
{
	static buffer::dispatchIndirect dispatchIndirect(animation::particle::emitterInitializer &initializer);
	static buffer::drawIndexedIndirect drawIndirect(ui32 instanceCount, ui32 startInstanceLocation);
	static ui32 particleCount(animation::particle::emitterInitializer &initializer);
	static animation::particle::ringIndices ringIndices(ui32 maxParticles);
	static std::vector<instance::particle> instances(ui32 maxParticles);
	static std::vector<animation::particle::transform> ringTransforms(ui32 maxParticles);
}

namespace animation::particle
{
	// Used for per frame random generation.
	static std::random_device rd;
	static std::mt19937 generator(rd());
	static std::uniform_real_distribution<fl32> distribution(-1.f, 1.f);
	
	using multiMesh = mesh::multi<vertex::particle, instance::particle, mesh::triangle<vertex::particle>, mesh::instance<instance::particle>>;

	emitter::emitter(emitterInitializer initializer, buffer::texture::input2d particleTexture, buffer::texture::output3d vectorField, buffer::structured::input<animation::field::emitter::data> emitterField) :
		particleCount{ initialize::particleCount(initializer) },

		particleTexture{ particleTexture },
		particleMesh{ helper::quad(1.f), initialize::instances(particleCount) },

		ringTransforms{ initialize::ringTransforms(particleCount) } ,
		ringData{ helper::particleData(particleCount) },
		ringIndices{ initialize::ringIndices(particleCount), 1 },

		creationBuffer{ initializer.particles },
		perFrameBuffer{ perFrame() },

		dispatchIndirect{ initialize::dispatchIndirect(initializer) },

		drawIndirect{ initialize::drawIndirect(0, 0) },
		drawRemainderIndirect{ initialize::drawIndirect(0, 0) },		

		vectorField{ vectorField },
		
		emitterField{ emitterField },
		
		createTime{ initializer.particles.createRate },
		currentCreateTime{ 0.f },
		position{ initializer.perFrame.emitterPosition },
		perFrameData{ initializer.perFrame }
	{
		perFrameData.emitterLifetime = 0.f;
		
		if (createTime < 0.f) {
			createTime = 0.f;
		}

		auto device = d3d::getDevice();
		HRESULT result;
		// Initialize ringData SRV:
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.Buffer.FirstElement = 0;
		srvDesc.Buffer.NumElements = particleCount;
		
		result = device->CreateShaderResourceView(ringData.data.Get(), &srvDesc, ringDataSRV.GetAddressOf());
		result = device->CreateShaderResourceView(ringTransforms.data.Get(), &srvDesc, ringTransformSRV.GetAddressOf());

		D3D11_TEXTURE3D_DESC vfTexDesc;
		vectorField.texture3d->GetDesc(&vfTexDesc);

		D3D11_SHADER_RESOURCE_VIEW_DESC vectorFieldDesc;
		vectorFieldDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
		vectorFieldDesc.Format = vfTexDesc.Format;
		vectorFieldDesc.Texture3D.MipLevels = 1;
		vectorFieldDesc.Texture3D.MostDetailedMip = 0;

		result = device->CreateShaderResourceView(vectorField.texture3d.Get(), &vectorFieldDesc, vectorFieldSRV.GetAddressOf());
	}

	void emitter::update()
	{
		auto context = d3d::getDeviceContext();
		
		// Update per frame buffer.
		perFrameData.deltaTime = bunktime::dt();
		perFrameData.emitterLifetime += perFrameData.deltaTime;
		perFrameData.random.x() = distribution(generator);
		perFrameData.random.y() = distribution(generator);
		perFrameData.random.z() = distribution(generator);
		perFrameData.random.w() = distribution(generator);

		fl32 time = perFrameData.emitterLifetime;
		//perFrameData.gravity = v3(0.f, sin(time)*sin(time*10.f)*sin(time*20.f), 1.0f)*time;
		
		perFrameBuffer.set(perFrameData);

		// Set up shader inputs
		std::array<ID3D11Buffer*, 2> constantBuffers = {
			creationBuffer.data.Get(),
			perFrameBuffer.data.Get()
		};

		context->CSSetConstantBuffers(0, (ui32)constantBuffers.size(), constantBuffers.data());

		// UAV shader inputs/outputs are set here.
		std::array<ID3D11UnorderedAccessView*, 6> uaViews = {
			ringTransforms.view.Get(),
			ringData.view.Get(),
			dispatchIndirect.view.Get(),
			drawIndirect.view.Get(),
			drawRemainderIndirect.view.Get(),
			ringIndices.view.Get()
		};
		context->CSSetUnorderedAccessViews(0, (ui32)uaViews.size(), uaViews.data(), 0);

		// SRV shader inputs are set here.
		std::array<ID3D11ShaderResourceView*, 2> srViews = {
			vectorFieldSRV.Get(),
			emitterField.view.Get()
		};
		context->CSSetShaderResources(0, (ui32)srViews.size(), srViews.data());

		currentCreateTime += bunktime::dt();

		//if (currentCreateTime > createTime) {
			createDestroy();
			//currentCreateTime -= createTime;
		//}		
		updateBuffers();
		simulate();

		// Unbind the UAVs for the particle data from the compute shaders.
		std::array<ID3D11UnorderedAccessView*, 2> nullViews = {
			nullptr,
			nullptr
		};
		context->CSSetUnorderedAccessViews(0, (ui32)nullViews.size(), nullViews.data(), 0);	

		
	}

	void emitter::createDestroy()
	{
		auto context = d3d::getDeviceContext();

		auto createDestroyShader = shader::compute::find(fp::createDestroy);
		context->CSSetShader(createDestroyShader.Get(), 0, 0);

		context->DispatchIndirect(dispatchIndirect.data.Get(), 0);
	}

	void emitter::updateBuffers()
	{
		auto context = d3d::getDeviceContext();

		auto updateBuffersShader = shader::compute::find(fp::updateBuffers);
		context->CSSetShader(updateBuffersShader.Get(), 0, 0);
		
		context->Dispatch(1,1,1);
	}	

	void emitter::simulate()
	{
		auto context = d3d::getDeviceContext();
		
		auto simulateShader = shader::compute::find(fp::simulate);
		context->CSSetShader(simulateShader.Get(), 0, 0);

		context->Dispatch((ui32)std::ceil((fl32)particleCount/1024.f), 1, 1);		
	}

	void emitter::render()
	{
		auto context = d3d::getDeviceContext();

		// Bind corresponding SRV's of particle data to vertex shaders.
		ID3D11ShaderResourceView *srViews[] = {
			ringTransformSRV.Get(),
			ringDataSRV.Get()
		};
		context->VSSetShaderResources(0, 2, srViews);

		particleTexture.render();

		ui32 strides[] = {sizeof(vertex::particle), sizeof(instance::particle)};
		ui32 offsets[] = {0, 0};			   		 

		ID3D11Buffer *buffers[2] = {
			particleMesh.mesh.data.vertices.Get(), 
			particleMesh.instance.instances.Get()
		};

		context->IASetVertexBuffers(0, 2, buffers, strides, offsets);
		context->IASetIndexBuffer(particleMesh.mesh.data.indices.Get(), DXGI_FORMAT_R32_UINT, 0);
		
		context->DrawIndexedInstancedIndirect(drawIndirect.data.Get(), 0);
		context->DrawIndexedInstancedIndirect(drawRemainderIndirect.data.Get(), 0);

		// Once rendering is done, lets unbind the vertex shaders
		ID3D11ShaderResourceView *nullViews[] = {nullptr, nullptr};
		context->VSSetShaderResources(0,2, nullViews);
	}
}

namespace initialize
{
	
	buffer::dispatchIndirect dispatchIndirect(animation::particle::emitterInitializer &initializer)
	{
		buffer::dispatchIndirect d;
		//d.threadGroupCountX = (ui32)std::ceil((fl32)initializer.particles.createCount / 32.f);
		d.threadGroupCountX = initializer.particles.createCount;
		d.threadGroupCountY = 1;
		d.threadGroupCountZ = 1;
		return d;
	}

	buffer::drawIndexedIndirect drawIndirect(ui32 instanceCount, ui32 startInstanceLocation)
	{
		buffer::drawIndexedIndirect d;
		d.indexCountPerInstance = 6;
		d.instanceCount = instanceCount;		
		d.startIndexLocation = 0;
		d.baseVertexLocation = 0;
		d.startInstanceLocation = startInstanceLocation;
		return d;		
	}

	ui32 particleCount(animation::particle::emitterInitializer &initializer)
	{
		auto &p = initializer.particles;
		// min create rate is 60 times per second.
		if ((fl64)p.createRate < (1.0 / 60.0)) {
			p.createRate = 1.f/60.f;
			return (p.createCount + 4)* (ui32)(p.lifetime * 60.f);
		} // The +4 here is to
		return (p.createCount + 4) * (ui32)(p.lifetime * (1.f/p.createRate));
	}

	animation::particle::ringIndices ringIndices(ui32 maxParticles)
	{
		animation::particle::ringIndices c;
		c.destroyed = 0;
		c.created = 0;

		c.startIndex = 0;
		c.endIndex = 0;
		return c;
	}

	std::vector<instance::particle> instances(ui32 maxParticles)
	{
		ui32 indexCount = 1; // per quad
		std::vector<instance::particle> instances(maxParticles);
		// Double check indices later.
		for (ui32 i = 0; i < maxParticles; i ++) {;
			instances[i].index = i;
		}

		return instances;
	}

	static std::vector<animation::particle::transform> ringTransforms(ui32 maxParticles)
	{
		return std::vector<animation::particle::transform>(maxParticles);
	}
}

namespace helper
{
	mesh::triangle<vertex::particle> quad(fl32 quadScale) 
	{
		ui32 vertexSize = 4;
		auto vertices = std::make_unique<vertex::particle[]>(vertexSize);
		
		v3 norm = v3(0.f, 0.f, 1.f);
		vertices[0].position = v3(-0.5f, -0.5f, 0.f)*quadScale;
		vertices[0].textureCoordinate = v2(0.f, 0.f);
		vertices[0].normal = norm;
		
		vertices[1].position = v3(-0.5f, 0.5f, 0.f)*quadScale;
		vertices[1].textureCoordinate = v2(0.f, 1.f);
		vertices[1].normal = norm;
		
		vertices[2].position = v3(0.5f, 0.5f, 0.f)*quadScale;
		vertices[2].textureCoordinate = v2(1.f, 1.f);
		vertices[2].normal = norm;
		
		vertices[3].position = v3(0.5f, -0.5f, 0.f)*quadScale;
		vertices[3].textureCoordinate = v2(1.f, 0.f);
		vertices[3].normal = norm;

		ui32 indexSize = 6;
		auto indices = std::make_unique<ui32[]>(indexSize);

		indices[0] = 0;
		indices[1] = 1;
		indices[2] = 2;

		indices[3] = 0;
		indices[4] = 2;
		indices[5] = 3;
		
		return mesh::triangle<vertex::particle>(vertices.get(), indices.get(), vertexSize, indexSize);
	}

	static std::random_device r;

	std::vector<animation::particle::particle> particleData(ui32 maxParticles) 
	{
		std::vector<animation::particle::particle> particles(maxParticles);
			   
		animation::particle::particle particle;
		particle.lifetime = 0.f;
		particle.velocity = v3::Zero();
		std::fill(particles.data(), particles.data() + particles.size(), particle);

		return particles;
	}
}


/*struct oddOrEven 
{
ui32 indexOffset;
ui32 bufferWidth;
fl32 padding[2]; 
};

void emitter::sort()
{
auto context = d3d::getDeviceContext();

auto sortShader = shader::compute::find(fp::sort);
context->CSSetShader(sortShader.Get(), 0, 0);

oddOrEven oe;
oe.bufferWidth = particleCount;
oe.indexOffset = 0;
auto oddOrEvenBuffer = buffer::constant<oddOrEven>(oddOrEven());
context->CSSetConstantBuffers(2, 1, oddOrEvenBuffer.data.GetAddressOf());

for (ui32 i = 0; i <= particleCount; i+=2) {
oe.indexOffset = 0;
oddOrEvenBuffer.set(oe);
context->Dispatch(particleCount/1024, 1, 1);			
oe.indexOffset = 1;
oddOrEvenBuffer.set(oe);
context->Dispatch(particleCount/1024, 1, 1);			
}
}*/