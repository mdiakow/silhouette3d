#include "aniParCommon.cms"

///////////////////////////////////
// SHADER HELPER FUNCTIONS
///////////////////////////////////

// RNG test from:
// https://stackoverflow.com/questions/5149544/can-i-generate-a-random-number-inside-a-pixel-shader
float rng(float val)
{
	float2 k1 = float2(
		23.14069263277926, // e^pi (Gelfond's constant)
		2.665144142690225 // 2^sqrt(2) (Gelfond schneider constant)
	);

	float2 k2 = float2(val, val);

	return frac(cos(val * 123456.789));
}

[numthreads(1, 1, 1)]
void csMain(uint3 dt : SV_DispatchThreadID)
{
	uint maxParticles, particleStride;
	ringTransforms.GetDimensions(maxParticles, particleStride);
	// Create a new particle, adds to end of current buffer.
	uint createIndex = ringIndices[0].endIndex + dt.x;
	if (createIndex > maxParticles-1) {
		createIndex -= maxParticles;
	}
	if (ringData[createIndex].lifetime <= 0.f) {
		uint emitterFieldSize, strideJunk;
		emitterField.GetDimensions(emitterFieldSize, strideJunk);

		float x = (float)dt.x;
		uint randomIndex = (uint)(rng(x + random.x + deltaTime)*float(emitterFieldSize-2));
		// Set position
		ringTransforms[createIndex].position.xyz = emitterPosition + emitterField[randomIndex].position;

		// Set direction
		float newForce = forceMin + ((rng(deltaTime+random.y)*0.5f + 1.f)*(forceMax-forceMin));
		if (newForce > 0.f) {
			ringData[createIndex].velocity = emitterField[randomIndex].direction * newForce;
		} else {
			ringData[createIndex].velocity = float3(0,0,0);
		}

		// Set scale
		ringData[createIndex].scaleStart = scaleStartMin + abs(rng(random.y + deltaTime + x))*(scaleStartMax - scaleStartMin);
		ringData[createIndex].scaleEnd = scaleEndMin + abs(rng(random.z + deltaTime + x))*(scaleEndMax - scaleEndMin);

		// Set lifetime
		ringData[createIndex].lifetime = lifetime;

		InterlockedAdd(ringIndices[0].created, 1);
	}

	// delete old particles that would always be at the start of the buffer.
	uint destroyIndex = ringIndices[0].startIndex + dt.x;
	if (destroyIndex > maxParticles-1) {
		destroyIndex -= maxParticles;
	}
	if (ringData[destroyIndex].lifetime <= 0.f) {
		InterlockedAdd(ringIndices[0].destroyed, 1);
	}
}