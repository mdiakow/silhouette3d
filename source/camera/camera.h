#pragma once

#include <directxmath.h>
#include "math/linalg.h"

using namespace DirectX;

namespace render
{
	class camera
	{
	public:
		camera();
		camera(v3 position, v3 rotation);
		camera(const camera&);
		~camera();
	public:

		void render();

		m4 getView();
	public:
		v3 position;
		v3 rotation;
	private:
		m4 view;
	};
}

