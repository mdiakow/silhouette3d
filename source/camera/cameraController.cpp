#include "camera/cameraController.h"
#include "systemCore/time/bunktime.h"
#include "math/linalg.h"
#include "input/input.h"

#include "render/render.h"

#include "debugger/bunkdebug.h"

namespace camera
{
	controller::controller(input::axes move, input::axes look, fl32 moveSpeed, fl32 lookSpeed, v3 position, v3 rotation) :
		inputMove(std::move(move)),
		inputLook(std::move(look)),
		moveSpeed(moveSpeed),
		lookSpeed(lookSpeed),
		camera(position, rotation)
	{

	}

	camera::controller controller::default2d(fl32 moveSpeed, fl32 lookSpeed)
	{
		return camera::controller(
			std::move(input::axes(
				nullptr, 0u,
				nullptr, 0u,
				new ui16{'W'}, 1u,
				new ui16{'S'}, 1u,
				new ui16{'D'}, 1u,
				new ui16{'A'}, 1u
			)), std::move(input::axes(
				nullptr, 0u,
				nullptr, 0u,
				nullptr, 0u,
				nullptr, 0u,
				nullptr, 0u,
				nullptr, 0u
			)), moveSpeed, lookSpeed, v3(0.f, 0.f, -500.f), v3::Zero());
	}

	camera::controller controller::default3d(fl32 moveSpeed, fl32 lookSpeed)
	{
		return camera::controller(
			std::move(input::axes(
				new ui16{'W'}, 1u,
				new ui16{'S'}, 1u,
				nullptr, 0u,
				nullptr, 0u,
				new ui16{'D'}, 1u,
				new ui16{'A'}, 1u
			)), std::move(input::axes(
				nullptr, 0u,
				nullptr, 0u,
				new ui16{input::key::right}, 1u,
				new ui16{input::key::left}, 1u,
				new ui16{input::key::up}, 1u,
				new ui16{input::key::down}, 1u
			)), moveSpeed, lookSpeed, v3::Zero(), v3::Zero());
	}

	void controller::update()
	{
		fl32 pitch = camera.rotation.x() * 0.0174532925f;
		fl32 yaw = camera.rotation.y() * 0.0174532925f;
		fl32 roll = camera.rotation.z() * 0.0174532925f;

		m3 rotation = linalg::rotation::yawPitchRoll(yaw, pitch, roll);

		camera.position += rotation * (inputMove.getAxes() * bunktime::dt() * moveSpeed);

		camera.rotation += (inputLook.getAxes() * bunktime::dt() * lookSpeed);
	}

	void controller::makeMain()
	{
		render::setMainCamera(&camera);
	}
}