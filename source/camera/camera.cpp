#include "Camera.h"

#include "debugger/bunkdebug.h"
namespace render
{


	camera::camera()
	{
		position = v3::Zero();
		rotation = v3::Zero();
	}

	camera::camera(v3 position, v3 rotation) :
		position(position),
		rotation(rotation)
	{
	}

	camera::camera(const camera &other)
	{
	}

	camera::~camera()
	{
	}


	void camera::render()
	{
		using namespace linalg;

		// convert from degrees to radians
		fl32 pitch = rotation(0) * 0.0174532925f;
		fl32 yaw = rotation(1) * 0.0174532925f;
		fl32 roll = rotation(2) * 0.0174532925f;

		m3 rotation = rotation::yawPitchRoll(yaw, pitch, roll);

		// reorient the up and look vector by the camera's current rotation.
		v3 la = rotation * lefthand::look;
		v3 uv = rotation * lefthand::up;

		la += position;
	
		// recalculates view based on current position and rotation of the camera.
		view = lefthand::lookAt(position, la, uv);
	}

	m4 camera::getView()
	{
		return view;
	}

}