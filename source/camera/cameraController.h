#ifndef CAMERA_CONTR_H_
#define CAMERA_CONTR_H_

#include <memory>
#include "camera/camera.h"
#include "input/inputAxes.h"

namespace camera
{
	class controller
	{
	public:
		controller(input::axes move, input::axes look, fl32 moveSpeed, fl32 lookSpeed, v3 position, v3 rotation);
		
		static camera::controller default2d(fl32 moveSpeed, fl32 lookSpeed);
		static camera::controller default3d(fl32 moveSpeed, fl32 lookSpeed);
	public:
		void update();
		// Make the camera attached to this controller the one to render from.
		void makeMain();
	public:
		fl32 lookSpeed;
		fl32 moveSpeed;

	private:
		render::camera camera;

		input::axes inputLook;
		input::axes inputMove;
	};
}

#endif