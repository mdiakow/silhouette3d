#include "System.h"
#include "update/coreUpdate.h"
//#include "render.h"
#include "input/coreInput.h"
#include "render/core/coreRender.h"
#include "userInterface/coreUserInterface.h"
#include "programObjects/programobjects.h"

#include "bunkcore.h"
#include "systemCore/time/bunktime.h"
#include "systemCore/fps/bunkfps.h"
#include "strsafe.h"

namespace bunkcore
{
	static void initializeWindows(ui32 &screenWidth, ui32 &screenHeight);
	static void cleanupWindows();

	static const ui32 windowWidth = 1920;
	static const ui32 windowHeight = 1080;

	LPCWSTR applicationName;
	HINSTANCE hinstance;
	HWND hwnd;

	static bool isRunning;

	// Called to start running, contains loop of code related specifically to windows.
	void run()
	{
		while (isRunning) {
			bunktime::updateCurrent();
			bunkfps::update();

			input::poll();
			update::all();
			render::all();

			bunktime::updatePrevious();
		}
	}

	static LRESULT CALLBACK windowsProcess(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
	{
		switch (umessage) {
			// Check if the window is being destroyed.
			case WM_DESTROY:
				PostQuitMessage(0);
				return 0;
			 // Check if the window is being closed.
			case WM_CLOSE:
				PostQuitMessage(0);
				return 0;
			// All other messages pass to the message handler in the input class.
			default:
				return input::messageHandler(hwnd, umessage, wparam, lparam);
		}
	}

	void initialize()
	{
		ui32 screenWidth, screenHeight;

		isRunning = true;

		screenWidth = 0;
		screenHeight = 0;

		initializeWindows(screenWidth, screenHeight);
		
		bunktime::initialize();

		input::initialize();
		render::initialize(screenWidth, screenHeight, hwnd);
		update::initialize();

		userInterface::initialize();
		bunkfps::initialize();
		programObjects::initialize();
	}

	void cleanup()
	{
		programObjects::cleanup();
		bunkfps::cleanup();
		userInterface::cleanup();

		update::cleanup();
		render::cleanup();
		input::cleanup();
		
		bunktime::cleanup();

		cleanupWindows();
	}

	void shutdown()
	{
		isRunning = false;
	}

	static void initializeWindows(ui32 &screenWidth, ui32 &screenHeight)
	{
		WNDCLASSEX wc;
		DEVMODE dmScreenSettings;
		ui32 posX, posY;

		hinstance = GetModuleHandle(NULL);

		applicationName = L"Engine";

		wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		wc.lpfnWndProc = windowsProcess;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = hinstance;
		wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		wc.hIconSm = wc.hIcon;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = applicationName;
		wc.cbSize = sizeof(WNDCLASSEX);

		RegisterClassEx(&wc);

		screenWidth = GetSystemMetrics(SM_CXSCREEN);
		screenHeight = GetSystemMetrics(SM_CYSCREEN);

		if (render::isFullscreen())
		{
			// If full screen set the screen to maximum size of the users desktop and 32bit.
			memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
			dmScreenSettings.dmSize = sizeof(dmScreenSettings);
			dmScreenSettings.dmPelsWidth = (unsigned long)screenWidth;
			dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
			dmScreenSettings.dmBitsPerPel = 32;
			dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

			// Change the display settings to full screen.
			ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

			// Set the position of the window to the top left corner.
			posX = posY = 0;
		} else
		{
			// If windowed then set it to 800x600 resolution.
			screenWidth = windowWidth;
			screenHeight = windowHeight;

			// Place the window in the middle of the screen.
			posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
			posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
		}

		// Create the window with the screen settings and get the handle to it.
		hwnd = CreateWindowEx(WS_EX_APPWINDOW, applicationName, applicationName,
			WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
			posX, posY, screenWidth, screenHeight, NULL, NULL, hinstance, NULL);

		// Bring the window up on the screen and set it as main focus.
		ShowWindow(hwnd, SW_SHOW);
		SetForegroundWindow(hwnd);
		SetFocus(hwnd);

		// Hide the mouse cursor.
		ShowCursor(true);
	}

	static void cleanupWindows()
	{
		ShowCursor(true);

		if (render::isFullscreen()) {
			ChangeDisplaySettings(NULL, 0);
		}

		DestroyWindow(hwnd);

		hwnd = NULL;
	}
}
