#include "bunkdebug.h"
#include "coreBunkdebug.h"

#include <iostream>
#include <fstream>

namespace debug
{


	void log(str toLog)
	{
		std::ofstream file;
		file.open("debug.txt", std::ios_base::app);
		file << toLog;
		//file.close();
	}

	static Eigen::IOFormat fmt2d(4, 0, ", ", " ", " ", " ");
	void log(const v2 &vec2)
	{
		std::ofstream file;
		file.open("debug.txt", std::ios_base::app);
		file << vec2.format(fmt2d) << std::endl;
	}

	void log(const v3 &vec3)
	{
		std::ofstream file;
		file.open("debug.txt", std::ios_base::app);
		file << vec3.format(fmt2d) << std::endl;
	}



	void initialize()
	{

	}
	void cleanup()
	{

	}
}