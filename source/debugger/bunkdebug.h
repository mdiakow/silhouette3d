#ifndef BUNK_DEBUG_H_
#define BUNK_DEBUG_H_

#include "bunkcore.h"

#include <iostream>
#include <fstream>

namespace debug
{
	//void log(str toLog);

	void log(const v2 &vec2);
	void log(const v3 &vec3);

	template <typename T>
	void log(T toLog)
	{
		std::ofstream file;
		file.open("debug.txt", std::ios_base::app);
		file << toLog << std::endl;
		//file.close();
	}
}

#endif