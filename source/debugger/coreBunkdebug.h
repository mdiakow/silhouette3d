#ifndef CORE_BUNK_DEBUG_H_
#define CORE_BUNK_DEBUG_H_

namespace debug
{
	void initialize();
	void cleanup();
}

#endif