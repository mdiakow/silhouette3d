#include "D3D.h"
#include <memory>
#include <wrl/client.h>

#include <stdio.h>
#include <tchar.h>

namespace d3d
{
	void errorDescription(HRESULT hr);

	bool vsyncEnabled;
	int videoCardMemory;
	char videoCardDescription[128];
	IDXGISwapChain* swapChain;
	ID3D11Device* device;
	ID3D11DeviceContext* deviceContext;
	ID3D11RenderTargetView* renderTargetView;
	ID3D11Texture2D* depthStencilBuffer;
	ID3D11DepthStencilState* depthStencilState;
	ID3D11DepthStencilView* depthStencilView;
	ID3D11RasterizerState* rasterState;

	ID3D11DepthStencilState* depthDisabledStencilState;
	ID3D11DepthStencilState* readOnlyDepth;

	ID3D11BlendState *alphaEnabled;
	ID3D11BlendState *alphaDisabled;
	ID3D11BlendState *particleBlend;

	void beginScene()
	{
		fl32 color[4];

		color[0] = 0.f;
		color[1] = 0.f;
		color[2] = 0.f;
		color[3] = 0.f;

		// Clear back buffer
		deviceContext->ClearRenderTargetView(renderTargetView, color);

		// Clear the depth buffer
		deviceContext->ClearDepthStencilView(depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	}

	void endScene()
	{
		if (vsyncEnabled)
			swapChain->Present(1, 0);
		else
			swapChain->Present(0, 0);

		deviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);
	}

	void enableZBuffer()
	{
		deviceContext->OMSetDepthStencilState(depthStencilState, 1);
	}

	void disableZBuffer()
	{
		deviceContext->OMSetDepthStencilState(depthDisabledStencilState, 1);
	}

	void enableAlphaBlending()
	{
		fl32 blendFactor[4];

		blendFactor[0] = 0.f;
		blendFactor[1] = 0.f;
		blendFactor[2] = 0.f;
		blendFactor[3] = 0.f;

		deviceContext->OMSetBlendState(alphaEnabled, blendFactor, 0xffffffff);
	}

	void disableAlphaBlending()
	{
		fl32 blendFactor[4];

		blendFactor[0] = 0.f;
		blendFactor[1] = 0.f;
		blendFactor[2] = 0.f;
		blendFactor[3] = 0.f;

		deviceContext->OMSetBlendState(alphaDisabled, blendFactor, 0xffffffff);
	}

	void setParticleBlending()
	{
		deviceContext->OMSetDepthStencilState(readOnlyDepth, 1);
	}

	ID3D11Device* getDevice()
	{
		return device;
	}
	ID3D11DeviceContext* getDeviceContext()
	{
		return deviceContext;
	}
	
	void getVideoCardInfo(char *cardName, int &memory)
	{
		strcpy_s(cardName, 128, videoCardDescription);
		memory = videoCardMemory;
	}

	void initialize(ui32 screenWidth, ui32 screenHeight, bool vsync, HWND hwnd, bool fullscreen, fl32 screenDepth, fl32 screenNear)
	{
		HRESULT result;

		IDXGIFactory* factory;
		IDXGIAdapter* adapter;
		IDXGIOutput* adapterOutput;

		unsigned int numModes, numerator, denominator;
		unsigned long long stringLength;

		DXGI_MODE_DESC* displayModeList;
		DXGI_ADAPTER_DESC adapterDesc;

		int error;

		DXGI_SWAP_CHAIN_DESC swapChainDesc;
		D3D_FEATURE_LEVEL featureLevel;
		ID3D11Texture2D* backBuffer;
		D3D11_TEXTURE2D_DESC depthBufferDesc;
		D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
		D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
		D3D11_RASTERIZER_DESC rasterDesc;
		D3D11_VIEWPORT viewport;
		D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;
		float fieldOfView, screenAspect;

		vsyncEnabled = vsync;

		result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
		/*if (FAILED(result))
		return false;*/

		result = factory->EnumAdapters(0, &adapter);
		/*if (FAILED(result))
		return false;*/

		result = adapter->EnumOutputs(0, &adapterOutput);
		/*if (FAILED(result))
		return false;*/

		result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
		/*if (FAILED(result))
		//return false;*/

		displayModeList = new DXGI_MODE_DESC[numModes];
		/*if (!displayModeList)
		//return false;*/

		result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
		/*if (FAILED(result))
		//return false;*/

		// Find the monitor refresh rate
		for (unsigned int i = 0; i < numModes; i++)
		{
			if (displayModeList[i].Width == (unsigned int)screenWidth && displayModeList[i].Height == (unsigned int)screenHeight)
			{
				numerator = displayModeList[i].RefreshRate.Numerator;
				denominator = displayModeList[i].RefreshRate.Denominator;
			}
		}

		result = adapter->GetDesc(&adapterDesc);
		/*if (FAILED(result))
		//return false;*/

		// Video card memory in megabytes
		videoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

		error = wcstombs_s(&stringLength, videoCardDescription, 128, adapterDesc.Description, 128);
		/*if (error != 0)
		//return false;*/

		// Clear memory used for getting refresh rate
		delete[] displayModeList;
		displayModeList = 0;

		adapterOutput->Release();
		adapterOutput = 0;

		adapter->Release();
		adapter = 0;

		factory->Release();
		factory = 0;

		// Initialize swap chain description
		ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

		// Number of back buffers
		swapChainDesc.BufferCount = 2;

		// Set width/height of back buffer
		swapChainDesc.BufferDesc.Width = screenWidth;
		swapChainDesc.BufferDesc.Height = screenHeight;

		swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

		// Back buffer refresh rate
		if (vsyncEnabled)
		{
			swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
			swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
		}
		else
		{
			swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
			swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
		}

		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

		swapChainDesc.OutputWindow = hwnd;

		// Turn multisampling off
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;

		// enable/disable windowed mode
		if (fullscreen)
			swapChainDesc.Windowed = false;
		else
			swapChainDesc.Windowed = true;

		swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

		// discard back buffer contents after rendering
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;

		swapChainDesc.Flags = 0;

		featureLevel = D3D_FEATURE_LEVEL_11_0;

		// initialize swapChain, device and deviceContext

		result = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, D3D11_CREATE_DEVICE_DEBUG, &featureLevel, 1, D3D11_SDK_VERSION, &swapChainDesc, &swapChain, &device, NULL, &deviceContext);
		/*if (FAILED(result))
		//return false;*/

		// initialize backBuffer
		result = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
		//if (FAILED(result))
		//	//return false;

		// initialize renderTargetView
		result = device->CreateRenderTargetView(backBuffer, NULL, &renderTargetView);
		
		//if (FAILED(result))
		//	//return false;

		// free backBuffer
		backBuffer->Release();
		backBuffer = 0;

		// initilize depth buffer
		ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

		depthBufferDesc.Width = screenWidth;
		depthBufferDesc.Height = screenHeight;
		depthBufferDesc.MipLevels = 1;
		depthBufferDesc.MipLevels = 1;
		depthBufferDesc.ArraySize = 1;
		depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthBufferDesc.SampleDesc.Count = 1;
		depthBufferDesc.SampleDesc.Quality = 0;
		depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		depthBufferDesc.CPUAccessFlags = 0;
		depthBufferDesc.MiscFlags = 0;

		// Create texture for depth buffer using above initialization
		result = device->CreateTexture2D(&depthBufferDesc, NULL, &depthStencilBuffer);
		//if (FAILED(result))
		//	//return false;

		ZeroMemory(&depthDisabledStencilDesc, sizeof(depthDisabledStencilDesc));

		depthDisabledStencilDesc.DepthEnable = false;
		depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
		depthDisabledStencilDesc.StencilEnable = true;
		depthDisabledStencilDesc.StencilReadMask = 0xFF;
		depthDisabledStencilDesc.StencilWriteMask = 0xFF;
		depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
		depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		device->CreateDepthStencilState(&depthDisabledStencilDesc, &depthDisabledStencilState);

		// depth stencil settings
		ZeroMemory(&depthStencilDesc, sizeof(depthBufferDesc));

		depthStencilDesc.DepthEnable = true;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

		depthStencilDesc.StencilEnable = true;
		depthStencilDesc.StencilReadMask = 0xff;
		depthStencilDesc.StencilWriteMask = 0xff;

		// operations for front facing pixels
		depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
		depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		// operations on back facing pixels
		depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
		depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
		depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

		// Initialize depth stencil
		result = device->CreateDepthStencilState(&depthStencilDesc, &depthStencilState);
		//if (FAILED(result))
		//	//return false;
		
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		result = device->CreateDepthStencilState(&depthStencilDesc, &readOnlyDepth);

		deviceContext->OMSetDepthStencilState(depthStencilState, 1);

		// set up view depth stencil buffer
		ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

		depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		depthStencilViewDesc.Texture2D.MipSlice = 0;

		// initialize depth stencil view
		result = device->CreateDepthStencilView(depthStencilBuffer, &depthStencilViewDesc, &depthStencilView);
		//if (FAILED(result))
		//	//return false;

		// Set up the blend descriptions (alphaEnabled, alphaDisabled)
		D3D11_BLEND_DESC blendDesc;
		ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

		result = device->CreateBlendState(&blendDesc, &alphaEnabled);

		blendDesc.RenderTarget[0].BlendEnable = FALSE;

		result = device->CreateBlendState(&blendDesc, &alphaDisabled);

		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

		result = device->CreateBlendState(&blendDesc, &particleBlend);

		// create render target view
		deviceContext->OMSetRenderTargets(1, &renderTargetView, depthStencilView);

		// determine how polygons are drawn
		rasterDesc.AntialiasedLineEnable = false;
		rasterDesc.CullMode = D3D11_CULL_BACK;
		rasterDesc.DepthBias = 0;
		rasterDesc.DepthBiasClamp = 0.0f;
		rasterDesc.DepthClipEnable = true;
		rasterDesc.FillMode = D3D11_FILL_SOLID;
		rasterDesc.FrontCounterClockwise = false;
		rasterDesc.MultisampleEnable = false;
		rasterDesc.ScissorEnable = false;
		rasterDesc.SlopeScaledDepthBias = 0.0f;

		// Initialize raster description
		result = device->CreateRasterizerState(&rasterDesc, &rasterState);
		if (FAILED(result))
			//return false;

			// set raster state
			deviceContext->RSSetState(rasterState);

		// set up viewport
		viewport.Width = (fl32)screenWidth;
		viewport.Height = (fl32)screenHeight;
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;
		viewport.TopLeftX = 0.0f;
		viewport.TopLeftY = 0.0f;

		// initialize viewport
		deviceContext->RSSetViewports(1, &viewport);

		// Setup projection matrix
		fieldOfView = 3.141592654f / 4.0f;
		screenAspect = (float)screenWidth / (float)screenHeight;
	}

	void cleanup()
	{
		if (swapChain)
			swapChain->SetFullscreenState(false, NULL);

		if(depthDisabledStencilState)
		{
			depthDisabledStencilState->Release();
			depthDisabledStencilState = 0;
		}

		if (rasterState)
		{
			rasterState->Release();
			rasterState = 0;
		}

		if (depthStencilView)
		{
			depthStencilView->Release();
			depthStencilView = 0;
		}

		if (depthStencilState)
		{
			depthStencilState->Release();
			depthStencilState = 0;
		}

		if (depthStencilBuffer)
		{
			depthStencilBuffer->Release();
			depthStencilState = 0;
		}

		if (renderTargetView) 
		{
			renderTargetView->Release();
			renderTargetView = 0;
		}

		if (deviceContext)
		{
			deviceContext->Release();
			deviceContext = 0;
		}

		if (device)
		{
			device->Release();
			device = 0;
		}

		if (swapChain)
		{
			swapChain->Release();
			swapChain = 0;
		}

		d3d::release(&alphaEnabled);
		d3d::release(&alphaDisabled);
	}



	void errorDescription(HRESULT hr) 
	{ 
		if(FACILITY_WINDOWS == HRESULT_FACILITY(hr)) 
			hr = HRESULT_CODE(hr); 
		TCHAR* szErrMsg; 

		if(FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM, 
			NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
			(LPTSTR)&szErrMsg, 0, NULL) != 0) 
		{ 
			_tprintf(TEXT("%s"), szErrMsg); 
			LocalFree(szErrMsg); 
		} else 
			_tprintf( TEXT("[Could not find a description for error # %#x.]\n"), hr); 
	}
}



