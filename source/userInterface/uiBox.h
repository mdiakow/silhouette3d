#ifndef UI_BOX_H2_
#define UI_BOX_H2_

#include "bunkcore.h"
#include "d3d.h"

#include "userInterface/uiQuad.h"
#include "userInterface/uiTexture.h"

namespace userInterface
{
	class box
	{
	public:
		box(v2 lowerLeft, v2 upperRight, v4 colour);
		// 1 pixel thick border surrounding the cube.
		box(v2 lowerLeft, v2 upperRight, v4 backgroundColour, v4 borderColour);
		void render();
	private:
		userInterface::quad quad;
		userInterface::texture texture;
	};
}

#endif