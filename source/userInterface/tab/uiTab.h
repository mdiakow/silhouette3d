#ifndef UI_TAB_H_
#define UI_TAB_H_

#include <memory>
#include <functional>

#include "bunkcore.h"
#include "userInterface/uiBox.h"
#include "userInterface/uiBoundingBox.h"
#include "userInterface/text/uiTextLine.h"
#include "userInterface/font/uiFontData.h"
#include "userInterface/uiInput.h"

namespace userInterface
{
	class tab
	{
	public:
		tab(str label, v2 lowerLeft, v2 upperRight, userInterface::style *style, std::shared_ptr<userInterface::input> input, std::function<void()> onTabPress);
	public:
		void render();
		void update();
	public:
		std::shared_ptr<userInterface::input> input;
		userInterface::text8::line line;

		userInterface::box neutralBox;
		userInterface::box onMouseOverBox;		
		userInterface::box onPressBox;

		userInterface::box *currentBox;

		userInterface::boundingBox bounds;

		std::function<void()> onTabPress;
	};
}



#endif