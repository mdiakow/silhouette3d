#ifndef UI_TAB_BAR_H_
#define UI_TAB_BAR_H_

#include "bunkcore.h"

#include <deque>
#include <memory>
#include <functional>

#include "userInterface/tab/uiTab.h"
#include "userInterface/uiStyle.h"
#include "userInterface/uiInput.h"
#include "userInterface/uiBox.h"

namespace userInterface
{
	class tabBar
	{
	public:
		tabBar(v2 lowerLeft, v2 upperRight, std::shared_ptr<userInterface::input> input, std::shared_ptr<userInterface::style> style);

	public:
		void render();
		void update();

		void addTab(str label, std::function<void()> onPress);
	private:
		userInterface::box bar; // The bar to hold the tabs.
		std::deque<userInterface::tab> tabs;
		userInterface::tab *focusTab;
		
		v2 currentPosition; // Used to place the next tab.

		std::shared_ptr<userInterface::input> input;
		std::shared_ptr<userInterface::style> style;
	};
}

#endif