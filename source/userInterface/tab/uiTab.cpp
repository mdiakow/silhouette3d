#include "userInterface/tab/uiTab.h"
#include "Input/input.h"
namespace userInterface
{
	tab::tab(str label, v2 lowerLeft, v2 upperRight, userInterface::style * style, std::shared_ptr<userInterface::input> input, std::function<void()> onTabPress) :
		line(userInterface::text8::line(label, lowerLeft + v2(style->font->format.margin.left, style->font->format.margin.bottom), upperRight, style->font)),
		neutralBox(userInterface::box(lowerLeft, upperRight, style->neutral.background)),
		onMouseOverBox(userInterface::box(lowerLeft, upperRight, style->onMouseOver.background)),
		onPressBox(userInterface::box(lowerLeft, upperRight, style->onPress.background)),
		bounds(userInterface::boundingBox(lowerLeft, upperRight)),
		input(input),
		onTabPress(onTabPress)
	{
		currentBox = &neutralBox;
	}

	void tab::render()
	{
		currentBox->render();
		line.render();
	}

	void tab::update()
	{
		v2 mousePosition = ::input::mouse::position::ui();
		if (bounds.isInside(mousePosition)) {
			currentBox = &onMouseOverBox;
			if (input->accept->isDown()) {
				currentBox = &onPressBox;
			}
			if (input->accept->onRelease()) {
				onTabPress();
			}
		} else {
			currentBox = &neutralBox;
		}
	}
}