#include "userInterface/tab/uiTabBar.h"

namespace userInterface
{
	tabBar::tabBar(v2 lowerLeft, v2 upperRight, std::shared_ptr<userInterface::input> input, std::shared_ptr<userInterface::style> style) :
		input(input),
		style(style),
		bar(userInterface::box(lowerLeft, upperRight, style->neutral.background)),
		tabs()
	{
		currentPosition = lowerLeft;
	}

	void tabBar::render()
	{
		bar.render();
		for (auto &tab : tabs) {
			tab.render();
		}
	}

	void tabBar::update()
	{
		for (auto &tab : tabs) {
			tab.update();
		}
	}

	void tabBar::addTab(str label, std::function<void()> onPress)
	{
		auto format = style->font->format;
		v2 lowerLeft = currentPosition;
		fl32 height = (fl32)(format.fontHeight + format.margin.top + format.margin.bottom);
		fl32 width = style->font->getStringWidth(label) + (fl32)(format.margin.left + format.margin.right);
		v2 upperRight = lowerLeft + v2(width, height);
		tabs.push_back(userInterface::tab(label, lowerLeft, upperRight, style.get(), input, onPress));

		currentPosition += v2(width, 0.f);
	}
}