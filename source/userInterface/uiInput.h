#ifndef UI_INPUT_H_
#define UI_INPUT_H_

#include "Input/inputButton.h"
#include <memory>

namespace userInterface
{
	struct input
	{
		std::unique_ptr<::input::button> accept;
	};
}

#endif