#include "userInterface/uiStyle.h"

namespace userInterface
{
	style::style(std::shared_ptr<userInterface::font::data8> font, userInterface::colour neutral, userInterface::colour onMouseOver, userInterface::colour onPress, userInterface::border border) :
		font(font),
		neutral(neutral),
		onMouseOver(onMouseOver),
		onPress(onPress),
		border(border)
	{
	}
}