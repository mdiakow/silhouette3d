#include "uiBox.h"
#include "error/errorLog.h"
#include "colour.h"

userInterface::box::box(v2 lowerLeft, v2 upperRight, v4 colour) try :
	quad(userInterface::quad(lowerLeft, upperRight)),
	texture(userInterface::texture((ui16)(upperRight.x() - lowerLeft.x()),(ui16)(upperRight.y() - lowerLeft.y()), colour))
{
	if (upperRight.x() <= lowerLeft.x() || upperRight.y() <= lowerLeft.y()) {
		throw std::invalid_argument(std::string(__FILE__) + ": Box points are invalid (Make sure lowerLeft is less than upperRight in both x and y dimensions.)");
	}
}
catch (std::invalid_argument &ia)
{
	error::log(ia.what());
}


userInterface::box::box(v2 lowerLeft, v2 upperRight, v4 backgroundColour, v4 borderColour) :
	quad(userInterface::quad(lowerLeft, upperRight)),
	texture()
{
	ui32 width = (ui32)(upperRight.x() - lowerLeft.x());
	ui32 height = (ui32)(upperRight.y() - lowerLeft.y());

	auto textureBuffer = std::make_unique<ui32[]>(width*height);

	ui32 formattedBGColour = colour::rgba32(backgroundColour);
	ui32 formattedBDColour = colour::rgba32(borderColour);

	std::fill(&textureBuffer[0], &textureBuffer[0] + (width*height), formattedBGColour);

	ui32 bottomRowIndex = width*(height-1);
	for (ui32 x = 0; x < width; x++) { 
		textureBuffer[x] = formattedBDColour; // Fills top row.
		textureBuffer[bottomRowIndex + x] = formattedBDColour;
	}
	ui32 leftIndex;
	ui32 rightIndex;
	for (ui32 y = 1; y < height - 1; y++) {
		leftIndex = y*width;
		textureBuffer[leftIndex] = formattedBDColour;
		rightIndex = leftIndex + (width-1);
		textureBuffer[rightIndex] = formattedBDColour;
	}
	texture = userInterface::texture(width, height, textureBuffer);	
}

void userInterface::box::render()
{
	texture.render();
	quad.render();
}
