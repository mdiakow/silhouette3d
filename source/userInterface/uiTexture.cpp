#include "uiTexture.h"
#include "colour.h"
#include <comdef.h>
#include <memory>
#include "d3d.h"

static const ui32 maxTextureDimension = D3D11_REQ_TEXTURE2D_U_OR_V_DIMENSION;


namespace helper 
{
	static D3D11_TEXTURE2D_DESC textureDescription(ui32 height, ui32 width);
	static D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE2D_DESC textureDesc);
}

namespace userInterface {


	texture::texture() :
		texture2d(),
		view()
	{
	}

	texture::texture(ui16 width, ui16 height, v4 colour) :
		texture2d(),
		view()
	{
		if (width > maxTextureDimension || height > maxTextureDimension) {
			throw std::invalid_argument(std::string(__FILE__) + ": Texture size limit exceeded.");
		} else {
			HRESULT result;
			auto device = d3d::getDevice();

			// Make a texture CPU side to be copied in at texture creation.
			auto textureData = std::make_unique<ui32[]>(width*height);

			ui32 formattedColour = colour::rgba32(colour);
			std::fill(&textureData[0], &textureData[0] + (width*height), formattedColour);

			D3D11_TEXTURE2D_DESC textureDesc = helper::textureDescription(height, width);

			D3D11_SUBRESOURCE_DATA initData;
			initData.pSysMem = textureData.get();
			initData.SysMemPitch = width*sizeof(ui32);
			initData.SysMemSlicePitch = 0;

			result = device->CreateTexture2D(&textureDesc, &initData, texture2d.GetAddressOf());

			D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

			result = device->CreateShaderResourceView(texture2d.Get(), &srvDesc, view.GetAddressOf());
		}

	}

	texture::texture(ui16 width, ui16 height, std::unique_ptr<ui32[]>& textureBuffer)
	{
		HRESULT result;
		auto device = d3d::getDevice();

		D3D11_TEXTURE2D_DESC textureDesc = helper::textureDescription(height, width);

		D3D11_SUBRESOURCE_DATA initData;
		initData.pSysMem = textureBuffer.get();
		initData.SysMemPitch = (ui32)width*sizeof(ui32);
		initData.SysMemSlicePitch = 0;

		result = device->CreateTexture2D(&textureDesc, &initData, texture2d.GetAddressOf());

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

		result = device->CreateShaderResourceView(texture2d.Get(), &srvDesc, view.GetAddressOf());
	}

	void texture::render()
	{
		auto context = d3d::getDeviceContext();
		context->PSSetShaderResources(0, 1, view.GetAddressOf());
	}
}

namespace helper
{
	D3D11_TEXTURE2D_DESC textureDescription(ui32 height, ui32 width)
	{
		D3D11_TEXTURE2D_DESC textureDesc;

		textureDesc.Height = height;
		textureDesc.Width = width;
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.SampleDesc.Quality = 0;
		textureDesc.Usage = D3D11_USAGE_DYNAMIC;
		textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		textureDesc.MiscFlags = 0;

		return textureDesc;
	}
	
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE2D_DESC textureDesc)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;

		srvDesc.Format = textureDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = 1;

		return srvDesc;
	}
}