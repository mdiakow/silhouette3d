#ifndef CORE_USER_INTERFACE_H_
#define CORE_USER_INTERFACE_H_

namespace userInterface
{
	void initialize();
	void cleanup();
}

#endif