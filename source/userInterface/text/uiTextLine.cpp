#include "uiTextLine.h"

#include "render/render.h"

namespace userInterface::text8
{
	line::line(str text, v2 lowerLeft, v2 upperRight, std::shared_ptr<userInterface::font::data8> font) :
		bounds(userInterface::boundingBox(lowerLeft, upperRight)),
		text(text),
		nextCharPos(lowerLeft)
	{
		this->font = font;
		
		quads = std::make_unique<userInterface::quad[]>(text.length());
		
		nextCharPos = setMemory(text);
	}

	void line::render()
	{
		for (ui32 i = 0; i < text.length(); i++) {
			font->getTexture(text[i]).render();
			quads[i].render();
		}
	}
	
	void line::setText(str text)
	{
		this->text = text;

		quads.reset();

		quads = std::make_unique<userInterface::quad[]>(text.length());
		
		nextCharPos = setMemory(text);
	}

	v2 line::getNextCharacterPosition()
	{
		if (text.length() > 0) {
			return nextCharPos;
		} else {
			return bounds.getLowerLeft();
		}
		
	}

	v2 line::setMemory(str text)
	{
		ui32 textLength = static_cast<ui32>(text.length());

		// Iterate along string and generate the appropriate quads.
		v2 currentPosition = bounds.getLowerLeft();
		v2 lowerLeft, upperRight;
		for (ui32 i = 0; i < textLength; i++) {
			lowerLeft = currentPosition;
			v2 characterDimension = font->getDimension(text[i]);
			upperRight = currentPosition + characterDimension;

			quads[i] = userInterface::quad(lowerLeft, upperRight);

			currentPosition += v2(characterDimension.x(), 0.f);
			if (currentPosition.x() > bounds.getUpperRight().x()) {
				currentPosition.y() = currentPosition.y() - characterDimension.y();
				currentPosition.x() = bounds.getLowerLeft().x();
			}
		}
		return currentPosition;
	}
}

