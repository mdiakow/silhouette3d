#ifndef UI_TEXT_INPUT_H_
#define UI_TEXT_INPUT_H_

#include <memory>
#include <regex>

#include "bunkcore.h"

#include "userInterface/text/uiTextLine.h"
#include "userInterface/uiBox.h"
#include "userInterface/uiStyle.h"
#include "Input/input.h"

namespace userInterface
{
	namespace text8
	{
		class input
		{
		public:
			input(std::regex invalidCharacters, v2 lowerLeft, v2 upperRight, userInterface::style *style);

		public:
			void render();
			void update();
			void clear();

			std::string getBuffer();

			bool inBounds(v2 position);

			static friend void ::input::text::setFocus(userInterface::text8::input *newInput); // Inside input.cpp
		private:
			std::string inputBuffer;
			ui32 cursor;

			std::regex invalidCharacters;
			userInterface::text8::line line;
			userInterface::box box;
			
			// Don't always take input during update. Controlled externally in input.cpp
			bool isFocused;
		};
	}
}

#endif