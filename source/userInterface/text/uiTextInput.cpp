#include "userInterface/text/uiTextInput.h"


namespace userInterface::text8
{
	// avoid collision with text input class
	namespace uInput = input;

	

	input::input(std::regex invalidCharacters, v2 lowerLeft, v2 upperRight, userInterface::style *style) :
		line(userInterface::text8::line("Type here.", lowerLeft, upperRight, style->font)),
		box(userInterface::box(lowerLeft - v2(style->border.left, style->border.bottom), upperRight + v2(style->border.right, style->border.top), style->neutral.background, style->neutral.border))
	{
		inputBuffer = str();
		isFocused = false;
		cursor = 0;

		this->invalidCharacters = invalidCharacters;

		line.font->cursor.setPosition(lowerLeft);
		isFocused = false;
	}

	void input::render()
	{
		box.render();
		line.render();
		if (isFocused) {
			line.font->cursor.render();
		}
		
	}

	void input::update()
	{
		if (isFocused) {
			bool updateBuffer = false;

			// Text buffer comes in from input
			str textBuffer = uInput::text::buffer();
			if (!textBuffer.empty()) {
				// Remove invalid characters from the text buffer.
				std::stringstream cleaned;
				std::regex_replace(std::ostream_iterator<char>(cleaned), textBuffer.begin(), textBuffer.end(), invalidCharacters, "");
				std::string newBuffer = cleaned.str();
				inputBuffer.append(cleaned.str());
				updateBuffer = true;
			}

			if (uInput::key::onPress(uInput::key::backspace)) {
				if (!inputBuffer.empty()) {
					inputBuffer.pop_back();
					updateBuffer = true;
				}			
			}

			if (updateBuffer) {
				line.setText(inputBuffer);
				v2 cursorPosition = line.getNextCharacterPosition();
				line.font->cursor.setPosition(cursorPosition);
			}
		}
	}

	void input::clear()
	{
		inputBuffer.erase();
		line.setText(inputBuffer);
		line.font->cursor.setPosition(line.getNextCharacterPosition());
		cursor = 0;
	}

	std::string input::getBuffer()
	{
		return this->inputBuffer;
	}

	bool input::inBounds(v2 position)
	{
		return line.bounds.isInside(position);
	}
}