#include "uiTextOutput.h"

namespace userInterface::text8
{
	output::output(v2 lowerLeft, v2 upperRight, std::shared_ptr<userInterface::font::data8> font) :
		lines(),
		lineCount(0),
		currentLine(0)
	{
		v2 charDimension = font->getDimension(' ');
		fl32 betweenLine = (fl32)font->format.betweenLines;

		fl32 totalHeight = upperRight.y() - lowerLeft.y();
		fl32 lineCount = (totalHeight + betweenLine) / (charDimension.y() + betweenLine);
		this->lineCount = (ui32)floor(lineCount);

		v2 currentLowerLeft = v2(lowerLeft.x(), upperRight.y() - charDimension.y());
		v2 currentUpperRight = upperRight;
		for (ui32 i = 0; i < lineCount; i++) {
			lines.push_back(userInterface::text8::line("", currentLowerLeft, currentUpperRight, font));

			currentLowerLeft.y() -= (charDimension.y() + betweenLine);
			currentUpperRight.y() -= (charDimension.y() + betweenLine);
		}
	}

	void output::render()
	{
		for (ui32 i = 0; i < lineCount; i++) {
			lines[i].render();
		}
	}

	void output::addText(str text)
	{
	}

	void output::addTextLine(str text)
	{
		lines[currentLine].setText(text);
		currentLine++;
		if (currentLine >= lineCount) {
			currentLine = 0;
		}
	}

	void output::setText(str text)
	{
	}
}

