// This is for rendering a single line of text to the screen that never changes.
#ifndef USER_INTERFACE_TEXT_LINE_H_
#define USER_INTERFACE_TEXT_LINE_H_

#include "bunkcore.h"

#include "userInterface/font/uiFontData.h"
#include "userInterface/uiStyle.h"
#include "userInterface/uiQuad.h"
#include "userInterface/uiBoundingBox.h"

#include <memory>

namespace userInterface
{
	namespace text8
	{
		class line
		{
		public:
			line(str text, v2 lowerLeft, v2 upperRight, std::shared_ptr<userInterface::font::data8> font);

			void render();

			void setText(str text);
			
			v2 getNextCharacterPosition();
		public:
			std::shared_ptr<userInterface::font::data8> font;
			userInterface::boundingBox bounds;
		private:
			std::unique_ptr<userInterface::quad[]> quads;
			
			str text;			

			v2 nextCharPos;
		private:
			
			v2 setMemory(str test);
		};
	}
}


#endif