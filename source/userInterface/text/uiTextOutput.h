#ifndef UI_TEXT_OUTPUT_H_
#define UI_TEXT_OUTPUT_H_

#include "bunkcore.h"

#include "userInterface/font/uiFontData.h"
#include "userInterface/text/uiTextLine.h"

namespace userInterface
{
	namespace text8
	{
		class output
		{
		public:
			output(v2 lowerLeft, v2 upperRight, std::shared_ptr<userInterface::font::data8> font);
		public:
			void render();

			void addText(str text);
			void addTextLine(str text);
			void setText(str text);
		private:
			std::vector<userInterface::text8::line> lines;

			ui32 lineCount;
			ui32 currentLine;
		};
	}
}

#endif