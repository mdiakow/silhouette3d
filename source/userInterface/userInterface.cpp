#include "coreUserInterface.h"
#include "userInterface.h"
#include "Input/input.h"
#include "render/render.h"

namespace userInterface
{
	userInterface::textFormatting format;

	userInterface::colour onMousePress;
	userInterface::colour onMouseOver;
	userInterface::colour neutral;
	userInterface::border bborder;

	std::shared_ptr<userInterface::font::data8> defaultFont;
	std::shared_ptr<userInterface::input> defaultInput;
	std::shared_ptr<userInterface::style> defaultStyle;

	// coreUserInterface.h start
	void initialize()
	{
		namespace ui = userInterface;
		static const ui32 fontWidth = 8;
		static const ui32 fontHeight = 12;

		format.betweenLines = 8;
		format.margin.top = 4;
		format.margin.left = 4;
		format.margin.right = 4;
		format.margin.bottom = 4;
		format.maxLineWidth = 1000;
		format.fontHeight = fontHeight;
		format.fontWidth = fontWidth;

		v4 bgColour = v4(1.0f, 1.0f, 1.0f, 1.f);
		neutral.background = v4(0.5f, 0.f, 0.f, 0.5f);
		neutral.border = bgColour;
		neutral.font = v4(1.f, 1.f, 1.f, 1.f);
		neutral.foreground = bgColour;

		onMouseOver.background = v4(0.5f, 0.5f, 0.f, 1.f);
		onMouseOver.border = bgColour;
		onMouseOver.font = v4 (1.f, 1.f, 1.f, 1.f);
		onMouseOver.foreground = bgColour;

		onMousePress.background = v4(0.f, 0.5f, 0.5f, 1.f);
		onMousePress.border = bgColour;
		onMousePress.font = v4 (1.f, 1.f, 1.f, 1.f);
		onMousePress.foreground = bgColour;
		
		bborder.top = 2;
		bborder.left = 2;
		bborder.right = 2;
		bborder.bottom = 2;

		v4 fontBack = v4(0.f, 0.f, 0.f, 0.f);
		defaultFont = std::move(userInterface::font::data8::load("source/userInterface/font/defaultFont.txt", fontWidth, fontHeight, neutral.font, fontBack, format));
		auto inputp = std::make_unique<ui::input>();
		inputp->accept = std::make_unique<::input::button>(new ui16{::input::mouse::leftButton}, 1u);
		defaultInput = std::move(inputp);

		auto style = std::make_unique<ui::style>(ui::getDefaultFont(), ui::defaultNeutral(), ui::defaultMouseOver(), ui::defaultMousePress(), ui::defaultBorder());
		defaultStyle = std::move(style);
	}

	void cleanup()
	{
		//delete defaultFont;
	}
	// coreUserInterface.h end

	// userInterface.h start
	std::shared_ptr<userInterface::font::data8> getDefaultFont()
	{
		return defaultFont;
	}

	std::shared_ptr<userInterface::input> getDefaultInput()
	{
		return defaultInput;
	}

	std::shared_ptr<userInterface::style> getDefaultStyle()
	{
		return defaultStyle;
	}

	userInterface::colour defaultMousePress()
	{
		return onMousePress;
	}

	userInterface::colour defaultMouseOver()
	{
		return onMouseOver;
	}

	userInterface::colour defaultNeutral()
	{
		return neutral;
	}
	
	userInterface::border defaultBorder()
	{
		return bborder;
	}

	namespace screen
	{
		v2 topRight()
		{
			return topLeft() + v2(render::getScreenSize()(0),0.f);
		}
		v2 topLeft()
		{
			v2 screen = render::getScreenSize();
			return v2(-screen(0), screen(1)) * 0.5f;
		}
		v2 bottomRight()
		{
			return topRight() + v2(0.f, -render::getScreenSize()(1));
		}
		v2 bottomLeft()
		{
			return topLeft() + v2(0.f, -render::getScreenSize()(1));
		}
	}

	// userInterface.h end
}