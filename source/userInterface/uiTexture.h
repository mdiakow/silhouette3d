#ifndef UI_TEXTURE_H_
#define UI_TEXTURE_H_

#include "bunkcore.h"
#include "d3d.h"

#include <wrl/client.h>
#include <memory>

namespace userInterface
{
	class texture
	{
	public:
		texture();
		texture(ui16 width, ui16 height, v4 colour);
		texture(ui16 width, ui16 height, std::unique_ptr<ui32[]> &textureBuffer);
	public:
		void render();
	public:
		Microsoft::WRL::ComPtr<ID3D11Texture2D> texture2d;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> view;

		
	};
}

#endif