#ifndef UI_FORM_INPUT_H_
#define UI_FORM_INPUT_H_

#include <type_traits>
#include <memory>
#include <iostream>
#include <sstream>
#include "bunkcore.h"

#include "userInterface/text/uiTextInput.h"
#include "userInterface/text/uiTextLine.h"
#include "userInterface/font/uiFontData.h"
#include "userInterface/uiStyle.h"

namespace userInterface
{
	namespace form
	{
		template <typename type>
		class input
		{

		};

		template<>
		class input<fl32>
		{
		public:
			input(str label, v2 lowerLeft, v2 upperRight, userInterface::style *style) :
				textLabel(userInterface::text8::line(label, lowerLeft, upperRight, style->font)),
				textInput(userInterface::text8::input(std::regex("[^0-9.]"), lowerLeft + v2(style->font->getStringWidth(label), 0.f), upperRight, style))
			{
			}
		public:
			void render() {
				textLabel.render();
				textInput.render();
			}
			void update() {
				textInput.update();
			}

			fl32 getValue() {
				std::stringstream strm(textInput.getBuffer());
				fl32 out;
				strm >> out;
				return out;
			}
			
			// Try to set focus to this input. Returns true if it worked.
			bool tryFocus(v2 position) {
				if (textInput.inBounds(position)) {
					::input::text::setFocus(&textInput);
					return true;
				}
				return false;
			}
		public:
			userInterface::text8::input textInput;
			userInterface::text8::line textLabel;
		};

		template<>
		class input<v4>
		{
		public:
			input(str titleText, str xText, str yText, str zText, str wText, v2 lowerLeft, v2 upperRight, userInterface::style *style) :
				textLabel(userInterface::text8::line(titleText, lowerLeft, upperRight, style->font)),
				xLabel(userInterface::text8::line(xText, lowerLeft - v2(0.f, style->font->format.betweenLines + style->font->format.betweenLines), upperRight - v2(0.f, style->font->format.betweenLines + style->font->format.betweenLines), style->font)),
				yLabel(userInterface::text8::line(yText, lowerLeft - v2(0.f, 2*(style->font->format.betweenLines + style->font->format.betweenLines)), upperRight - v2(0.f, 2*(style->font->format.betweenLines + style->font->format.betweenLines)), style->font)),
				zLabel(userInterface::text8::line(zText, lowerLeft - v2(0.f, 3*(style->font->format.betweenLines + style->font->format.betweenLines)), upperRight - v2(0.f, 3*(style->font->format.betweenLines + style->font->format.betweenLines)), style->font)),
				wLabel(userInterface::text8::line(wText, lowerLeft - v2(0.f, 4*(style->font->format.betweenLines + style->font->format.betweenLines)), upperRight - v2(0.f, 4*(style->font->format.betweenLines + style->font->format.betweenLines)), style->font)),
				x(userInterface::text8::input(std::regex("[^0-9.-]"), lowerLeft - v2(0.f, style->font->format.betweenLines + style->font->format.betweenLines) + v2(style->font->getStringWidth(xText), 0.f), upperRight - v2(0.f, style->font->format.betweenLines + style->font->format.betweenLines), style)),
				y(userInterface::text8::input(std::regex("[^0-9.-]"), lowerLeft - v2(0.f, 2*(style->font->format.betweenLines + style->font->format.betweenLines)) + v2(style->font->getStringWidth(yText), 0.f), upperRight - v2(0.f, 2*(style->font->format.betweenLines + style->font->format.betweenLines)), style)),
				z(userInterface::text8::input(std::regex("[^0-9.-]"), lowerLeft - v2(0.f, 3*(style->font->format.betweenLines + style->font->format.betweenLines)) + v2(style->font->getStringWidth(zText), 0.f), upperRight - v2(0.f, 3*(style->font->format.betweenLines + style->font->format.betweenLines)), style)),
				w(userInterface::text8::input(std::regex("[^0-9.-]"), lowerLeft - v2(0.f, 4*(style->font->format.betweenLines + style->font->format.betweenLines)) + v2(style->font->getStringWidth(wText), 0.f), upperRight - v2(0.f, 4*(style->font->format.betweenLines + style->font->format.betweenLines)), style))
			{
			}
		public:
			void render() {
				textLabel.render();
				xLabel.render();
				yLabel.render();
				zLabel.render();
				wLabel.render();
				x.render();
				y.render();
				z.render();
				w.render();
			}

			void update() {
				x.update();
				y.update();
				z.update();
				w.update();
			}

			v4 getValue() {
				std::stringstream sx(x.getBuffer());
				std::stringstream sy(y.getBuffer());
				std::stringstream sz(z.getBuffer());
				std::stringstream sw(w.getBuffer());
				v4 out;
				sx >> out.x();
				sy >> out.y();
				sz >> out.z();
				sw >> out.w();
				return out;
			}

			// Try to set focus to this input. Returns true if it worked.
			bool tryFocus(v2 position) {
				if (x.inBounds(position)) {
					::input::text::setFocus(&x);
					return true;
				} else if (y.inBounds(position)) {
					::input::text::setFocus(&y);
					return true;
				} else if (z.inBounds(position)) {
					::input::text::setFocus(&z);
					return true;
				} else if (w.inBounds(position)) {
					::input::text::setFocus(&w);
					return true;
				}
				return false;
			}
		public:
			// 1 Input for each component of the vector
			userInterface::text8::input x,y,z,w;
			userInterface::text8::line textLabel;
			userInterface::text8::line xLabel, yLabel, zLabel, wLabel;
		};


	}
}



#endif