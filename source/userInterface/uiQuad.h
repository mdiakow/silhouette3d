#ifndef UI_FONT_QUAD_H_
#define UI_FONT_QUAD_H_

#include <wrl/client.h>
#include "d3d.h"
#include "bunkcore.h"

namespace userInterface
{
	struct vertex
	{
		v3 position;
		v2 texture;
	};

	class quad
	{
	public:
		quad();
		// Creates a quad using two points
		quad(v2 lowerLeft, v2 upperRight);

		Microsoft::WRL::ComPtr<ID3D11Buffer> indexBuffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> vertexBuffer;

		void render();
	};	
}

#endif