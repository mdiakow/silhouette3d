#ifndef UI_FONT_CURSOR_H_
#define UI_FONT_CURSOR_H_

#include "bunkcore.h"

#include "userInterface/uiTexture.h"
#include "userInterface/uiQuad.h"

namespace userInterface
{
	namespace font
	{
		class cursor
		{
		public:
			cursor();
			cursor(ui32 width, ui32 height, v4 fontColour, v4 backgroundColour);

		public:
			void render();
		public:
			v2 getDimensions();
			void setPosition(v2 position);
		public:
			userInterface::texture texture;
			userInterface::quad quad;
			v2 dimensions;
		private:
			fl32 currentTime;
			fl32 blinkTime;
		};
	}
}


#endif