#include "userInterface/font/uiFontCursor.h"
#include "colour.h"
#include "systemCore/time/bunktime.h"

namespace userInterface::font
{
	cursor::cursor() :
		texture(),
		quad(),
		dimensions(v2(0.f, 0.f))
	{
	}

	cursor::cursor(ui32 width, ui32 height, v4 fColour, v4 bgColour) :
		texture(),
		quad(),
		dimensions(v2((fl32)width, (fl32)height))
	{
		auto fontColour = ::colour::rgba32(fColour);
		auto textureBuffer = std::make_unique<ui32[]>(width*height);
		std::fill(&textureBuffer[0], &textureBuffer[0] + (width*height), fontColour);

		texture = userInterface::texture(width, height, textureBuffer);

		setPosition(v2::Zero());
		currentTime = 0.f;
		blinkTime = 0.25f;
	}

	void cursor::render()
	{
		currentTime += bunktime::dt();
		if (currentTime > blinkTime*2.f) {
			currentTime -= blinkTime*2.f;
		}
		if (currentTime > blinkTime) {
			texture.render();
			quad.render();
		}
	}

	v2 cursor::getDimensions()
	{
		return dimensions;
	}

	void cursor::setPosition(v2 position)
	{
		v2 lowerLeft = position;
		v2 upperRight = lowerLeft + dimensions;
		quad = userInterface::quad(lowerLeft, upperRight);
	}
}
