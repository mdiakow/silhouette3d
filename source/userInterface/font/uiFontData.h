//////////////////////////////////////////////////////
// This class is mean to hold the data for a single font:
//  1) Vertex/Index buffers quads (per character).
//  2) Texture buffer (per character).
//  3) A list of valid characters.
//////////////////////////////////////////////////////

#ifndef UI_FONT_DATA_H_
#define UI_FONT_DATA_H_

#include "bunkcore.h"
#include "userInterface/uiTexture.h"
#include "userInterface/uiQuad.h"
#include "userInterface/font/uiFontCursor.h"

#include <filesystem>
#include <vector>
#include <memory>

namespace userInterface
{
	struct margin
	{
		ui16 top;
		ui16 bottom;
		ui16 left;
		ui16 right;
	};

	struct textFormatting
	{
		ui32 maxLineWidth; // Max line width.
		ui32 betweenLines;
		ui32 fontHeight;
		ui32 fontWidth;
		userInterface::margin margin;
	};

	namespace font
	{
		// This data class is for 8 bit character rendering.
		class data8
		{
		public:
			data8();
			static std::unique_ptr<data8> load(std::filesystem::path path, ui32 width, ui32 height, v4 fontColour, v4 backgroundColour, userInterface::textFormatting format);

			userInterface::texture getTexture(ui8 character);
			v2 getDimension(ui8 character);

			fl32 getStringWidth(str string);
			userInterface::textFormatting format;

			userInterface::font::cursor cursor;
		private:
			std::vector<userInterface::texture> characterTextures;
			std::vector<v2> characterDimensions;

			//std::unique_ptr<userInterface::font::cursor> cursor;
			
		};
	}

	// Future reference for myself in case I want to do 16 bit text rendering:
	//  Use a map/unordered_map instead of the pair array. This would probably make all font rendering a bit slower
	//  So maybe it's best to just use different data8s for different languages/characters? I am not sure at this point.
}

#endif