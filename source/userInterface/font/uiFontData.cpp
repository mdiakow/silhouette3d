#include "userInterface/font/uiFontData.h"

#include <wrl/client.h>
#include <fstream>

#include "math/linalgbezier.h"
#include "render/shader/shader.h"

#include "error/errorLog.h"

#include "colour.h"


#include "Console/console.h"
#include "Debugger/bunkdebug.h"

namespace internal
{
	// Draws a spline on a textureBuffer
	void drawSpline(linalg::bezier<v2> &spline, std::unique_ptr<ui32[]> &textureBuffer);
	// Draws a line on a textureBuffer.
	void drawLine(v2 start, v2 end, std::unique_ptr<ui32[]> &textureBuffer, fl32 textureWidth, ui32 fontColour);
}

using namespace Microsoft::WRL;
namespace userInterface::font
{
	static const ui8 startChar = ' ';
	static const ui8 endChar = '~';

	data8::data8() :
		characterTextures(),
		characterDimensions()
	{
	}

	std::unique_ptr<data8> data8::load(std::filesystem::path path, ui32 width, ui32 height, v4 fColour, v4 bgColour, userInterface::textFormatting format)
	{
		auto data = std::make_unique<data8>();
		
		data->format = format;

		auto fontColour = ::colour::rgba32(fColour);
		auto backgroundColour = ::colour::rgba32(bgColour);
		auto textureBuffer = std::make_unique<ui32[]>(width*height);
		std::fill(&textureBuffer[0], &textureBuffer[0] + (width*height), backgroundColour);
		
		//ui32 arrSize = endChar - startChar + 1;

		//data->characters.reserve(arrSize);
		ui32 vectorSize = 256;
		data->characterTextures.resize(vectorSize);
		data->characterDimensions.resize(vectorSize);
		
		std::ifstream file(path);
		if (file.fail()) {
			error::log("Failed to load UI Font Data from path: " + path.string() + "\n");
		}		

		str line;
		linalg::bezier<v2> curve[24];

		// For each character:
		//   Read in the bezier curves from the file
		//   For each bezier curve:
		//       Output a rounded solution using the splines on to a Texture2D.
		//   NOT DONE: Apply gaussian blur to smooth it.
		//   Place finished texture in the array for future rendering (I think I will write a font shader so I can
		//   Dynamically change colour)
		while (std::getline(file, line)) {
			std::istringstream iss(line);
			ui8 character;
			iss >> character;
			ui32 curveCount = 0;

			std::getline(file, line); // skip opening brace

			std::getline(file, line);
			while (line != "}") {
				// purge brackets from current line
				line.erase(std::remove(line.begin(), line.end(), '('), line.end()); 
				line.erase(std::remove(line.begin(), line.end(), ')'), line.end()); 

				std::istringstream stream(line);

				// reads the line and inserts the bezier control points
				stream >> curve[curveCount].start.x(); 
				stream >> curve[curveCount].start.y();

				stream >> curve[curveCount].startControl.x();
				stream >> curve[curveCount].startControl.y();

				stream >> curve[curveCount].endControl.x();
				stream >> curve[curveCount].endControl.y(); 

				stream >> curve[curveCount].end.x(); 
				stream >> curve[curveCount].end.y();

				std::getline(file, line); // Get next spline.

				curveCount++;
			}

			fl32 resolution = 256.f; // how many iterations to do.
			fl32 stepSize = 1.f/resolution;

			v2 currentRaw, rounded;

			fl32 flWidth = static_cast<fl32>(width);
			fl32 flheight = static_cast<fl32>(height);
			v2 scale = v2(flWidth, flheight);
			for (ui32 i = 0; i < curveCount; i++) {
				// Scale the splines up to the current texture's width and height.
				curve[i].start = curve[i].start.cwiseProduct(scale);
				curve[i].startControl = curve[i].startControl.cwiseProduct(scale);
				curve[i].endControl = curve[i].endControl.cwiseProduct(scale);
				curve[i].end = curve[i].end.cwiseProduct(scale);
				// Increment t to calculate the values of the spline
				for (fl32 t = stepSize; t <= 1.f; t += stepSize) {
					currentRaw = curve[i].calculate(t);

					// Round to get a proper texture coordinate.
					rounded = v2(roundf(currentRaw.x()), roundf(currentRaw.y()));
					ui32 index = static_cast<ui32>(rounded.y() * flWidth + rounded.x());
					textureBuffer[index] = fontColour;
				}
			}
			
			data->characterTextures[character] = userInterface::texture(width, height, textureBuffer);

			// Reset the texture for the next character
			std::fill(&textureBuffer[0], &textureBuffer[0] + (width*height), backgroundColour);
		}

		data->characterTextures[' '] = userInterface::texture(width, height, textureBuffer);

		fl32 flWidth = static_cast<fl32>(width);
		fl32 flHeight = static_cast<fl32>(height);
		for (ui32 i = 0; i < vectorSize; i++) {
			data->characterDimensions[i] = v2(flWidth, flHeight);
		}

		//data->cursor = std::make_unique<userInterface::font::cursor>(width, height, fColour, bgColour);
		data->cursor = userInterface::font::cursor(width, height, fColour, bgColour);

		return data;
	}
	
	userInterface::texture data8::getTexture(ui8 character)
	{
		if (characterTextures[character].texture2d) {
			return characterTextures[character];
		} else {
			// This is a fallback to prevent trying to render null textures/views
			return characterTextures[' '];
		}
	}
	v2 data8::getDimension(ui8 character)
	{
		if (characterTextures[character].texture2d) {
			return characterDimensions[character];
		} else {
			// This is a fallback to prevent trying to render null textures/views
			return characterDimensions[' '];
		}
	}
	fl32 data8::getStringWidth(str string)
	{
		fl32 stringWidth = 0.f;
		for (ui32 i = 0; i < (ui32)string.length(); i++) {
			stringWidth += getDimension(string[i]).x();
		}
		return stringWidth;
	}
}

namespace internal
{
	// Draws a spline on a textureBuffer
	void drawSpline(linalg::bezier<v2> &spline, std::unique_ptr<ui32[]> &textureBuffer, fl32 stepSize)
	{

	}

	// Draws a line on a textureBuffer. (I realized I forgot a bunch of cases and will fix this in a bit)
	void drawLine(v2 start, v2 end, std::unique_ptr<ui32[]> &textureBuffer, fl32 textureWidth, ui32 fontColour)
	{
		v2 diff = end - start;
		fl32 m = diff.y()/diff.x();
		if (diff.x() > diff.y()) {
			fl32 currentY = start.y();
			for (fl32 x = roundf(start.x()); x < roundf(end.x()); x += 1.f) {
				ui32 index = static_cast<ui32>(x + textureWidth * currentY);
				textureBuffer[index] = fontColour;
				
				currentY += m;
			}
		} else {
			fl32 currentX = start.x();
			m = 1.f/m;
			for (fl32 y = roundf(start.y()); y < roundf(end.y()); y += 1.f) {
				ui32 index = static_cast<ui32>(currentX + textureWidth * y);
				textureBuffer[index] = fontColour;

				currentX += m; 
			}
		}
	}
}
