#ifndef UI_STYLE_H_
#define UI_STYLE_H_

#include <memory>

#include "bunkcore.h"
#include "userInterface/font/uiFontData.h"

namespace userInterface
{
	// This is in pixel size.
	struct border
	{
		ui16 top;
		ui16 bottom;
		ui16 left;
		ui16 right;
	};

	struct colour
	{
		v4 background; // Behind text, etc.
		v4 foreground; // The colour of buttons, tabs and other things you can interact with

		v4 border; 
		v4 font;
	};
	
	struct style
	{
		style(std::shared_ptr<userInterface::font::data8> font,
			userInterface::colour neutral,
			userInterface::colour onMouseOver,
			userInterface::colour onPress,
			userInterface::border border
		);

		std::shared_ptr<userInterface::font::data8> font;

		userInterface::colour neutral; // The colours when something is not being interacted with.
		userInterface::colour onMouseOver;
		userInterface::colour onPress;

		userInterface::border border;
	};
}

#endif