#include "userInterface/uiBoundingBox.h"

namespace userInterface
{
	boundingBox::boundingBox(v2 lowerLeft, v2 upperRight) :
		lowerLeft(lowerLeft),
		upperRight(upperRight)
	{
		//static_assert(lowerLeft.x() != )
	}
	bool boundingBox::isInside(v2 position)
	{
		return position.x() >= lowerLeft.x() && position.x() <= upperRight.x() 
			&& position.y() >= lowerLeft.y() && position.y() <= upperRight.y();
	}
	v2 boundingBox::getLowerLeft()
	{
		return lowerLeft;
	}
	v2 boundingBox::getUpperRight()
	{
		return upperRight;
	}
	v2 boundingBox::getUpperLeft()
	{
		return v2(lowerLeft.x(), upperRight.y());
	}
	v2 boundingBox::getLowerRight()
	{
		return v2(upperRight.x(), lowerLeft.y());
	}
	/*
	void boundingBox::setBounds(v2 lowerLeft, v2 upperRight)
	{
		this->lowerLeft = lowerLeft;
		this->upperRight = upperRight;
	}*/
}