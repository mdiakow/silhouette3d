#ifndef UI_BOUNDING_BOX_H_
#define UI_BOUNDING_BOX_H_

#include "bunkcore.h"

namespace userInterface
{
	class boundingBox
	{
	public:
		boundingBox(v2 lowerLeft, v2 upperRight);

		// Tests if position is inside the bounding box.
		bool isInside(v2 position);

		v2 getLowerLeft();
		v2 getUpperRight();
		v2 getUpperLeft();
		v2 getLowerRight();
	private:
		v2 lowerLeft, upperRight;
	};
}


#endif