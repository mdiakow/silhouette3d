#include "uiQuad.h"
#include <memory.h>

namespace userInterface
{
	quad::quad() :
		vertexBuffer(),
		indexBuffer()
	{
	}

	quad::quad(v2 lowerLeft, v2 upperRight) :
		vertexBuffer(),
		indexBuffer()
	{
		if (lowerLeft.x() ==  upperRight.x() && lowerLeft.y() == upperRight.y()) {
			throw std::invalid_argument(std::string(__FILE__) + ": Can't create a quad with 2 equal points.");
		} else {
			// Cpu side set up of vertex and index data.
			v3 lowLeft = v3(lowerLeft.x(), lowerLeft.y(), 0.f);
			v3 topLeft = v3(lowerLeft.x(), upperRight.y(), 0.f);
			v3 bottomRight = v3(upperRight.x(), lowerLeft.y(), 0.f);
			v3 topRight = v3(upperRight.x(), upperRight.y(), 0.f);

			vertex vertices[4];

			vertices[0].position = lowLeft;
			vertices[0].texture = v2(0.0f, 0.0f);

			vertices[1].position = topLeft;
			vertices[1].texture = v2(0.0f, 1.0f);

			vertices[2].position = bottomRight;
			vertices[2].texture = v2(1.0f, 0.0f);

			vertices[3].position = topRight;
			vertices[3].texture = v2(1.0f, 1.0f);

			ui32 indices[6];
			indices[0] = 0;
			indices[1] = 1;
			indices[2] = 2;

			indices[3] = 3;
			indices[4] = 2;
			indices[5] = 1;

			// GPU side set up of vertex and index data.

			D3D11_BUFFER_DESC vbDesc;
			vbDesc.Usage = D3D11_USAGE_DYNAMIC;
			vbDesc.ByteWidth = sizeof(vertex) * 4;
			vbDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
			vbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			vbDesc.MiscFlags = 0;
			vbDesc.StructureByteStride = 0;

			D3D11_SUBRESOURCE_DATA vData;
			vData.pSysMem = vertices;
			vData.SysMemPitch = 0;
			vData.SysMemSlicePitch = 0;

			d3d::getDevice()->CreateBuffer(&vbDesc, &vData, &vertexBuffer);

			D3D11_BUFFER_DESC ibDesc;
			ibDesc.Usage = D3D11_USAGE_DEFAULT;
			ibDesc.ByteWidth = sizeof(ui32) * 6;
			ibDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
			ibDesc.CPUAccessFlags = 0;
			ibDesc.MiscFlags = 0;
			ibDesc.StructureByteStride = 0;

			D3D11_SUBRESOURCE_DATA iData;
			iData.pSysMem = indices;
			iData.SysMemPitch = 0;
			iData.SysMemSlicePitch = 0;

			d3d::getDevice()->CreateBuffer(&ibDesc, &iData, &indexBuffer);
		}		
	}

	void quad::render()
	{
		auto context = d3d::getDeviceContext();

		ui32 stride = sizeof(userInterface::vertex);
		ui32 offset = 0;

		context->IASetVertexBuffers(0, 1, vertexBuffer.GetAddressOf(), &stride, &offset);
		context->IASetIndexBuffer(indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		context->DrawIndexed(6, 0, 0);
	}
}
