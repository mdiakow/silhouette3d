#ifndef USER_INTERFACE_H_
#define USER_INTERFACE_H_

#include "bunkcore.h"
#include "math/linalg.h"

//#include "uiFont.h"
#include "userInterface/font/uiFontData.h"
#include "userInterface/uiStyle.h"
#include "uiBox.h"

#include "userInterface/text/uiTextLine.h"
#include "userInterface/text/uiTextInput.h"

#include "userInterface/uiInput.h"
#include "userInterface/uiStyle.h"
namespace userInterface
{
	std::shared_ptr<userInterface::font::data8> getDefaultFont();
	std::shared_ptr<userInterface::input> getDefaultInput();
	std::shared_ptr<userInterface::style> getDefaultStyle();

	userInterface::colour defaultMousePress();
	userInterface::colour defaultMouseOver();
	userInterface::colour defaultNeutral();
	userInterface::border defaultBorder();

	namespace screen
	{
		v2 topRight();
		v2 topLeft();
		v2 bottomRight();
		v2 bottomLeft();
	}
}

#endif