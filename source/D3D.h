#pragma once

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

#include <d3d11.h>
#include <directxmath.h>

#include "bunkcore.h"

using namespace DirectX;


namespace d3d
{
	void beginScene();
	void endScene();

	void enableZBuffer();
	void disableZBuffer();

	void enableAlphaBlending();
	void disableAlphaBlending();
	void setParticleBlending();

	ID3D11Device* getDevice();
	ID3D11DeviceContext* getDeviceContext();

	void getVideoCardInfo(char *cardName, int &memory);

	template<class T>
	inline void release(T **toRelease) 
	{
		if ((*toRelease != nullptr)) {
			(*toRelease)->Release();
			(*toRelease) = nullptr;
		}
	}

	void initialize(ui32 screenWidth, ui32 screenHeight, bool vsync, HWND hwnd, bool fullscreen, fl32 screenDepth, fl32 screenNear);
	void cleanup();
}