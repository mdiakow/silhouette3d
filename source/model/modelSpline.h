#ifndef MODEL_SPLINE_H_
#define MODEL_SPLINE_H_

#include <d3d11.h>

#include "bunkcore.h"
//#include "linalg.h"
#include "mesh/meshSpline.h"

namespace model
{
	class spline
	{
	public:
		spline(::mesh::spline *mesh);
		~spline();

		

		void render();

	private:
		ID3D11Buffer *vertexBuffer, *indexBuffer;
		ui32 vertexCount, indexCount;

	};
}

#endif