#ifndef MODEL_QUAD_H_
#define MODEL_QUAD_H_

#include "d3d.h"
#include "bunkcore.h"

namespace model
{
	//template<type T>
	template <class vertex>
	class quad
	{
		quad(v2 lowerLeft, v2 upperRight);
	private:
		Microsoft::WRL::ComPtr<ID3D11Buffer> indexBuffer;
		Microsoft::WRL::ComPtr<ID3D11Buffer> vertexBuffer;
	};
}


#endif