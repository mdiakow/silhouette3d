#include "modelSpline.h"

#include "render/render.h"

model::spline::spline(::mesh::spline *mesh)
{
	this->vertexCount = mesh->vertexCount();
	this->indexCount = mesh->indexCount();

	HRESULT result;

	D3D11_BUFFER_DESC vbDesc;
	vbDesc.Usage = D3D11_USAGE_DEFAULT;
	vbDesc.ByteWidth = sizeof(vertex::spline) * vertexCount;
	vbDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbDesc.CPUAccessFlags = 0;
	vbDesc.MiscFlags = 0;
	vbDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vData;
	vData.pSysMem = mesh->getVertices();
	vData.SysMemPitch = 0;
	vData.SysMemSlicePitch = 0;

	result = d3d::getDevice()->CreateBuffer(&vbDesc, &vData, &vertexBuffer);
	if (FAILED(result)){
		// TODO: error check.
	}

	D3D11_BUFFER_DESC ibDesc;	
	ibDesc.Usage = D3D11_USAGE_DEFAULT;
	ibDesc.ByteWidth = sizeof(ui32) * indexCount;
	ibDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibDesc.CPUAccessFlags = 0;
	ibDesc.MiscFlags = 0;
	ibDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA iData;
	iData.pSysMem = mesh->getIndices();
	iData.SysMemPitch = 0;
	iData.SysMemSlicePitch = 0;

	result = d3d::getDevice()->CreateBuffer(&ibDesc, &iData, &indexBuffer);
	if (FAILED(result)){
		// TODO: error check.
	}


}

model::spline::~spline()
{
	if (indexBuffer) {
		indexBuffer->Release();
		indexBuffer = nullptr;
	}

	if (vertexBuffer) {
		vertexBuffer->Release();
		vertexBuffer = nullptr;
	}
}

void model::spline::render()
{
	

	ui32 stride = sizeof(vertex::spline);
	ui32 offset = 0;

	auto context = d3d::getDeviceContext();
	
	context->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP);

	
	//shader::colour::render(indexCount, render::getWorld(), render::getView(), render::getProjection());	
}
