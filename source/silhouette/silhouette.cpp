#include "silhouette.h"

#include "imageProcessing/imageProcessing.h"
#include "colour.h"

#include "console/console.h"
//
//std::tuple<mesh::line<vertex::light>*, ui32> silhouette::getOutline(canvas::data *cnvs)
//{
//	auto context = d3d::getDeviceContext();
//	auto device = d3d::getDevice();
//	HRESULT result;
//
//	// ISOLATE COLOUR
//	// Create an additional texture of same size as a tile for compute shader input
//	D3D11_TEXTURE2D_DESC textureDesc;
//	cnvs->tiles[0].texture2d->GetDesc(&textureDesc);
//
//	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
//	cnvs->tiles[0].textureView->GetDesc(&srvDesc);
//
//	ID3D11Texture2D *inputTexture;
//	result = device->CreateTexture2D(&textureDesc, nullptr, &inputTexture);
//
//	ID3D11ShaderResourceView *inputView;
//	result = device->CreateShaderResourceView(inputTexture, &srvDesc, &inputView);
//
//	cbuffer::isolateColour isoCbuff;
//
//	isoCbuff.targetColour = v4(0.0f, 1.0f, 0.0f, 1.0f);
//	context->CopyResource(inputTexture, cnvs->tiles[0].texture2d);
//
//	// 1. Convert to a binary image, anything that matches target is set to white.
//	imageProcessing::isolateColour(inputView, cnvs->outputView, isoCbuff);
//
//	context->CopyResource(inputTexture, cnvs->outputTexture);
//	
//	// 2. Find the lookup values from the marching square.
//	imageProcessing::marchingSquares::cellIndex(inputView, cnvs->outputView);
//
//	context->CopyResource(inputTexture, cnvs->outputTexture);
//	context->CopyResource(cnvs->tiles[0].texture2d, cnvs->outputTexture);
//
//	// 3. Build cubic splines from above lookup values.
//	// NOTES FOR TOMORROW NIGHT:
//	// I think I can take the above values, ride the outside edge of a square composed of lookup values.
//	// Above step is similar to finding the lookup values. If I find a non zero value,
//	// I can use the lookup table to determine the next part of the edge and keep doing this until I hit another
//	// edge of the square. These will give the start and end of a cubic spline. Traversal of the edge would give me some
//	// way to calculate control points, depending on square size I could use middle points as guesses. And if 
//	// I want the splines to be continuous between neighbouring squares I could also use an average of the neighbours
//	// flipped control points.
//
//
//
//	d3d::release(&inputTexture);
//	d3d::release(&inputView);
//
//	
//	//return std::tuple<mesh::line<vertex::light>*, ui32>();
//	return std::make_tuple(nullptr, 0);
//}

/******
// OLD MEANSHIFT CODE. I don't really need it anymore
cbuffer::meanShift meanCbuff;
meanCbuff.center = v2(0.f, 0.f);
meanCbuff.windowOffset = v2(1.f, 1.f);

struct meanShiftOutput
{
v2 position;
fl32 padding[2];
};

D3D11_BUFFER_DESC msobd;
msobd.Usage = D3D11_USAGE_DEFAULT;
msobd.ByteWidth = sizeof(meanShiftOutput);
msobd.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
msobd.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
msobd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
msobd.StructureByteStride = sizeof(meanShiftOutput);

ID3D11Buffer *meanShiftOutputBuffer;
result = device->CreateBuffer(&msobd, nullptr, &meanShiftOutputBuffer);

D3D11_UNORDERED_ACCESS_VIEW_DESC msuavd;
msuavd.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
msuavd.Format = DXGI_FORMAT_UNKNOWN;
msuavd.Buffer.FirstElement = 0;
msuavd.Buffer.NumElements = 1;
msuavd.Buffer.Flags = 0;

ID3D11UnorderedAccessView *meanShiftOutputView;
result = device->CreateUnorderedAccessView(meanShiftOutputBuffer, &msuavd, &meanShiftOutputView);

imageProcessing::meanShift(inputView, meanShiftOutputView, meanCbuff);

meanShiftOutput cpuOutput;

D3D11_MAPPED_SUBRESOURCE subresource;
result = context->Map(meanShiftOutputBuffer, 0, D3D11_MAP_READ, 0, &subresource);

std::memcpy(&cpuOutput, subresource.pData, sizeof(meanShiftOutput));		

context->Unmap(meanShiftOutputBuffer, 0);

console::log(cpuOutput.position);

d3d::release(&meanShiftOutputBuffer);
d3d::release(&meanShiftOutputView);
*/


/*
// APPLY EDGE TO ENTIRE IMAGE
auto kernel = imageProcessing::kernel::edge3x3();

kernel->apply(inputView, cnvs->outputView);

context->CopyResource(cnvs->tiles[0].texture2d, cnvs->outputTexture);
*/