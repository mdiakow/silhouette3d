#ifndef BUNKCORE_H_
#define BUNKCORE_H_

#include <cstdint>
#include <string>
#include <iostream>

#include <Eigen/Core>
#include <Eigen/Dense>

//#include "Math/mathDualQuat.h"

// Signed Integers
using si8 = int8_t;
using si16 = int16_t;
using si32 = int32_t;
using si64 = int64_t;

// Unsigned Integers
using ui8 = uint8_t;
using ui16 = uint16_t;
using ui32 = uint32_t;
using ui64 = uint64_t;

// Floating Point
using fl32 = float;
using fl64 = double;

// String
using str = std::string;

// 4 x 4 matrix
using m4 = Eigen::Matrix4f;

// 3 x 3 matrix
using m3 = Eigen::Matrix3f;

// aliases for eigen vectors
using v4 = Eigen::Vector4f;
using v3 = Eigen::Vector3f;
using v2 = Eigen::Vector2f;

// quaternion
using quat = Eigen::Quaternionf;
//using dq = math::dualQuaternion;

#endif