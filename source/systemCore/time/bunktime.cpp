#include "bunktime.h"
#include <ctime>
#include <chrono>
namespace bunktime
{
	static std::chrono::time_point<std::chrono::steady_clock> currentTime;
	static std::chrono::time_point<std::chrono::steady_clock> previousTime;

	static fl32 deltaTime;
	static fl32 totalTime;
	

	fl32 dt()
	{
		return deltaTime;
	}
	// Didn't want to break previous functionality for now.
	fl32 delta()
	{
		return deltaTime;
	}

	fl32 total()
	{
		return totalTime;
	}

	void updateCurrent()
	{
		currentTime = std::chrono::high_resolution_clock::now();
	}

	void updatePrevious()
	{
		std::chrono::duration<fl32> diff = currentTime - previousTime;
		deltaTime = diff.count();
		totalTime += deltaTime;
		previousTime = currentTime;
	}

	void initialize()
	{
		currentTime = std::chrono::high_resolution_clock::now();
		previousTime = std::chrono::high_resolution_clock::now();

		deltaTime = 0.f;
		totalTime = 0.f;
	}

	void cleanup()
	{

	}
}
