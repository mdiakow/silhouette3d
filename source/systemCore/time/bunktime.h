#ifndef BUNKTIME_H_
#define BUNKTIME_H_

#include "bunkcore.h"

namespace bunktime
{
	fl32 dt();
	// Returns time to render frame.
	fl32 delta();
	// Returns total time since start.
	fl32 total();

	void updateCurrent();
	void updatePrevious();

	void initialize();
	void cleanup();

}

#endif