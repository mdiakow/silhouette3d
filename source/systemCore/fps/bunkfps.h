#ifndef BUNK_FPS_H_
#define BUNK_FPS_H_

#include "bunkcore.h"

namespace bunkfps
{
	// returns the number of frames drawn in a second
	ui32 fps();

	void initialize();
	void cleanup();

	void update();
}

#endif