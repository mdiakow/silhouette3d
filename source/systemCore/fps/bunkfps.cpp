#include "systemCore/fps/bunkfps.h"
#include "systemCore/time/bunktime.h"
#include "debugger/bunkdebug.h"

#include "userInterface/userInterface.h"

#include "render/render.h"
#include "update/update.h"
#include "console/console.h"

namespace bunkfps
{
	void render();

	void showFps();
	void hideFps();

	static fl32 timeCount;
	static ui32 frameCount;

	static ui32 currentFrameRate;

	namespace ui = userInterface;
	ui::text8::line *fpsDisplay;
	
	bool isShown;
	// returns the number of frames drawn in a second
	ui32 fps()
	{
		return currentFrameRate;
	}

	void initialize()
	{
		timeCount = 0.f;
		frameCount = 0;
		currentFrameRate = 0;

		//ui::font *font = ui::getDefaultFont();

		v2 position = ui::screen::bottomLeft();
		v2 end = position + v2(20.f, 20.f);
		fpsDisplay = new ui::text8::line("Hold Up!", position, end, ui::getDefaultFont());

		isShown = true;

		render::findShader("ui")->add(render);
		update::add(update);

		console::add("show_fps 1", showFps);
		console::add("show_fps 0", hideFps);
	}

	void cleanup()
	{
		delete fpsDisplay;
	}

	void update()
	{
		frameCount++;
		timeCount += bunktime::dt();
		
		if (timeCount > 1.0f) {
			currentFrameRate = frameCount;

			fpsDisplay->setText(std::to_string(currentFrameRate));

			timeCount = 0.f;
			frameCount = 0;
		}

	}

	void render()
	{
		if (isShown) {
			fpsDisplay->render();
		}
	}

	void showFps()
	{
		isShown = true;
	}

	void hideFps()
	{
		isShown = false;
	}
}
