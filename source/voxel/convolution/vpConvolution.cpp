#include "voxel/convolution/vpConvolution.h"

#include <math.h>
#include <memory>
#include <filesystem>

#include "render/shader/common/shaderHelper.h"

#include "buffer/constant/bufferConstant.h"
#include "voxel/convolution/vpConvolutionTables.h"

namespace helper
{
	// Sets the view grid for a panel.
	static void viewStandard(si32 x, si32 y, si32 z, const voxel::panel &panel, std::vector<buffer::texture::input3d> &inputVoxels, ID3D11ShaderResourceView *views[27]);
	// Calculates which views will be used inside the separable apply function.
	static void viewSeparable(si32 x, si32 y, si32 z, ui32 viewOffset, const voxel::panel &panel, std::vector<buffer::texture::input3d> &inputVoxels, ID3D11ShaderResourceView *views[3]);

	namespace vcm = voxel::convolution::mask;
	static void applySeparable(std::vector<buffer::texture::input3d> &inputVoxels, const voxel::panel &panel, const vcm::separable &mask);
	static void applyStandard(std::vector<buffer::texture::input3d> &inputVoxels, const voxel::panel &panel, const vcm::standard &mask);
}

namespace fp
{
	static std::filesystem::path sepX = "source/voxel/convolution/vpConvSepX.cms";
	static std::filesystem::path sepY = "source/voxel/convolution/vpConvSepY.cms";
	static std::filesystem::path sepZ = "source/voxel/convolution/vpConvSepZ.cms";
	static std::filesystem::path full = "source/voxel/convolution/vpConvFull.cms";
}

namespace voxel
{
	namespace convolution
	{
		namespace mask
		{
			// takes dimension argument and picks the corresponding buffers, picks the zero buffer for anything else.
			static std::tuple<fl32*, fl32*, fl32*> bufferPicker(dimensions dimension, fl32 *bx, fl32 *by, fl32 *bz, fl32 *zero);

			///////////////////////
			// SEPARABLE KERNEL
			///////////////////////
			separable::separable(ui32 dimension, fl32 x[], fl32 y[], fl32 z[]) :
				x(buffer::texture::input1d(dimension, x)),
				y(buffer::texture::input1d(dimension, y)),
				z(buffer::texture::input1d(dimension, z))
			{
			}

			separable separable::gaussian(fl32 sigma, ui32 dimension)
			{
				if(dimension % 2 == 0) dimension += 1;

				fl32 constant = 1.f/(sqrtf(2.f*math::pi)*sigma);
				auto krnl = std::make_unique<fl32[]>(dimension);
				fl32 out;
				fl32 sum = 0.f;
				ui32 mid = (dimension/2);
				for (ui32 i = 1; i < mid+1; i++) {
					out = constant*expf(-((fl32)(i*i)/(2.f*sigma*sigma)));
					krnl[mid + i] = out;
					krnl[mid - i] = out;
					sum += out + out;
				}
				out = constant;
				sum += out;
				krnl[mid] = out;
				// normalization
				for (ui32 i = 0; i < dimension; i++) {
					krnl[i] /= sum;
				}

				return separable(dimension, krnl.get(), krnl.get(), krnl.get());
			}

			separable separable::average(ui32 dimension)
			{
				if(dimension % 2 == 0) dimension += 1;

				fl32 average = 1.f/((fl32)dimension);

				auto krnl = std::make_unique<fl32[]>(dimension);
				std::fill(&krnl[0], &krnl[0] + dimension, average);

				return separable(dimension, krnl.get(), krnl.get(), krnl.get());
			}
		
			standard::standard(ui16 width, ui16 height, ui16 depth, fl32 buffer[]) :
				kernel( buffer::texture::input3d(width, height, depth, buffer) )
			{
			}

			standard standard::laplacian(ui32 dimension)
			{
				if(dimension % 2 == 0) dimension += 1;

				ui32 total = dimension*dimension*dimension;

				fl32 factor = -1.f/(fl32)(total-1);
				auto krnl = std::make_unique<fl32[]>(total);
				std::fill(&krnl[0], &krnl[0] + (total), factor);

				ui16 hd = dimension/2;
				ui16 mid = hd + dimension*(hd + dimension*hd);
				krnl[mid] = 1.f;

				return standard(dimension, dimension, dimension, krnl.get());
			}

			standard standard::laplacian(dimensions dimension, ui32 width)
			{
				ui32 d3 = width*width*width;
				ui32 d2 = width*width;
				ui32 d1 = width;

				ui32 hw = width/2;

				ui32 m3 = hw + width*(hw + width*hw);
				ui32 m2 = hw + width*hw;
				ui32 m1 = hw;

				fl32 f3 = -1.f/(fl32)(d3-1);
				fl32 f2 = -1.f/(fl32)(d2-1);
				fl32 f1 = -1.f/(fl32)(d1-1);				
				
				auto mk3 = std::make_unique<fl32[]>(d3);
				auto mk2 = std::make_unique<fl32[]>(d2);
				auto mk1 = std::make_unique<fl32[]>(d1);
				
				std::fill(&mk3[0], &mk3[0] + (d3), f3);
				std::fill(&mk2[0], &mk2[0] + (d2), f2);
				std::fill(&mk1[0], &mk1[0] + (d1), f1);

				mk3[m3] = 1.f;
				mk2[m2] = 1.f;
				mk1[m1] = 1.f;
				
				switch (dimension) {
				case dimensions::x: return standard(width, 1, 1, mk1.get());
				case dimensions::y: return standard(1, width, 1, mk1.get());
				case dimensions::z: return standard(1, 1, width, mk1.get());
				case dimensions::xy: return standard(width, width, 1, mk2.get());
				case dimensions::xz: return standard(width, 1, width, mk2.get());
				case dimensions::yz: return standard(1, width, width, mk2.get());
				case dimensions::xyz: return standard(width, width, width, mk3.get());
				default: return standard(width, width, width, mk3.get());
				}
			}

			standard standard::laplacianXZ(ui32 dimension)
			{
				if(dimension % 2 == 0) dimension += 1;

				ui32 total = dimension*dimension;

				fl32 factor = -1.f/(fl32)(total-1);
				auto krnl = std::make_unique<fl32[]>(total);
				std::fill(&krnl[0], &krnl[0] + (total), factor);

				ui16 hd = dimension/2;
				ui16 mid = hd + dimension*hd;
				krnl[mid] = 1.f;


				return standard(dimension, 1, dimension, krnl.get());
			}
		}

		struct convolutionConstant
		{
			si32 voxelDimensions[3];
			float padding;
		};

		void apply(voxel::panel & panel, const mask::separable & separable)
		{
			helper::applySeparable(panel.voxels, panel, separable);
		}

		std::vector<buffer::texture::input3d> calculate(const voxel::panel &panel, const mask::separable &separable)
		{
			auto voxelsInput = panel.inputVoxels();

			helper::applySeparable(voxelsInput, panel, separable);

			return voxelsInput;
		}

		void apply(voxel::panel & panel, const mask::standard & standard)
		{
			helper::applyStandard(panel.voxels, panel, standard);
		}

		std::vector<buffer::texture::input3d> calculate(const voxel::panel &panel, const mask::standard &mask)
		{
			auto voxelsInput = panel.inputVoxels();

			helper::applyStandard(voxelsInput, panel, mask);

			return voxelsInput;
		}
	}
}

namespace helper
{
	void viewStandard(si32 x, si32 y, si32 z, const voxel::panel &panel, std::vector<buffer::texture::input3d> &inputVoxels, ID3D11ShaderResourceView *views[27])
	{
		auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();

		si32 currentX;
		si32 currentY;
		si32 currentZ;

		for (ui32 i = 0; i < table::fullAccessHeight; i++) {
			currentX = x + table::fullAccess[i][0];
			currentY = y + table::fullAccess[i][1];
			currentZ = z + table::fullAccess[i][2];

			bool isValidX = currentX >= 0 && currentX < panelWidth;
			bool isValidY = currentY >= 0 && currentY < panelHeight;
			bool isValidZ = currentZ >= 0 && currentZ < panelDepth;

			if (isValidX && isValidY && isValidZ) {
				views[i] = inputVoxels[panel.flatIndex(currentX,currentY,currentZ)].view.Get();
			} else {
				views[i] = nullptr;
			}			
		}
	}

	static void viewSeparable(si32 x, si32 y, si32 z, ui32 tableOffset, const voxel::panel &panel, std::vector<buffer::texture::input3d> &inputVoxels, ID3D11ShaderResourceView *views[3])
	{
		auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();
			   
		si32 currentX;
		si32 currentY;
		si32 currentZ;

		for (ui32 i = 0; i < 3; i++) {
			currentX = x + table::separable[tableOffset+i][0];
			currentY = y + table::separable[tableOffset+i][1];
			currentZ = z + table::separable[tableOffset+i][2];

			bool isValidX = currentX >= 0 && currentX < panelWidth;
			bool isValidY = currentY >= 0 && currentY < panelHeight;
			bool isValidZ = currentZ >= 0 && currentZ < panelDepth;

			if (isValidX && isValidY && isValidZ) {
				views[i] = inputVoxels[panel.flatIndex(currentX,currentY,currentZ)].view.Get();
			} else {
				views[i] = nullptr;
			}
		}
		
	}

	static void applySeparable(std::vector<buffer::texture::input3d> &inputVoxels, const voxel::panel &panel, const vcm::separable &separable)
	{
		auto context = d3d::getDeviceContext();

		static const ui32 viewSize = 3;
		Microsoft::WRL::ComPtr<ID3D11ComputeShader> shaders[viewSize];

		shaders[0] = shader::compute::find(fp::sepX);
		shaders[1] = shader::compute::find(fp::sepY);
		shaders[2] = shader::compute::find(fp::sepZ);

		ID3D11ShaderResourceView *kernelViews[3] = {
			separable.x.view.Get(),
			separable.y.view.Get(),
			separable.z.view.Get()
		};
		context->CSSetShaderResources(3, 3, kernelViews);

		auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();
		auto [voxelWidth, voxelHeight, voxelDepth] = panel.voxelDimensions();

		voxel::convolution::convolutionConstant cc;
		cc.voxelDimensions[0] = (si32)voxelWidth;
		cc.voxelDimensions[1] = (si32)voxelHeight;
		cc.voxelDimensions[2] = (si32)voxelDepth;
		auto constantBuffer = buffer::constant(cc);
		context->CSSetConstantBuffers(0, 1, constantBuffer.data.GetAddressOf());

		ID3D11ShaderResourceView *inputViews[3];

		auto outputVoxels = panel.outputVoxels();
		auto panelSize = (ui32)(panelWidth*panelHeight*panelDepth);

		for (ui32 shaderIndex = 0; shaderIndex < viewSize; shaderIndex++) {
			context->CSSetShader(shaders[shaderIndex].Get(), nullptr, 0);
			ui32 offset = shaderIndex*3;

			for (ui32 z = 0; z < panelDepth; z++) {
				for (ui32 y = 0; y < panelHeight; y++) {
					for (ui32 x = 0; x < panelWidth; x++) {
						helper::viewSeparable(x,y,z, offset, panel, inputVoxels, inputViews);
						context->CSSetShaderResources(0, 3, inputViews);

						auto outputVoxelIndex = panel.flatIndex(x,y,z);
						context->CSSetUnorderedAccessViews(0, 1, outputVoxels[outputVoxelIndex].view.GetAddressOf(), nullptr);

						context->Dispatch(voxelWidth/8, voxelHeight/8, voxelDepth/8);
					}
				}
			}

			for (ui32 i = 0; i < panelSize; i++) {
				context->CopyResource(inputVoxels[i].texture3d.Get(), outputVoxels[i].texture3d.Get());
			}
		}	
	}

	static void applyStandard(std::vector<buffer::texture::input3d> &inputVoxels, const voxel::panel &panel, const vcm::standard &mask)
	{
		auto context = d3d::getDeviceContext();

		auto shader = shader::compute::find( fp::full );	
		context->CSSetShader(shader.Get(), nullptr, 0);

		ID3D11ShaderResourceView *views[27];

		auto [voxelWidth, voxelHeight, voxelDepth] = panel.voxelDimensions();
		voxel::convolution::convolutionConstant cc;
		cc.voxelDimensions[0] = (si32)voxelWidth;
		cc.voxelDimensions[1] = (si32)voxelHeight;
		cc.voxelDimensions[2] = (si32)voxelDepth;
		auto constantBuffer = buffer::constant(cc);
		context->CSSetConstantBuffers(0, 1, constantBuffer.data.GetAddressOf());

		context->CSSetShaderResources(27, 1, mask.kernel.view.GetAddressOf());

		auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();

		auto outputVoxels = panel.outputVoxels();
		auto panelSize = (ui32)(panelWidth*panelHeight*panelDepth);

		for (ui32 z = 0; z < panelDepth; z++) {
			for (ui32 y = 0; y < panelHeight; y++) {
				for (ui32 x = 0; x < panelWidth; x++) {
					helper::viewStandard(x,y,z, panel, inputVoxels, views);

					context->CSSetShaderResources(0, 27, views);						

					context->CSSetUnorderedAccessViews(0, 1, outputVoxels[panel.flatIndex(x,y,z)].view.GetAddressOf(), nullptr);

					context->Dispatch(voxelWidth/8, voxelHeight/8, voxelDepth/8);
				}
			}
		}

		for (ui32 i = 0; i < panelSize; i++) {
			context->CopyResource(inputVoxels[i].texture3d.Get(), outputVoxels[i].texture3d.Get());
		}
	}
}