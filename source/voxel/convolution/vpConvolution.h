#ifndef VP_KERNEL_H_
#define VP_KERNEL_H_

#include "bunkcore.h"
#include "math/bunkmath.h"
#include "buffer/texture/bufTex1d.h"
#include "buffer/texture/bufTex3d.h"

#include "voxel/vpPanel.h"

#include <wrl/client.h>
#include <vector>
#include "d3d.h"
namespace voxel
{
	namespace convolution
	{
		namespace mask
		{
			// used to select dimensions for certain creation functions
			enum class dimensions {xyz, xy, xz, yz, x, y, z};
			class separable
			{
			public:
				separable(ui32 dimension, fl32 x[], fl32 y[], fl32 z[]);
			public:
				// Various filters added for ease of use.
				static separable gaussian(fl32 sigma, ui32 dimension);
				static separable average(ui32 dimension);
			public:
				// 3 dimensions of the separated kernel/filter.
				buffer::texture::input1d x, y, z;
			};

			class standard
			{
			public:
				standard(ui16 width, ui16 height, ui16 depth, fl32 buffer[]);
			public:
				// A square laplacian.
				static standard laplacian(ui32 width);
				static standard laplacian(dimensions dimension, ui32 width);
				// Laplacian in only XZ direction
				static standard laplacianXZ(ui32 width);				
			public:
				buffer::texture::input3d kernel;
			};
		}
		// Applies separable mask to panel.
		void apply(voxel::panel &panel, const mask::separable &separable);
		// Applies separable mask to a copy of panel's voxels and returns that copy.
		std::vector<buffer::texture::input3d> calculate(const voxel::panel &panel, const mask::separable &separable);
		std::vector<buffer::texture::input3d> calculate(const voxel::panel &panel, const mask::standard &mask);
		void apply(voxel::panel &panel, const mask::standard &standard);
	}
	
}

#endif