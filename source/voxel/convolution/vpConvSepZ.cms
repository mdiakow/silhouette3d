///////////////////////////////////
// CONSTANT BUFFERS
///////////////////////////////////

cbuffer cbrsh
{
	int3 voxelDimensions;
float pad;
};

///////////////////////////////////
// INPUT/OUTPUT
///////////////////////////////////

Texture3D<float> voxelInput[3] : register(t0);
Texture1D<float> kernel : register(t3);

RWTexture3D<float> voxelOutput : register(u0);

///////////////////////////////////
// SHADER HELPER FUNCTIONS
///////////////////////////////////

int getVoxelIndex(inout int3 voxelIndex)
{
	int voxelInputIndex = 1;
	if (voxelIndex.z < 0) {
		voxelIndex.z += voxelDimensions.z;
		voxelInputIndex -= 1;
	} else if (voxelIndex.z > voxelDimensions.z-1) {
		voxelIndex.z -= voxelDimensions.z;
		voxelInputIndex += 1;
	}
	return voxelInputIndex;
}

float sampleVoxel(int3 voxelIndex)
{
	int voxelInputIndex = getVoxelIndex(voxelIndex);

	switch (voxelInputIndex) {
	case 0: return voxelInput[0][voxelIndex];
	case 1: return voxelInput[1][voxelIndex];
	case 2: return voxelInput[2][voxelIndex];
	}
}

[numthreads(8, 8, 8)]
void csMain(uint3 dt : SV_DispatchThreadID)
{
	uint depth;
	kernel.GetDimensions(depth);

	int halfDepth = depth/2;

	int3 start = dt - int3(0, 0, halfDepth);
	int3 end = start + int3(0, 0, depth);

	float sum = 0.f;
	for (int z = start.z; z < end.z; z++) {
		
		sum += kernel[z-start.z] * sampleVoxel(int3(dt.x, dt.y, z));
	}

	voxelOutput[dt] = sum;
}