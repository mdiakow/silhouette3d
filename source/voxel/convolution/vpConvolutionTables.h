#ifndef VP_CONV_TABLES_H_
#define VP_CONV_TABLES_H_

namespace table
{
	static const ui32 fullAccessHeight = 27;
	static const ui32 fullAccessWidth = 3;
	static si32 fullAccess[fullAccessHeight][fullAccessWidth] = {
		{-1,-1,-1},
		{-1,-1,0},
		{-1,-1,1},
		{-1,0,-1},
		{-1,0,0},
		{-1,0,1},	
		{-1,1,-1},
		{-1,1,0},
		{-1,1,1},
		{0,-1,-1},
		{0,-1,0},
		{0,-1,1},
		{0,0,-1},
		{0,0,0},
		{0,0,1},	
		{0,1,-1},
		{0,1,0},
		{0,1,1},
		{1,-1,-1},
		{1,-1,0},
		{1,-1,1},
		{1,0,-1},
		{1,0,0},
		{1,0,1},	
		{1,1,-1},
		{1,1,0},
		{1,1,1}
	};

	static const ui32 separableHeight = 9; // x,y,z
	static const ui32 separableWidth = 3;
	static si32 separable[separableHeight][separableWidth] = {
		{-1, 0, 0},
		{0,0,0},
		{1,0,0},
		{0,-1,0},
		{0,0,0},
		{0,1,0},
		{0,0,-1},
		{0,0,0},
		{0,0,1}
	};
}

#endif