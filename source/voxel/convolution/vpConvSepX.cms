///////////////////////////////////
// CONSTANT BUFFERS
///////////////////////////////////

cbuffer cbrsh
{
	int3 voxelDimensions;
	float pad;
};

///////////////////////////////////
// INPUT/OUTPUT
///////////////////////////////////

Texture3D<float> voxelInput[3] : register(t0);
Texture1D<float> kernel : register(t3);

RWTexture3D<float> voxelOutput : register(u0);

///////////////////////////////////
// SHADER HELPER FUNCTIONS
///////////////////////////////////

int getVoxelIndex(inout int3 voxelIndex)
{
	int voxelInputIndex = 1;
	if (voxelIndex.x < 0) {
		voxelIndex.x += voxelDimensions.x;
		voxelInputIndex -= 1;
	} else if (voxelIndex.x > voxelDimensions.x-1) {
		voxelIndex.x -= voxelDimensions.x;
		voxelInputIndex += 1;
	}
	return voxelInputIndex;
}

float sampleVoxel(int3 voxelIndex)
{
	int voxelInputIndex = getVoxelIndex(voxelIndex);
	
	switch (voxelInputIndex) {
		case 0: return voxelInput[0][voxelIndex];
		case 1: return voxelInput[1][voxelIndex];
		case 2: return voxelInput[2][voxelIndex];
	}
}

[numthreads(8, 8, 8)]
void csMain(uint3 dt : SV_DispatchThreadID)
{
	uint width;
	kernel.GetDimensions(width);

	int halfWidth = width/2;

	int3 start = dt - int3(halfWidth , 0, 0);
	int3 end = start + int3(width, 0, 0);

	float sum = 0.f;
	for (int x = start.x; x < end.x; x++) {
		sum += kernel[x-start.x] * sampleVoxel(int3(x, dt.y, dt.z));
	}

	voxelOutput[dt] = sum;
}
