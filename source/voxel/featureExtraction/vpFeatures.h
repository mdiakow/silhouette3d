#ifndef VP_FEATURES_H_
#define VP_FEATURES_H_

#include <vector>

#include "buffer/texture/bufTex3d.h"

#include "voxel/convolution/vpConvolution.h"
#include "voxel/vpPanel.h"

namespace voxel
{
	namespace featureExtraction
	{
		template<typename mask>
		std::vector<v3> points(const voxel::panel &panel, const mask& m, fl32 minimum, fl32 maximum);

		buffer::texture::output3d magnitude(voxel::panel &panel, convolution::mask::separable &mask);
	}
	
}

#endif