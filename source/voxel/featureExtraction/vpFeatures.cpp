#include "voxel/featureExtraction/vpFeatures.h"


#include <array>

#include "buffer/structured/bufferAppendStructured.h"
#include "buffer/constant/bufferConstant.h"

#include "render/shader/common/shaderHelper.h"

namespace voxel::featureExtraction
{
	namespace fp
	{
		static std::filesystem::path points = "source/voxel/featureExtraction/vpPointExtractor.cms";
		static std::filesystem::path gradMag = "source/voxel/featureExtraction/vpGradMag.cms";
	}

	struct pointsConstant
	{
		fl32 minimum;
		v3 positionOffset;
	};

	template<typename mask>
	std::vector<v3> points(const voxel::panel &panel, const mask& m, fl32 minimum, fl32 maximum)
	{
		auto context = d3d::getDeviceContext();

		auto [voxelWidth, voxelHeight, voxelDepth] = panel.voxelDimensions();
		auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();

		auto maxSize = (voxelWidth*voxelHeight*voxelDepth*panelWidth*panelHeight*panelDepth);
		auto positionOut = buffer::appendStructured::output<v3>(v3(), maxSize);

		auto voxels = voxel::convolution::calculate(panel, m);

		pointsConstant pc;
		pc.minimum = minimum;
		auto constantBuffer = buffer::constant(pc);

		auto shader = shader::compute::find(fp::points);
		context->CSSetShader(shader.Get(), nullptr, 0);

		context->CSSetConstantBuffers(0, 1, constantBuffer.data.GetAddressOf());
		ui32 reset = 0;
		context->CSSetUnorderedAccessViews(0, 1, positionOut.appendView.GetAddressOf(), &reset);

		v3 voxelPosition;
		for (ui32 z = 0; z < panelDepth; z++) {
			for (ui32 y = 0; y < panelHeight; y++) {
				for (ui32 x = 0; x < panelWidth; x++) {
					voxelPosition = v3((fl32)(x*voxelWidth), (fl32)(y*voxelHeight), (fl32)(z*voxelDepth));

					pc.positionOffset = voxelPosition;
					constantBuffer.set(pc);

					context->CSSetShaderResources(0, 1, voxels[panel.flatIndex(x,y,z)].view.GetAddressOf());

					context->Dispatch(voxelWidth/8, voxelHeight/8, voxelDepth/8);
				}
			}
		}

		return positionOut.get();
	}	

	/*std::vector<v3> points(const voxel::panel & panel, const voxel::convolution::mask::separable & mask, fl32 minimum, fl32 maximum)
	{
		auto context = d3d::getDeviceContext();

		auto [voxelWidth, voxelHeight, voxelDepth] = panel.voxelDimensions();
		auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();

		auto maxSize = (voxelWidth*voxelHeight*voxelDepth*panelWidth*panelHeight*panelDepth);
		auto positionOut = buffer::appendStructured::output<v3>(v3(), maxSize);

		auto voxels = voxel::convolution::calculate(panel, mask);
		
		pointsConstant pc;
		pc.minimum = minimum;
		auto constantBuffer = buffer::constant(pc);

		auto shader = shader::compute::find(fp::points);
		context->CSSetShader(shader.Get(), nullptr, 0);

		context->CSSetConstantBuffers(0, 1, constantBuffer.data.GetAddressOf());
		ui32 reset = 0;
		context->CSSetUnorderedAccessViews(0, 1, positionOut.view.GetAddressOf(), &reset);

		v3 voxelPosition;
		for (ui32 z = 0; z < panelDepth; z++) {
			for (ui32 y = 0; y < panelHeight; y++) {
				for (ui32 x = 0; x < panelWidth; x++) {
					voxelPosition = v3((fl32)(x*voxelWidth), (fl32)(y*voxelHeight), (fl32)(z*voxelDepth));

					pc.positionOffset = voxelPosition;
					constantBuffer.set(pc);

					context->CSSetShaderResources(0, 1, voxels[panel.flatIndex(x,y,z)].view.GetAddressOf());

					context->Dispatch(voxelWidth/8, voxelHeight/8, voxelDepth/8);
				}
			}
		}

		return positionOut.get();
	}

	std::vector<v3> points(const voxel::panel& panel, const voxel::convolution::mask::standard &mask, fl32 minimum, fl32 maximum)
	{

	}*/

	buffer::texture::output3d magnitude(buffer::texture::input3d & voxel, convolution::mask::separable & mask)
	{
		auto context = d3d::getDeviceContext();

		auto magnitudeOutput = buffer::texture::output3d::fromInput(voxel);

		auto shader = shader::compute::load(fp::gradMag);

		auto srvs = std::array<ID3D11ShaderResourceView*, 4>{{voxel.view.Get(), mask.x.view.Get(), mask.y.view.Get(), mask.z.view.Get()}};
		context->CSSetShaderResources(0, 4, srvs.data());
		
		context->CSSetUnorderedAccessViews(0, 1, magnitudeOutput.view.GetAddressOf(), 0);

		auto [width, height, depth] = voxel.dimensions();

		context->Dispatch(width/8, height/8, depth/8);

		return magnitudeOutput;
	}

}
template std::vector<v3> voxel::featureExtraction::points<voxel::convolution::mask::standard>(const voxel::panel &panel, const voxel::convolution::mask::standard &mask, fl32 minimum, fl32 maximum);
template std::vector<v3> voxel::featureExtraction::points<voxel::convolution::mask::separable>(const voxel::panel &panel, const voxel::convolution::mask::separable &mask, fl32 minimum, fl32 maximum);

