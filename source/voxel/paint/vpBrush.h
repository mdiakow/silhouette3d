#ifndef VP_BRUSH_H_
#define VP_BRUSH_H_

#include "bunkcore.h"
#include "buffer/texture/bufTex3d.h"

namespace voxel
{ // Brush is for 3d painting.
	class brush
	{
	public:
		brush(ui16 width, ui16 height, ui16 depth, fl32 buffer[]);
		brush(buffer::texture::input3d other);
	public:
		static brush sphere(fl32 radius, fl32 value);
		static brush cube(ui16 width, ui16 height, ui16 depth, fl32 value);
		// Returns width, height, and depth of the brush.
		std::tuple<ui16, ui16, ui16> dimensions() const;
	public:
		buffer::texture::input3d data; // holds underlying brush data
	};
}

#endif