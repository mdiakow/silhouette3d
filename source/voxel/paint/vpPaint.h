#ifndef VP_PAINTER_H_
#define VP_PAINTER_H_

#include <vector>
#include <wrl/client.h>

#include "render/shader/common/shaderHelper.h"

#include "buffer/texture/bufTex3d.h"
#include "voxel/paint/vpBrush.h"
#include "voxel/vpPanel.h"

namespace voxel
{
	namespace paint
	{
		namespace additive
		{
			// Applies brush to voxel at a point
			void point(v3 point, voxel::brush &brush, buffer::texture::input3d &voxel);

			void point(v3 point, const voxel::brush &brush, voxel::panel &panel);
			// Applies brush to a voxel for each point in points
			void points(std::vector<v3> points, voxel::brush &brush, buffer::texture::input3d &voxel);

			void points(std::vector<v3> points, const voxel::brush &brush, voxel::panel &panel);
		}

		namespace replace
		{
			// Applies brush to voxel at a point
			void point(v3 point, voxel::brush &brush, buffer::texture::input3d &voxel);
			
			void point(v3 point, const voxel::brush &brush, voxel::panel &panel);

			// Applies brush to a voxel for each point in points
			void points(std::vector<v3> points, voxel::brush &brush, buffer::texture::input3d &voxel);

			void points(std::vector<v3> points, const voxel::brush &brush, voxel::panel &panel);
		}
	}
	
}

#endif