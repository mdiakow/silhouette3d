#include "voxel/paint/vpBrush.h"
#include "voxel/shapes/voxelSphere.h"

namespace voxel
{
	brush::brush(ui16 width, ui16 height, ui16 depth, fl32 buffer[]) :
		data(buffer::texture::input3d(width, height, depth, buffer))
	{

	}

	brush::brush(buffer::texture::input3d other) :
		data(other)
	{
	}

	brush brush::sphere(fl32 radius, fl32 value)
	{
		ui16 dimension = (ui16)(radius*2.f + 2.f);

		v3 center = v3((fl32)dimension/2.f, (fl32)dimension/2.f, (fl32)dimension/2.f);

		auto brushGen = voxel::sphere(dimension, dimension, dimension, center, radius, value);

		return brush(brushGen);
	}
	brush brush::cube(ui16 width, ui16 height, ui16 depth, fl32 value)
	{
		auto cpuBuffer = std::make_unique<fl32[]>(width*height*depth);
		std::fill(&cpuBuffer[0], &cpuBuffer[0] + (width*height*depth), value);

		return brush(width, height, depth, cpuBuffer.get());
	}
	std::tuple<ui16, ui16, ui16> brush::dimensions() const
	{
		return data.dimensions();
	}
}


