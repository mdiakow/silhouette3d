#include "voxel/paint/vpPaint.h"
#include "buffer/constant/bufferConstant.h"

//using computeShader = Microsoft::WRL::ComPtr<ID3D11ComputeShader>;

namespace fp
{
	static std::filesystem::path pointAdd = "source/voxel/paint/vpPointAdd.cms";
	static std::filesystem::path pointReplace = "source/voxel/paint/vpPointReplace.cms";
}


namespace helper
{
	static std::tuple<ui16, ui16, ui16> dispatchDimensions(buffer::texture::input3d & voxel);
	// Properly offset position to draw from the middle of the brush, rather than the bottom corner.
	static v3 offsetPosition(v3 position, const voxel::brush &brush);
	// Returns the bounds (lowerLeft and upperRight) from the position.
	static std::tuple<v3, v3> brushBounds(v3 position, const voxel::brush &brush);

	static std::tuple<si32, si32, si32> indexStart(v3 &brushStart, v3 &voxelDimensions);
	static std::tuple<si32, si32, si32> indexEnd(v3 &brushEnd, v3 &voxelDimensions, const voxel::panel &panel);

	static void paintPanel(v3 position, const voxel::brush & brush, voxel::panel & panel, Microsoft::WRL::ComPtr<ID3D11ComputeShader> shader);
}

namespace voxel::paint 
{
	struct pointBuffer
	{
		si32 brushPosition[3];
		float pad;

		void setPosition(v3 position) {
			brushPosition[0] = (si32)roundf(position.x());
			brushPosition[1] = (si32)roundf(position.y());
			brushPosition[2] = (si32)roundf(position.z());
		}
	};
	namespace additive
	{
		void point(v3 position, voxel::brush & brush, buffer::texture::input3d & voxel)
		{
			auto pointAdd = shader::compute::find(fp::pointAdd);

			auto context = d3d::getDeviceContext();

			context->CSSetShader(pointAdd.Get(), nullptr, 0);

			auto voxelOut = buffer::texture::output3d::fromInput(voxel);

			pointBuffer pb;
			pb.setPosition( helper::offsetPosition(position, brush) );

			pb.pad = 0.f;
			auto pointConstant = buffer::constant(pb);

			context->CSSetConstantBuffers(0, 1, pointConstant.data.GetAddressOf());
			context->CSSetUnorderedAccessViews(0, 1, voxelOut.view.GetAddressOf(), nullptr);
			context->CSSetShaderResources(0, 1, brush.data.view.GetAddressOf());

			context->CopyResource(voxelOut.texture3d.Get(), voxel.texture3d.Get());

			auto [width, height, depth] = helper::dispatchDimensions(voxel);

			context->Dispatch(width, height, depth);

			context->CopyResource(voxel.texture3d.Get(), voxelOut.texture3d.Get());
		}

		void point(v3 position, const voxel::brush & brush, voxel::panel & panel)
		{
			auto shader = shader::compute::find(fp::pointAdd);

			helper::paintPanel(position, brush, panel, shader);
		}

		void points(std::vector<v3> points, const voxel::brush &brush, voxel::panel &panel)
		{
			auto shader = shader::compute::find(fp::pointAdd);

			for (auto &point : points) {
				helper::paintPanel(point, brush, panel, shader);
			}
		}

		void points(std::vector<v3> positions, voxel::brush & brush, buffer::texture::input3d & voxel)
		{
			auto pointAdd = shader::compute::find(fp::pointAdd);

			auto context = d3d::getDeviceContext();

			context->CSSetShader(pointAdd.Get(), nullptr, 0);

			auto voxelOut = buffer::texture::output3d::fromInput(voxel);

			pointBuffer pb;
			pb.setPosition(v3::Zero());
			pb.pad = 0.f;
			auto pointConstant = buffer::constant(pb);

			context->CSSetConstantBuffers(0, 1, pointConstant.data.GetAddressOf());
			context->CSSetUnorderedAccessViews(0, 1, voxelOut.view.GetAddressOf(), nullptr);
			context->CSSetShaderResources(0, 1, brush.data.view.GetAddressOf());

			context->CopyResource(voxelOut.texture3d.Get(), voxel.texture3d.Get());

			auto [width, height, depth] = helper::dispatchDimensions(voxel);

			for (auto &position : positions) {
				pb.setPosition( helper::offsetPosition(position, brush) );
				pointConstant.set(pb);

				context->Dispatch(width, height, depth);
			}		

			context->CopyResource(voxel.texture3d.Get(), voxelOut.texture3d.Get());
		}
	}
	
	namespace replace
	{
		void point(v3 position, voxel::brush & brush, buffer::texture::input3d & voxel)
		{
			auto pointAdd = shader::compute::find(fp::pointReplace);

			auto context = d3d::getDeviceContext();

			context->CSSetShader(pointAdd.Get(), nullptr, 0);

			auto voxelOut = buffer::texture::output3d::fromInput(voxel);

			pointBuffer pb;
			pb.setPosition( helper::offsetPosition(position, brush) );

			pb.pad = 0.f;
			auto pointConstant = buffer::constant(pb);

			context->CSSetConstantBuffers(0, 1, pointConstant.data.GetAddressOf());
			context->CSSetUnorderedAccessViews(0, 1, voxelOut.view.GetAddressOf(), nullptr);
			context->CSSetShaderResources(0, 1, brush.data.view.GetAddressOf());

			context->CopyResource(voxelOut.texture3d.Get(), voxel.texture3d.Get());

			auto [width, height, depth] = helper::dispatchDimensions(voxel);

			context->Dispatch(width, height, depth);

			context->CopyResource(voxel.texture3d.Get(), voxelOut.texture3d.Get());
		}

		void point(v3 position, const voxel::brush & brush, voxel::panel & panel)
		{
			auto shader = shader::compute::find(fp::pointReplace);

			helper::paintPanel(position, brush, panel, shader);
		}

		void points(std::vector<v3> positions, const voxel::brush &brush, voxel::panel &panel)
		{
			auto shader = shader::compute::find(fp::pointReplace);

			for (auto &point : positions) {
				helper::paintPanel(point, brush, panel, shader);
			}
		}

		void points(std::vector<v3> positions, voxel::brush & brush, buffer::texture::input3d & voxel)
		{
			auto pointAdd = shader::compute::find(fp::pointReplace);

			auto context = d3d::getDeviceContext();

			context->CSSetShader(pointAdd.Get(), nullptr, 0);

			auto voxelOut = buffer::texture::output3d::fromInput(voxel);

			pointBuffer pb;
			pb.setPosition(v3::Zero());
			pb.pad = 0.f;
			auto pointConstant = buffer::constant(pb);

			context->CSSetConstantBuffers(0, 1, pointConstant.data.GetAddressOf());
			context->CSSetUnorderedAccessViews(0, 1, voxelOut.view.GetAddressOf(), nullptr);
			context->CSSetShaderResources(0, 1, brush.data.view.GetAddressOf());

			context->CopyResource(voxelOut.texture3d.Get(), voxel.texture3d.Get());

			auto [width, height, depth] = helper::dispatchDimensions(voxel);

			for (auto &position : positions) {
				//pb.setPosition( helper::offsetPosition(position, brush) );
				pb.setPosition(helper::offsetPosition(position, brush));
				pointConstant.set(pb);

				context->Dispatch(width, height, depth);
			}		

			context->CopyResource(voxel.texture3d.Get(), voxelOut.texture3d.Get());
		}
	}
}



namespace helper
{

	std::tuple<ui16, ui16, ui16> dispatchDimensions(buffer::texture::input3d & voxel)
	{
		auto [height, width, depth] = voxel.dimensions();

		ui16 h = (ui16)ceilf((fl32)height/1.f);
		ui16 w = (ui16)ceilf((fl32)width/1.f);
		ui16 d = (ui16)ceilf((fl32)depth/1.f);

		return std::make_tuple(width, height, depth);
	}

	static v3 offsetPosition(v3 position, const voxel::brush &brush)
	{
		auto [width, height, depth] = brush.dimensions();

		v3 half = v3((fl32)width*0.5f, (fl32)height*0.5f, (fl32)depth*0.5f);

		return position - half;
	}

	static std::tuple<v3, v3> brushBounds(v3 position, const voxel::brush &brush)
	{
		auto [width, height, depth] = brush.dimensions();
		v3 half = v3((fl32)width*0.5f, (fl32)height*0.5f, (fl32)depth*0.5f);

		v3 brushStart = position - half;
		v3 brushEnd = brushStart + v3((fl32)width, (fl32)height, (fl32)depth);

		return std::make_tuple(brushStart, brushEnd);
	}

	static std::tuple<si32, si32, si32> indexStart(v3 &brushStart, v3 &voxelDimensions)
	{
		si32 startX = (si32)std::floor(brushStart.x() / voxelDimensions.x());
		si32 startY = (si32)std::floor(brushStart.y() / voxelDimensions.y());
		si32 startZ = (si32)std::floor(brushStart.z() / voxelDimensions.z());
		startX = (std::max)(startX, 0);
		startY = (std::max)(startY, 0);
		startZ = (std::max)(startZ, 0);

		return std::make_tuple(startX, startY, startZ);
	}

	static std::tuple<si32, si32, si32> indexEnd(v3 &brushEnd, v3 &voxelDimensions, const voxel::panel &panel)
	{
		si32 endX = (si32)std::floor(brushEnd.x() / voxelDimensions.x());
		si32 endY = (si32)std::floor(brushEnd.y() / voxelDimensions.y());
		si32 endZ = (si32)std::floor(brushEnd.z() / voxelDimensions.z());
		auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();
		endX = (std::min)(endX, panelWidth-1);
		endY = (std::min)(endY, panelHeight-1);
		endZ = (std::min)(endZ, panelDepth-1);

		return std::make_tuple(endX, endY, endZ);
	}

	static void paintPanel(v3 position, const voxel::brush & brush, voxel::panel & panel, Microsoft::WRL::ComPtr<ID3D11ComputeShader> shader)
	{
		auto context = d3d::getDeviceContext();

		context->CSSetShader(shader.Get(), nullptr, 0);

		auto outputVoxel = buffer::texture::output3d::fromInput(panel.voxel(0,0,0));

		voxel::paint::pointBuffer pb;
		pb.setPosition(v3::Zero());
		auto pointConstant = buffer::constant(pb);

		context->CSSetConstantBuffers(0, 1, pointConstant.data.GetAddressOf());
		context->CSSetUnorderedAccessViews(0, 1, outputVoxel.view.GetAddressOf(), nullptr);
		context->CSSetShaderResources(0, 1, brush.data.view.GetAddressOf());

		auto [voxelWidth, voxelHeight, voxelDepth] = panel.voxelDimensions();
		auto [brushStart, brushEnd] = helper::brushBounds(position, brush);

		v3 voxelDimensions = v3((fl32)voxelWidth, (fl32)voxelHeight, (fl32)voxelDepth);

		auto [startX, startY, startZ] = helper::indexStart(brushStart, voxelDimensions);
		auto [endX, endY, endZ] = helper::indexEnd(brushEnd, voxelDimensions, panel);

		auto [dispatchWidth, dispatchHeight, dispatchDepth] = helper::dispatchDimensions(panel.voxel(0,0,0));
		for (si32 y = startY; y <= endY; y++) {
			for (si32 x = startX; x <= endX; x++) {
				for (si32 z = startZ; z <= endZ; z++) {
					pb.setPosition(brushStart - v3((fl32)(x*voxelWidth), (fl32)(y*voxelHeight), (fl32)(z*voxelDepth)));
					pointConstant.set(pb);

					context->CopyResource(outputVoxel.texture3d.Get(), panel.voxel(x,y,z).texture3d.Get());
					context->Dispatch(dispatchWidth, dispatchHeight, dispatchDepth);
					context->CopyResource(panel.voxel(x,y,z).texture3d.Get(), outputVoxel.texture3d.Get());
				}
			}
		}
	}
}

