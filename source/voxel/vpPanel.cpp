#include "voxel/vpPanel.h"

#include <algorithm>

namespace initialize
{
	static std::vector<buffer::indirect> indirectBuffers(ui32 size);
	static std::vector<buffer::structured::output<vertex::light>> gpuMeshes(ui32 panelWidth, ui32 panelHeight, ui32 panelDepth, ui32 voxelWidth, ui32 voxelDepth, ui32 voxelHeight);
	static std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> srViews(ui32 panelWidth, ui32 panelHeight, ui32 panelDepth, ui32 voxelWidth, ui32 voxelDepth, ui32 voxelHeight, std::vector<buffer::structured::output<vertex::light>> &gpuMeshes);
}

namespace voxel
{
	panel::panel(ui32 voxelDimensions, ui32 voxelCount, v3 position) :
		width( voxelCount ),
		height( voxelCount ),
		depth( voxelCount ),
		indirectBuffers( initialize::indirectBuffers(voxelCount*voxelCount*voxelCount) ),
		gpuMeshes( initialize::gpuMeshes(voxelCount, voxelCount, voxelCount, voxelDimensions, voxelDimensions, voxelDimensions) ),
		srViews( initialize::srViews(voxelCount, voxelCount, voxelCount, voxelDimensions, voxelDimensions, voxelDimensions, gpuMeshes) )
	{
		auto bufferSize = voxelDimensions*voxelDimensions*voxelDimensions;
		auto buffer = std::make_unique<fl32[]>(bufferSize);
		std::fill(&buffer[0], &buffer[0] + bufferSize, 0.0f);
				 
		
		voxels = std::vector<buffer::texture::input3d>(width*height*depth, buffer::texture::input3d(voxelDimensions, voxelDimensions, voxelDimensions, buffer.get()));
	}

	void panel::render()
	{
		ui32 stride = sizeof(vertex::light);
		ui32 offset = 0;

		auto context = d3d::getDeviceContext();

		ui16 max = width*height*depth;
		for (ui32 i = 0; i < max; i++) {
			
			context->VSSetShaderResources(0, 1, srViews[i].GetAddressOf());

			context->DrawInstancedIndirect(indirectBuffers[i].data.Get(), 0);

			//context->Draw(4000, 0);
		}
	}

	std::tuple<ui16, ui16, ui16> panel::dimensions() const
	{
		return std::make_tuple(width, height, depth);
	}

	std::tuple<ui16, ui16, ui16> panel::voxelDimensions() const
	{
		return voxels[0].dimensions();
	}

	ui32 panel::flatIndex(ui16 x, ui16 y, ui16 z) const
	{
		return (ui32)(x + width*(y + depth * z));
	}

	buffer::texture::input3d * panel::getVoxel(ui16 x, ui16 y, ui16 z)
	{
		if (x < width && y < height && z < depth) {
			return &voxels[flatIndex(x,y,z)];
		}
		return nullptr;
	}

	std::vector<buffer::texture::output3d> panel::outputVoxels() const
	{
		auto size = voxels.size();

		auto [voxelWidth, voxelHeight, voxelDepth] = voxelDimensions();

		return std::vector<buffer::texture::output3d>(size, buffer::texture::output3d(voxelWidth, voxelHeight, voxelDepth));
	}

	std::vector<buffer::texture::input3d> panel::inputVoxels() const
	{		
		std::vector<buffer::texture::input3d> out(voxels.size());

		for (ui32 i = 0; i < voxels.size(); i++) {
			out[i] = buffer::texture::input3d(voxels[i]);
		}

		return out;
	}

	buffer::texture::input3d & panel::voxel(ui16 x, ui16 y, ui16 z)
	{
		return voxels[flatIndex(x,y,z)];
	}
}


namespace initialize
{
	std::vector<buffer::indirect> indirectBuffers(ui32 size)
	{
		buffer::drawIndirect initBuffer;
		
		initBuffer.vertexCountPerInstance = 0;
		initBuffer.instanceCount = 1;
		initBuffer.startVertexLocation = 0;
		initBuffer.startInstanceLocation = 0;	

		std::vector<buffer::indirect> buffers(size);

		for (auto &buffer : buffers) {
			buffer = buffer::indirect(initBuffer);
		}

		return buffers;
	}

	std::vector<buffer::structured::output<vertex::light>> gpuMeshes(ui32 panelWidth, ui32 panelHeight, ui32 panelDepth, ui32 voxelWidth, ui32 voxelDepth, ui32 voxelHeight)
	{
		ui32 panelCount = panelWidth*panelHeight*panelDepth;
		std::vector<buffer::structured::output<vertex::light>> meshes(panelCount);

		auto testSize = sizeof(vertex::light);

		for (ui32 i = 0; i < panelCount; i++) {
			meshes[i] = buffer::structured::output<vertex::light>(vertex::light(), voxelWidth*voxelHeight*voxelDepth*5);
		}
		
		return meshes;
	}

	std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> srViews(ui32 panelWidth, ui32 panelHeight, ui32 panelDepth, ui32 voxelWidth, ui32 voxelDepth, ui32 voxelHeight, std::vector<buffer::structured::output<vertex::light>> &gpuMeshes)
	{
		auto device = d3d::getDevice();

		ui32 panelCount = panelWidth*panelHeight*panelDepth;
		std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> srViews(panelCount);

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.Buffer.FirstElement = 0;
		srvDesc.Buffer.NumElements = voxelWidth*voxelHeight*voxelDepth*5;


		HRESULT result;
		for (ui32 i = 0; i < panelCount; i++) {
			result = device->CreateShaderResourceView(gpuMeshes[i].data.Get(), &srvDesc, srViews[i].GetAddressOf());
		}

		return srViews;
	}
}


