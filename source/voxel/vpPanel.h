#ifndef VP_PANEL_H_
#define VP_PANEL_H_

#include "bunkcore.h"

#include <vector>
#include <tuple>

#include "mesh/meshTriangle.h"
#include "shaders/cpp/shaderLight.h"

#include "buffer/texture/bufTex3d.h"

#include "buffer/indirect/bufferIndirect.h"
#include "buffer/structured/bufferStructured.h"
#include "buffer/bufferIndex.h"

// A panel consists of a number of voxels
namespace voxel
{
	class panel
	{
	public:
		panel(ui32 voxelDimensions, ui32 voxelCount, v3 position);
	public:
		void render();
		// returns how many voxels are in each dimension.
		std::tuple<ui16, ui16, ui16> dimensions() const;
		// returns the width, hight, and depth of the underlying voxels.
		std::tuple<ui16, ui16, ui16> voxelDimensions() const;

		// Flattened index.
		ui32 flatIndex(ui16 x, ui16 y, ui16 z) const;

		// Returns voxel at x,y,z. NOTE: no bounds checking.
		buffer::texture::input3d &voxel(ui16 x, ui16 y, ui16 z);

		// Get an individual voxel, returns nullptr if x,y,z are out of bounds.
		buffer::texture::input3d *getVoxel(ui16 x, ui16 y, ui16 z);
		// The output panel is used to hold the results of some algorithm
		// applied to the panel itself.
		std::vector<buffer::texture::output3d> outputVoxels() const;
		
		std::vector<buffer::texture::input3d> inputVoxels() const;
	public:
		std::vector<buffer::texture::input3d> voxels;

		// New experiment, try to never bring data back to the CPU in voxel rendering.
		std::vector<buffer::indirect> indirectBuffers;
		std::vector<buffer::structured::output<vertex::light>> gpuMeshes;
		std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> srViews;
	private:
		ui16 width, height, depth;
	};
}


#endif