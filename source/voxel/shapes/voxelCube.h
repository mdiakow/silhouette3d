#ifndef VOXEL_CUBE_H_
#define VOXEL_CUBE_H_

#include "bunkcore.h"
#include "buffer/texture/bufTex3d.h"

namespace voxel
{
	// Create a sphere voxel using the GPU, the sphere is automatically centered.
	buffer::texture::input3d cube(ui16 width, ui16 height, ui16 depth, v3 lowerLeft, v3 upperRight, fl32 isoValue);
}

#endif