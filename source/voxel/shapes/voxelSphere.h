#ifndef VOXEL_SPHERE_H_
#define VOXEL_SPHERE_H_

#include "bunkcore.h"
#include "buffer/texture/bufTex3d.h"

namespace voxel
{
	// Create a sphere voxel using the GPU, the sphere is automatically centered.
	buffer::texture::input3d sphere(ui16 width, ui16 height, ui16 depth, v3 position, fl32 radius, fl32 value);
}

#endif