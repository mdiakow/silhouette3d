#include "voxel/shapes/voxelSphere.h"

#include "render/shader/common/shaderHelper.h"
#include "buffer/constant/bufferConstant.h"

namespace fp
{
	static const std::filesystem::path sphereShader = "source/voxel/shapes/voxelSphere.cms";
}

namespace voxel
{
	struct sphereBuffer
	{
		fl32 radius;
		v3 position;
		fl32 value;
		fl32 padding[3];
	};

	buffer::texture::input3d sphere(ui16 width, ui16 height, ui16 depth, v3 position, fl32 radius, fl32 value)
	{
		auto context = d3d::getDeviceContext();

		auto sphereOutput = buffer::texture::output3d(width, height, depth);

		auto shader = shader::compute::load(fp::sphereShader);

		auto si = sphereBuffer();
		si.radius = radius;
		si.position = position;
		si.value = value;
		auto sphereInput = buffer::constant(si);

		context->CSSetShader(shader.Get(), nullptr, 0);

		context->CSSetConstantBuffers(0, 1, sphereInput.data.GetAddressOf());

		context->CSSetUnorderedAccessViews(0, 1, sphereOutput.view.GetAddressOf(), nullptr);

		ui16 w = (ui16)ceilf((fl32)width/8.f);
		ui16 h = (ui16)ceilf((fl32)height/8.f);
		ui16 d = (ui16)ceilf((fl32)depth/8.f);

		context->Dispatch(w, h, d);

		auto sphereReturn = buffer::texture::input3d(width, height, depth);
		
		context->CopyResource(sphereReturn.texture3d.Get(), sphereOutput.texture3d.Get());

		return sphereReturn;
	}
}
