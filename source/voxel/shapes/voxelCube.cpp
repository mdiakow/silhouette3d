#include "voxel/shapes/voxelCube.h"

#include "render/shader/common/shaderHelper.h"
#include "buffer/constant/bufferConstant.h"

namespace fp
{
	static const std::filesystem::path cubeShader = "source/voxel/shapes/voxelCube.cms";
}

namespace voxel
{

	struct cubeBuffer
	{
		v3 lowerLeft;
		fl32 isoValue;
		v3 upperRight;
		fl32 padding;
	};

	buffer::texture::input3d cube(ui16 width, ui16 height, ui16 depth, v3 lowerLeft, v3 upperRight, fl32 isoValue)
	{
		auto context = d3d::getDeviceContext();

		auto cubeOutput = buffer::texture::output3d(width, height, depth);

		auto shader = shader::compute::load(fp::cubeShader);

		cubeBuffer cu;
		cu.lowerLeft = lowerLeft;
		cu.isoValue = isoValue;
		cu.upperRight = upperRight;
		auto cubeInput = buffer::constant(cu);

		context->CSSetShader(shader.Get(), nullptr, 0);

		context->CSSetConstantBuffers(0, 1, cubeInput.data.GetAddressOf());

		context->CSSetUnorderedAccessViews(0, 1, cubeOutput.view.GetAddressOf(), nullptr);

		context->Dispatch(width/8, height/8, depth/8);

		auto cubeReturn = buffer::texture::input3d(width, height, depth);
		
		context->CopyResource(cubeReturn.texture3d.Get(), cubeOutput.texture3d.Get());

		return cubeReturn;
	}
}
