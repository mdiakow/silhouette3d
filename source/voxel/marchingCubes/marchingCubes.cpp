#include "voxel/marchingCubes/marchingCubes.h"
#include "Debugger/bunkdebug.h"
#include "bunkcore.h"
#include <array>
#include <bitset>

#include "render/shader/common/shaderHelper.h"

#include "buffer/structured/bufferStructured.h"
#include "buffer/structured/bufferAppendStructured.h"
#include "buffer/constant/bufferConstant.h"
#include "buffer/texture/bufTex3d.h"

#include "voxel/shapes/voxelSphere.h"
#include "voxel/shapes/voxelCube.h"

namespace fp
{
	std::filesystem::path mesh = "source/voxel/marchingCubes/mcMesh.cms";
	std::filesystem::path march = "source/voxel/marchingCubes/march.cms";
}

namespace marchingCubes
{
	meshBuilder::meshBuilder() :
		marchArguments( constBuffer() ),
		triangleTable( triangleBuffer() )
	{
		marchShader = shader::compute::load(fp::march);

		triangleBuffer triTable;
		triTable.data = table::triangles;
		triangleTable.set(triTable);
	}

	void meshBuilder::mesh(voxel::panel &panel, fl32 isoValue)
	{
		auto context = d3d::getDeviceContext();
		auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();
		auto [voxelWidth, voxelHeight, voxelDepth] = panel.voxel(0,0,0).dimensions();

		struct outVertices { v3 vertices[3]; };
		struct outCount { ui32 vertexCount; };

		auto verticesOutput = buffer::appendStructured::output(outVertices(), (voxelWidth*voxelHeight*voxelDepth*5));

		constBuffer cb;
		cb.isoValue = isoValue;
		cb.voxelLowerLeft = v3::Zero();
		cb.voxelDimensions[0] = voxelWidth;
		cb.voxelDimensions[1] = voxelHeight;
		cb.voxelDimensions[2] = voxelDepth;
		auto constantBuffer = buffer::constant(cb);
		
		context->CSSetConstantBuffers(1, 1, triangleTable.data.GetAddressOf());

		auto meshShader = shader::compute::find(fp::mesh);

		ui32 dispatchWidth = (ui32)ceil((fl32)voxelWidth/8.f);
		ui32 dispatchHeight = (ui32)ceil((fl32)voxelHeight/8.f);
		ui32 dispatchDepth = (ui32)ceil((fl32)voxelDepth/8.f);

		for (ui32 z = 0; z < panelDepth; z++) {
			for (ui32 y = 0; y < panelHeight; y++) {
				for (ui32 x = 0; x < panelWidth; x++) {
					context->CSSetShader(marchShader.Get(), nullptr, 0);

					cb.voxelLowerLeft = v3((fl32)(x*voxelWidth), (fl32)(y*voxelHeight), (fl32)(z*voxelDepth));
					constantBuffer.set(cb);

					bool xin = x+1 < panelWidth;
					bool yin = y+1 < panelHeight;
					bool zin = z+1 < panelDepth;

					ID3D11ShaderResourceView *views[8];

					views[0] = panel.voxel(x,y,z).view.Get();
					views[1] = xin ? panel.voxel(x+1,y,z).view.Get() : nullptr;
					views[2] = yin ? panel.voxel(x,y+1,z).view.Get() : nullptr;
					views[3] = yin && xin ? panel.voxel(x+1,y+1,z).view.Get() : nullptr;
					views[4] = zin ? panel.voxel(x,y,z+1).view.Get() : nullptr;
					views[5] = zin && xin ? panel.voxel(x+1,y,z+1).view.Get() : nullptr;
					views[6] = zin && yin ? panel.voxel(x,y+1,z+1).view.Get() : nullptr;
					views[7] = zin && yin && xin ? panel.voxel(x+1,y+1,z+1).view.Get() : nullptr;

					context->CSSetShaderResources(0, 8, views);

					context->CSSetConstantBuffers(0, 1, constantBuffer.data.GetAddressOf());

					ui32 index = panel.flatIndex(x,y,z);

					ID3D11UnorderedAccessView *uaViews[2] = {
						panel.gpuMeshes[index].view.Get(),
						panel.indirectBuffers[index].view.Get()
					};
					context->CSSetUnorderedAccessViews(0, 2, uaViews, nullptr);
					
					context->Dispatch(dispatchWidth, dispatchHeight, dispatchDepth);			
				}
			}
		}

		ID3D11UnorderedAccessView *uaNull[2] = {
			nullptr,
			nullptr
		};
		context->CSSetUnorderedAccessViews(0, 2, uaNull, nullptr);
	}

	void meshBuilder::mesh(voxel::panel & panel, fl32 isoValue, ui32 x, ui32 y, ui32 z)
	{

	}
}

