#ifndef MARCHING_CUBES_H_
#define MARCHING_CUBES_H_

#include <memory>
#include <array>
#include <vector>
#include <wrl/client.h>

#include "bunkcore.h"
#include "mesh/meshTriangle.h"
#include "mesh/meshLine.h"

#include "render/shader/common/shaderHelper.h"
#include "shaders/cpp/shaderLight.h"

#include "voxel/marchingCubes/marchingCubesTable.h"
#include "voxel/vpPanel.h"

#include "buffer/texture/bufTex3d.h"
#include "buffer/constant/bufferConstant.h"
#include "buffer/structured/bufferStructured.h"
#include "buffer/structured/bufferAppendStructured.h"

namespace marchingCubes
{
	struct constBuffer
	{
		fl32 isoValue;
		v3 voxelLowerLeft;
		ui32 voxelDimensions[3];
		fl32 padding;
	};	

	struct triangleBuffer { std::array<si32, table::triangleSize> data; };

	class meshBuilder
	{
	public:
		meshBuilder();
		// Remeshes the entire panel.
		void mesh(voxel::panel &panel, fl32 isoValue);
		void mesh(voxel::panel &panel, fl32 isoValue, ui32 x, ui32 y, ui32 z);
	public:
		Microsoft::WRL::ComPtr<ID3D11ComputeShader> marchShader;
		buffer::constant<triangleBuffer> triangleTable;
			   	
		// Constant input parameters for march shader.
		buffer::constant<constBuffer> marchArguments;
	};
}

#endif

/*
//		std::vector<mesh::line<vertex::light>> baseTriangleBoxes();

// Visualizes intial table so I can make sure they are correct before trying to generate
// the rest.
std::unique_ptr<mesh::triangle<vertex::light>> baseTriangleTable();
// Visual bounding boxes to show the edges of the volume.*/


/*
template<class msh, class vrtx>
inline std::unique_ptr<msh> builder<msh, vrtx>::mesh(buffer::texture::input3d &voxel, fl32 isoValue, v3 position)
{
auto context = d3d::getDeviceContext();
auto [width, height, depth] = voxel.dimensions();

struct outVertices { v3 vertices[15]; };
struct outCount { ui32 vertexCount; };

auto verticesOutput = buffer::structured::output(outVertices(), (width*height*depth));
auto countOutput = buffer::structured::output(outCount(), (width*height*depth));

constBuffer cb;
cb.isoValue = isoValue;
cb.voxelLowerLeft = position;
auto constantBuffer = buffer::constant(cb);

context->CSSetShader(shader.Get(), nullptr, 0);

context->CSSetShaderResources(0, 1, voxel.view.GetAddressOf());

context->CSSetConstantBuffers(0, 1, constantBuffer.data.GetAddressOf());
context->CSSetConstantBuffers(1, 1, triangleTable.data.GetAddressOf());

context->CSSetUnorderedAccessViews(0, 1, verticesOutput.view.GetAddressOf(), nullptr);
context->CSSetUnorderedAccessViews(1, 1, countOutput.view.GetAddressOf(), nullptr);

ui32 dispatchWidth = (ui32)ceil((fl32)width/8.f);
ui32 dispatchHeight = (ui32)ceil((fl32)height/8.f);
ui32 dispatchDepth = (ui32)ceil((fl32)depth/8.f);

context->Dispatch(dispatchWidth, dispatchHeight, dispatchDepth);

auto cOut = countOutput.get();
auto size = countOutput.size();

std::vector<ui32> validVertices;
ui32 vertexCount = 0;
for (ui32 i = 0; i < size; i++) {
if (cOut[i].vertexCount > 0) {
validVertices.push_back(i);
vertexCount += cOut[i].vertexCount;
}
}

auto vOut = verticesOutput.get(validVertices);

std::vector<vrtx> vertices(vertexCount);
std::vector<ui32> indices(vertexCount);

v3 trianglePoints[3];
ui32 currentIndex = 0;
ui32 currentVertex = 0;
for (ui32 i = 0; i < validVertices.size(); i++) {
ui32 innerCount = cOut[validVertices[i]].vertexCount/3;
for (ui32 j = 0; j < innerCount; j++) {
for (ui32 t = 0; t < 3; t++) {
trianglePoints[t] = vOut[i].vertices[(3*j)+t];
}
v3 sn = helpers::surfaceNormal(trianglePoints[0], trianglePoints[1], trianglePoints[2]);
for (ui32 t = 0; t < 3; t++) {
vertices[currentVertex].position = trianglePoints[t];
vertices[currentVertex].normal = sn;
indices[currentIndex] = currentIndex;
currentIndex++;
currentVertex++;
}
}
}

return std::make_unique<msh>(vertices.data(), indices.data(), (ui32)vertices.size(), (ui32)indices.size());
}
*/