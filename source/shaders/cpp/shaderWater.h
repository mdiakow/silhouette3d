#ifndef SHADER_WATER_H_
#define SHADER_WATER_H_

#include "bunkcore.h"
#include <d3d11.h>

#include <filesystem>
#include <vector>

namespace shader::water
{
	str name();
	std::filesystem::path vertex();
	std::filesystem::path pixel();
}

#endif