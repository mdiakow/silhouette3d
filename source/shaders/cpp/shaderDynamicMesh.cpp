#include "shaders/cpp/shaderDynamicMesh.h"

namespace shader::dynamicMesh
{
	str name() 
	{
		return "dynamicMesh";
	}

	std::filesystem::path vertex()
	{
		return "source/shaders/vertex/dynamicMesh.vs";
	}

	std::filesystem::path pixel()
	{
		return "source/shaders/pixel/light.ps";
	}
}