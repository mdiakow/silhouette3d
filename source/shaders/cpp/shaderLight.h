#ifndef SHADER_3DLIGHT_H_
#define SHADER_3DLIGHT_H_

#include <filesystem>
#include <vector>

#include "d3d.h"
#include "bunkcore.h"

namespace vertex
{
	struct light
	{
		v3 position;
		v2 textureCoordinate;
		v3 normal;
		v3 tangent;
		v3 binormal;
		v2 padding;
	};
}

namespace shader
{
	namespace light
	{
		static const str name = "3d";
		static const std::filesystem::path vertex = "source/shaders/vertex/light.vs";
		static const std::filesystem::path pixel = "source/shaders/pixel/light.ps";

		std::vector<D3D11_INPUT_ELEMENT_DESC> description();
	}
}

#endif