#ifndef SHADER_INST_LIGHT_H_
#define SHADER_INST_LIGHT_H_

#include <filesystem>
#include <vector>

#include "d3d.h"
#include "bunkcore.h"

namespace vertex
{
	namespace animated
	{
		struct light
		{
			v3 position;
			v2 textureCoordinate;
			v3 normal;
			v3 tangent;
			v3 binormal;
			fl32 weights[2];
			si32 indices[2];
		};
	}
}

namespace instance
{
	namespace animated
	{
		struct light
		{
			ui32 orientationIndex;
		};
	}
}

namespace shader
{
	namespace animated
	{
		namespace light
		{
			static const str name = "instAni3d";
			static const std::filesystem::path vertex = "source/shaders/vertex/instAniLight.vs";
			static const std::filesystem::path pixel = "source/shaders/pixel/instAniLight.ps";
			
			std::vector<D3D11_INPUT_ELEMENT_DESC> description();
		}

	}

}


#endif