#include "shaderWater.h"

namespace shader::water
{
	str name()
	{
		return "shaderWater";
	}

	std::filesystem::path vertex()
	{
		return "source/shaders/vertex/water.vs";
	}

	std::filesystem::path pixel()
	{
		return "source/shaders/pixel/water.ps";
	}
}


