#ifndef SHAD_DYN_MESH_H_
#define SHAD_DYN_MESH_H_

#include <filesystem>
#include <vector>

#include "d3d.h"
#include "bunkcore.h"

namespace shader
{
	namespace dynamicMesh
	{
		str name();
		std::filesystem::path vertex();
		std::filesystem::path pixel();
	}
}

#endif