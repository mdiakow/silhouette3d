#ifndef SHAD_INSTLINE_H_
#define SHAD_INSTLINE_H_

#include <filesystem>
#include <vector>

#include "d3d.h"
#include "bunkcore.h"

namespace vertex
{
	struct line
	{
		v3 position;
	};
}

namespace instance
{
	struct line
	{
		v3 position;
	};
}

namespace shader
{
	namespace instanced
	{
		namespace line
		{

			static const str name = "instLine";
			static const std::filesystem::path vertex = "source/shaders/vertex/instLine.vs";
			static const std::filesystem::path pixel = "source/shaders/pixel/line.ps";

			std::vector<D3D11_INPUT_ELEMENT_DESC> description();
		}
	}

}

#endif