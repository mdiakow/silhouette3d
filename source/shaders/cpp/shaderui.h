#ifndef SHADER_UI_H_
#define SHADER_UI_H_

#include <filesystem>
#include <vector>

#include "d3d.h"
#include "bunkcore.h"

namespace shader
{
	namespace ui
	{
		static const str name = "ui";
		static const std::filesystem::path vertex = "source/shaders/vertex/ui.vs";
		static const std::filesystem::path pixel = "source/shaders/pixel/texture.ps";

		class vertex
		{
			v3 position;
			v2 textureCoordinate;
		};		

		std::vector<D3D11_INPUT_ELEMENT_DESC> description() {
			std::vector<D3D11_INPUT_ELEMENT_DESC> vertexLayout(2);
			// create layout from VertexType structure in model and shader
			vertexLayout[0].SemanticName = "POSITION";
			vertexLayout[0].SemanticIndex = 0;
			vertexLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
			vertexLayout[0].InputSlot = 0;
			vertexLayout[0].AlignedByteOffset = 0;
			vertexLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
			vertexLayout[0].InstanceDataStepRate = 0;

			vertexLayout[1].SemanticName = "TEXCOORD";
			vertexLayout[1].SemanticIndex = 0;
			vertexLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
			vertexLayout[1].InputSlot = 0;
			vertexLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
			vertexLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
			vertexLayout[1].InstanceDataStepRate = 0;

			return vertexLayout;
		}
	}
}


#endif