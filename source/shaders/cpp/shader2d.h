#ifndef SHADER_2D_H_
#define SHADER_2D_H_

#include <filesystem>
#include <vector>

#include "d3d.h"
#include "bunkcore.h"

namespace vertex
{
	struct twod
	{
		v3 position;
		v2 textureCoordinate;
	};
}

namespace shader
{
	namespace twod
	{
		static const str name = "2d";
		static const std::filesystem::path vertex = "source/shaders/vertex/texture.vs";
		static const std::filesystem::path pixel = "source/shaders/pixel/texture.ps";
		
		std::vector<D3D11_INPUT_ELEMENT_DESC> description();
	}
}



#endif