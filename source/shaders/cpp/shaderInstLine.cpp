#include "shaders/cpp/shaderInstLine.h"

namespace shader
{
	namespace instanced
	{
		namespace line
		{
			std::vector<D3D11_INPUT_ELEMENT_DESC> description() {
				std::vector<D3D11_INPUT_ELEMENT_DESC> vertexLayout(2);

				vertexLayout[0] = {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0};
				// instance setup
				//vertexLayout[1] = {"COLOUR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1};
				vertexLayout[1] = {"INSTANCEPOSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1};

				return vertexLayout;
			}
		}
	}
}