#include "shaders/cpp/shaderParticle.h"

namespace shader
{

	namespace particle
	{
		str name() 
		{
			return "inst3d";
		}

		std::filesystem::path vertex()
		{
			return "source/shaders/vertex/particle.vs";
		}

		std::filesystem::path pixel()
		{
			return "source/shaders/pixel/particle.ps";
		}

		std::vector<D3D11_INPUT_ELEMENT_DESC> description() {
			std::vector<D3D11_INPUT_ELEMENT_DESC> vertexLayout(4);

			vertexLayout[0] = {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0};
			vertexLayout[1] = {"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0};
			vertexLayout[2] = {"NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0};
			// Instanced data
			vertexLayout[3] = {"INSTANCEINDEX", 0, DXGI_FORMAT_R32_UINT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1};

			return vertexLayout;
		}		
	}
}