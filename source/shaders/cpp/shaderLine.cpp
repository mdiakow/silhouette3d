#include "shaderLine.h"

namespace shader::line
{
	str name()
	{
		return "shaderLine";
	}

	std::filesystem::path vertex()
	{
		return "source/shaders/vertex/line.vs";
	}

	std::filesystem::path pixel()
	{
		return "source/shaders/pixel/line.ps";
	}

	std::vector<D3D11_INPUT_ELEMENT_DESC> description() {
		std::vector<D3D11_INPUT_ELEMENT_DESC> vertexLayout(1);

		vertexLayout[0] = {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0};

		return vertexLayout;
	}
}

