#ifndef SHADER_LINE_H_
#define SHADER_LINE_H_

#include "bunkcore.h"
#include <d3d11.h>

#include <filesystem>
#include <vector>

//namespace vertex
//{
//	struct line
//	{
//		v3 position;
//	};
//}

namespace shader::line
{
	str name();
	std::filesystem::path vertex();
	std::filesystem::path pixel();

	std::vector<D3D11_INPUT_ELEMENT_DESC> description();
}

#endif