#ifndef SHA_INS_LIGHT_H_
#define SHA_INS_LIGHT_H_

#include <filesystem>
#include <vector>

#include "d3d.h"
#include "bunkcore.h"

namespace vertex
{
	struct particle
	{
		v3 position;
		v2 textureCoordinate;
		v3 normal;

		// Instance data.
		ui32 instanceIndex;
	};	
}

namespace instance
{
	struct particle
	{
		ui32 index;
	};
}

namespace shader
{
	namespace particle
	{
		str name();
		std::filesystem::path vertex();
		std::filesystem::path pixel();

		std::vector<D3D11_INPUT_ELEMENT_DESC> description();	
	}

}


#endif