////////////////////////////////////////////////////////////////////////////////
// Filename: texture.vs
////////////////////////////////////////////////////////////////////////////////


/////////////
// GLOBALS //
/////////////
cbuffer projections : register(b0)
{
	matrix world;
	matrix viewPerspective;
	matrix viewOrthographic;
	matrix orthographic;
};

struct vertexInput
{
    float3 position : POSITION;
	float3 instancePosition : INSTANCEPOSITION;
};

struct pixelOutput
{
    float4 position : SV_POSITION;
};

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
pixelOutput vsMain(vertexInput input)
{
    pixelOutput output;
   	float3 p = input.position + input.instancePosition;
	float4 position = float4(p.xyz, 1);

    // UI elements are only concerned with the proper projection.
	output.position = mul(position, world);
	output.position = mul(output.position, viewPerspective);

    return output;
}
