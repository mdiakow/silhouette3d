/////////////////////////////////
// TABLES
///////////////////////////////////
namespace table
{
	static float4 quad[6] = {
		float4(-0.5, 0.0, -0.5, 1.0),
		float4(-0.5, 0.0, 0.5, 1.0),
		float4(0.5, 0.0, 0.5, 1.0),		
		float4(-0.5, 0.0, -0.5, 1.0),
		float4(0.5, 0.0, 0.5, 1.0),
		float4(0.5, 0.0, -0.5, 1.0)
	};

	static uint2 heightMapIndex[6] = {
		uint2(0,0),
		uint2(0,1),
		uint2(1,1),
		uint2(0,0),
		uint2(1,1),
		uint2(1,0)
	};
}

/////////////////////////////////
// CONSTANT BUFFER INPUT
///////////////////////////////////
cbuffer projections : register(b0)
{
	matrix world;
	matrix viewPerspective;
	matrix viewOrthographic;
	matrix orthographic;
	matrix view;
	float3 cameraRight;
	float pp1;
	float3 cameraUp;
	float pp2;
	float3 cameraPosition;
	float pp3;
};

cbuffer constBuffer : register(b1)
{
	float scale;
	float constPad[3];
};

///////////////////////////////////
// DATA STRUCTURES
///////////////////////////////////
struct vsIn
{
	uint instance : SV_InstanceID;
	uint vertex : SV_VertexID;
};

struct vsOut
{
	float4 position : SV_POSITION;
	float4 colour : COLOUR;
	float3 normal : NORMAL;
	float3 viewDirection : VIEWDIRECTION;
};

struct vertex
{
	float3 position;
};

//////////////////////////
// INPUT BUFFERS
//////////////////////////

Texture2D<float> heightMap : register(t0);

///////////////////////////////////
// HELPER FUNCTIONS
///////////////////////////////////


vsOut vsMain(vsIn indices) 
{
	uint width, height;
	heightMap.GetDimensions(width, height);

	vsOut output;
	float4 offset = float4(0.0, 0, 0, 1.0); // temp

	uint2 hmi = table::heightMapIndex[indices.vertex];

	uint ind = indices.instance;
	uint x = (ind % width);
	uint z = (ind / width);

	float4 instanceOffset = float4(x, 0, z, 1);

	output.position = (offset + instanceOffset + table::quad[indices.vertex])*scale;

	output.position.y = heightMap[uint2(x,z) + hmi] + 100;

	

	float3 tangent = float3(1, 0, heightMap[uint2(x - 1, z)] - heightMap[uint2(x + 1, z)]);
	float3 bitangent = float3(0, 1, heightMap[uint2(x, z - 1)] - heightMap[uint2(x, z + 1)]);
	float3 normal = normalize(cross(tangent, bitangent));

	output.normal = normal;
	
	float3 cameraForward = normalize(cross(cameraRight, cameraUp));

	output.position.w = 1;
	output.position = mul(output.position, viewPerspective);

	float3 viewDirection = normalize(cameraPosition - output.position);

	output.viewDirection = viewDirection;

	float alpha = abs(dot(normal, float3(0,1,0)));

	float3 green = float3(0,0.6,0.6);
	float3 blue = float3(0,0.1,0.7);

	//float3 colour = blue*alpha + (1-alpha)*green;
	float3 colour = green;
	output.colour = float4(colour, 1);
	
	return output;
}

