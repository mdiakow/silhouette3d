/////////////////////////////////
// CONSTANT BUFFER INPUT
///////////////////////////////////
cbuffer projections : register(b0)
{
	matrix world;
	matrix viewPerspective;
	matrix viewOrthographic;
	matrix orthographic;
	matrix view;
	float3 cameraRight;
	float pp1;
	float3 cameraUp;
	float pp2;
};

///////////////////////////////////
// DATA STRUCTURES
///////////////////////////////////
struct vsIn
{
	float3 position : POSITION;
	float2 textureCoordinate : TEXCOORD0;
	float3 normal : NORMAL;
	
	uint instanceIndex : INSTANCEINDEX;
};

struct vsOut
{
	float4 position : SV_POSITION;
	float2 textureCoordinate : TEXCOORD0;
	float3 normal : NORMAL;
	float4 colour: COLOUR;
};

struct transform
{
	float3 position;
	float scale;
};

struct particle
{
	float3 velocity;
	float lifetime;

	float scaleStart;
	float scaleEnd;

	float4 colour;
};

//////////////////////////
// INPUT BUFFERS
//////////////////////////

StructuredBuffer<transform> particleTransforms : register(t0);
StructuredBuffer<particle> particleData : register(t1);

///////////////////////////////////
// HELPER FUNCTIONS
///////////////////////////////////

vsOut vsMain(vsIn input) 
{
	vsOut output;

	transform t = particleTransforms[input.instanceIndex];
	particle p = particleData[input.instanceIndex];

	output.position.xyz = t.position + cameraRight*input.position.x*t.scale + cameraUp*input.position.y*t.scale;
	output.position.w = 1.f;

	output.position = mul(output.position, viewPerspective);
	output.textureCoordinate = input.textureCoordinate;

	output.colour = p.colour;

	output.normal = mul(input.normal, world);
	output.normal = normalize(output.normal);

	return output;
}