cbuffer projections : register(b0)
{
	matrix world;
	matrix viewPerspective;
	matrix viewOrthographic;
};


struct vsIn
{
	float3 position : POSITION;
	float2 textureCoordinate : TEXCOORD0;
	float3 normal : NORMAL;
	float3 tangent: TANGENT;
	float3 binormal : BINORMAL;
	float2 padding : PADDING;	
};


struct vsOut
{
	float4 position : SV_POSITION;
	float2 textureCoordinate : TEXCOORD0;
	float3 normal : NORMAL;
};

vsOut vsMain(vsIn input) 
{
	vsOut output;

	float4 inPosition = {input.position, 1.0};
	float4 inNormal = {input.normal, 1.0};
	output.position = mul(inPosition, world);
	output.position = mul(output.position, viewPerspective);
	//output.position = mul(output.position, view);
	//output.position = mul(output.position, projection);

	output.textureCoordinate = input.textureCoordinate;

	output.normal = mul(input.normal, world);
	output.normal = normalize(output.normal);

	return output;
}
