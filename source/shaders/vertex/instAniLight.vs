struct dualQuaternion
{
	float4 rotation; // In quaternions
	float4 translation; 
};

StructuredBuffer<dualQuaternion> orientations;

cbuffer projections : register(b0)
{
	matrix world;
	matrix viewPerspective;
	matrix viewOrthographic;
};

struct vsIn
{
	float3 position : POSITION;
	float2 textureCoordinate : TEXCOORD0;
	float3 normal : NORMAL;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
	float2 weightValues : WEIGHTVALS;
	int2 weightIndices : WEIGHTINDICES;

	uint orientationIndex : ORIENTATIONINDEX; // This would be the root of the current skeleton.
};

struct vsOut
{
	float4 position : SV_POSITION;
	float2 textureCoordinate : TEXCOORD0;
	float3 normal : NORMAL;
};

// Quaternion q and position p.
float3x3 quat2m3(float4 q)
{
	float3x3 r;

	float xx = 2.f*q.x*q.x;
	float yy = 2.f*q.y*q.y;
	float zz = 2.f*q.z*q.z;
	float ww = 2.f*q.w*q.w;

	float xy = 2.f*q.x*q.y;
	float xz = 2.f*q.x*q.z;
	float xw = 2.f*q.x*q.w;
	float yz = 2.f*q.y*q.z;
	float yw = 2.f*q.y*q.w;
	float zw = 2.f*q.z*q.w;

	r[0][0] = 1.f - yy - zz;
	r[0][1] = xy - zw;
	r[0][2] = xz + yw;

	r[1][0] = xy + zw;
	r[1][1] = 1.0 - xx - zz;
	r[1][2] = yz - xw;

	r[2][0] = xz - yw;
	r[2][1] = yz - xw;
	r[2][2] = 1.0 - xx - yy;

	return r;
}

float4 quatMul(float4 q1, float4 q2)
{
	return float4(q1.w*q2.xyz + q2.w*q1.xyz + cross(q1.xyz, q2.xyz), q1.w*q2.w - dot(q1.xyz, q2.xyz));
}

float4 quatConj(float4 q)
{
	return float4(-q.xyz, q.w);
}
// Get the translation component back from the dual quaternion
float4 dqTranslation(dualQuaternion dq)
{
	float4 output;

	output = quatMul((dq.translation * 2), quatConj(dq.rotation));
	output.w = 1;

	return output;
}

float4 dqRotation(dualQuaternion dq) 
{
	return dq.rotation;
}

// Apply the transform on the point using dq
float4 dqTransform(dualQuaternion dq, float4 pt)
{
	float4 t = dqTranslation(dq);
	pt.w = 0;
	//return t + quatMul(dq.rotation, quatMul(pt, quatConj(dq.rotation)));
	return t + quatMul(quatMul(dq.rotation, pt), quatConj(dq.rotation));
}

dualQuaternion dqAdd(dualQuaternion lhs, dualQuaternion rhs)
{
	dualQuaternion dqOut;
	dqOut.rotation = lhs.rotation + rhs.rotation;
	dqOut.translation = lhs.translation + rhs.translation;
	return dqOut;
}

dualQuaternion dqNormalize(dualQuaternion dq)
{
	dualQuaternion dqOut;
	dq.rotation = normalize(dq.rotation);
	float invSqLen = 1.f/dot(dq.rotation, dq.rotation);
	dqOut.rotation = dq.rotation * invSqLen;
	dqOut.translation = dq.translation * invSqLen;
	return dqOut;
}

vsOut vsMain(vsIn input) 
{
	vsOut output;
	float4 outPos;
	float4 inPos = float4(input.position, 1.f);

	dualQuaternion q1 = orientations[input.orientationIndex + input.weightIndices.x];
	dualQuaternion q2 = orientations[input.orientationIndex + input.weightIndices.y];

	float weight1 = input.weightValues.x;
	float weight2 = input.weightValues.y;

	q1.rotation *= weight1;
	q1.translation *= weight1;

	q2.rotation *= weight2;
	q2.translation *= weight2;

	dualQuaternion q3 = dqNormalize(dqAdd(q1, q2));

	//outPos = dqTransform(orientations[input.orientationIndex + input.weightIndices.x], inPos);
	//outPos = inPos + dqTranslation(orientations[input.orientationIndex]);
	outPos = dqTransform(q3, inPos);

	outPos.w = 1.0;
	output.position = mul(outPos, world);
	//output.position = mul(output.position, view);
	//output.position = mul(output.position, projection);

	output.position = mul(output.position, viewPerspective);
	output.textureCoordinate = input.textureCoordinate;

	output.normal = mul(input.normal, (float3x3)world);
	output.normal = normalize(output.normal);

	return output;
}
