///////////////////////////////////
// CONSTANT BUFFER INPUT
///////////////////////////////////
cbuffer projections : register(b0)
{
	matrix world;
	matrix viewPerspective;
	matrix viewOrthographic;
	matrix orthographic;
	matrix view;
	float3 cameraRight;
	float pp1;
	float3 cameraUp;
	float pp2;
};

///////////////////////////////////
// DATA STRUCTURES
///////////////////////////////////
struct vsOut
{
	float4 position : SV_POSITION;
	float2 textureCoordinate : TEXCOORD0;
	float3 normal : NORMAL;
	float4 colour: COLOUR;
};

struct vertex
{
	float3 position;
	float2 textureCoordinate;
	float3 normal;
	float3 tangent;
	float3 binormal;
	float2 padding;
};

//////////////////////////
// INPUT BUFFERS
//////////////////////////

StructuredBuffer<vertex> vertices : register(t0);

///////////////////////////////////
// HELPER FUNCTIONS
///////////////////////////////////

vsOut vsMain(uint v : SV_VertexID) 
{
	vsOut output;
	
	vertex input = vertices[v];

	output.position.xyz = input.position;

	output.position.w = 1.f;

	output.position = mul(output.position, viewPerspective);
	output.textureCoordinate = input.textureCoordinate;

	output.normal = mul(input.normal, world);
	output.normal = normalize(output.normal);

	return output;
}

