////////////////////////////////////////////////////////////////////////////////
// Filename: texture.vs
////////////////////////////////////////////////////////////////////////////////


/////////////
// GLOBALS //
/////////////
cbuffer projections : register(b0)
{
	matrix world;
	matrix viewPerspective;
	matrix viewOrthographic;
	matrix orthographic;
};

//////////////
// TYPEDEFS //
//////////////
struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
};

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputType vsMain(VertexInputType input)
{
    PixelInputType output;
   
    input.position.w = 1.0f;
	// Prevents camera from clipping UI elements
	input.position.z += 10.f;

    // UI elements are only concerned with the proper projection.
	output.position = mul(input.position, orthographic);

    // Store the texture coordinates for the pixel shader.
    output.tex = input.tex;

    return output;
	
}
