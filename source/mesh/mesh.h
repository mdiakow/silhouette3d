#ifndef MESH_H_
#define MESH_H_
#include "bunkcore.h"
#include "mesh/meshdata.h"
#include "mesh/meshTriangle.h"
#include "mesh/meshLine.h"
#include "mesh/meshvertex.h"
#include "math/linalg.h"

#include <memory>
#include "shaders/cpp/shaderLight.h"

namespace mesh
{
	// Gives that positions and triangle layouts for a cube.
	// Given vertex type must have .position attribute.


	namespace cube
	{
		ui32 *indices();


		template <class vertex>
		vertex *vertices(fl32 scale, v3 position)
		{
			vertex *vertices = new vertex[8];

			// Create vertex data.
			v3 frontBottomLeft = position;
			v3 frontTopLeft = position + v3(0.0f, 1.0f, 0.0f);
			v3 frontBottomRight = position + v3(1.0f, 0.0f, 0.0f);
			v3 frontTopRight = position + v3(1.0f, 1.0f, 0.0f);

			v3 backBottomLeft = position + v3(0.0f, 0.0f, 1.0f);
			v3 backTopLeft = position + v3(0.0f, 1.0f, 1.0f);
			v3 backBottomRight = position + v3(1.0f, 0.0f, 1.0f);
			v3 backTopRight = position + v3(1.0f, 1.0f, 1.0f);

			vertices[0].position = scale*frontBottomLeft;
			vertices[1].position = scale*frontTopLeft;
			vertices[2].position = scale*frontBottomRight;
			vertices[3].position = scale*frontTopRight;
			vertices[4].position = scale*backBottomLeft;
			vertices[5].position = scale*backTopLeft;
			vertices[6].position = scale*backBottomRight;
			vertices[7].position = scale*backTopRight;

			return vertices;
		}

		mesh::data<vertex::light> *light(v3 position, fl32 scale);
		
	}

	namespace sphere
	{
		mesh::data<vertex::light> *light(v3 position, fl32 radius, ui32 verticesPerSemiCircle);
	}	

	mesh::data<vertex::light> *splinePatch();
	
	mesh::triangle<vertex::light> *splinePatch(linalg::bezier<v3> b1, linalg::bezier<v3> b2, linalg::bezier<v3> b3, linalg::bezier<v3> b4);
	//mesh::triangle<vertex::light> *quad(v3 lowerLeft, v3 topRight);
	std::unique_ptr<mesh::triangle<vertex::light>> quad(v3 lowerLeft, v3 topRight);
}


#endif