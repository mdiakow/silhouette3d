#include "meshSpline.h"

mesh::spline::spline(vertex::spline *vertices, ui32 *indices, ui32 indexCount, ui32 vertexCount)
{
	this->vertices = vertices;
	this->indices = indices;

	indCount = indexCount;
	vertCount = vertexCount;
}



mesh::spline::~spline()
{
	delete vertices;
	delete indices;
}

ui32 mesh::spline::vertexCount()
{
	return vertCount;
}
ui32 mesh::spline::indexCount()
{
	return indCount;
}

vertex::spline * mesh::spline::getVertices()
{
	return vertices;
}

ui32 * mesh::spline::getIndices()
{
	return indices;
}

