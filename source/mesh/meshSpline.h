#ifndef MESH_SPLINE_H_
#define MESH_SPLINE_H_

#include "bunkcore.h"
//#include "linalg.h"

#include "D3D.h"

namespace vertex
{
	struct spline
	{
		v3 position;
		v4 colour;
	};

	
}


namespace mesh
{
	class spline
	{
	public:
		spline(vertex::spline *vertices, ui32 *indices, ui32 indexCount, ui32 vertexCount);
		~spline();

		ui32 vertexCount();
		ui32 indexCount();

		vertex::spline *getVertices();
		ui32 *getIndices();
	private:
		vertex::spline *vertices;
		ui32 *indices;

		ui32 indCount, vertCount;
	};
}



#endif
