#ifndef MESH_QUAD_H_
#define MESH_QUAD_H_

#include "bunkcore.h"

#include "meshData.h"
#include "meshVertex.h"

namespace mesh
{
	std::pair<ID3D11Buffer * , ID3D11Buffer *> quad(fl32 width, fl32 height, v3 position);
}

#endif