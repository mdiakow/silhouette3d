#include "mesh/mesh.h"
#include "math/bunkmath.h"
#include "math/linalg.h"
#include "debugger/bunkdebug.h"

// utility functions for calculations
static v3 sphereCoord(fl32 radius, fl32 rotationXY, fl32 rotationYZ);
static std::pair<v3,v3> tangentBinormal(vertex::light t1, vertex::light t2, vertex::light t3);

ui32 * mesh::cube::indices()
{
	ui32 *indices = new ui32[12 * 3];

	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;

	indices[3] = 3;
	indices[4] = 2;
	indices[5] = 1;
	// Right Quad
	indices[6] = 2;
	indices[7] = 3;
	indices[8] = 6;

	indices[9] = 7;
	indices[10] = 6;
	indices[11] = 3;
	// Back Quad
	indices[12] = 6;
	indices[13] = 7;
	indices[14] = 4;

	indices[15] = 5;
	indices[16] = 4;
	indices[17] = 7;
	// Left Quad
	indices[18] = 0;
	indices[19] = 4;
	indices[20] = 1;

	indices[21] = 4;
	indices[22] = 5;
	indices[23] = 1;
	// Top Quad
	indices[24] = 1;
	indices[25] = 5;
	indices[26] = 3;

	indices[27] = 5;
	indices[28] = 7;
	indices[29] = 3;
	// Bottom Quads
	indices[30] = 0;
	indices[31] = 2;
	indices[32] = 4;

	indices[33] = 4;
	indices[34] = 2;
	indices[35] = 6;

	return indices;
}

mesh::data<vertex::light> *mesh::cube::light(v3 position, fl32 scale)
{
	vertex::light *vertices = new vertex::light[4*6];
	ui32 *indices = new ui32[4*6*2];

	ui32 ci = 0;
	ui32 cv = 0;
	for (ui32 i = 0; i < 6; i++) {
		vertices[cv].position = v3(0.f,0.f,0.f)*scale;
		vertices[cv].normal = v3(0.f,0.f,-1.f);

		vertices[cv+1].position = v3(0.f,1.f,0.f)*scale;
		vertices[cv+1].normal = v3(0.f,0.f,-1.f);

		vertices[cv+2].position = v3(1.f,0.f,0.f)*scale;
		vertices[cv+2].normal = v3(0.f,0.f,-1.f);

		vertices[cv+3].position = v3(1.f,1.f,0.f)*scale;
		vertices[cv+3].normal = v3(0.f,0.f,-1.f);

		indices[ci] = cv;
		indices[ci+1] = cv+1;
		indices[ci+2] = cv+2;

		indices[ci+3] = cv;
		indices[ci+4] = cv+3;
		indices[ci+5] = cv+1;
		
		cv += 4;
		ci += 6;
	}

	mesh::data<vertex::light> *cube = new mesh::data<vertex::light>(vertices, indices, 4*6, 4*6*2);

	delete[] vertices;
	delete[] indices;

	return cube;
}

mesh::data<vertex::light>* mesh::sphere::light(v3 position, fl32 radius, ui32 verticesPerSemiCircle)
{

	return nullptr;
}

mesh::data<vertex::light>* mesh::splinePatch()
{
	linalg::bezier<v3> bx;
	// First spline (X)
	bx.start = v3(0.f, 0.f, 0.f);
	bx.end = v3(500.f, 0.f, 500.f);
	bx.startControl = v3(500.f, 0.f, 0.f);
	bx.endControl = v3(500.f, 0.f, 0.f);

	
	// Second spline (Y)
	linalg::bezier<v3> by;
	by.start = v3(0.f, 0.f, 0.f);
	by.end = v3(0.f, 500.f, 0.f);
	by.startControl = v3(0.f, 100.f, 0.f);
	by.endControl = v3(0.f, 400.f, 0.f);

	const ui32 pointsPerSpline = 30;
	//v3 pointsX[pointsPerSpline];
	//v3 pointsY[pointsPerSpline];

	vertex::light pointsX[pointsPerSpline];
	vertex::light pointsY[pointsPerSpline];

	vertex::light previousX, previousY;
	
	fl32 t = 0.f;
	fl32 tStep = 1.0f/(fl32)(pointsPerSpline-1);

	t += tStep;
	for (ui32 i = 0; i < pointsPerSpline; i++) {
		pointsX[i].position = bx.calculate(t);
		pointsY[i].position = by.calculate(t);

		pointsX[i].tangent = bx.differentiate1(t).normalized();
		pointsY[i].tangent = by.differentiate1(t).normalized();

		pointsX[i].normal = -pointsX[i].tangent.cross(linalg::lefthand::up).normalized();
		pointsY[i].normal = -pointsY[i].tangent.cross(linalg::lefthand::up).normalized();
		
		previousX = pointsX[i];
		previousY = pointsY[i];

		t += tStep;
	}

	ui32 vertexCount = pointsPerSpline*pointsPerSpline*4;
	ui32 indexCount = (vertexCount * 6)/4;
	vertex::light *vertices = new vertex::light[vertexCount];
	ui32 *indices = new ui32[indexCount];
	ui32 cv = 0;
	ui32 ci = 0;

	fl32 textureScale = 1.f/64.f;
	// Texture coordinates of the four corners of the quad.
	
	v2 bottomLeft = v2(0.f,0.f);
	v2 topLeft, topRight, bottomRight;
	v2 loopTopLeft;

	v3 normTest = v3(0.f, 0.f, -1.f);

	for (ui32 i = 0; i < pointsPerSpline-1; i++) {
		vertices[cv].position = pointsX[0].position + pointsY[i].position;
		vertices[cv].textureCoordinate = bottomLeft;
		//vertices[cv].normal = (pointsX[0].normal + pointsY[i].normal).normalized();
		vertices[cv].normal = pointsX[0].normal;

		vertices[cv+2].position = pointsX[1].position + pointsY[i].position;
		fl32 d20 = (vertices[cv+2].position - vertices[cv].position).norm() * textureScale;
		bottomRight = bottomLeft + v2(d20, 0.f);
		vertices[cv+2].textureCoordinate = bottomRight;
		//vertices[cv+2].normal = (pointsX[1].normal + pointsY[i].normal).normalized();
		vertices[cv+2].normal = pointsX[1].normal;

		vertices[cv+3].position = pointsX[0].position + pointsY[i+1].position;
		fl32 d30 = (vertices[cv+3].position - vertices[cv].position).norm()*textureScale;
		topLeft = bottomLeft + v2(0.f, d30);
		vertices[cv+3].textureCoordinate = topLeft;
		//vertices[cv+3].normal = (pointsX[0].normal + pointsY[i+1].normal).normalized();
		vertices[cv+3].normal = pointsX[0].normal;

		vertices[cv+1].position = pointsX[1].position + pointsY[i+1].position;
		fl32 d12 = (vertices[cv+1].position - vertices[cv+2].position).norm()*textureScale;
		fl32 d13 = (vertices[cv+1].position - vertices[cv+3].position).norm()*textureScale;
		topRight = bottomLeft + v2(d13, d12);
		vertices[cv+1].textureCoordinate = topRight;
		//vertices[cv+1].normal = (pointsX[1].normal + pointsY[i+1].normal).normalized();
		vertices[cv+1].normal = pointsX[1].normal;

		indices[ci] = cv;
		indices[ci+1] = cv+1;
		indices[ci+2] = cv+2;

		indices[ci+3] = cv+1;
		indices[ci+4] = cv;
		indices[ci+5] = cv+3;

		cv += 4;
		ci += 6;
		loopTopLeft = topLeft;
		bottomLeft = bottomRight;
		topLeft = topRight;
		for (ui32 j = 1; j < pointsPerSpline-1; j++) {
			vertices[cv].position = pointsX[j].position + pointsY[i].position;
			vertices[cv].textureCoordinate = bottomLeft;
			//vertices[cv].normal = (pointsX[j].normal + pointsY[i].normal).normalized();
			vertices[cv].normal = pointsX[j].normal;

			vertices[cv+2].position = pointsX[j+1].position + pointsY[i].position;
			fl32 d20 = (vertices[cv+2].position - vertices[cv].position).norm() * textureScale;
			bottomRight = bottomLeft + v2(d20, 0.f);
			vertices[cv+2].textureCoordinate = bottomRight;
			//vertices[cv+2].normal = (pointsX[j+1].normal + pointsY[i].normal).normalized();
			vertices[cv+2].normal = pointsX[j+1].normal;

			vertices[cv+3].position = pointsX[j].position + pointsY[i+1].position;
			//fl32 d30 = (vertices[cv+3].position - vertices[cv].position).norm()*textureScale;
			//topLeft = bottomLeft + v2(0.f, d30);
			vertices[cv+3].textureCoordinate = topLeft;
			//vertices[cv+3].normal = (pointsX[j].normal + pointsY[i+1].normal).normalized();
			vertices[cv+3].normal = pointsX[j].normal;

			vertices[cv+1].position = pointsX[j+1].position + pointsY[i+1].position;
			fl32 d12 = (vertices[cv+1].position - vertices[cv+2].position).norm()*textureScale;
			fl32 d13 = (vertices[cv+1].position - vertices[cv+3].position).norm()*textureScale;
			topRight = bottomLeft + v2(d13, d12);
			vertices[cv+1].textureCoordinate = topRight;
			//vertices[cv+1].normal = (pointsX[j+1].normal + pointsY[i+1].normal).normalized();
			vertices[cv+1].normal = pointsX[j+1].normal;
			
			indices[ci] = cv;
			indices[ci+1] = cv+1;
			indices[ci+2] = cv+2;

			indices[ci+3] = cv+1;
			indices[ci+4] = cv;
			indices[ci+5] = cv+3;

			cv += 4;
			ci += 6;

			bottomLeft = bottomRight;
			topLeft = topRight;
		}
		bottomLeft = loopTopLeft;
	}

	return new mesh::data<vertex::light>(vertices, indices, vertexCount, indexCount);
}


static v3 blerp(v3 p00, v3 p10, v3 p01, v3 p11, fl32 w1, fl32 w2);


mesh::triangle<vertex::light>* mesh::splinePatch(linalg::bezier<v3> b1, linalg::bezier<v3> b2, linalg::bezier<v3> b3, linalg::bezier<v3> b4)
{
	const ui32 divCount = 5;
	
	// For initial calculateions of the vertices (Position, Normal, Binorm, etc..).
	vertex::light init[divCount][divCount];
	v3 positions[divCount][divCount];
	
	ui32 cv = 0;

	fl32 w1 = 0.f;
	fl32 w2 = 0.f;

	fl32 t = 0.f;
	fl32 s = 0.f;
	fl32 tsStep = 1.f/(fl32)(divCount-1);

	// Calculate quads based on the four splines. The w1,w2 are weight values to weight the points between them.
	for (ui32 i = 0; i < divCount; i++) {
		t = 0.f;
		for (ui32 j = 0; j < divCount; j++) {
			// Steven A. Coons patch
			fl32 dt = 1.f - t;
			fl32 ds = 1.f - s;
			init[i][j].position = dt*b1.calculate(s) + t*b3.calculate(s) + ds*b2.calculate(t) + s*b4.calculate(t) - (ds*dt*b1.calculate(0.f) + s*dt*b1.calculate(1.f) + ds*t*b3.calculate(0.f) + s*t*b3.calculate(1.f));
			init[i][j].tangent = (dt*b1.differentiate1(s) + t*b3.differentiate1(s) + ds*b2.differentiate1(t) + s*b4.differentiate1(t) - (ds*dt*b1.differentiate1(0.f) + s*dt*b1.differentiate1(1.f) + ds*t*b3.differentiate1(0.f) + s*t*b3.differentiate1(1.f))).normalized();
			init[i][j].normal = -init[i][j].tangent.cross(linalg::lefthand::up).normalized();

			cv++;
			t += tsStep;
		}
		s += tsStep;
	}
	ui32 vertexCount = divCount*divCount*4;
	ui32 indexCount = (vertexCount * 6)/4;

	ui32 *indices = new ui32[indexCount];
	vertex::light *vertices = new vertex::light[vertexCount];

	cv = 0;
	ui32 ci = 0;

	for (ui32 i = 0; i < divCount-1; i++) {
		for (ui32 j = 0; j < divCount-1; j++) {
			vertices[cv] = init[i][j];
			vertices[cv].textureCoordinate = v2(0.f, 0.f);

			vertices[cv+2] = init[i+1][j];
			vertices[cv+2].textureCoordinate = v2(1.f, 0.f);

			vertices[cv+3] = init[i][j+1];
			vertices[cv+3].textureCoordinate = v2(0.f, 1.f);

			vertices[cv+1] = init[i+1][j+1];
			vertices[cv+1].textureCoordinate = v2(1.f, 1.f);

			indices[ci] = cv;
			indices[ci+1] = cv+1;
			indices[ci+2] = cv+2;

			indices[ci+3] = cv+1;
			indices[ci+4] = cv;
			indices[ci+5] = cv+3;

			cv += 4;
			ci += 6;
		}
	}

	auto mesh = new mesh::triangle<vertex::light>(vertices, indices, vertexCount, indexCount);

	delete[] vertices;
	delete[] indices;

	return mesh;
}
std::unique_ptr<mesh::triangle<vertex::light>> mesh::quad(v3 lowerLeft, v3 topRight)
{
	ui32 vertexCount = 4;
	ui32 indexCount = 6;
	auto indices = std::make_unique<ui32[]>(indexCount);
	//ui32 *indices = new ui32[indexCount];
	//vertex::light *vertices = new vertex::light[vertexCount];
	auto vertices = std::make_unique<vertex::light[]>(vertexCount);
	
	v3 normal = v3(0.f, 1.f, 0.f);
	v3 topLeft = v3(lowerLeft.x(), topRight.y(), topRight.z());
	v3 bottomRight = v3(topRight.x(), lowerLeft.y(), lowerLeft.z());
	vertices[0].position = lowerLeft;
	vertices[0].textureCoordinate = v2(0.f, 0.f);
	vertices[0].normal = normal;

	vertices[2].position = topLeft;
	vertices[2].textureCoordinate = v2(1.f, 0.f);
	vertices[2].normal = normal;

	vertices[3].position = bottomRight;
	vertices[3].textureCoordinate = v2(0.f, 1.f);
	vertices[3].normal = normal;

	vertices[1].position = topRight;
	vertices[1].textureCoordinate = v2(1.f, 1.f);
	vertices[1].normal = normal;

	indices[0] = 2;
	indices[1] = 1;
	indices[2] = 0;

	indices[3] = 3;
	indices[4] = 0;
	indices[5] = 1;
	   
	auto mesh = std::make_unique<mesh::triangle<vertex::light>>(vertices.get(), indices.get(), vertexCount, indexCount);

	return mesh;
}



/*
vertices[cv].position = positions[cv];
indices[ci] = ci;
cv++;
ci++;
vertices[cv].position = positions[cv];
vertices[cv].textureCoordinate = v2(0.f, 0.f);

vertices[cv+2].position = positions[cv+divCount];
vertices[cv+2].textureCoordinate = v2(1.f, 0.f);

vertices[cv+3].position = positions[cv+1];
vertices[cv+3].textureCoordinate = v2(0.f, 1.f);

vertices[cv+1].position = positions[cv+divCount+1];
vertices[cv+1].textureCoordinate = v2(1.f, 1.f);

indices[ci] = cv;
indices[ci+1] = cv+1;
indices[ci+2] = cv+2;

indices[ci+3] = cv+1;
indices[ci+4] = cv;
indices[ci+5] = cv+3;

cv += 4;
ci += 6;
*/

v3 blerp(v3 p00, v3 p10, v3 p01, v3 p11, fl32 w1, fl32 w2)
{
	fl32 dw1 = (1.f-w1);
	fl32 dw2 = (1.f-w2);

	return (p01*dw1*dw2) + (p11*dw2*w1) + (p10*dw1*w2) + (p00*w1*w2);
}

// WTF experiements:
// Gives correct corners, however points tend to be more prominent towards the end of the splines. I don't
// expect event spacing but even a straight line tends to bunch the points towards the end of the spline.
// This distortion carries in to the middle of the mesh.
// return (p00*dw1*dw2) + (p10*dw2*w1) + (p11*dw1*w2) + (p01*w1*w2);
//
// Below gives a correct lower left and top left, but the top right is misplaced, bottom right is compressed looking
// return (p11*dw1*dw2) + (p10*dw2*w1) + (p01*dw1*w2) + (p00*w1*w2); 


v3 sphereCoord(fl32 radius, fl32 rotationXY, fl32 rotationYZ)
{
	fl32 x = radius*cosf(rotationXY)*sinf(rotationYZ);
	fl32 y = radius*sinf(rotationXY)*sinf(rotationYZ);
	fl32 z = radius*cosf(rotationYZ);

	return v3(x,y,z);
}

std::pair<v3, v3> tangentBinormal(vertex::light t1, vertex::light t2, vertex::light t3)
{
	v3 vec1 = t2.position - t1.position;
	v3 vec2 = t3.position - t1.position;

	v2 tc1 = t2.textureCoordinate - t1.textureCoordinate;
	v2 tc2 = t3.textureCoordinate - t1.textureCoordinate;

	v2 tu = v2(tc1(0), tc2(0));
	v2 tv = v2(tc1(1), tc2(1));

	fl32 den = 1.0f/(tu(0)*tv(1) - tu(1)*tv(0));

	v3 tangent, binormal;

	tangent(0) = (tv(1)*vec1(0) - tv(0)*vec2(0))*den;
	tangent(1) = (tv(1)*vec1(1) - tv(0)*vec2(1))*den;
	tangent(2) = (tv(1)*vec1(2) - tv(0)*vec2(2))*den;

	binormal(0) = (tu(0)*vec2(0) - tu(1)*vec1(0))*den;
	binormal(1) = (tu(0)*vec2(1) - tu(1)*vec1(1))*den;
	binormal(2) = (tu(0)*vec2(2) - tu(1)*vec1(2))*den;

	tangent.normalize();
	binormal.normalize();

	return std::make_pair(tangent, binormal);
}


/* 
vertices[cv].position = blerp(b1Points[j].position, b2Points[i].position, b3Points[j].position, b4Points[i].position, w2, w1);
vertices[cv].textureCoordinate = v2(0.f, 0.f);

vertices[cv+2].position = blerp(b1Points[j].position, b2Points[i+1].position, b3Points[j].position, b4Points[i+1].position, w2, w1+tStep);
vertices[cv+2].textureCoordinate = v2(1.f, 0.f);

vertices[cv+3].position = blerp(b1Points[j+1].position, b2Points[i].position, b3Points[j+1].position, b4Points[i].position, w2+tStep, w1);
vertices[cv+3].textureCoordinate = v2(0.f, 1.f);

vertices[cv+1].position = blerp(b1Points[j+1].position, b2Points[i+1].position, b3Points[j+1].position, b4Points[i+1].position, w2+tStep, w1+tStep);
vertices[cv+1].textureCoordinate = v2(1.f, 1.f);
*/

/* SIMPLE ADDITION: WORKS BUT DOESNT RESTRICT ITSELF TO SPLINES AROUND EDGE
vertices[cv].position = b1Points[j].position + b2Points[i].position + b3Points[j].position +b4Points[i].position;
vertices[cv].textureCoordinate = v2(0.f, 0.f);

vertices[cv+2].position = b1Points[j].position + b2Points[i+1].position + b3Points[j].position + b4Points[i+1].position;
vertices[cv+2].textureCoordinate = v2(1.f, 0.f);

vertices[cv+3].position = b1Points[j+1].position + b2Points[i].position + b3Points[j+1].position + b4Points[i].position;
vertices[cv+3].textureCoordinate = v2(0.f, 1.f);

vertices[cv+1].position = b1Points[j+1].position + b2Points[i+1].position + b3Points[j+1].position + b4Points[i+1].position;
vertices[cv+1].textureCoordinate = v2(1.f, 1.f);

// OOOOOLD
vertices[cv].position = (1.f-w2)*b1Points[i].position + (1.f-w1)*b2Points[j].position + (w2)*b3Points[i].position + (w1)*b4Points[j].position;
vertices[cv].textureCoordinate = v2(0.f, 0.f);

vertices[cv+2].position = (1.f-w2)*b1Points[i].position + (1.f-w1)*b2Points[j+1].position + (w2)*b3Points[i].position + (w1)*b4Points[j+1].position;
vertices[cv+2].textureCoordinate = v2(1.f, 0.f);

vertices[cv+3].position = (1.f-w2)*b1Points[i+1].position + (1.f-w1)*b2Points[j].position + (w2)*b3Points[i+1].position + (w1)*b4Points[j].position;
vertices[cv+3].textureCoordinate = v2(0.f, 1.f);

vertices[cv+1].position = (1.f-w2)*b1Points[i+1].position + (1.f-w1)*b2Points[j+1].position + (w2)*b3Points[i+1].position + (w1)*b4Points[j+1].position;
vertices[cv+1].textureCoordinate = v2(1.f, 1.f);
*/


// DEBUG
/*
debug::log(vertices[cv].position);
debug::log(vertices[cv+2].position);
debug::log(vertices[cv+3].position);
debug::log(vertices[cv+1].position);
debug::log("END QUAD");
*/

//debug::log("END ROW");


// AKL:SKL:DFKL:ASDKL:AS:DKL:
//return (p00*dw1*dw2) + (p10*dw2*w1) + (p01*dw1*w2) + (p11*w1*w2);
//return (p00*dw1*dw2) + (p10*dw2*w1) + (p11*dw1*w2) + (p01*w1*w2);
//return (p00*dw1*dw2) + (p01*dw2*w1) + (p10*dw1*w2) + (p11*w1*w2);
//return (p00*dw1*dw2) + (p01*dw2*w1) + (p11*dw1*w2) + (p10*w1*w2);
//return (p00*dw1*dw2) + (p11*dw2*w1) + (p10*dw1*w2) + (p01*w1*w2);
//return (p00*dw1*dw2) + (p11*dw2*w1) + (p01*dw1*w2) + (p10*w1*w2);

//return (p10*dw1*dw2) + (p00*dw2*w1) + (p01*dw1*w2) + (p11*w1*w2);
//return (p10*dw1*dw2) + (p00*dw2*w1) + (p11*dw1*w2) + (p01*w1*w2);
//return (p10*dw1*dw2) + (p01*dw2*w1) + (p00*dw1*w2) + (p11*w1*w2); // Distorted but correct
//return (p10*dw1*dw2) + (p01*dw2*w1) + (p11*dw1*w2) + (p00*w1*w2);
//return (p10*dw1*dw2) + (p11*dw2*w1) + (p00*dw1*w2) + (p01*w1*w2);
//return (p10*dw1*dw2) + (p11*dw2*w1) + (p01*dw1*w2) + (p00*w1*w2);

//return (p01*dw1*dw2) + (p00*dw2*w1) + (p10*dw1*w2) + (p11*w1*w2);
//return (p01*dw1*dw2) + (p00*dw2*w1) + (p11*dw1*w2) + (p10*w1*w2);
//return (p01*dw1*dw2) + (p10*dw2*w1) + (p00*dw1*w2) + (p11*w1*w2); 
//return (p01*dw1*dw2) + (p10*dw2*w1) + (p11*dw1*w2) + (p00*w1*w2);
//return (p01*dw1*dw2) + (p11*dw2*w1) + (p00*dw1*w2) + (p10*w1*w2);
//return (p01*dw1*dw2) + (p11*dw2*w1) + (p10*dw1*w2) + (p00*w1*w2); // Distorted but correct

//return (p11*dw1*dw2) + (p00*dw2*w1) + (p01*dw1*w2) + (p10*w1*w2); // Edges are wrong but middle of mesh looks "more" correct
//return (p11*dw1*dw2) + (p00*dw2*w1) + (p10*dw1*w2) + (p01*w1*w2);
//return (p11*dw1*dw2) + (p01*dw2*w1) + (p00*dw1*w2) + (p10*w1*w2); 
//return (p11*dw1*dw2) + (p01*dw2*w1) + (p10*dw1*w2) + (p00*w1*w2);
//return (p11*dw1*dw2) + (p10*dw2*w1) + (p00*dw1*w2) + (p01*w1*w2);
//return (p11*dw1*dw2) + (p10*dw2*w1) + (p01*dw1*w2) + (p00*w1*w2);


// OLD MESH - Interal pieces seem entirely wrong. Think blerping is incorrect
/*
w2 = 0.f;
vertices[cv].position = blerp(b1Points[i].position, b2Points[0].position, b3Points[i].position, b4Points[0].position, w2, w1);

vertices[cv+2].position = blerp(b1Points[i].position, b2Points[1].position, b3Points[i].position, b4Points[1].position, w2+tStep, w1);

vertices[cv+3].position = blerp(b1Points[i+1].position, b2Points[0].position, b3Points[i+1].position, b4Points[0].position, w2, w1+tStep);

vertices[cv+1].position = blerp(b1Points[i+1].position, b2Points[1].position, b3Points[i+1].position, b4Points[1].position, w2+tStep, w1+tStep);

vertices[cv].textureCoordinate = bottomLeft;

fl32 d20 = (vertices[cv+2].position - vertices[cv].position).norm() * textureScale;
bottomRight = bottomLeft + v2(d20, 0.f);
vertices[cv+2].textureCoordinate = bottomRight;

fl32 d30 = (vertices[cv+3].position - vertices[cv].position).norm()*textureScale;
topLeft = bottomLeft + v2(0.f, d30);
vertices[cv+3].textureCoordinate = topLeft;

fl32 d12 = (vertices[cv+1].position - vertices[cv+2].position).norm()*textureScale;
fl32 d13 = (vertices[cv+1].position - vertices[cv+3].position).norm()*textureScale;
topRight = bottomLeft + v2(d13, d12);
vertices[cv+1].textureCoordinate = topRight;

indices[ci] = cv;
indices[ci+1] = cv+1;
indices[ci+2] = cv+2;

indices[ci+3] = cv+1;
indices[ci+4] = cv;
indices[ci+5] = cv+3;

cv += 4;
ci += 6;

w2 += tStep;
loopTopLeft = topLeft;
bottomLeft = bottomRight;
topLeft = topRight;

for (ui32 j = 1; j < divCount-1; j++) {
vertices[cv].position = blerp(b1Points[i].position, b2Points[j].position, b3Points[i].position, b4Points[j].position, w2, w1);

vertices[cv+2].position = blerp(b1Points[i].position, b2Points[j+1].position, b3Points[i].position, b4Points[j+1].position, w2+tStep, w1);

vertices[cv+3].position = blerp(b1Points[i+1].position, b2Points[j].position, b3Points[i+1].position, b4Points[j].position, w2, w1+tStep);

vertices[cv+1].position = blerp(b1Points[i+1].position, b2Points[j+1].position, b3Points[i+1].position, b4Points[j+1].position, w2+tStep, w1+tStep);

vertices[cv].textureCoordinate = bottomLeft;

fl32 d20 = (vertices[cv+2].position - vertices[cv].position).norm() * textureScale;
bottomRight = bottomLeft + v2(d20, 0.f);
vertices[cv+2].textureCoordinate = bottomRight;

vertices[cv+3].textureCoordinate = topLeft;

fl32 d12 = (vertices[cv+1].position - vertices[cv+2].position).norm()*textureScale;
fl32 d13 = (vertices[cv+1].position - vertices[cv+3].position).norm()*textureScale;
topRight = bottomLeft + v2(d13, d12);
vertices[cv+1].textureCoordinate = topRight;

indices[ci] = cv;
indices[ci+1] = cv+1;
indices[ci+2] = cv+2;

indices[ci+3] = cv+1;
indices[ci+4] = cv;
indices[ci+5] = cv+3;

cv += 4;
ci += 6;

w2 += tStep;

bottomLeft = bottomRight;
topLeft = topRight;
}
bottomLeft = loopTopLeft;
w1 += tStep;

*/

// LINEAR INTERPOLATION BETWEEN TWO SPLINES
/*
for (ui32 i = 0; i < divCount; i++) {
w2 = 0.f;
for (ui32 j = 0; j < divCount; j++) {
//positions[cv] = w2*b2Points[i].position + (1.f-w2)*b4Points[i].position;
//cv++;
//positions[cv] = w1*b1Points[j].position + (1.f-w1)*b3Points[j].position;
//cv++;
//positions[cv] = b2Points[i].position + b4Points[i].position + b1Points[j].position + b3Points[j].position;

//positions[cv] = blerp(b1Points[i].position, b2Points[j].position, b3Points[i].position, b4Points[j].position, w1, w2);

cv++;
w2 += tStep;
}
w1 += tStep;
}
*/



/*

vertex::light b1Points[divCount];
vertex::light b2Points[divCount];
vertex::light b3Points[divCount];
vertex::light b4Points[divCount];
fl32 t = 0.f;
fl32 tStep = 1.f/(fl32)(divCount-1);
for (ui32 i = 0; i < divCount; i++) {
b1Points[i].position = b1.calculate(1.f-t);
b2Points[i].position = b2.calculate(1.f-t);
b3Points[i].position = b3.calculate(t);
b4Points[i].position = b4.calculate(t);

b1Points[i].tangent = b1.differentiate1(1.f-t).normalized();
b2Points[i].tangent = b2.differentiate1(1.f-t).normalized();
b3Points[i].tangent = b3.differentiate1(t).normalized();
b4Points[i].tangent = b4.differentiate1(t).normalized();

b1Points[i].normal = b1Points[i].tangent.cross(linalg::lefthand::up).normalized();
b2Points[i].normal = b2Points[i].tangent.cross(linalg::lefthand::up).normalized();
b3Points[i].normal = b3Points[i].tangent.cross(linalg::lefthand::up).normalized();
b4Points[i].normal = b4Points[i].tangent.cross(linalg::lefthand::up).normalized();

t += tStep;
}
*/