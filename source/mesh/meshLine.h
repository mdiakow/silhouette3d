#ifndef MESH_LINE_H_
#define MESH_LINE_H_

#include <memory>

#include "meshData.h"
#include "math/linalg.h"

namespace mesh
{
	template<class vertex> 
	class line
	{
	public:
		line();
		line(vertex *vertices, ui32 *indices, ui32 vertexCount, ui32 indexCount);
		//~line();
	public:
		template<class T>
		static line<vertex> *fromSpline(linalg::bezier<T> bezier, ui32 subdivisions);
		
		static line<vertex> box(v3 lowerLeft, v3 upperRight);

		void render();
	public:
		mesh::data<vertex> data;
	};

	template<class vertex>
	inline line<vertex>::line() :
		data( mesh::data<vertex>() )
	{
	}

	template<class vertex>
	inline line<vertex>::line(vertex * vertices, ui32 * indices, ui32 vertexCount, ui32 indexCount) :
		data( mesh::data<vertex>(vertices, indices, vertexCount, indexCount) )
	{
	}

	template<class vertex>
	template<class T>
	inline line<vertex>* mesh::line<vertex>::fromSpline(linalg::bezier<T> bezier, ui32 subdivisions)
	{
		vertex *vertices = new vertex[subdivisions+1];
		ui32 *indices = new ui32[subdivisions+1];

		fl32 t = 0.f;
		fl32 tStep = 1.f/(fl32)(subdivisions);
		for (ui32 i = 0; i < subdivisions+1; i++) {
			vertices[i].position = bezier.calculate(t);
			indices[i] = i;
			t += tStep;
		}

		auto toReturn = new line<vertex>(vertices, indices, subdivisions+1, subdivisions+1);

		delete[] vertices;
		delete[] indices;

		return toReturn;
	}

	template<class vertex>
	inline line<vertex> line<vertex>::box(v3 lowerLeft, v3 upperRight)
	{
		v3 lowerLeftFront = lowerLeft;
		v3 upperLeftFront = v3(lowerLeft.x(), upperRight.y(), lowerLeft.z());
		v3 lowerRightFront = v3(upperRight.x(), lowerLeft.y(), lowerLeft.z());
		v3 upperRightFront = v3(upperRight.x(), upperRight.y(), lowerLeft.z());

		v3 lowerLeftBack = v3(lowerLeft.x(), lowerLeft.y(), upperRight.z());
		v3 lowerRightBack = v3(lowerRightFront.x(), lowerRightFront.y(), upperRight.z());
		v3 upperLeftBack = v3(upperLeftFront.x(), upperLeftFront.y(), upperRight.z());
		v3 upperRightBack = upperRight;

		ui32 vertexCount = 8;
		ui32 indexCount = 24;
		auto vertices = std::make_unique<vertex[]>(vertexCount);
		
		vertices[0].position = lowerLeftFront;
		vertices[1].position = upperLeftFront;
		vertices[2].position = lowerRightFront;
		vertices[3].position = upperRightFront;
		vertices[4].position = lowerLeftBack;
		vertices[5].position = upperLeftBack;
		vertices[6].position = lowerRightBack;
		vertices[7].position = upperRightBack;

		auto indices = std::unique_ptr<ui32[]>(
			new ui32[indexCount] {
				0, 1,
				0, 2,
				3, 1,
				3, 2,
				0, 4,
				1, 5,
				2, 6,
				3, 7,
				4, 5,
				4, 6,
				7, 5,
				7, 6
			});

		return line<vertex>(vertices.get(), indices.get(), vertexCount, indexCount);
	}

	template<class vertex>
	inline void line<vertex>::render()
	{
		data.render();
	}
}

#endif