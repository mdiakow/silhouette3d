#ifndef MESH_INSTANCE_H_
#define MESH_INSTANCE_H_

#include <wrl/client.h>
#include "bunkcore.h"

#include "d3d.h"

namespace mesh
{
	// template that takes a mesh class and instance class
	template<class struct_t>
	class instance
	{
	public:
		instance();
		instance(std::vector<struct_t> instances);
	public:
		ui32 size() { return count; }
	public:
		Microsoft::WRL::ComPtr<ID3D11Buffer> instances;
	private:
		ui32 count;
	};

	template<class struct_t>
	inline instance<struct_t>::instance() :
		count( 0 )
	{}

	template<class struct_t>
	inline instance<struct_t>::instance(std::vector<struct_t> instances)
	{
		HRESULT result;
		auto device = d3d::getDevice();

		count = (ui32)instances.size();
		D3D11_BUFFER_DESC bufferDesc;
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.ByteWidth = sizeof(struct_t)*count;
		bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc.CPUAccessFlags = 0;
		bufferDesc.MiscFlags = 0;
		bufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA init;
		init.pSysMem = instances.data();
		init.SysMemPitch = 0;
		init.SysMemSlicePitch = 0;

		result = device->CreateBuffer(&bufferDesc, &init, this->instances.GetAddressOf());
	}
}

#endif