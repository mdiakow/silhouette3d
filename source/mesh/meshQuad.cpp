#include "meshQuad.h"

std::pair<ID3D11Buffer*, ID3D11Buffer*> mesh::quad(fl32 width, fl32 height, v3 position)
{
	auto device = d3d::getDevice();

	// Set up the vertex and index buffers
	ui32 vertexCount = 4;
	ui32 indexCount = 6;

	ui32 *indices = new ui32[indexCount];
	vertex::texture *vertices = new vertex::texture[vertexCount];

	fl32 left = position[0];
	fl32 top = position[1] + width;
	fl32 right = position[0] + height;
	fl32 bottom = position[1];

	// Vertex positions and texture coordinate assignment.
	vertices[0].position = v3(left, bottom, 0.0f); // bottom left
	vertices[0].textureCoordinate = v2(0.0f, 0.0f);

	vertices[1].position = v3(left, top, 0.0f); // top left
	vertices[1].textureCoordinate = v2(0.0f, 1.0f);

	vertices[2].position = v3(right, bottom, 0.0f); // bottom right
	vertices[2].textureCoordinate = v2(1.0f, 0.0f);

	vertices[3].position = v3(right, top, 0.0f); // top right
	vertices[3].textureCoordinate = v2(1.0f, 1.0f);

	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;

	indices[3] = 3;
	indices[4] = 2;
	indices[5] = 1;

	D3D11_BUFFER_DESC vbd; // vertex buffer description
	vbd.Usage = D3D11_USAGE_DYNAMIC;
	vbd.ByteWidth = sizeof(vertex::texture) * vertexCount;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vbd.MiscFlags = 0;
	vbd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vd; // vertex data
	vd.pSysMem = vertices;
	vd.SysMemPitch = 0;
	vd.SysMemSlicePitch = 0;

	ID3D11Buffer *vertexBuffer;
	device->CreateBuffer(&vbd, &vd, &vertexBuffer);

	D3D11_BUFFER_DESC ibd; // index buffer description
	ibd.Usage = D3D11_USAGE_DEFAULT;
	ibd.ByteWidth = sizeof(ui32) * indexCount;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	ibd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA id; // index data
	id.pSysMem = indices;
	id.SysMemPitch = 0;
	id.SysMemSlicePitch = 0;

	ID3D11Buffer *indexBuffer;
	device->CreateBuffer(&ibd, &id, &indexBuffer);

	delete[] indices;
	delete[] vertices;

	return std::make_pair(vertexBuffer, indexBuffer);
}
