#ifndef MESH_MULTI_H_
#define MESH_MULTI_H_

#include "D3D.h"
#include "bunkcore.h"

namespace mesh
{
	// Vertex uses the same vertex as the mesh, struct is the same struct
	// the instance buffer is built from.
	template<class vertex_t, class struct_t, class mesh_t, class instance_t>
	class multi
	{
	public:
		multi();
		multi(mesh_t mesh, instance_t instance);
	public:
		void render();
		void render(ui32 instanceStart, ui32 instanceCount);
	public:
		mesh_t mesh;
		instance_t instance;
	};

	template<class vertex_t, class struct_t, class mesh_t, class instance_t>
	inline multi<vertex_t, struct_t, mesh_t, instance_t>::multi() :
		mesh( mesh_t() ),
		instance ( instance_t() )
	{
	}

	template<class vertex_t, class struct_t, class mesh_t, class instance_t>
	inline multi<vertex_t, struct_t, mesh_t, instance_t>::multi(mesh_t mesh, instance_t instance)  :
		mesh( mesh ),
		instance( instance )
	{
	}

	template<class vertex_t, class struct_t, class mesh_t, class instance_t>
	inline void multi<vertex_t, struct_t, mesh_t, instance_t>::render()
	{
		auto context = d3d::getDeviceContext();

		ui32 strides[] = {sizeof(vertex_t), sizeof(struct_t)};
		ui32 offsets[] = {0, 0};

		ID3D11Buffer *buffers[2] = {mesh.data.vertices.Get(), instance.instances.Get()};

		context->IASetVertexBuffers(0, 2, buffers, strides, offsets);
		context->IASetIndexBuffer(mesh.data.indices.Get(), DXGI_FORMAT_R32_UINT, 0);

		context->DrawIndexedInstanced(mesh.data.indexSize(), instance.size(), 0, 0, 0);
	}

	template<class vertex_t, class struct_t, class mesh_t, class instance_t>
	inline void multi<vertex_t, struct_t, mesh_t, instance_t>::render(ui32 instanceStart, ui32 instanceCount)
	{
		auto context = d3d::getDeviceContext();

		ui32 strides[] = {sizeof(vertex_t), sizeof(struct_t)};
		ui32 offsets[] = {0, 0};

		ID3D11Buffer *buffers[2] = {mesh.data.vertices.Get(), instance.instances.Get()};

		context->IASetVertexBuffers(0, 2, buffers, strides, offsets);
		context->IASetIndexBuffer(mesh.data.indices.Get(), DXGI_FORMAT_R32_UINT, 0);

		context->DrawIndexedInstanced(mesh.data.indexSize(), instanceCount, 0, 0, instanceStart);
	}

}

#endif