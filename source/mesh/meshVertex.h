// This encompasses the different types of vertices and their related helper functions.
#ifndef MESH_VERTEX_H_
#define MESH_VERTEX_H_

#include "bunkcore.h"
//#include "linalg.h"

#include "D3D.h"

#include <array>

namespace vertex
{
	struct simple
	{
		v3 position;
		v4 colour;

		static std::array<D3D11_INPUT_ELEMENT_DESC, 2> layout();
	};
	
	struct colour
	{
		v3 position;
		v4 mcolour;

		static std::array<D3D11_INPUT_ELEMENT_DESC, 2> layout();
	};

	struct texture
	{
		v3 position;
		v2 textureCoordinate;

		static std::array<D3D11_INPUT_ELEMENT_DESC, 2> layout();
	};

	struct lightInstance
	{
		v3 position;
		v2 textureCoordinate;
		v3 normal;
		v3 tangent;
		v3 binormal;

		static std::array<D3D11_INPUT_ELEMENT_DESC, 6> layout();
	};

	struct animatedLightInstance
	{
		v3 position;
		v2 textureCoordinate;
		v3 normal;
		v3 tangent;
		v3 binormal;
		fl32 weights[2];
		si32 indices[2];

		static std::array<D3D11_INPUT_ELEMENT_DESC, 8> layout();
	};
}


#endif