#ifndef MESH_DATA_H_
#define MESH_DATA_H_

#include <wrl/client.h>
#include "bunkcore.h"
//#include "linalg.h"

#include "d3d.h"

namespace mesh
{
	template<class vertex> 
	class data
	{
	public:
		data();
		data(vertex *vertices, ui32 *indices, ui32 vertexCount, ui32 indexCount);
		//~data();
	public:
		void render();
		
		ui32 vertexSize() { return vertexCount; }
		ui32 indexSize() { return indexCount; }
	public:
		Microsoft::WRL::ComPtr<ID3D11Buffer> vertices;
		Microsoft::WRL::ComPtr<ID3D11Buffer> indices;
	private:
		ui32 vertexCount, indexCount;
	};

	template<class vertex>
	mesh::data<vertex>::data() :
		vertexCount(0),
		indexCount(0)
	{

	}

	template<class vertex>
	mesh::data<vertex>::data(vertex * vertices, ui32 * indices, ui32 vertexCount, ui32 indexCount)
	{
		this->indexCount = indexCount;
		this->vertexCount = vertexCount;

		HRESULT result;

		D3D11_BUFFER_DESC vbDesc;
		vbDesc.Usage = D3D11_USAGE_DEFAULT;
		vbDesc.ByteWidth = sizeof(vertex) * vertexCount;
		vbDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vbDesc.CPUAccessFlags = 0;
		vbDesc.MiscFlags = 0;
		vbDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA vData;
		vData.pSysMem = vertices;
		vData.SysMemPitch = 0;
		vData.SysMemSlicePitch = 0;

		result = d3d::getDevice()->CreateBuffer(&vbDesc, &vData, this->vertices.GetAddressOf());
		if (FAILED(result)){
			// TODO: error check.
		}

		D3D11_BUFFER_DESC ibDesc;	
		ibDesc.Usage = D3D11_USAGE_DEFAULT;
		ibDesc.ByteWidth = sizeof(ui32) * indexCount;
		ibDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		ibDesc.CPUAccessFlags = 0;
		ibDesc.MiscFlags = 0;
		ibDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA iData;
		iData.pSysMem = indices;
		iData.SysMemPitch = 0;
		iData.SysMemSlicePitch = 0;

		result = d3d::getDevice()->CreateBuffer(&ibDesc, &iData, this->indices.GetAddressOf());
		if (FAILED(result)){
			// TODO: error check.
		}
	}
	template<class vertex>
	inline void data<vertex>::render()
	{
		ui32 stride = sizeof(vertex);
		ui32 offset = 0;

		auto context = d3d::getDeviceContext();

		context->IASetVertexBuffers(0, 1, vertices.GetAddressOf(), &stride, &offset);
		context->IASetIndexBuffer(indices.Get(), DXGI_FORMAT_R32_UINT, 0);

		context->DrawIndexed(indexCount, 0, 0);
	}
}



#endif