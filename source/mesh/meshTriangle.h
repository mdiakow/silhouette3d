#ifndef MESH_TRIANGLE_H_
#define MESH_TRIANGLE_H_

#include "bunkcore.h"
#include "meshData.h"


namespace mesh
{
	template<class vertex> 
	class triangle
	{
	public:
		triangle();
		triangle(vertex *vertices, ui32 *indices, ui32 vertexCount, ui32 indexCount);

		void render();

	public:
		 mesh::data<vertex> data;
	};

	template<class vertex>
	inline triangle<vertex>::triangle()
	{
	}

	template<class vertex>
	inline triangle<vertex>::triangle(vertex * vertices, ui32 * indices, ui32 vertexCount, ui32 indexCount) :
		data( mesh::data<vertex>(vertices, indices, vertexCount, indexCount) )
	{
	}

	template<class vertex>
	inline void triangle<vertex>::render()
	{
		data.render();
	}
}


#endif