#include "canvas/cnvsPanel.h"

canvas::panel::panel(ui32 tileSize, ui32 tileCount, v3 position, v4 colour) :
	textures( std::vector<std::vector<buffer::texture::input2d>>(tileCount, std::vector<buffer::texture::input2d>(tileCount))),
	meshes( std::vector<std::vector<mesh::triangle<vertex::twod>>>(tileCount, std::vector<mesh::triangle<vertex::twod>>(tileCount)))
{
	this->tileCount = tileCount;
	this->pos = position;
	// Initialize textures
	for (ui32 y = 0; y < tileCount; y++) {
		for (ui32 x = 0; x < tileCount; x++) {
			textures[y][x] = buffer::texture::input2d(tileSize, tileSize, colour);
		}
	}

	// Initialize meshes
	ui32 vertexCount = 4;
	ui32 indexCount = 6;

	auto indices = std::make_unique<ui32[]>(indexCount);
	auto vertices = std::make_unique<vertex::twod[]>(vertexCount);

	fl32 left = position.x();
	fl32 top = position.y() + tileSize;
	fl32 right = position.x() + tileSize;
	fl32 bottom = position.y();
	v3 basePositions[4];
	// Vertex positions and texture coordinate assignment.
	basePositions[0] = v3(left, bottom, 0.0f); // bottom left
	vertices[0].textureCoordinate = v2(0.0f, 0.0f);

	basePositions[1] = v3(left, top, 0.0f); // top left
	vertices[1].textureCoordinate = v2(0.0f, 1.0f);

	basePositions[2] = v3(right, bottom, 0.0f); // bottom right
	vertices[2].textureCoordinate = v2(1.0f, 0.0f);

	basePositions[3] = v3(right, top, 0.0f); // top right
	vertices[3].textureCoordinate = v2(1.0f, 1.0f);

	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;

	indices[3] = 3;
	indices[4] = 2;
	indices[5] = 1;

	for (ui32 y = 0; y < tileCount; y++) {
		for (ui32 x = 0; x < tileCount; x++) {
			vertices[0].position = basePositions[0] + v3((fl32)(x*tileSize), (fl32)(y*tileSize), 0.f);
			vertices[1].position = basePositions[1] + v3((fl32)(x*tileSize), (fl32)(y*tileSize), 0.f);
			vertices[2].position = basePositions[2] + v3((fl32)(x*tileSize), (fl32)(y*tileSize), 0.f);
			vertices[3].position = basePositions[3] + v3((fl32)(x*tileSize), (fl32)(y*tileSize), 0.f);

			meshes[y][x] = mesh::triangle<vertex::twod>(vertices.get(), indices.get(), vertexCount, indexCount);
		}
	}
}

void canvas::panel::render()
{
	for (ui32 y = 0; y < tileCount; y++) {
		for (ui32 x = 0; x < tileCount; x++) {
			textures[y][x].render();
			meshes[y][x].render();			
		}
	}
}

std::tuple<ui16, ui16> canvas::panel::dimensions()
{
	return std::make_tuple(tileCount, tileCount);
	//return std::tuple<ui16, ui16>();
}

v2 canvas::panel::position()
{
	return v2(pos.x(), pos.y());
}
