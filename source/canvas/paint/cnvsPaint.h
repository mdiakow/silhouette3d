#ifndef CNVS_PAINT_H_
#define CNVS_PAINT_H_

#include "bunkcore.h"

#include "canvas/paint/cnvsBrush.h"
#include "buffer/texture/bufTex2d.h"
#include "canvas/cnvsPanel.h"

namespace canvas
{
	namespace paint
	{
		void point(v2 position, buffer::texture::input2d &texture, canvas::brush &brush);
		void point(v2 position, canvas::panel &panel, canvas::brush &brush);

		void line(v2 start, v2 end, canvas::panel &panel, canvas::brush &brush);
	}
}

#endif