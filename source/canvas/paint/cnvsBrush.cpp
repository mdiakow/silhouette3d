#include "canvas/paint/cnvsBrush.h"

#include "colour.h"

#include <memory>

namespace canvas
{
	brush::brush(ui32 width, ui32 height, v4 colour) :
		texture( buffer::texture::input2d(width, height, colour) )
	{
	}

	brush::brush(ui32 width, ui32 height, ui32 brushBuffer[]) :
		texture( buffer::texture::input2d(width, height, brushBuffer) )
	{
	}

	brush brush::circle(fl32 radius, v4 colour)
	{
		ui32 width = (ui32)(radius * 2.f);
		ui32 height = width;

		auto outColour = colour::rgba32(v4::Zero());
		auto inColour = colour::rgba32(colour);
		
		auto brushData = std::make_unique<ui32[]>(width*height);
		v2 centre = v2((fl32)(width/2), (fl32)(height/2));
		v2 currentPoint = v2::Zero();
		ui32 currentIndex = 0;
		for (ui32 y = 0; y < height; y++) {
			for (ui32 x = 0; x < width; x++) {
				currentPoint.x() = (fl32)x;
				fl32 dist = linalg::distance(currentPoint, centre);
				if (dist <= radius) {
					brushData[currentIndex] = inColour;
				} else brushData[currentIndex] = outColour;
				currentIndex++;
			}
			currentPoint.y() = (fl32)y;
		}

		return brush(width, height, brushData.get());
	}
}