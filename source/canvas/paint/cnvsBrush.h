#ifndef CNVS_BRUSH_H_
#define CNVS_BRUSH_H_

#include "buffer/texture/bufTex2d.h"

namespace canvas
{
	class brush
	{
	public:
		brush(ui32 width, ui32 height, v4 colour);
		brush(ui32 width, ui32 height, ui32 brushBuffer[]);
	public:
		static brush circle(fl32 radius, v4 colour);
	public:
		buffer::texture::input2d texture;
	};
}

#endif