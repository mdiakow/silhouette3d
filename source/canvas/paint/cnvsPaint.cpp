#include "canvas/paint/cnvsPaint.h"

#include <filesystem>
#include <cmath>
#include <algorithm>

#include "render/shader/common/shaderHelper.h"
#include "buffer/constant/bufferConstant.h"

namespace helper
{
	static v2 offset(v2 position, canvas::brush &brush);
}

namespace canvas::paint
{
	namespace fp
	{
		const static std::filesystem::path paintPoint = "source/canvas/cnvsPointReplace.cms";
	}

	struct pointBuffer
	{
		si32 brushPosition[2];
		si32 padding[2];

		void setPosition(v2 position)
		{
			brushPosition[0] = (si32)std::round(position.x());
			brushPosition[1] = (si32)std::round(position.y());
		}
	};

	void point(v2 position, buffer::texture::input2d &texture, canvas::brush &brush)
	{
		auto context = d3d::getDeviceContext();

		auto shader = shader::compute::find(fp::paintPoint);
		context->CSSetShader(shader.Get(), nullptr, 0);

		pointBuffer pb;
		pb.setPosition( helper::offset(position, brush) );
		auto pointConstant = buffer::constant(pb);
		context->CSSetConstantBuffers(0, 1, pointConstant.data.GetAddressOf());

		context->CSSetShaderResources(0, 1, brush.texture.view.GetAddressOf());
		
		auto outputTexture = buffer::texture::output2d(texture);
		context->CopyResource(outputTexture.texture2d.Get(), texture.texture2d.Get());
		context->CSSetUnorderedAccessViews(0, 1, outputTexture.view.GetAddressOf(), 0);

		auto [width, height] = brush.texture.dimensions();
		ui32 dispatchX = (ui32)std::ceil( (fl32)width/16.f );
		ui32 dispatchY = (ui32)std::ceil( (fl32)height/16.f );

		context->Dispatch(dispatchX, dispatchY, 1);

		context->CopyResource(texture.texture2d.Get(), outputTexture.texture2d.Get());
	}

	void point(v2 position, canvas::panel &panel, canvas::brush &brush)
	{
		auto context = d3d::getDeviceContext();

		auto shader = shader::compute::find(fp::paintPoint);
		context->CSSetShader(shader.Get(), nullptr, 0);

		pointBuffer pb;
		pb.setPosition( helper::offset(position, brush) );
		auto pointConstant = buffer::constant(pb);
		context->CSSetConstantBuffers(0, 1, pointConstant.data.GetAddressOf());

		context->CSSetShaderResources(0, 1, brush.texture.view.GetAddressOf());

		auto outputTexture = buffer::texture::output2d(panel.textures[0][0]);
		context->CSSetUnorderedAccessViews(0, 1, outputTexture.view.GetAddressOf(), 0);
		
		auto [brushWidth, brushHeight] = brush.texture.dimensions();
		ui32 dispatchX = (ui32)std::ceil( (fl32)brushWidth/16.f );
		ui32 dispatchY = (ui32)std::ceil( (fl32)brushHeight/16.f );

		// Calculate which specific textures in the panel need to be changed.
		auto [tileWidth, tileHeight] = panel.textures[0][0].dimensions();
		auto panelPosition = panel.position();
		v2 brushStart = helper::offset(position, brush) - panelPosition;
		v2 brushEnd = brushStart + v2((fl32)brushWidth, (fl32)brushHeight);

		si32 startX = (si32)std::floor(brushStart.x() / (fl32)tileWidth);
		si32 startY = (si32)std::floor(brushStart.y() / (fl32)tileHeight);
		startX = (std::max)(startX, 0);
		startY = (std::max)(startY, 0);

		si32 endX = (si32)std::floor(brushEnd.x() / (fl32)tileWidth);
		si32 endY = (si32)std::floor(brushEnd.y() / (fl32)tileHeight);
		auto [countWidth, countHeight] = panel.dimensions();
		endX = (std::min)(endX, countWidth-1);
		endY = (std::min)(endY, countHeight-1);
		if(endX < 0) endX = 0;
		if(endY < 0) endY = 0;

		for (si32 y = startY; y <= endY; y++) {
			for (si32 x = startX; x <= endX; x++) {
				// realign brush to the local space of each texture.
				pb.setPosition(brushStart - v2((fl32)(x*tileWidth), (fl32)(y*tileHeight)));
				pointConstant.set(pb);
				context->CopyResource(outputTexture.texture2d.Get(), panel.textures[y][x].texture2d.Get());
				context->Dispatch(dispatchX, dispatchY, 1);
				context->CopyResource(panel.textures[y][x].texture2d.Get(), outputTexture.texture2d.Get());
			}
		}
	}

	void line(v2 start, v2 end, canvas::panel & panel, canvas::brush & brush)
	{
		v2 diff = end - start;
		fl32 diffMax = (std::max)(abs(diff.x()), abs(diff.y()));
		diff = diff/diffMax;

		v2 position = start;
		for (ui32 i = 0; i < (ui32)diffMax; i++) {
			canvas::paint::point(position, panel, brush);
			position += diff;
		}
	}
}

namespace helper
{
	static v2 offset(v2 position, canvas::brush &brush)
	{
		auto [width, height] = brush.texture.dimensions();

		v2 half = v2((fl32)width*0.5f, (fl32)height*0.5f);

		return position - half;
	}
}