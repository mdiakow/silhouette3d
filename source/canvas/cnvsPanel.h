#ifndef CNVS_PANEL_H_
#define CNVS_PANEL_H_

#include "bunkcore.h"

#include <vector>
#include <tuple>

#include "buffer/texture/bufTex2d.h"
#include "shaders/cpp/shader2d.h"
#include "mesh/meshTriangle.h"

namespace canvas
{
	class panel
	{
	public:
		panel(ui32 tileSize, ui32 tileCount, v3 position, v4 colour);
	public:
		void render();
		std::tuple<ui16, ui16> dimensions();
		v2 position();
	public:
		std::vector<std::vector<buffer::texture::input2d>> textures;
		std::vector<std::vector<mesh::triangle<vertex::twod>>> meshes;
	private:
		ui32 tileCount;
		v3 pos;
	};
}

#endif