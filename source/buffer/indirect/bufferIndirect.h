#ifndef BUFFER_INDIRECT_H_
#define BUFFER_INDIRECT_H_

#include <type_traits>

#include <wrl/client.h>

#include "D3D.h"
#include "bunkcore.h"

namespace buffer
{
	struct dispatchIndirect
	{ // Matches dispatch
		ui32 threadGroupCountX;
		ui32 threadGroupCountY;
		ui32 threadGroupCountZ;
	};

	struct drawIndexedIndirect
	{ // 
		ui32 indexCountPerInstance;
		ui32 instanceCount;
		ui32 startIndexLocation;
		si32 baseVertexLocation;
		ui32 startInstanceLocation;
	};

	struct drawIndirect
	{
		ui32 vertexCountPerInstance;
		ui32 instanceCount;
		ui32 startVertexLocation;
		ui32 startInstanceLocation;
	};

	class indirect
	{
	public:
		indirect();
		indirect(dispatchIndirect data);
		indirect(drawIndexedIndirect data);
		indirect(drawIndirect data);
	public:
		Microsoft::WRL::ComPtr<ID3D11Buffer> data;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> view;
	};

}

#endif