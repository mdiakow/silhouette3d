#include "bufferIndirect.h"

namespace initialize
{
	template<class structure> void indirect(structure data, buffer::indirect *buffer)
	{
		auto device = d3d::getDevice();
		HRESULT result;

		D3D11_BUFFER_DESC bufferDesc;
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.ByteWidth = sizeof(structure);
		bufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
		bufferDesc.CPUAccessFlags = 0;
		bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_DRAWINDIRECT_ARGS;
		bufferDesc.StructureByteStride = sizeof(ui32);

		D3D11_SUBRESOURCE_DATA initialData;
		initialData.SysMemPitch = 0;
		initialData.SysMemSlicePitch = 0;
		initialData.pSysMem = &data;

		result = device->CreateBuffer(&bufferDesc, &initialData, buffer->data.GetAddressOf());

		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		uavDesc.Format = DXGI_FORMAT_R32_UINT;
		uavDesc.Buffer.FirstElement = 0;
		uavDesc.Buffer.NumElements = sizeof(structure)/sizeof(ui32);
		uavDesc.Buffer.Flags = 0;

		result = device->CreateUnorderedAccessView(buffer->data.Get(), &uavDesc, buffer->view.GetAddressOf());
	}
}

buffer::indirect::indirect()
{
}

buffer::indirect::indirect(dispatchIndirect data)
{
	initialize::indirect(data, this);
}

buffer::indirect::indirect(drawIndexedIndirect data)
{
	initialize::indirect(data, this);
}

buffer::indirect::indirect(drawIndirect data)
{
	initialize::indirect(data, this);
}
