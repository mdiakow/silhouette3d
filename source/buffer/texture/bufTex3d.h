#ifndef TEX_BUF_INPUT3D_H_
#define TEX_BUF_INPUT3D_H_

#include <memory>
#include <tuple>
#include <wrl/client.h>

#include "bunkcore.h"
#include "D3D.h"

namespace buffer
{
	namespace texture
	{
		class input3d
		{
		public:
			input3d();
			input3d(ui16 width, ui16 height, ui16 depth);
			input3d(ui16 width, ui16 height, ui16 depth, fl32 buffer[]);

			input3d(const input3d &other);
		public:
			void render();
			// Returns width, height, depth of underlying texture3d.
			std::tuple<ui16, ui16, ui16> dimensions() const;
		public:
			Microsoft::WRL::ComPtr<ID3D11Texture3D> texture3d;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> view;
		};

		class output3d
		{
		public:
			output3d(ui16 width, ui16 height, ui16 depth);
			output3d(D3D11_TEXTURE3D_DESC description);

			output3d(const output3d &other);
		public:
			static output3d fromInput(input3d &input);
			// returns width, height, and depth of underlying texture3d.
			std::tuple<ui16, ui16, ui16> dimensions() const;
		public:
			Microsoft::WRL::ComPtr<ID3D11Texture3D> texture3d;
			Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> view;
		};
	}
}

#endif