#ifndef TEX_BUF_INPUT2D_H_
#define TEX_BUF_INPUT2D_H_

#include "bunkcore.h"

#include <memory>
#include <wrl/client.h>
#include <tuple>

#include "d3d.h"

namespace buffer
{
	namespace texture
	{
		
		enum class fill2d 
		{
			fadeOutCentre,
			solid
		};
		
		class input2d
		{
		public:
			input2d();
			input2d(ui16 width, ui16 height, v4 colour);
			input2d(D3D11_TEXTURE2D_DESC description);
			input2d(ui16 width, ui16 height, std::unique_ptr<ui32[]> textureData);
			input2d(ui16 width, ui16 height, ui32 textureData[]);
			// Simple circle in texture.
			input2d(fill2d fill, fl32 radius, v4 colour);
		public:
			void render();
			
			// returns width, height of the underlying texture2d.
			std::tuple<ui16, ui16> dimensions() const;
			static std::unique_ptr<input2d> checkerBoard(ui16 width, ui16 height, ui32 checkCount);
		public:
			Microsoft::WRL::ComPtr<ID3D11Texture2D> texture2d;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> view;
		};

		class output2d
		{
		public:
			output2d(ui16 width, ui16 height);
			output2d(D3D11_TEXTURE2D_DESC description);
			output2d(input2d &input);
		public:

		public:
			Microsoft::WRL::ComPtr<ID3D11Texture2D> texture2d;
			Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> view;
		};
	}
}

#endif