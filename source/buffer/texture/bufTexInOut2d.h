#ifndef BUF_TEX_INOUT_2D_H_
#define BUF_TEX_INOUT_2D_H_

#include "d3d.h"
#include <wrl/client.h>
#include <tuple>

namespace buffer::texture
{
	class inputOutput2d
	{
	public:
		inputOutput2d(D3D11_TEXTURE2D_DESC textureDesc);
	public:
		std::tuple<ui16, ui16> dimensions() const;
	public:
		Microsoft::WRL::ComPtr<ID3D11Texture2D> texture2d;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> srView;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> uaView;

	};
}


#endif