#include "buffer/texture/bufTex1d.h"

namespace helper 
{
	static D3D11_TEXTURE1D_DESC textureDescription(ui32 width);
	static D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE1D_DESC textureDesc);
}

namespace buffer
{
	namespace texture
	{
		input1d::input1d(ui16 width, fl32 buffer[])
		{
			HRESULT result;

			D3D11_TEXTURE1D_DESC textureDesc = helper::textureDescription(width);

			D3D11_SUBRESOURCE_DATA initData;
			initData.pSysMem = buffer;
			initData.SysMemPitch = sizeof(fl32)*width;
			initData.SysMemSlicePitch = 0;

			result = d3d::getDevice()->CreateTexture1D(&textureDesc, &initData, texture1d.GetAddressOf());

			D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

			result = d3d::getDevice()->CreateShaderResourceView(texture1d.Get(), &srvDesc, view.GetAddressOf());
		}

		ui16 input1d::width() const
		{
			D3D11_TEXTURE1D_DESC textureDesc;
			texture1d->GetDesc(&textureDesc);

			return textureDesc.Width;
		}
	}
}

namespace helper
{
	D3D11_TEXTURE1D_DESC textureDescription(ui32 width)
	{
		D3D11_TEXTURE1D_DESC textureDesc;

		textureDesc.Width = width;
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = DXGI_FORMAT_R32_FLOAT;
		textureDesc.Usage = D3D11_USAGE_DYNAMIC;
		textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		textureDesc.MiscFlags = 0;

		return textureDesc;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE1D_DESC textureDesc)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;

		srvDesc.Format = textureDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = 1;

		return srvDesc;
	}
}