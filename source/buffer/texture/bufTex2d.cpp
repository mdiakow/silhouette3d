#include "buffer/texture/bufTex2d.h"
#include "colour.h"

namespace helper 
{
	static D3D11_TEXTURE2D_DESC textureDescription(ui32 width, ui32 height);
	static D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE2D_DESC textureDesc);
	static D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescription(D3D11_TEXTURE2D_DESC textureDesc);
}

using namespace buffer::texture;
using namespace Microsoft::WRL;

///////////////////
// INPUT 2D
///////////////////
buffer::texture::input2d::input2d()
{
}

buffer::texture::input2d::input2d(ui16 width, ui16 height, v4 colour)
{
	HRESULT result;

	// Initialize direct x texture data.
	D3D11_TEXTURE2D_DESC textureDesc = helper::textureDescription(width, height);

	auto textureData = std::make_unique<ui32[]>(width*height);
	ui32 formattedColour = colour::rgba32(colour);
	std::fill(&textureData[0], &textureData[0] + (width*height), formattedColour);

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = textureData.get();
	initData.SysMemPitch = width*sizeof(ui32);
	initData.SysMemSlicePitch = 0;

	result = d3d::getDevice()->CreateTexture2D(&textureDesc, &initData, texture2d.GetAddressOf());

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

	result = d3d::getDevice()->CreateShaderResourceView(texture2d.Get(), &srvDesc, view.GetAddressOf());
}

buffer::texture::input2d::input2d(D3D11_TEXTURE2D_DESC description)
{
	HRESULT result;

	result = d3d::getDevice()->CreateTexture2D(&description, nullptr, texture2d.GetAddressOf());

	auto srvDesc = helper::srvDescription(description);

	result = d3d::getDevice()->CreateShaderResourceView(texture2d.Get(), &srvDesc, view.GetAddressOf());
}

buffer::texture::input2d::input2d(ui16 width, ui16 height, std::unique_ptr<ui32[]> textureData)
{
	HRESULT result;

	D3D11_TEXTURE2D_DESC textureDesc = helper::textureDescription(width, height);

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = textureData.get();
	initData.SysMemPitch = width*sizeof(ui32);
	initData.SysMemSlicePitch = 0;

	result = d3d::getDevice()->CreateTexture2D(&textureDesc, &initData, texture2d.GetAddressOf());

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

	result = d3d::getDevice()->CreateShaderResourceView(texture2d.Get(), &srvDesc, view.GetAddressOf());
}

buffer::texture::input2d::input2d(ui16 width, ui16 height, ui32 textureData[])
{
	HRESULT result;

	D3D11_TEXTURE2D_DESC textureDesc = helper::textureDescription(width, height);

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = textureData;
	initData.SysMemPitch = width*sizeof(ui32);
	initData.SysMemSlicePitch = 0;

	result = d3d::getDevice()->CreateTexture2D(&textureDesc, &initData, texture2d.GetAddressOf());

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

	result = d3d::getDevice()->CreateShaderResourceView(texture2d.Get(), &srvDesc, view.GetAddressOf());
}

buffer::texture::input2d::input2d(fill2d fill, fl32 radius, v4 colour)
{
	ui16 width = (ui16)std::ceil((radius*2.f));
	auto textureData = std::make_unique<ui32[]>(width*width);
	
	v2 centre = v2((fl32)width/2.f, (fl32)width/2.f);
	v2 current;

	if(fill == fill2d::fadeOutCentre) {
		for(ui32 y = 0; y < width; y++) {
			for(ui32 x = 0; x < width; x++) {
				ui32 index = x + y*width;

				current = v2((fl32)x, (fl32)y);
				fl32 distance = linalg::distance(current, centre);
				if(distance <= radius) {
					v4 currentColour = colour * (1.f-(distance/radius));
					currentColour.w() = (1.f-(distance/radius));
					textureData[index] = colour::rgba32(currentColour);
				} else textureData[index] = 0u;
			}
		}
	} else if (fill == fill2d::solid) {
		for(ui32 y = 0; y < width; y++) {
			for(ui32 x = 0; x < width; x++) {
				ui32 index = x + y*width;

				current = v2((fl32)x, (fl32)y);
				fl32 distance = linalg::distance(current, centre);
				if(distance <= width) {
					textureData[index] = colour::rgba32(colour);
				} else textureData[index] = 0u;
			}
		}
	}

	HRESULT result;

	D3D11_TEXTURE2D_DESC textureDesc = helper::textureDescription(width, width);

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = textureData.get();
	initData.SysMemPitch = width*sizeof(ui32);
	initData.SysMemSlicePitch = 0;

	result = d3d::getDevice()->CreateTexture2D(&textureDesc, &initData, texture2d.GetAddressOf());

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

	result = d3d::getDevice()->CreateShaderResourceView(texture2d.Get(), &srvDesc, view.GetAddressOf());
}

void buffer::texture::input2d::render()
{
	auto context = d3d::getDeviceContext();
	context->PSSetShaderResources(0, 1, view.GetAddressOf());
}

std::tuple<ui16, ui16> buffer::texture::input2d::dimensions() const
{
	D3D11_TEXTURE2D_DESC textureDesc;
	texture2d->GetDesc(&textureDesc);

	return std::make_tuple(textureDesc.Width, textureDesc.Height);
}

std::unique_ptr<input2d> buffer::texture::input2d::checkerBoard(ui16 width, ui16 height, ui32 checkCount)
{
	auto textureData = std::make_unique<ui32[]>(width*height);

	ui32 white = colour::rgba32(v4(1.0f, 1.0f, 1.0f, 1.0f));
	ui32 black = colour::rgba32(v4(1.0f, 0.0f, 0.0f, 1.0f));

	ui32 currentColour = black;

	ui32 index = 0;
	for(ui32 a = 0; a < checkCount; a++) {
		for (ui32 b = 0; b < height / checkCount; b++) {
			for (ui32 c = 0; c < checkCount; c++) {
				for (ui32 d = 0; d < width / checkCount; d++) {
					textureData[index] = currentColour;
					index++;
				}
				if (currentColour == black) {
					currentColour = white;
				} else {
					currentColour = black;
				}
			}
		}
		if (currentColour == black) {
			currentColour = white;
		} else {
			currentColour = black;
		}
	}

	return std::make_unique<input2d>(width, height, std::move(textureData));
}

///////////////////
// OUTPUT 2D
///////////////////
buffer::texture::output2d::output2d(ui16 width, ui16 height)
{
	D3D11_TEXTURE2D_DESC textureDesc = helper::textureDescription(width, height);

	HRESULT result = d3d::getDevice()->CreateTexture2D(&textureDesc, nullptr, texture2d.GetAddressOf());

	auto uavDesc = helper::uavDescription(textureDesc);

	result = d3d::getDevice()->CreateUnorderedAccessView(texture2d.Get(), &uavDesc, view.GetAddressOf());
}

buffer::texture::output2d::output2d(D3D11_TEXTURE2D_DESC description)
{
	HRESULT result = d3d::getDevice()->CreateTexture2D(&description, nullptr, texture2d.GetAddressOf());

	auto uavDesc = helper::uavDescription(description);

	result = d3d::getDevice()->CreateUnorderedAccessView(texture2d.Get(), &uavDesc, view.GetAddressOf());
}

buffer::texture::output2d::output2d(input2d & input)
{
	D3D11_TEXTURE2D_DESC textureDesc;
	input.texture2d->GetDesc(&textureDesc);

	HRESULT result = d3d::getDevice()->CreateTexture2D(&textureDesc, nullptr, texture2d.GetAddressOf());

	auto uavDesc = helper::uavDescription(textureDesc);

	result = d3d::getDevice()->CreateUnorderedAccessView(texture2d.Get(), &uavDesc, view.GetAddressOf());
}

namespace helper
{
	D3D11_TEXTURE2D_DESC textureDescription(ui32 width, ui32 height)
	{
		D3D11_TEXTURE2D_DESC textureDesc;

		textureDesc.Height = height;
		textureDesc.Width = width;
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.SampleDesc.Quality = 0;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;

		return textureDesc;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE2D_DESC textureDesc)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;

		srvDesc.Format = textureDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = 1;

		return srvDesc;
	}

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescription(D3D11_TEXTURE2D_DESC textureDesc)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;

		uavDesc.Format = textureDesc.Format;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
		uavDesc.Texture2D.MipSlice = 0;

		return uavDesc;
	}
}


