#include "bufTexInOut2d.h"

namespace helper
{
	static D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE2D_DESC textureDesc);
	static D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescription(D3D11_TEXTURE2D_DESC textureDesc);
}

namespace buffer::texture
{
	inputOutput2d::inputOutput2d(D3D11_TEXTURE2D_DESC textureDesc)
	{
		HRESULT result;
		auto device = d3d::getDevice();

		result = device->CreateTexture2D(&textureDesc, nullptr, texture2d.GetAddressOf());

		result = device->CreateUnorderedAccessView(this->texture2d.Get(), &helper::uavDescription(textureDesc), this->uaView.GetAddressOf());

		result = device->CreateShaderResourceView(this->texture2d.Get(), &helper::srvDescription(textureDesc), this->srView.GetAddressOf());
	}
	std::tuple<ui16, ui16> inputOutput2d::dimensions() const
	{
		D3D11_TEXTURE2D_DESC textureDesc;
		texture2d->GetDesc(&textureDesc);

		return std::make_tuple(textureDesc.Width, textureDesc.Height);
	}
}

namespace helper
{
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE2D_DESC textureDesc)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;

		srvDesc.Format = textureDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = 1;

		return srvDesc;
	}

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescription(D3D11_TEXTURE2D_DESC textureDesc)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;

		uavDesc.Format = textureDesc.Format;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
		uavDesc.Texture2D.MipSlice = 0;

		return uavDesc;
	}
}




