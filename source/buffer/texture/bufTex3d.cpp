#include "buffer/texture/bufTex3d.h"

using namespace buffer::texture;
using namespace Microsoft::WRL;

namespace helper 
{
	static D3D11_TEXTURE3D_DESC textureDescription(ui32 width, ui32 height, ui32 depth);
	static D3D11_TEXTURE3D_DESC outputDescription(ui32 width, ui32 height, ui32 depth);
	static D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE3D_DESC textureDesc);
	static D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescription(D3D11_TEXTURE3D_DESC textureDesc);
}


/////////////
// input3d
/////////////
buffer::texture::input3d::input3d()
{
}

input3d::input3d(ui16 width, ui16 height, ui16 depth)
{
	HRESULT result;
	auto device = d3d::getDevice();

	// Initialize direct x texture data.
	D3D11_TEXTURE3D_DESC textureDesc = helper::textureDescription(width, height, depth);

	auto textureData = std::make_unique<fl32[]>(width*height*depth);
	std::fill(&textureData[0], &textureData[0] + (width*height*depth), 0.f);

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = textureData.get();
	initData.SysMemPitch = width*sizeof(fl32);
	initData.SysMemSlicePitch = width*height*sizeof(fl32);

	result = device->CreateTexture3D(&textureDesc, &initData, texture3d.GetAddressOf());

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

	result = device->CreateShaderResourceView(texture3d.Get(), &srvDesc, view.GetAddressOf());
}

buffer::texture::input3d::input3d(ui16 width, ui16 height, ui16 depth, fl32 buffer[])
{
	HRESULT result;
	auto device = d3d::getDevice();

	// Initialize direct x texture data.
	D3D11_TEXTURE3D_DESC textureDesc = helper::textureDescription(width, height, depth);

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = buffer;
	initData.SysMemPitch = width*sizeof(fl32);
	initData.SysMemSlicePitch = width*height*sizeof(fl32);

	result = device->CreateTexture3D(&textureDesc, &initData, texture3d.GetAddressOf());

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

	result = device->CreateShaderResourceView(texture3d.Get(), &srvDesc, view.GetAddressOf());
}

buffer::texture::input3d::input3d(const input3d & other)
{
	if (other.texture3d) {
		HRESULT result;
		auto device = d3d::getDevice();
		auto context = d3d::getDeviceContext();

		auto [width, height, depth] = other.dimensions();

		D3D11_TEXTURE3D_DESC textureDesc = helper::textureDescription(width, height, depth);

		result = device->CreateTexture3D(&textureDesc, nullptr, texture3d.GetAddressOf());

		context->CopyResource(texture3d.Get(), other.texture3d.Get());

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = helper::srvDescription(textureDesc);

		result = device->CreateShaderResourceView(texture3d.Get(), &srvDesc, view.GetAddressOf());
	}
}

void input3d::render()
{
	auto context = d3d::getDeviceContext();
	context->PSSetShaderResources(0, 1, view.GetAddressOf());
}

std::tuple<ui16, ui16, ui16> buffer::texture::input3d::dimensions() const
{
	if (texture3d) {
		D3D11_TEXTURE3D_DESC textureDesc;
		texture3d->GetDesc(&textureDesc);

		return std::make_tuple(textureDesc.Width, textureDesc.Height, textureDesc.Depth);
	}
	return std::make_tuple(0u,0u,0u);
}
///////////////
// output3d
///////////////
output3d::output3d(ui16 width, ui16 height, ui16 depth)
{
	HRESULT result;

	D3D11_TEXTURE3D_DESC textureDesc = helper::outputDescription(width, height, depth);

	result = d3d::getDevice()->CreateTexture3D(&textureDesc, nullptr, texture3d.GetAddressOf());

	auto uavDesc = helper::uavDescription(textureDesc);

	result = d3d::getDevice()->CreateUnorderedAccessView(texture3d.Get(), &uavDesc, view.GetAddressOf());
}

buffer::texture::output3d::output3d(D3D11_TEXTURE3D_DESC description)
{
	HRESULT result;
	auto device = d3d::getDevice();

	result = device->CreateTexture3D(&description, nullptr, texture3d.GetAddressOf());
	
	auto uavDesc = helper::uavDescription(description);

	result = device->CreateUnorderedAccessView(texture3d.Get(), &uavDesc, view.GetAddressOf());
}

buffer::texture::output3d::output3d(const output3d & other)
{
	if (other.texture3d) {
		HRESULT result;
		auto device = d3d::getDevice();
		auto context = d3d::getDeviceContext();

		auto [width, height, depth] = other.dimensions();

		D3D11_TEXTURE3D_DESC textureDesc;
		other.texture3d->GetDesc(&textureDesc);
		result = device->CreateTexture3D(&textureDesc, nullptr, texture3d.GetAddressOf());
		
		auto uavDesc = helper::uavDescription(textureDesc);
		result = d3d::getDevice()->CreateUnorderedAccessView(texture3d.Get(), &uavDesc, view.GetAddressOf());

		context->CopyResource(texture3d.Get(), other.texture3d.Get());
	}
}

std::tuple<ui16, ui16, ui16> buffer::texture::output3d::dimensions() const
{
	if (texture3d) {
		D3D11_TEXTURE3D_DESC textureDesc;
		texture3d->GetDesc(&textureDesc);

		return std::make_tuple(textureDesc.Width, textureDesc.Height, textureDesc.Depth);
	}
	return std::make_tuple(0u,0u,0u);
}

output3d buffer::texture::output3d::fromInput(input3d & input)
{
	auto [width, height, depth] = input.dimensions();

	return output3d(width, height, depth);
}

namespace helper 
{
	D3D11_TEXTURE3D_DESC textureDescription(ui32 width, ui32 height, ui32 depth)
	{
		D3D11_TEXTURE3D_DESC textureDesc;

		textureDesc.Height = height;
		textureDesc.Width = width;
		textureDesc.Depth = depth;
		textureDesc.MipLevels = 1;
		textureDesc.Format = DXGI_FORMAT_R32_FLOAT;
		textureDesc.Usage = D3D11_USAGE_DYNAMIC;
		textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		textureDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		textureDesc.MiscFlags = 0;

		return textureDesc;
	}

	static D3D11_TEXTURE3D_DESC outputDescription(ui32 width, ui32 height, ui32 depth)
	{
		D3D11_TEXTURE3D_DESC textureDesc;

		textureDesc.Height = height;
		textureDesc.Width = width;
		textureDesc.Depth = depth;
		textureDesc.MipLevels = 1;
		textureDesc.Format = DXGI_FORMAT_R32_FLOAT;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;

		return textureDesc;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(D3D11_TEXTURE3D_DESC textureDesc)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;

		srvDesc.Format = textureDesc.Format;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;
		srvDesc.Texture3D.MipLevels = textureDesc.MipLevels;
		srvDesc.Texture3D.MostDetailedMip = 0;

		return srvDesc;
	}

	static D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescription(D3D11_TEXTURE3D_DESC textureDesc)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;

		uavDesc.Format = textureDesc.Format;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE3D;
		uavDesc.Texture3D.MipSlice = 0;
		uavDesc.Texture3D.WSize = textureDesc.Depth;
		uavDesc.Texture3D.FirstWSlice = 0;

		return uavDesc;
	}
}