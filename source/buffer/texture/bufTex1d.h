#ifndef BUF_TEX1D_H_
#define BUF_TEX1D_H_

#include <memory>
#include <wrl/client.h>
#include "d3d.h"

#include "bunkcore.h"

namespace buffer
{
	namespace texture
	{
		class input1d
		{
		public:
			input1d(ui16 width, fl32 buffer[]);
		public:
			ui16 width() const;
		public:			
			Microsoft::WRL::ComPtr<ID3D11Texture1D> texture1d;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> view;
		};
	}
}

#endif