#ifndef B_APPEND_STRUCTURED_H_
#define B_APPEND_STRUCTURED_H_

#include <type_traits>
#include <vector>
#include <wrl/client.h>

#include "D3D.h"
#include "bunkcore.h"

namespace buffer
{
	namespace appendStructured
	{
		template <class structure>
		class output
		{
		public:
			output(structure data, ui32 structureCount);
		public:
			// Returns a CPU side copy of the buffer.
			std::vector<structure> get();

			//ui32 size();
		public:
			Microsoft::WRL::ComPtr<ID3D11Buffer> append;
			Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> appendView;

			Microsoft::WRL::ComPtr<ID3D11Buffer> count;
		};

		template<class structure>
		inline output<structure>::output(structure data, ui32 structureCount)
		{
			static_assert(std::is_standard_layout<structure>::value);

			auto device = d3d::getDevice();
			HRESULT result;

			D3D11_BUFFER_DESC bufferDesc;
			bufferDesc.Usage = D3D11_USAGE_DEFAULT;
			bufferDesc.ByteWidth = sizeof(structure) * structureCount;
			bufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS;
			bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			bufferDesc.StructureByteStride = sizeof(structure);

			result = device->CreateBuffer(&bufferDesc, nullptr, this->append.GetAddressOf());

			D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
			uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
			uavDesc.Format = DXGI_FORMAT_UNKNOWN;
			uavDesc.Buffer.FirstElement = 0;
			uavDesc.Buffer.NumElements = structureCount;
			uavDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_APPEND;

			result = device->CreateUnorderedAccessView(this->append.Get(), &uavDesc, appendView.GetAddressOf());

			D3D11_BUFFER_DESC ocDesc;
			ocDesc.Usage = D3D11_USAGE_STAGING;
			ocDesc.BindFlags = 0;
			ocDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			ocDesc.MiscFlags = 0;
			ocDesc.ByteWidth = sizeof(ui32);
			
			device->CreateBuffer(&ocDesc, nullptr, count.GetAddressOf());
		}

		template<class structure>
		inline std::vector<structure> output<structure>::get()
		{
			HRESULT result;
			auto context = d3d::getDeviceContext();
			auto device = d3d::getDevice();

			context->CopyStructureCount(count.Get(), 0, appendView.Get());

			// Get the structure count back from the GPU
			ui32 structureCount;
			
			D3D11_MAPPED_SUBRESOURCE subresource;
			result = context->Map(count.Get(), 0, D3D11_MAP_READ, 0, &subresource);
			std::memcpy(&structureCount, static_cast<ui32*>(subresource.pData), sizeof(ui32));	
			context->Unmap(count.Get(), 0);

			// Get the array of structures back from the GPU
			std::vector<structure> cpuBuffer(structureCount);

			if (structureCount > 0) {
				result = context->Map(this->append.Get(), 0, D3D11_MAP_READ, 0, &subresource);
				std::memcpy(&cpuBuffer[0], static_cast<structure*>(subresource.pData), sizeof(structure)*structureCount);	
				context->Unmap(this->append.Get(), 0);
			}

			return cpuBuffer;
		}

		/*
		template<class structure>
		inline std::unique_ptr<structure[]> output<structure>::get()
		{
			HRESULT result;
			auto context = d3d::getDeviceContext();
			auto device = d3d::getDevice();

			Microsoft::WRL::ComPtr<ID3D11Buffer> outputCount;

			D3D11_BUFFER_DESC bufferDesc;
			bufferDesc.Usage = D3D11_USAGE_STAGING;
			bufferDesc.BindFlags = 0;
			bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			bufferDesc.MiscFlags = 0;
			bufferDesc.ByteWidth = sizeof(ui32);

			device->CreateBuffer(&bufferDesc, nullptr, outputCount.GetAddressOf());

			context->CopyStructureCount(outputCount.Get(), 0, view.Get());

			ui32 structureCount;
			D3D11_MAPPED_SUBRESOURCE subresource;
			result = context->Map(outputCount.Get(), 0, D3D11_MAP_READ, 0, &subresource);

			std::memcpy(&structureCount ,(ui32*)subresource.pData, sizeof(ui32));	

			context->Unmap(outputCount.Get(), 0);

			auto cpuBuffer = std::make_unique<structure[]>(structureCount);

			result = context->Map(this->data.Get(), 0, D3D11_MAP_READ, 0, &subresource);

			std::memcpy(&cpuBuffer[0] ,(structure*)subresource.pData, sizeof(structure)*structureCount);	

			context->Unmap(this->data.Get(), 0);

			return std::make_tuple(cpuBuffer, structureCount);
		}*/
	}
}


#endif