#ifndef BUF_STRUC_INOUT_H_
#define BUF_STRUC_INOUT_H_

#include "bunkcore.h"

#include "D3D.h"
#include <vector>

namespace buffer::structured
{
	template <class structure>
	class inputOutput
	{
	public:
		inputOutput(structure data, ui32 structureCount);
		inputOutput(std::vector<structure> data);
	public:
		Microsoft::WRL::ComPtr<ID3D11Buffer> data;
		Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> uaView;
		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> srView;

	private:
		// Initialization helpers
		D3D11_BUFFER_DESC bufferDescription(ui32 structureCount);
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDescription(ui32 structureCount);
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDescription(ui32 structureCount);
	};

	template<class structure>
	inline inputOutput<structure>::inputOutput(structure data, ui32 structureCount)
	{
		static_assert(std::is_standard_layout<structure>::value);

		auto device = d3d::getDevice();
		HRESULT result;

		result = device->CreateBuffer(&bufferDescription(structureCount), nullptr, this->data.GetAddressOf());

		result = device->CreateUnorderedAccessView(this->data.Get(), &uavDescription(structureCount), this->uaView.GetAddressOf());

		result = device->CreateShaderResourceView(this->data.Get(), &srvDescription(structureCount), this->srView.GetAddressOf());
	}

	template<class structure>
	inline inputOutput<structure>::inputOutput(std::vector<structure> data)
	{
		auto device = d3d::getDevice();
		HRESULT result;

		result = device->CreateBuffer(&bufferDescription(data.size()), nullptr, this->data.GetAddressOf());

		result = device->CreateUnorderedAccessView(this->data.Get(), &uavDescription(data.size()), this->uaView.GetAddressOf());

		result = device->CreateShaderResourceView(this->data.Get(), &srvDescription(data.size()), this->srView.GetAddressOf());
	}

	template<class structure>
	inline D3D11_BUFFER_DESC inputOutput<structure>::bufferDescription(ui32 structureCount)
	{
		D3D11_BUFFER_DESC bufferDesc;
		bufferDesc.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc.ByteWidth = sizeof(structure) * structureCount;
		bufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
		bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
		bufferDesc.StructureByteStride = sizeof(structure);

		return bufferDesc;
	}
	template<class structure>
	inline D3D11_UNORDERED_ACCESS_VIEW_DESC inputOutput<structure>::uavDescription(ui32 structureCount)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		uavDesc.Format = DXGI_FORMAT_UNKNOWN;
		uavDesc.Buffer.FirstElement = 0;
		uavDesc.Buffer.NumElements = structureCount;
		uavDesc.Buffer.Flags = 0;

		return uavDesc;
	}
	template<class structure>
	inline D3D11_SHADER_RESOURCE_VIEW_DESC inputOutput<structure>::srvDescription(ui32 structureCount)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.Buffer.FirstElement = 0;
		srvDesc.Buffer.NumElements = structureCount;

		return srvDesc;
	}
}

std::vector<buffer::indirect> indirectBuffers;
std::vector<buffer::structured::output<vertex::light>> gpuMeshes;
std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> srViews;


#endif