#ifndef BUFFER_STRUCTURED_H_
#define BUFFER_STRUCTURED_H_

#include <type_traits>
#include <vector>

#include <wrl/client.h>

#include "D3D.h"
#include "bunkcore.h"

namespace buffer
{
	namespace structured
	{
		template <class structure>
		class output
		{
		public:
			output();
			output(structure data, ui32 structureCount);
			output(std::vector<structure> data);
		public:
			// Returns a CPU side copy of the buffer.
			std::unique_ptr<structure[]> get();
			// Partial copy.
			std::unique_ptr<structure[]> get(std::vector<ui32> validIndices);
			ui32 size();
		public:
			Microsoft::WRL::ComPtr<ID3D11Buffer> data;
			Microsoft::WRL::ComPtr<ID3D11UnorderedAccessView> view;
		};

		template <class structure>
		class input
		{
		public:
			input(structure data, ui32 structureCount);
			input(std::vector<structure> &data);
		public:
			void set(std::vector<structure> &structures);
			void set(ui32 index, structure &data);
		public:
			Microsoft::WRL::ComPtr<ID3D11Buffer> data;
			Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> view;
		};

		
		template<class structure>
		inline output<structure>::output()
		{
		}
		//////////////////////
		// OUTPUT STRUCTURED
		//////////////////////
		template<class structure>
		inline output<structure>::output(structure data, ui32 structureCount)
		{
			static_assert(std::is_standard_layout<structure>::value);

			auto device = d3d::getDevice();
			HRESULT result;

			D3D11_BUFFER_DESC bufferDesc;
			bufferDesc.Usage = D3D11_USAGE_DEFAULT;
			bufferDesc.ByteWidth = sizeof(structure) * structureCount;
			bufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
			bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			bufferDesc.StructureByteStride = sizeof(structure);

			result = device->CreateBuffer(&bufferDesc, nullptr, this->data.GetAddressOf());

			D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
			uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
			uavDesc.Format = DXGI_FORMAT_UNKNOWN;
			uavDesc.Buffer.FirstElement = 0;
			uavDesc.Buffer.NumElements = structureCount;
			uavDesc.Buffer.Flags = 0;

			result = device->CreateUnorderedAccessView(this->data.Get(), &uavDesc, view.GetAddressOf());
		}

		template<class structure>
		inline output<structure>::output(std::vector<structure> data)
		{
			static_assert(std::is_standard_layout<structure>::value);

			auto device = d3d::getDevice();
			HRESULT result;
			ui32 structureCount = (ui32)data.size();

			D3D11_BUFFER_DESC bufferDesc;
			bufferDesc.Usage = D3D11_USAGE_DEFAULT;
			bufferDesc.ByteWidth = sizeof(structure) * structureCount;
			bufferDesc.BindFlags = D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE;
			bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
			bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			bufferDesc.StructureByteStride = sizeof(structure);

			D3D11_SUBRESOURCE_DATA initialData;
			initialData.SysMemPitch = 0;
			initialData.SysMemSlicePitch = 0;
			initialData.pSysMem = data.data();

			result = device->CreateBuffer(&bufferDesc, &initialData, this->data.GetAddressOf());

			D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
			uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
			uavDesc.Format = DXGI_FORMAT_UNKNOWN;
			uavDesc.Buffer.FirstElement = 0;
			uavDesc.Buffer.NumElements = structureCount;
			uavDesc.Buffer.Flags = 0;

			result = device->CreateUnorderedAccessView(this->data.Get(), &uavDesc, view.GetAddressOf());
		}

		template<class structure>
		inline std::unique_ptr<structure[]> output<structure>::get()
		{
			HRESULT result;
			auto context = d3d::getDeviceContext();

			D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
			view->GetDesc(&uavDesc);

			ui32 structureCount = uavDesc.Buffer.NumElements;

			auto cpuBuffer = std::make_unique<structure[]>(structureCount);

			D3D11_MAPPED_SUBRESOURCE subresource;
			result = context->Map(this->data.Get(), 0, D3D11_MAP_READ, 0, &subresource);
					
			std::memcpy(&cpuBuffer[0] ,(structure*)subresource.pData, sizeof(structure)*structureCount);	

			context->Unmap(this->data.Get(), 0);

			return cpuBuffer;
		}
		template<class structure>
		inline std::unique_ptr<structure[]> output<structure>::get(std::vector<ui32> validIndices)
		{
			HRESULT result;
			auto context = d3d::getDeviceContext();

			auto cpuBuffer = std::make_unique<structure[]>(validIndices.size());
			auto size = (ui32)validIndices.size();
			D3D11_MAPPED_SUBRESOURCE subresource;
			result = context->Map(this->data.Get(), 0, D3D11_MAP_READ, 0, &subresource);
						
			auto rc = static_cast<structure*>(subresource.pData);
			for (ui32 i = 0; i < size; i++) {
				std::memcpy(&cpuBuffer[i] , &rc[validIndices[i]], sizeof(structure));
			}				

			context->Unmap(this->data.Get(), 0);


			return cpuBuffer;
		}
		template<class structure>
		inline ui32 output<structure>::size()
		{
			D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
			view->GetDesc(&uavDesc);

			return (ui32)uavDesc.Buffer.NumElements;
		}

		//////////////////////
		// INPUT STRUCTURED
		//////////////////////
		template<class structure>
		inline input<structure>::input(structure data, ui32 structureCount)
		{
			static_assert(std::is_standard_layout<structure>::value);

			auto device = d3d::getDevice();
			HRESULT result;

			D3D11_BUFFER_DESC bufferDesc;
			bufferDesc.Usage = D3D11_USAGE_DEFAULT;
			bufferDesc.ByteWidth = sizeof(structure) * structureCount;
			bufferDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			bufferDesc.StructureByteStride = sizeof(structure);

			result = device->CreateBuffer(&bufferDesc, nullptr, this->data.GetAddressOf());

			D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
			srvDesc.Format = DXGI_FORMAT_UNKNOWN;
			srvDesc.Buffer.FirstElement = 0;
			srvDesc.Buffer.NumElements = structureCount;

			result = device->CreateShaderResourceView(this->data.Get(), &srvDesc, view.GetAddressOf());
		}

		template<class structure>
		inline input<structure>::input(std::vector<structure>& data)
		{
			static_assert(std::is_standard_layout<structure>::value);

			auto device = d3d::getDevice();
			HRESULT result;

			D3D11_BUFFER_DESC bufferDesc;
			bufferDesc.Usage = D3D11_USAGE_DEFAULT;
			bufferDesc.ByteWidth = sizeof(structure) * (ui32)data.size();
			bufferDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
			bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
			bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
			bufferDesc.StructureByteStride = sizeof(structure);

			D3D11_SUBRESOURCE_DATA initialData;
			initialData.SysMemPitch = 0;
			initialData.SysMemSlicePitch = 0;
			initialData.pSysMem = data.data();

			result = device->CreateBuffer(&bufferDesc, &initialData, this->data.GetAddressOf());

			D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
			srvDesc.Format = DXGI_FORMAT_UNKNOWN;
			srvDesc.Buffer.FirstElement = 0;
			srvDesc.Buffer.NumElements = (ui32)data.size();

			result = device->CreateShaderResourceView(this->data.Get(), &srvDesc, view.GetAddressOf());
		}

		template<class structure>
		inline void input<structure>::set(std::vector<structure> &structures)
		{
			HRESULT result;
			auto context = d3d::getDeviceContext();
			ui32 size = sizeof(structure);

			D3D11_MAPPED_SUBRESOURCE subresource;
			result = context->Map(this->data.Get(), 0, D3D11_MAP_WRITE, 0, &subresource);

			std::memcpy((structure*)subresource.pData, structures.data(), size*structures.size());

			context->Unmap(this->data.Get(), 0);
		}

		template<class structure>
		inline void input<structure>::set(ui32 index, structure &data)
		{
			HRESULT result;
			auto context = d3d::getDeviceContext();
			ui32 size = sizeof(structure);

			D3D11_MAPPED_SUBRESOURCE subresource;
			result = context->Map(this->data.Get(), 0, D3D11_MAP_WRITE, 0, &subresource);
			
			std::memcpy((structure*)subresource.pData + (index), &data, size);

			context->Unmap(this->data.Get(), 0);
		}


	}
}

#endif