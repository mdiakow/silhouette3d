#include "buffer/bufferIndex.h"

buffer::index::index(std::vector<ui32> indices)
{
	HRESULT result;

	D3D11_BUFFER_DESC ibDesc;	
	ibDesc.Usage = D3D11_USAGE_DEFAULT;
	ibDesc.ByteWidth = sizeof(ui32) * (ui32)indices.size();
	ibDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibDesc.CPUAccessFlags = 0;
	ibDesc.MiscFlags = 0;
	ibDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA iData;
	iData.pSysMem = indices.data();
	iData.SysMemPitch = 0;
	iData.SysMemSlicePitch = 0;

	result = d3d::getDevice()->CreateBuffer(&ibDesc, &iData, this->data.GetAddressOf());
	if (FAILED(result)) {
		// TODO: error check.
	}
}
