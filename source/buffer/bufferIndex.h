#ifndef BUFFER_INDEX_H_
#define BUFFER_INDEX_H_

#include <wrl/client.h>
#include <vector>
#include "bunkcore.h"

#include "d3d.h"

namespace buffer
{
	class index
	{
	public:
		index(std::vector<ui32> indices);
	public:
		Microsoft::WRL::ComPtr<ID3D11Buffer> data;
	};
}

#endif