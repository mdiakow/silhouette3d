#ifndef BUFFER_CONSTANT_H_
#define BUFFER_CONSTANT_H_

#include <type_traits>

#include <wrl/client.h>
#include "D3D.h"


namespace buffer
{
	template <class structure>
	class constant
	{
	public:
		constant(structure data);
		constant(const constant &other);
	public:
		void set(structure data);
	public:
		Microsoft::WRL::ComPtr<ID3D11Buffer> data;
	};

	template<class structure>
	inline constant<structure>::constant(structure data)
	{
		static_assert(std::is_standard_layout<structure>::value, "Struct input must be standard layout.");

		HRESULT result;

		D3D11_BUFFER_DESC description;
		description.Usage = D3D11_USAGE_DYNAMIC;
		description.ByteWidth = sizeof(structure);
		description.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		description.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		description.MiscFlags = 0;
		description.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA initData;
		initData.pSysMem = &data;
		initData.SysMemPitch = 0;
		initData.SysMemSlicePitch = 0;

		result = d3d::getDevice()->CreateBuffer(&description, &initData, this->data.GetAddressOf());
	}

	template<class structure>
	inline constant<structure>::constant(const constant & other)
	{
		HRESULT result;
		auto device = d3d::getDevice();
		auto context = d3d::getDeviceContext();

		D3D11_BUFFER_DESC description;
		other.data->GetDesc(&description);
		
		result = device->CreateBuffer(&description, nullptr, this->data.GetAddressOf());

		context->CopyResource(this->data.Get(), other.data.Get());
	}

	template<class structure>
	inline void constant<structure>::set(structure data)
	{
		HRESULT result;
		auto context = d3d::getDeviceContext();

		D3D11_MAPPED_SUBRESOURCE subresource;
		result = context->Map(this->data.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &subresource);

		std::memcpy((structure*)subresource.pData, &data, sizeof(structure));

		context->Unmap(this->data.Get(), 0);
	}

}


#endif