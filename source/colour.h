#ifndef COLOUR_H_
#define COLOUR_H_

#include "bunkcore.h"
#include "math/linalg.h"

namespace convert
{
	ui32 rgba32(v4 colour);
	ui8 *rgba8(v4 colour);
}

namespace colour
{
	ui32 rgba32(v4 colour);
	ui16 bgra16(v4 colour);
	ui8 *rgba8(v4 colour);
	ui32 r10g10b10a2(v4 colour);
}



#endif