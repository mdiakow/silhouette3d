#include "System.h"

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	bunkcore::initialize();

	bunkcore::run();

	bunkcore::cleanup();
}