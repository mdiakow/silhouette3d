#include "programs/marchingCubes/mcController.h"
#include "render/render.h"
#include "Update/update.h"

#include "shaders/cpp/shaderInstLine.h"
#include "shaders/cpp/shaderDynamicMesh.h"

using namespace marchingCubes;

controller::controller() :
	isVisible(false),
	isUpdating(false)
{
	id3d = render::findShader("3d")->add(std::bind(&controller::render3d, this));
	idil = render::findShader(shader::line::name)->add(std::bind(&controller::renderInstLine, this));
	iddynamic = render::findShader(shader::dynamicMesh::name())->add(std::bind(&controller::renderDynamic, this));
	update::add(std::bind(&controller::update, this));
}

void controller::render3d()
{
	if (isVisible) {
		model.render();
	}	
}

void marchingCubes::controller::renderInstLine()
{
	if (isVisible) {
		model.renderInstLine();
	}
}

void marchingCubes::controller::renderDynamic()
{
	if (isVisible) {
		model.renderDynamic();
	}
}

void controller::update()
{
	if (isUpdating) {
		model.update();
	}
}

void controller::enableRender()
{
	isVisible = true;
	model.camera.makeMain();
}
void controller::disableRender()
{
	isVisible = false;
}

void controller::enableUpdate()
{
	isUpdating = true;
}

void controller::disableUpdate()
{
	isUpdating = false;
}


