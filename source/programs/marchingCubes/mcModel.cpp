#include "programs/marchingCubes/mcModel.h"
#include "mesh/mesh.h"
#include "D3D.h"

#include "voxel/convolution/vpConvolution.h"
#include "voxel/shapes/voxelCube.h"
#include "voxel/shapes/voxelSphere.h"
#include "voxel/marchingCubes/marchingCubes.h"
#include "voxel/paint/vpPaint.h"
#include "voxel/featureExtraction/vpFeatures.h"

#include "random/randomFilter.h"

#include "systemCore/time/bunktime.h"

using namespace marchingCubes;

namespace helper
{
	static std::vector<v3> voxelPoints(const voxel::panel &panel);
}

static ui16 width = 16;
static ui16 height = width;
static ui16 depth = width;
static fl32 radius = (fl32)(width/2 - 2);
static fl32 isoValue = 0.5f;
static fl32 fillValue = 1.f;

static ui16 voxelCount = 4;

model::model() :
	camera( camera::controller::default3d(50, 100) ),
	panel( voxel::panel(width, voxelCount, v3::Zero()) ),
	builder( marchingCubes::meshBuilder() ),
	treeData{ generation::tree::base(v3(-100.f, 0.f, 0.f), quad::Identity()) }
{
	backQuad = mesh::quad(v3(-50.f, -50.f, 50.f), v3(50.f, 50.f, 50.f));
	backTexture = buffer::texture::input2d::checkerBoard(32, 32, 4);

	testPanel();

	voxelBounds = mesh::multi<vertex::line, instance::line, mesh::line<vertex::line>, mesh::instance<v3>>(mesh::line<vertex::line>::box(v3::Zero(), v3((fl32)width,(fl32)height, (fl32)depth)), mesh::instance<v3>(helper::voxelPoints(panel)));
	
	std::vector<render::light::directional> lights(2);
	lights[0].colour = v4(0.5f, 0.5f, 0.5f, 1.f);
	lights[0].direction = v3(-1.f, 0.f, -1.f).normalized();

	lights[1] = lights[0];
	lights[1].direction *= -1.f;

	render::light::add(lights);
}

void model::render()
{
	//backTexture->render();

	//panel.render();
}

static ui32 constantCount = 0;
static fl32 currentTime = 0.f;
static const fl32 timeStep = 0.0625f;
static const si32 tailLength = 12;

void model::renderInstLine()
{	
	voxelBounds.render();	
}

void marchingCubes::model::renderLine()
{
	treeSkeleton.render();
}

void model::renderDynamic()
{
	backTexture->render();
	panel.render();
}

void marchingCubes::model::testPanel()
{
	auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();
	
	v3 center = v3((fl32)(width*panelWidth)*0.5f, (fl32)(height*panelHeight)*0.5f, (fl32)(depth*panelDepth)*0.5f);

	auto sphere = voxel::brush::sphere(center.x()*0.75f, fillValue);
	voxel::paint::replace::point(center, sphere, panel);

	//auto cube = voxel::brush::cube((panelWidth*width*3)/4, (panelHeight*height*3)/4, (panelDepth*depth*3)/4, fillValue);
	//voxel::paint::replace::point(center, cube, panel);

	namespace vc = voxel::convolution;
	
	//auto lap = vc::mask::standard::laplacian(vc::mask::dimensions::xz, 5);
	//auto allPoints = voxel::featureExtraction::points(panel, lap, 0.2f, 1.f);
	//   
	//auto points = random::points(allPoints, 25*panelWidth*panelHeight*panelDepth);
	//auto box = voxel::brush::cube(width/4, height/4, depth/4, fillValue);
	//voxel::paint::replace::points(points, box, panel);

	//auto boxPoints = std::vector<v3>(width*panelWidth);
	//v3 currentPos = v3::Zero();
	//for (auto &point : points) {
	//	point = currentPos;
	//	currentPos.x() += 1.f;
	//}

	//boxesDebug = mesh::multi<vertex::line, instance::line, mesh::line<vertex::line>, mesh::instance<v3>>(mesh::line<vertex::line>::box(v3::Zero(), v3(1.f,1.f,1.f)), mesh::instance<v3>(boxPoints));
		
	auto smoother = vc::mask::separable::gaussian(2.f, 9);
	vc::apply(panel, smoother);

	builder.mesh(panel, isoValue);

	//build.mesh(panel, isoValue);
}

void model::update()
{
	camera.update();
}

namespace helper
{
	std::vector<v3> voxelPoints(const voxel::panel &panel)
	{
		auto [panelWidth, panelHeight, panelDepth] = panel.dimensions();
		auto [voxelWidth, voxelHeight, voxelDepth] = panel.voxelDimensions();
		std::vector<v3> points(panelWidth*panelHeight*panelDepth);

		for (ui32 x = 0; x < panelWidth; x++) {
			for (ui32 y = 0; y < panelHeight; y++) {
				for (ui32 z = 0; z < panelDepth; z++) {
					auto index = panel.flatIndex(x,y,z);
					points[index] = v3((fl32)(x*voxelWidth), (fl32)(y*voxelHeight), (fl32)(z*voxelDepth));
				}
			}
		}

		return points;
	}	
}

/*
std::unique_ptr<mesh::triangle<vertex::light>> marchingCubes::model::debugMesh()
{
//v3 offset = v3(width/8.f, height/8.f, depth/8.f);
//v3 end = v3(width, height, depth);
//auto voxelInput = voxel::cube(width, height, depth, offset, end - offset, isoValue + 1.f);

//v3 center = v3((fl32)width*0.5f, (fl32)height*0.5f, (fl32)depth*0.5f);
//auto voxelInput = voxel::sphere(width, height, depth, center, radius + 1.f, isoValue+1.f);

auto zeroBuffer = std::make_unique<fl32[]>(width*height*depth);
std::fill(&zeroBuffer[0], &zeroBuffer[0] + (width*height*depth), 0.f);
auto voxelInput = buffer::texture::input3d(width, height, depth, zeroBuffer.get());

auto brush = voxel::brush::sphere(8.f, isoValue+1.f);
//auto brush = voxel::brush::cube(32, 5, 32, isoValue+1.f);

v3 end = v3((fl32)width, (fl32)height, (fl32)depth);
std::vector<v3> points{end*0.25f, end*0.5f, end*0.75f, v3::Zero()};

voxel::paint::additive::points(points, brush, voxelInput);

//auto kernel = voxel::kernel::separable::average(9);
auto kernel = voxel::convolution::mask::separable::gaussian(3.f, 9);

//voxel::convolution::apply(voxelInput, kernel);

auto build = marchingCubes::builder<mesh::triangle<vertex::light>, vertex::light>();

return build.mesh(voxelInput, isoValue, v3::Zero());
}

std::unique_ptr<mesh::triangle<vertex::light>> marchingCubes::model::testTargetedPass()
{
auto zeroBuffer = std::make_unique<fl32[]>(width*height*depth);
std::fill(&zeroBuffer[0], &zeroBuffer[0] + (width*height*depth), 0.f);
auto voxelInput = buffer::texture::input3d(width, height, depth, zeroBuffer.get());

auto floor = voxel::brush::cube(width, height/8, depth, fillValue);
auto wall = voxel::brush::cube(width/8, height, depth, fillValue);
//auto wall = voxel::brush::sphere(12.f, fillValue);
v3 center = v3((fl32)width*0.5f, 10.f, (fl32)depth*0.5f);

voxel::paint::replace::point(center, floor, voxelInput);
voxel::paint::replace::point(center, wall, voxelInput);

//auto kernel = voxel::convolution::mask::separable::gaussian(3.5f, 9);
//voxel::convolution::apply(voxelInput, kernel);

namespace cm = voxel::convolution::mask;
auto kernel = cm::standard::laplacian(cm::dimensions::xz, 3);
auto rawPoints = voxel::featureExtraction::points(voxelInput, kernel, 0.0f, 100.0f);

auto points = random::points(rawPoints, 150);

boxesDebug = mesh::multi<vertex::line, instance::line, mesh::line<vertex::line>, mesh::instance<v3>>(
mesh::line<vertex::line>::box(v3::Zero(), v3(1.f,1.f,1.f)), 
mesh::instance<v3>(points));

auto sphere = voxel::brush::sphere(5.5f, fillValue);
voxel::paint::replace::points(points, sphere, voxelInput);

auto smooth = cm::separable::gaussian(1.f, 5);
voxel::convolution::apply(voxelInput, smooth);

auto build = marchingCubes::builder<mesh::triangle<vertex::light>, vertex::light>();

return build.mesh(voxelInput, isoValue, v3::Zero());
}
*/