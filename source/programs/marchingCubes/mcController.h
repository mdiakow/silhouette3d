#ifndef MARCH_CUBE_CONTROLLER_H_
#define MARCH_CUBE_CONTROLLER_H_

#include "programs/marchingCubes/mcModel.h"
#include "programs/marchingCubes/mcViewUi.h"

namespace marchingCubes
{
	class controller
	{
	public:
		controller();
	public:
		void render3d();
		void renderInstLine();
		void renderDynamic();
		void update();

		void enableRender();
		void disableRender();

		void enableUpdate();
		void disableUpdate();
	private:
		marchingCubes::model model;
		marchingCubes::viewui viewui;

		ui64 id3d;
		ui64 idil;
		ui64 iddynamic;

		bool isUpdating;
		bool isVisible;
	};
}


#endif