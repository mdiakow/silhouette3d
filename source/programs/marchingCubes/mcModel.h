#ifndef MARCUBES_MODEL_H_
#define MARCUBES_MODEL_H_

#include <vector>

#include "voxel/marchingCubes/marchingCubes.h"
#include "voxel/vpPanel.h"
#include "camera/cameraController.h"
#include "buffer/texture/bufTex2d.h"

#include "mesh/meshInstance.h"
#include "mesh/meshLine.h"
#include "mesh/meshMulti.h"

#include "shaders/cpp/shaderInstLine.h"
#include "render/light/renderLight.h"
#include "generation/tree/genTree.h"

namespace marchingCubes
{
	class model
	{
	public:
		model();
	public:
		void render();
		void renderInstLine();
		void renderLine();
		void renderDynamic();
		void update();

		void testPanel();
	public:
		camera::controller camera;

		voxel::panel panel;
		
		marchingCubes::meshBuilder builder;

		std::unique_ptr<mesh::triangle<::vertex::light>> meshDebug;
		std::unique_ptr<mesh::triangle<::vertex::light>> backQuad;
		std::unique_ptr<buffer::texture::input2d> backTexture;

		mesh::multi<vertex::line, instance::line, mesh::line<vertex::line>, mesh::instance<v3>> voxelBounds;
		mesh::multi<vertex::line, instance::line, mesh::line<vertex::line>, mesh::instance<v3>> boxesDebug;

		generation::tree::base treeData;
		mesh::line<vertex::line> treeSkeleton;
	};
}

#endif