#ifndef CNVS_VIEW_UI_H_
#define CNVS_VIEW_UI_H_

#include <memory>

#include "userInterface/userInterface.h"
#include "userInterface/text/uiTextLine.h"
#include "userInterface/form/uiFormInput.h"

namespace canvas
{
	class viewui
	{
	public:
		viewui();
		
		void render();
		void update();
	public:
		std::unique_ptr<userInterface::box> box;
		
		std::unique_ptr<userInterface::form::input<fl32>> radiusForm;
		std::unique_ptr<userInterface::form::input<v4>> colourForm;
	};
}



#endif