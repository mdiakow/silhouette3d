#ifndef PROG_CANVAS_2D_H_
#define PROG_CANVAS_2D_H_

#include <memory>
#include <vector>

#include "bunkcore.h"
#include "camera/cameraController.h"

#include "canvas/paint/cnvsBrush.h"
#include "canvas/cnvsPanel.h"


namespace canvas
{
	class model
	{
	public:
		model();
	public:
		void update();
		void setBrush(fl32 radius, v4 colour);
	public:
		camera::controller camera;
		canvas::panel panel;
		canvas::brush brush;
		
		v2 worldMousePrevious;
	};
}

#endif