#include "programs/canvas/canvasController.h"
#include "render/render.h"
#include "Update/update.h"
#include "console/console.h"

#include "canvas/paint/cnvsPaint.h"

namespace canvas
{
	controller::controller() :
		model(canvas::model()),
		viewui(canvas::viewui()),
		drawButton(input::button(new ui16{input::mouse::leftButton}, 1u)),
		isVisible(false),
		isUpdating(false)
	{
		id2d = render::findShader("2d")->add(std::bind(&controller::render2d, this));
		idui = render::findShader("ui")->add(std::bind(&controller::renderUi, this));
		update::add(std::bind(&controller::update, this));
	}

	controller::~controller()
	{
		render::findShader("2d")->remove(id2d);
		render::findShader("ui")->remove(idui);
	}

	void controller::update()
	{
		if (isUpdating) {
			v2 worldMouse = input::mouse::position::world();

			model.update();

			if (drawButton.onPress()) {
				canvas::paint::point(worldMouse, model.panel, model.brush);
			}

			if (drawButton.isHeld()) {
				canvas::paint::line(model.worldMousePrevious, worldMouse, model.panel, model.brush);
			}

			model.worldMousePrevious = worldMouse;

			v2 uiMouse = input::mouse::position::ui();
			if (input::key::isDown(input::mouse::leftButton)) {
				viewui.radiusForm->tryFocus(uiMouse);
				viewui.colourForm->tryFocus(uiMouse);
			}

			if (input::key::onPress(input::key::enter)) {
				fl32 radius = viewui.radiusForm->getValue();
				if (radius < 2.f) {
					radius = 2.f;
				}

				v4 colour = viewui.colourForm->getValue();
				for (ui32 i = 0; i < 4; i++) {
					if(colour[i] > 1.f) colour[i] = 1.f;
					if(colour[i] < 0.f) colour[i] = 0.f;
				}
				model.setBrush(radius, colour);
			}

			viewui.radiusForm->update();
			viewui.colourForm->update();
		}
	}

	void controller::render2d()
	{
		if (isVisible) {
			model.panel.render();
		}
	}

	void controller::renderUi()
	{
		if (isVisible) {
			viewui.render();
		}		
	}

	void controller::enableRender()
	{
		model.camera.makeMain();
		isVisible = true;
	}

	void controller::disableRender()
	{
		isVisible = false;
	}

	void controller::enableUpdate()
	{
		isUpdating = true;
	}

	void controller::disableUpdate()
	{
		isUpdating = false;
	}
}

