#ifndef CNVS_CONTROLLER_H_
#define CNVS_CONTROLLER_H_

#include "programs/canvas/canvasModel.h"
#include "programs/canvas/canvasViewUi.h"
#include "input/inputButton.h"
#include "input/inputAxes.h"

namespace canvas
{
	class controller
	{
	public:
		controller();
		~controller();
	public:
		void update();

		void render2d();
		void renderUi();

		void enableRender();
		void disableRender();

		void enableUpdate();
		void disableUpdate();
	private:
		canvas::model model;
		canvas::viewui viewui;

		ui64 idui;
		ui64 id2d;

		input::button drawButton;

		bool isVisible;
		bool isUpdating;
	};
}

#endif