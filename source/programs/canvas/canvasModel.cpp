#include "programs/canvas/canvasModel.h"
#include "render/render.h"
#include "systemCore/time/bunktime.h"

namespace canvas
{
	model::model() :
		camera( camera::controller::default2d(500, 0) ),
		brush( canvas::brush::circle(16.f, v4(1.f, 1.f, 0.f, 1.f)) ),
		panel( canvas::panel(256, 2, v3(0.f, 0.f, 0.f), v4(0.25f, 0.25f, 0.25f, 1.f)) )
	{
	}

	void model::setBrush(fl32 radius, v4 colour)
	{
		brush = canvas::brush::circle(radius, colour);
	}

	void model::update()
	{
		camera.update();
	}
}