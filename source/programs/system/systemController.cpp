#include "programs/system/systemController.h"
#include "render/render.h"
#include "Update/update.h"
#include "console/console.h"

namespace mainSystem
{

	auto enableVisibility = [](auto &system) { system->enableRender(); };
	auto disableVisibility = [](auto &system) { system->disableRender(); };

	auto enableUpdate = [](auto &system) { system->enableUpdate(); };
	auto disableUpdate = [](auto &system) { system->disableUpdate(); };

	controller::controller() :
		viewui(mainSystem::viewui()),
		systems()
	{	
		idui = render::findShader("ui")->add(std::bind(&controller::renderUi, this));
		update::add(std::bind(&controller::update, this));
		currentSystem = nullptr;
	}

	controller::~controller()
	{
		render::findShader("ui")->remove(idui);
	}

	void controller::update()
	{
		viewui.update();
	}
	
	void controller::renderUi()
	{
		viewui.render();
	}
	
	void controller::addSystem(systemVariant &system, str label)
	{
		systems.push_back(std::move(system));
		auto &sys = systems.back();
		// Callback that the tab will run when pressed
		auto onPress = [&sys, this]() { 
			// turns off visibility/updating of the current tab
			unsetSystem(); 
			// turn on visibility/updating for the tab that was pressed
			std::visit(enableVisibility, sys);
			std::visit(enableUpdate, sys);
			currentSystem = &sys;
		};

		viewui.tabBar->addTab(label, onPress);
		// This could be cleaned up (I should probably attach a camera to system controller so the program can
		// launch without error if no system is loaded)
		std::visit(enableVisibility, systems[0]);
		std::visit(enableUpdate, systems[0]);
		currentSystem = &systems[0];
	}

	void controller::unsetSystem()
	{
		std::visit(disableVisibility, *currentSystem);
		std::visit(disableUpdate, *currentSystem);
	}

}