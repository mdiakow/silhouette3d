#ifndef SYSTEM_CONTROLLER_H_
#define SYSTEM_CONTROLLER_H_

#include <variant>
#include <deque>
#include <memory>

#include "programs/system/systemViewUI.h"

// Various system components that can be added
#include "programs/canvas/canvasController.h"
#include "programs/foliage/foliageController.h"
#include "programs/marchingCubes/mcController.h"
namespace mainSystem
{
	using systemVariant = std::variant<
		std::unique_ptr<canvas::controller>,
		std::unique_ptr<foliage::controller>,
		std::unique_ptr<marchingCubes::controller>
	>;

	class controller
	{
	public:
		controller();
		~controller();
	public:
		void update();

		void renderUi();
		
		std::deque<mainSystem::systemVariant> systems;

		void addSystem(systemVariant &system, str label);
	private:
		void unsetSystem(); // Unsets the current system.
	private:
		::mainSystem::viewui viewui;
	
		mainSystem::systemVariant *currentSystem;

		ui64 idui;
	};
}


#endif