#include "programs/system/systemViewUI.h"
#include "userInterface/userInterface.h"
namespace mainSystem
{
	viewui::viewui()
	{
		namespace ui = userInterface;
		auto uiStyle = std::make_unique<ui::style>(ui::getDefaultFont(), ui::defaultNeutral(), ui::defaultMouseOver(), ui::defaultMousePress(), ui::defaultBorder());
		auto uiInput = ui::getDefaultInput();
		ui::textFormatting format = uiStyle->font->format;
		v2 spacer = v2(0.f, format.fontHeight + (2*format.margin.top));
		
		v2 lowerLeft = ui::screen::topLeft() - spacer;
		v2 upperRight = ui::screen::topRight();
		
		tabBar = std::make_unique<ui::tabBar>(lowerLeft, upperRight, ui::getDefaultInput(), ui::getDefaultStyle());
	}

	void viewui::render()
	{
		tabBar->render();
	}

	void viewui::update()
	{
		tabBar->update();
	}

	void viewui::addTab(str label)
	{

	}
}