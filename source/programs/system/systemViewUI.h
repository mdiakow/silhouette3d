#ifndef SYSTEM_VIEW_UI_H_
#define SYSTEM_VIEW_UI_H_

#include <memory>
#include <vector>

#include "userInterface/tab/uiTab.h"
#include "userInterface/tab/uiTabBar.h"

namespace mainSystem
{
	class viewui
	{
	public:
		viewui();

	public:
		void render();
		void update();

		void addTab(str label);
	public:
		//std::unique_ptr<userInterface::tab> tab;
		//std::vector<std::unique_ptr<userInterface::tab>> tabs;
		//std::unique_ptr<userInterface::box> tabBar;

		std::unique_ptr<userInterface::tabBar> tabBar;
	};
}


#endif