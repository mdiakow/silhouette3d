#include "programs/foliage/foliageModel.h"
#include "animation/field/aniFieldVector.h"
#include "animation/field/aniFieldEmitter.h"

#include <random>

namespace initialize 
{
	static animation::particle::emitterInitializer emitter();
	static buffer::structured::input<animation::water::waveTransform> waveTransforms();
}


namespace foliage
{
	fl32 majorRadius = 50.f;
	fl32 minorRadius = 2.f;
	ui32 countsPhi = 200;
	ui32 countsTheta = 200;

	fl32 sphereRadius = 75.f;
	fl32 sphereMagnitude = 3.f;
	static v4 initialColour = v4(0.05f, 0.0f, 0.0f, 0.005f);
	static v3 surfaceSize = v3(500.f, 0.f, 500.f);

	ui32 waterWidth = 512;
	ui32 waterHeight = 512;
	fl32 waterScale = 1.0f;	

	model::model() :
		foliage(std::make_unique<animation::foliage>(8*100, v3(0.f, 0.f, 0.f), v3(500.f, 0.f, 500.f))),
		foliageSurface(mesh::quad(v3(0.f, 0.f, 0.f), surfaceSize)),
		surfaceTexture(buffer::texture::input2d::checkerBoard(32, 32, 4)),
		camera(camera::controller::default3d(500, 50)),
		emitter( {initialize::emitter(), buffer::texture::input2d(buffer::texture::fill2d::fadeOutCentre, 8.f, initialColour), animation::field::vector::sphere(sphereRadius, sphereMagnitude*0.05f), animation::field::emitter::sphere(sphereRadius*0.75f, sphereMagnitude, countsTheta, countsPhi)} ),
		water{ animation::water::surface(waterWidth, waterHeight, waterScale, initialize::waveTransforms()) }
	{

	}


	void model::update()
	{
		camera.update();
		foliage->updateOrientations();
		emitter.update();
		water.update();
	}

	void model::render3d()
	{
		surfaceTexture->render();
		foliageSurface->render();
	}

	void model::render3danim()
	{
		foliage->render();
	}

	void model::renderWater()
	{
		water.render();
	}

	void model::render3dinst()
	{
		emitter.render();
	}
}

namespace initialize
{
	animation::particle::emitterInitializer emitter()
	{
		animation::particle::emitterInitializer e;
		//e.perFrame.emitterPosition = foliage::surfaceSize*0.5f;
		e.perFrame.emitterPosition = v3(-100.f, 0.f, 0.f);
		e.particles.emitterBound = 5.f;

		e.particles.createCount = 64;
		e.particles.createRate = 0.0f;

		e.particles.direction = v3(0.f, 1.f, 0.f).normalized();
		e.particles.directionAngle = 0.1f;

		e.particles.scaleStart.min = 10.0f;
		e.particles.scaleStart.max = 50.0f;

		e.particles.scaleEnd.min = 0.0f;
		e.particles.scaleEnd.max = 0.0f;

		e.particles.force.min = 0.0f;
		e.particles.force.max = 0.0f;

		e.particles.lifetime = 3.0f;
		
		e.perFrame.gravity = v3(0.f, 0.0f, 0.0f);

		e.particles.colourStart = v4(0.05f,0.05f,0.025f,0.0125f);
		e.particles.colourEnd = v4(0.4f,0.125f,0.0f,0.00f);

		return e;
	}
	
	static ui32 waveCount = 8;
	fl32 maxHeight = (fl32)(waveCount);
	static std::random_device rd;
	
	static std::mt19937 generator;
	namespace big 
	{
		static ui32 waveCount = 8;
		fl32 maxSize = (fl32)waveCount*2;
		static std::uniform_real_distribution<fl32> amplitude(0.f, maxSize);
		static std::uniform_real_distribution<fl32> frequency(0.005f, 0.02f);
		static std::uniform_real_distribution<fl32> direction(-1.f, 1.f);
		static std::uniform_real_distribution<fl32> speed(0.25f, 0.5f);
	}
	namespace little
	{
		static ui32 waveCount = 8;
		fl32 maxSize = (fl32)waveCount * 0.2f;
		static std::uniform_real_distribution<fl32> amplitude(0.f, maxSize);
		static std::uniform_real_distribution<fl32> frequency(0.1f, 0.2f);
		static std::uniform_real_distribution<fl32> direction(-1.f, 1.f);
		static std::uniform_real_distribution<fl32> speed(5.f, 10.f);
	}

	
	static buffer::structured::input<animation::water::waveTransform> waveTransforms()
	{
		std::vector<animation::water::waveTransform> waves(big::waveCount + little::waveCount);

		for (ui32 littleIndex = 0; littleIndex < little::waveCount; littleIndex++) {
			auto &littleWave = waves[littleIndex];
			
			littleWave.amplitude = little::amplitude(generator);
			littleWave.frequency = little::frequency(generator);
			
			littleWave.direction = v2(little::direction(generator), little::direction(generator)).normalized();
			//littleWave.speed = little::speed(generator);
			littleWave.speed = (fl32)(littleIndex + 1) * 0.25f;

		}

		for (ui32 bigIndex = 0; bigIndex < big::waveCount; bigIndex++) {
			auto &bigWave = waves[little::waveCount + bigIndex];
			bigWave.amplitude = big::amplitude(generator);
			bigWave.frequency = big::frequency(generator);
			
			bigWave.direction = v2(big::direction(generator), big::direction(generator)).normalized();
			//bigWave.speed = big::speed(generator);
			bigWave.speed = (fl32)(bigIndex + 1)* 0.1f;
		}

		return buffer::structured::input<animation::water::waveTransform>(waves);
	}
}