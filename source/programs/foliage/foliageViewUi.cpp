#include "programs/foliage/foliageViewUi.h"
#include "userInterface/userInterface.h"


namespace foliage
{
	viewui::viewui()
	{
		namespace ui = userInterface;

		v2 screenSize = ui::screen::topRight() - ui::screen::bottomLeft();

		v2 lowerLeft = ui::screen::bottomLeft() + v2(0.f, screenSize.y()*0.25f);
		v2 upperRight = ui::screen::topLeft() + v2(screenSize.x()*0.25f, -screenSize.y()*0.25f);

		ui::boundingBox bounds(lowerLeft, upperRight);
		auto uiStyle = std::make_unique<ui::style>(ui::getDefaultFont(), ui::defaultNeutral(), ui::defaultMouseOver(), ui::defaultMousePress(), ui::defaultBorder());
		box = std::make_unique<ui::box>(lowerLeft, upperRight, uiStyle->neutral.background);

		auto format = uiStyle->font->format;
		v2 marginUpperLeft = v2(-(fl32)format.margin.left, (fl32)format.margin.top);
		v2 marginUpperRight = v2((fl32)format.margin.right, (fl32)format.margin.top);

		lowerLeft = bounds.getUpperLeft() - marginUpperLeft - v2(0.f, (fl32)format.fontHeight);
		upperRight = bounds.getUpperRight() - marginUpperRight;
		force = std::make_unique<ui::form::input<v4>>("Force: ", "X  : ", "Y  : ", "Z  : ", "Mag: ",lowerLeft, upperRight, uiStyle.get());
		
		lowerLeft.y() -= ((fl32)format.fontHeight + (fl32)format.betweenLines)*5.f;
		upperRight.y() -= ((fl32)format.fontHeight + (fl32)format.betweenLines)*5.f;
		waveTransform = std::make_unique<ui::form::input<v4>>("WaveTransform: ", "amplitude: ", "frequency: ", "phase: ", "vertShift: ",lowerLeft, upperRight, uiStyle.get());
	}

	void viewui::renderui()
	{
		box->render();
		force->render();
		waveTransform->render();
	}

	void viewui::update()
	{
		v2 uiMouse = input::mouse::position::ui();

		if (input::key::isDown(input::mouse::leftButton)) {
			force->tryFocus(uiMouse);
			waveTransform->tryFocus(uiMouse);
		}

		force->update();
		waveTransform->update();
	}
}