#ifndef FOLIAGE_MODEL_H_
#define FOLIAGE_MODEL_H_

#include <memory>
#include "camera/camera.h"
#include "camera/cameraController.h"

#include "animation/foliage/aniFoliage.h"
#include "animation/particle/aniParEmitter.h"
#include "animation/water/aniWater.h"

#include "buffer/texture/bufTex2d.h"
#include "mesh/mesh.h"

namespace foliage
{
	class model
	{
	public:
		model();

		void update();

		void render3d();
		void render3dinst();
		void render3danim();
		void renderWater();
	public:
		camera::controller camera;
		fl32 lookSpeed = 50.f;
		fl32 moveSpeed = 500.f;

		std::unique_ptr<animation::foliage> foliage;
		std::unique_ptr<mesh::triangle<vertex::light>> foliageSurface;
		std::unique_ptr<buffer::texture::input2d> surfaceTexture;

		animation::particle::emitter emitter;
		animation::water::surface water;
		
	};
}


#endif