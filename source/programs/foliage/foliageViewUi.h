#ifndef FOLIAGE_VIEWUI_H_
#define FOLIAGE_VIEWUI_H_

#include "userInterface/form/uiFormInput.h"

namespace foliage
{
	class viewui
	{
	public:
		viewui();

	public:
		void renderui();
		void update();
	public:
		std::unique_ptr<userInterface::box> box;
		// x,y,z direction + a magnitude
		std::unique_ptr<userInterface::form::input<v4>> force;
		std::unique_ptr<userInterface::form::input<v4>> waveTransform;
	};
}


#endif