#ifndef FOLIAGE_CONTROLLER_H_
#define FOLIAGE_CONTROLLER_H_

#include "bunkcore.h"
#include "input/inputAxes.h"
#include "programs/foliage/foliageModel.h"
#include "programs/foliage/foliageViewUi.h"

namespace foliage
{
	class controller
	{
	public:
		controller();

	public:
		void update();

		void render3d();
		void render3dinst();
		void render3danim();
		void renderUi();
		void renderWater();

		void enableRender();
		void disableRender();

		void enableUpdate();
		void disableUpdate();
	public:
		foliage::model model;
		foliage::viewui viewui;

		ui64 idui;
		ui64 id3d;
		ui64 id3di;
		ui64 id3da;
		ui64 idWater;

		bool isVisible;
		bool isUpdating;
	};
}

#endif