#include "programs/foliage/foliageController.h"
#include "systemCore/time/bunktime.h"
#include "render/render.h"
#include "Update/update.h"

#include "console/console.h"

#include "shaders/cpp/shaderInstAniLight.h"
#include "shaders/cpp/shaderParticle.h"
#include "shaders/cpp/shaderWater.h"

namespace foliage
{
	controller::controller() :
		viewui(foliage::viewui()),
		model(foliage::model()),
		isUpdating(false),
		isVisible(false)
	{
		id3d = render::findShader("3d")->add(std::bind(&controller::render3d, this));
		id3da = render::findShader(shader::animated::light::name)->add(std::bind(&controller::render3danim, this));
		id3di = render::findShader(shader::particle::name())->add(std::bind(&controller::render3dinst, this));
		idWater = render::findShader(shader::water::name())->add(std::bind(&controller::renderWater, this));

		idui = render::findShader("ui")->add(std::bind(&controller::renderUi, this));
		update::add(std::bind(&controller::update, this));
	}

	void controller::update()
	{
		if (isUpdating) {
			model.update();
						
			if (input::key::onPress(input::key::enter)) {
				v4 force = viewui.force->getValue();
				v3 direction = v3(force.x(), force.y(), force.z()).normalized();
				if (direction.isZero(0)) {
					direction = v3(1.f,1.f,1.f).normalized();
				}
				fl32 magnitude = force.w();
				if (magnitude == 0.f) magnitude = 1.0f;

				model.foliage->wind->magnitude = magnitude;
				model.foliage->wind->direction = direction;

				v4 waveTransform = viewui.waveTransform->getValue();
				
				model.foliage->wind->transform.amplitude = waveTransform.x();
				model.foliage->wind->transform.frequency = waveTransform.y();
				model.foliage->wind->transform.phaseShift = waveTransform.z();
				model.foliage->wind->transform.verticalShift = waveTransform.w();
			}

			viewui.update();
		}
	}
	
	void controller::render3d()
	{
		if (isVisible) {
			model.render3d();
		}
	}

	void controller::render3dinst()
	{
		if (isVisible) {
			model.render3dinst();
		}
	}

	void controller::render3danim()
	{
		if (isVisible) {
			model.render3danim();
		}
	}

	void controller::renderUi()
	{
		if (isVisible) {
			viewui.renderui();
		}
	}

	void controller::renderWater()
	{
		if (isVisible) {
			model.renderWater();
		}
	}

	void controller::enableRender()
	{
		model.camera.makeMain();
		isVisible = true;
	}

	void controller::disableRender()
	{
		isVisible = false;
	}

	void controller::enableUpdate()
	{
		isUpdating = true;
	}

	void controller::disableUpdate()
	{
		isUpdating = false;
	}
}