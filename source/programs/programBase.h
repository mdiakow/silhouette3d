#ifndef PROGRAM_BASE_H_
#define PROGRAM_BASE_H_

namespace program
{
	class base
	{
	public:

		void show();
		void hide();

		void enableUpdating();
		void disbaleUpdating();
	protected:
		bool isVisible;
		bool isUpdating;


		base();
		virtual ~base();
	};
}

#endif