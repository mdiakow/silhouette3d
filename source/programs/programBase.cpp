#include "programs/programBase.h"

namespace program
{
	void base::show()
	{
		isVisible = true;
	}
	void base::hide()
	{
		isVisible = false;
	}
	void base::enableUpdating()
	{
		isUpdating = true;
	}
	void base::disbaleUpdating()
	{
		isUpdating = false;
	}
	base::base() :
		isVisible(true),
		isUpdating(true)
	{
	}
	base::~base()
	{
	}
}