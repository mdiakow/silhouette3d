#ifndef UPDATE_H_
#define UPDATE_H_

#include <functional>

namespace update
{
	void add(std::function<void()> fn);
}


#endif