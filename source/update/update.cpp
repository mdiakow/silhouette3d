#include "update.h"
#include "bunkcore.h"


#include <vector>

namespace update
{
	std::vector<std::function<void()>> active;
	std::vector<std::function<void()>> inactive;
	void all()
	{
		for (ui32 i = 0; i < active.size(); i++) {
			active[i]();
		}
	}

	void add(std::function<void()> fn)
	{
		active.push_back(fn);
	}

	void initialize()
	{

	}
	void cleanup()
	{

	}
}
