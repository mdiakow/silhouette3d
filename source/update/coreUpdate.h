// coreUpdate.h
// Core functionality of update (initialize, cleanup, and the
// function for general update).

#ifndef CORE_UPDATE_H_
#define CORE_UPDATE_H_

namespace update
{
	void all();

	void initialize();
	void cleanup();
}

#endif