#include "colour.h"

namespace convert
{
	ui32 rgba32(v4 colour)
	{
		ui8 red = (ui8)(colour(0) * 255.f);
		ui8 green = (ui8)(colour(1) * 255.f);
		ui8 blue = (ui8)(colour(2) * 255.f);
		ui8 alpha = (ui8)(colour(3) * 255.f);

		return (alpha << 24) | (blue << 16) | (green << 8) | (red);
	}

	ui8 *rgba8(v4 colour)
	{
		ui8 *colours = new ui8[4];

		colours[0] = (ui8)(colour(0) * 255.f); // red
		colours[1] = (ui8)(colour(1) * 255.f); // green
		colours[2] = (ui8)(colour(2) * 255.f); // blue
		colours[3] = (ui8)(colour(3) * 255.f); // alpha

		return colours;
	}
}

namespace colour
{
	ui32 rgba32(v4 colour)
	{
		ui8 red = (ui8)(colour(0) * 255.f);
		ui8 green = (ui8)(colour(1) * 255.f);
		ui8 blue = (ui8)(colour(2) * 255.f);
		ui8 alpha = (ui8)(colour(3) * 255.f);

		return (alpha << 24) | (blue << 16) | (green << 8) | (red);
	}

	ui32 r10g10b10a2(v4 colour)
	{
		ui32 red = (ui32)(colour(0)*1023.f); // 1023 = 10 bit max
		ui32 green = (ui32)(colour(1)*1023.f);
		ui32 blue = (ui32)(colour(2)*1023.f);
		ui32 alpha = (ui32)(colour(3)*3.f); // 3 = 2 bit max
		return (alpha << 30) | (blue << 20) | (green << 10) | (red);
	}

	ui16 bgra16(v4 colour)
	{
		ui16 red = (ui16)(colour(0) * 31.f);
		ui16 green = (ui16)(colour(1) * 31.f);
		ui16 blue = (ui16)(colour(2) * 31.f);
		ui16 alpha = (ui16)(colour(3) * 1.f);

		return (alpha << 15) | (red << 10) | (green << 5) | (blue);
	}

	ui8 *rgba8(v4 colour)
	{
		ui8 *colours = new ui8[4];

		colours[0] = (ui8)(colour(0) * 255.f); // red
		colours[1] = (ui8)(colour(1) * 255.f); // green
		colours[2] = (ui8)(colour(2) * 255.f); // blue
		colours[3] = (ui8)(colour(3) * 255.f); // alpha

		return colours;
	}

}
