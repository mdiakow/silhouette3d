#include "programObjects/programobjects.h"

#include "bunkcore.h"

//#include "canvasProgram.h"
#include "console/coreConsole.h"
//#include "programs/canvas/canvasController.h"
#include "programs/system/systemController.h"


namespace programObjects
{
	std::unique_ptr<canvas::controller> cvs;
	std::unique_ptr<mainSystem::controller> sys;
	
	void initialize()
	{
		console::initialize();
		
		//cvs = std::make_unique<canvas::controller>();
		sys = std::make_unique<mainSystem::controller>();
		
		mainSystem::systemVariant canvas = std::make_unique<canvas::controller>();
		mainSystem::systemVariant foliSim = std::make_unique<foliage::controller>();
		mainSystem::systemVariant marchingCube = std::make_unique<marchingCubes::controller>();

		sys->addSystem(foliSim, "Foliage Simulation");
		sys->addSystem(canvas, "Canvas");
		sys->addSystem(marchingCube, "marchingCubes");
	}

	void cleanup()
	{
		console::cleanup();
		//canvas::cleanup();
	}
}
