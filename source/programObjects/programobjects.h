#ifndef PROGRAM_OBJECTS_H_
#define PROGRAM_OBJECTS_H_

namespace programObjects
{
	void initialize();
	void cleanup();
}

#endif