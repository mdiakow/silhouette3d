#ifndef ERROR_LOG_H_
#define ERROR_LOG_H_

#include <iostream>
#include <fstream>

namespace error
{
	template <typename T>
	void log(T toLog)
	{
		std::ofstream fileStream;
		fileStream.open("error/errorOutput.txt", std::ios_base::app);
		fileStream << toLog << std::endl;
		fileStream.close();
	}
}

#endif