#ifndef RENDER_QUEUE_H_
#define RENDER_QUEUE_H_

#include "bunkcore.h"
#include <functional>
#include <vector>
#include <map>
#include <atomic>

namespace render
{
	template<class shdr, class func> 
	class queue
	{
	public:
		queue(shdr shader);
		virtual ~queue();

		ui64 add(func fn);
		void remove(ui64 byId);

		void all();

		// These functions are meant to be used for additional pipeline instructions.
		// EG, turn of zbuffering, alpha blending, etc.
		std::function<void()> start; // runs before all()
		std::function<void()> end; // runs after all()
		
		shdr shader;

		
	protected:
		std::map<ui64, func> functions;
		static inline std::atomic<ui64> uniqueId{0};
	};

	template<class shdr, class func>
	inline queue<shdr, func>::queue(shdr shader) :
		shader( shader )
	{
	}

	template<class shdr, class func>
	inline queue<shdr, func>::~queue()
	{
		
	}

	template<class shdr, class func>
	inline ui64 queue<shdr, func>::add(func fn)
	{
		//funcVector.push_back(fn);
		ui64 currentId = uniqueId;
		functions.insert(std::make_pair(currentId, fn));
		uniqueId++;
		return currentId;
	}

	template<class shdr, class func>
	inline void queue<shdr, func>::remove(ui64 byId)
	{
		functions.erase(byId);
	}

	template<class shdr, class func>
	inline void queue<shdr, func>::all()
	{
		for (auto const& function : functions) {
			function.second();
		}
	}
}


#endif