#ifndef RENDER_MATERIAL_H_
#define RENDER_MATERIAL_H_

#include "bunkcore.h"
#include "buffer/texture/bufTex2d.h"

namespace render
{
	class material
	{
	public:
		
	public:
		void render();
	public:
		fl32 emissive;
		fl32 specularity;

		buffer::texture::input2d diffuse;
		//buffer::texture::input2d normal;
		//buffer::texture::input2d specular;
	};
}

#endif