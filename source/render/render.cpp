#include "render.h"
#include "render/core/coreRender.h"
#include "render/core/coreShader.h"
#include "render/core/coreLight.h"

#include "debugger/bunkdebug.h"

#include <vector>

#include "bunkcore.h"

namespace render
{
	static void setMatrices();

	// List of functions to iterate through and render.
	std::vector<std::function<void()>> renders;

	static bool fullScreen = false;
	static bool vsyncEnabled = true;

	static fl32 screenFar = 10000.0f;
	static fl32 screenNear = 0.1f;

	// screen width and height
	static fl32 sWidth;
	static fl32 sHeight;
	
	camera *mainCamera;

	m4 world, view, projection, ortho, perspective;

	HWND handle;

	bool isFullscreen()
	{
		return fullScreen;
	}

	void all()
	{
		setMatrices();

		d3d::beginScene();

		render::light::all();
		render::shader::all();
		

		d3d::endScene();
	}

	void setMainCamera(camera *cam)
	{
		mainCamera = cam;
	}

	camera * getMainCamera()
	{
		return mainCamera;
	}

	v3 getCameraPosition()
	{
		return mainCamera->position;
	}

	v2 getScreenSize()
	{
		return v2(sWidth, sHeight);
	}

	m4 getWorld()
	{
		return world;
	}

	m4 getView()
	{
		return view;
	}

	m4 getPerspective()
	{
		return perspective;
	}

	m4 getOrtho()
	{
		return ortho;
	}

	HWND getHwnd()
	{
		return handle;
	}


	void add(std::function<void()> fn)
	{
		renders.push_back(fn);
	}

	void initialize(ui32 screenWidth, ui32 screenHeight, HWND hwnd)
	{
		handle = hwnd;

		sWidth = (fl32)screenWidth;
		sHeight = (fl32)screenHeight;
		fl32 fov = 3.141592654f / 4.0f;
		fl32 screenAspect = (float)screenWidth / (float)screenHeight;
		d3d::initialize(screenWidth, screenHeight, vsyncEnabled, hwnd, fullScreen, screenFar, screenNear);

		
		ortho = linalg::lefthand::orthographic(sWidth, sHeight, screenNear, screenFar);
		perspective = linalg::lefthand::perspective(fov, screenAspect, screenNear, screenFar);
		world = m4::Identity();

		XMMATRIX xmWorld = XMMatrixIdentity();
		XMMATRIX xmOrtho = XMMatrixOrthographicLH(sWidth, sHeight, screenNear, screenFar);
		XMMATRIX xmPersp = XMMatrixPerspectiveFovLH(fov, screenAspect, screenNear, screenFar);
				
		render::shader::initialize(hwnd);
		render::light::initialize();
	}

	void cleanup()
	{
		render::light::cleanup();
		
		render::shader::cleanup();
		d3d::cleanup();
	}

	static void setMatrices()
	{

		// Render main camera and get its view matrix;
		mainCamera->render();
		view = mainCamera->getView();
	}
}
