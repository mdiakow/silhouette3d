#ifndef RENDER_SHADER_H_
#define RENDER_SHADER_H_

#include <wrl/client.h>
#include <filesystem>
#include <vector>
#include <functional>

#include "D3D.h"

namespace verpix
{
	struct shader
	{
	public:
		shader(std::filesystem::path vertexShader, std::filesystem::path pixelShader, std::vector<D3D11_INPUT_ELEMENT_DESC> vertexLayoutDescriptions);
		// Creates a shader with no input layout. (Currently used for dynamicMesh).
		shader(std::filesystem::path vertexShader, std::filesystem::path pixelShader);
	public:
		// Sets this shaders internal data to the GPU pipeline
		void set();
	public:
		Microsoft::WRL::ComPtr<ID3D11VertexShader> vertexShader;
		Microsoft::WRL::ComPtr<ID3D11PixelShader> pixelShader;
		Microsoft::WRL::ComPtr<ID3D11SamplerState> samplerState;
		Microsoft::WRL::ComPtr<ID3D11InputLayout> inputLayout;
	};
}

#endif