#include "render/shader/shader.h"

#include "render/renderqueue.h"

#include <list>
#include <deque>

#include "render/core/coreShader.h"
#include "render/render.h"
#include "debugger/bunkdebug.h"
#include "mesh/meshVertex.h"

#include "shaders/cpp/shader2d.h"
#include "shaders/cpp/shaderui.h"
#include "shaders/cpp/shaderlight.h"
#include "shaders/cpp/shaderInstAniLight.h"
#include "shaders/cpp/shaderInstLine.h"
#include "shaders/cpp/shaderParticle.h"
#include "shaders/cpp/shaderDynamicMesh.h"
#include "shaders/cpp/shaderWater.h"
#include "shaders/cpp/shaderLine.h"

#include "buffer/constant/bufferConstant.h"

#include "systemCore/time/bunktime.h"

namespace render
{
	struct spaceConstants
	{
		m4 world;
		m4 viewPerspective;
		m4 viewOrthographic;
		m4 orthographic;
		m4 view;
		v3 cameraRight;
		fl32 scp1;
		v3 cameraUp;
		fl32 scp2;
		v3 cameraPosition;
		fl32 scp3;
	};

	// Internal: this manages data related to the shader step of the renderer.
	struct shading
	{
		std::deque<render::queue<verpix::shader, std::function<void()>>> queues;
		std::deque<str> names;

		buffer::constant<spaceConstants> spaces;

		shading(buffer::constant<spaceConstants> spaces) :
			spaces( spaces )
		{
		}
	};

	namespace shader
	{
		// Not sure if these are really the correct data structures, but I can always change it later. Shaders
		// will be constantly iterated through, so I feel like using a map doesn't really make as much sense.
		// Even though this looks like a name/value pair.
		
		static std::unique_ptr<shading> system;

		// GPU constant buffers
		//buffer::constant<spaceConstants> spaces(spaceConstants()); 

		void all()
		{		
			auto context = d3d::getDeviceContext();
			spaceConstants space;
			context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			// Calculate and set space matrices for various shaders to use.
			m4 orthographic = render::getOrtho();
			m4 view = render::getMainCamera()->getView();
			m4 perspective = render::getPerspective();

			space.viewPerspective = view*perspective;
			space.viewOrthographic = view*orthographic;
			space.world = m4::Identity();
			space.orthographic = orthographic;
			space.view = view;
		
			space.cameraRight = v3(view(0,0), view(1,0), view(2,0));
			space.cameraUp = v3(view(0,1), view(1,1), view(2,1));
			//space.cameraUp = render::getMainCamera()->
			space.cameraPosition = render::getMainCamera()->position;
		
			system->spaces.set(space);

			context->VSSetConstantBuffers(0, 1, system->spaces.data.GetAddressOf());

			for (auto &queue : system->queues) {
				queue.shader.set();

				queue.start();
				queue.all();
				queue.end();
			}		
		}

		void initialize(HWND hwnd)
		{
			system = std::make_unique<shading>(spaceConstants());
			//system->spaces = buffer::constant<spaceConstants>(spaceConstants());

			using renderQueue = render::queue<verpix::shader, std::function<void()>>;
			// 3D Light Shader
			namespace s3d = ::shader::light;
			system->queues.push_back(renderQueue(verpix::shader(s3d::vertex, s3d::pixel, s3d::description())));
			system->names.push_back(s3d::name);
			// 3D Animated Light Shader
			namespace si3d = ::shader::animated::light;
			system->queues.push_back(renderQueue(verpix::shader(si3d::vertex, si3d::pixel, si3d::description())));
			system->names.push_back(si3d::name);
			// 3D Dynamic Light Shader
			namespace sdm = ::shader::dynamicMesh;
			system->queues.push_back(renderQueue(verpix::shader(sdm::vertex(), sdm::pixel())));
			system->names.push_back(sdm::name());


			namespace s2d = ::shader::twod;
			system->queues.push_back(renderQueue(verpix::shader(s2d::vertex, s2d::pixel, s2d::description())));
			system->names.push_back(s2d::name);
			   
			for (auto &queue : system->queues) {
				queue.start = [](){};
				queue.end = [](){};
			}

			// 3D Water Shader
			namespace sw = ::shader::water;
			system->queues.push_back(renderQueue(verpix::shader(sw::vertex(), sw::pixel())));
			system->names.push_back(sw::name());
			system->queues.back().start = []() {
				//d3d::setParticleBlending();
				d3d::enableAlphaBlending();
			};
			system->queues.back().end = []() {
				d3d::disableAlphaBlending();
				//d3d::enableZBuffer();
			};
		
			namespace sp = ::shader::particle;
			system->queues.push_back(renderQueue(verpix::shader(sp::vertex(), sp::pixel(), sp::description())));
			system->names.push_back(sp::name());
			system->queues.back().start = []() {
				d3d::setParticleBlending();
				d3d::enableAlphaBlending();
			};
			system->queues.back().end = []() {
				d3d::disableAlphaBlending();
				d3d::enableZBuffer();
			};
		

			// Special case for line shader: Toggles line list on and off.
			namespace siLine = ::shader::instanced::line;
			system->queues.push_back(renderQueue(verpix::shader(siLine::vertex, siLine::pixel, siLine::description())));
			system->names.push_back(siLine::name);
			system->queues.back().start = []() {
				d3d::getDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
			};
			system->queues.back().end = []() {
				
			};

			namespace sLine = ::shader::line;
			system->queues.push_back(renderQueue(verpix::shader(sLine::vertex(), sLine::pixel(), sLine::description())));
			system->names.push_back(sLine::name());
			system->queues.back().start = []() {
				
			};
			system->queues.back().end = []() {
				d3d::getDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			};

			// UI has special case, must be rendered in order with alpha blending.
			namespace sui = ::shader::ui;
			system->queues.push_back(renderQueue(verpix::shader(sui::vertex, sui::pixel, sui::description())));
			system->queues.back().start = []() {
				d3d::disableZBuffer();
				d3d::enableAlphaBlending();
			};
			system->queues.back().end = []() {
				d3d::disableAlphaBlending();
				d3d::enableZBuffer();
			};
			system->names.push_back(sui::name);

		}

		void cleanup()
		{
	
		}
	}
}

namespace render
{
	render::queue<verpix::shader, std::function<void()>>* findShader(str shaderName)
	{
		for (ui32 i = 0; i < (ui32)shader::system->queues.size(); i++) {
			if (shaderName == shader::system->names[i]) {
				return &shader::system->queues[i];
			}
		}

		return nullptr;
	}
	/*
	render::queue<shader::t2d::data, std::function<void()>> *twod()
	{
		return ::shader::shader2d;
	}

	render::queue<shader::ui::data, std::function<void()>> *ui()
	{
		return ::shader::shaderui;
	}

	

	render::queue<shader::light::data, std::function<void()>> *threed()
	{
		return ::shader::shader3d;
	}
	render::queue<shader::instance::light::data, std::function<void()>>* inst3d()
	{
		return ::shader::shaderinstance3d;
	}*/
	/*
	render::queue<shader::base<vertex::colour, cbuffer::matrix>, std::function<void()>>* colour3d()
	{
	return ::shader::shadercolour3d;

	*/

}

