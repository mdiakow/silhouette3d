#ifndef SHADER_COMMON_H_
#define SHADER_COMMON_H_

#include <fstream>
#include <d3dcompiler.h>
#include "d3d.h"

#include <filesystem>
#include <wrl/client.h>

namespace shader
{
	// Helpers for writing shaders.
	namespace helper
	{
		void writeError(ID3D10Blob *errorMessage, HWND hwnd, LPCWSTR shaderFilename, LPCWSTR errorFilename);

		ID3D10Blob* compile(LPCWSTR shaderFile, LPCSTR entryPoint, LPCSTR version, LPCWSTR errorFile);
	}

	namespace compute
	{
		ID3D11ComputeShader *load(LPCWSTR shaderFile);
		Microsoft::WRL::ComPtr<ID3D11ComputeShader> load(std::filesystem::path shaderFile);

		Microsoft::WRL::ComPtr<ID3D11ComputeShader> find(std::filesystem::path shaderFile);
		void unload(std::filesystem::path shaderFile);

		ID3D10Blob* compile(LPCWSTR shaderFile);

	}

	namespace pixel
	{
		ID3D11PixelShader *load(LPCWSTR shaderFile);
		Microsoft::WRL::ComPtr<ID3D11PixelShader> load(std::filesystem::path shaderFile);
		ID3D10Blob* compile(LPCWSTR shaderFile);
	}

	namespace vertex
	{
		ID3D11VertexShader *load(LPCWSTR shaderFile);
		Microsoft::WRL::ComPtr<ID3D11VertexShader> load(std::filesystem::path shaderFile);
		ID3D10Blob* compile(LPCWSTR shaderFile);
	}
	

}

#endif