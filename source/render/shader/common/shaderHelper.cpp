#include "render/shader/common/shaderHelper.h"

#include <unordered_map>

#include "bunkcore.h"
#include <d3dcompiler.h>
#include "render/render.h"

//#define D3D_COMPILE_STANDARD_FILE_INCLUDE ((ID3DInclude*)(UINT_PTR)1)

namespace shader
{
	namespace helper
	{
		void writeError(ID3D10Blob *errorMessage, HWND hwnd, LPCWSTR shaderFilename, LPCWSTR errorFilename)
		{
			// Get a pointer to the error message text buffer.
			char* compileErrors = (char*)(errorMessage->GetBufferPointer());

			ui64 bufferSize = errorMessage->GetBufferSize();

			std::ofstream fout;
			fout.open(errorFilename);
			for(ui64 i = 0; i < bufferSize; i++)
			{
				fout << compileErrors[i];
			}
			fout.close();

			// Release the error message.
			errorMessage->Release();
			errorMessage = nullptr;

			std::wstring messageBoxOutput = L"Error compiling shader. Check " + std::wstring(errorFilename) + L" for details.";

			// Pop a message up on the screen to notify the user to check the text file for compile errors.
			MessageBox(hwnd, messageBoxOutput.c_str(), shaderFilename, MB_OK);
		}

		ID3D10Blob* compile(LPCWSTR shaderFile, LPCSTR entryPoint, LPCSTR version, LPCWSTR errorFile)
		{
			auto hwnd = render::getHwnd();

			ID3D10Blob* shaderBuffer;
			ID3D10Blob* errorMessage = nullptr;

			HRESULT result;

			// Compile both shaders to create them.
			result = D3DCompileFromFile(shaderFile, nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, entryPoint, version, D3D10_SHADER_ENABLE_STRICTNESS, 0, &shaderBuffer, &errorMessage);
			if (FAILED(result)) {
				if (errorMessage) {
					writeError(errorMessage, hwnd, shaderFile, errorFile);
				} else {
					MessageBox(hwnd, shaderFile, L"Missing Shader File", MB_OK);
				}
			}
			if (shaderBuffer) {
				return shaderBuffer;
			} else {
				return nullptr;
			}
		}
	}
}

namespace shader::compute
{
	
	std::unordered_map<std::string, Microsoft::WRL::ComPtr<ID3D11ComputeShader>> shaders;
	
	ID3D11ComputeShader *load(LPCWSTR shaderFile)
	{
		HRESULT result;
		auto device = d3d::getDevice();		

		auto byteCode = helper::compile(shaderFile, "csMain", "cs_5_0", L"errorComputeShader.txt");

		ID3D11ComputeShader *shader;
		result = device->CreateComputeShader(byteCode->GetBufferPointer(), byteCode->GetBufferSize(), nullptr, &shader);
		if(FAILED(result)) return nullptr;

		if(byteCode) byteCode->Release();

		return shader;
	}

	Microsoft::WRL::ComPtr<ID3D11ComputeShader> load(std::filesystem::path shaderFile)
	{
		HRESULT result;
		auto device = d3d::getDevice();		

		auto byteCode = helper::compile(shaderFile.wstring().c_str(), "csMain", "cs_5_0", L"errorComputeShader.txt");

		Microsoft::WRL::ComPtr<ID3D11ComputeShader> shader;
		result = device->CreateComputeShader(byteCode->GetBufferPointer(), byteCode->GetBufferSize(), nullptr, &shader);

		if(byteCode) byteCode->Release();

		return shader;
	}

	Microsoft::WRL::ComPtr<ID3D11ComputeShader> find(std::filesystem::path shaderFile)
	{
		auto key = shaderFile.string();
		auto shader = shaders.find(key);
		// If the compute shader doesn't already exist, compile it and insert it in to the map.
		if (shader != shaders.end()) {
			return shader->second;
		} else {
			auto newShader = load(shaderFile);
			shaders.insert({key, newShader});

			return newShader;
		}
	}

	void unload(std::filesystem::path shaderFile)
	{
		shaders.erase(shaderFile.string());
	}

	ID3D10Blob * shader::compute::compile(LPCWSTR shaderFile)
	{
		return helper::compile(shaderFile, "csMain", "cs_5_0", L"errorComputeShader.txt");
	}
}


ID3D11PixelShader *shader::pixel::load(LPCWSTR shaderFile)
{
	HRESULT result;
	auto device = d3d::getDevice();		

	auto byteCode = helper::compile(shaderFile, "psMain", "ps_5_0", L"errorPixelShader.txt");

	ID3D11PixelShader *shader;
	result = device->CreatePixelShader(byteCode->GetBufferPointer(), byteCode->GetBufferSize(), nullptr, &shader);
	if(FAILED(result)) return nullptr;

	if(byteCode) byteCode->Release();

	return shader;
}

Microsoft::WRL::ComPtr<ID3D11PixelShader> shader::pixel::load(std::filesystem::path shaderFile)
{
	HRESULT result;
	auto device = d3d::getDevice();		

	auto byteCode = helper::compile(shaderFile.wstring().c_str(), "psMain", "ps_5_0", L"errorPixelShader.txt");

	Microsoft::WRL::ComPtr<ID3D11PixelShader> shader;
	result = device->CreatePixelShader(byteCode->GetBufferPointer(), byteCode->GetBufferSize(), nullptr, &shader);

	if(byteCode) byteCode->Release();

	return shader;
}

ID3D10Blob * shader::pixel::compile(LPCWSTR shaderFile)
{
	return helper::compile(shaderFile, "psMain", "ps_5_0", L"errorPixelShader.txt");
}

ID3D11VertexShader *shader::vertex::load(LPCWSTR shaderFile)
{
	HRESULT result;
	auto device = d3d::getDevice();		

	auto byteCode = helper::compile(shaderFile, "vsMain", "vs_5_0", L"errorVertexShader.txt");

	ID3D11VertexShader *shader;
	result = device->CreateVertexShader(byteCode->GetBufferPointer(), byteCode->GetBufferSize(), nullptr, &shader);
	if(FAILED(result)) return nullptr;

	if(byteCode) byteCode->Release();

	return shader;
}

Microsoft::WRL::ComPtr<ID3D11VertexShader> shader::vertex::load(std::filesystem::path shaderFile)
{
	HRESULT result;
	auto device = d3d::getDevice();		

	auto byteCode = helper::compile(shaderFile.wstring().c_str(), "vsMain", "vs_5_0", L"errorVertexShader.txt");

	Microsoft::WRL::ComPtr<ID3D11VertexShader> shader;
	result = device->CreateVertexShader(byteCode->GetBufferPointer(), byteCode->GetBufferSize(), nullptr, &shader);

	if(byteCode) byteCode->Release();

	return shader;
}

ID3D10Blob * shader::vertex::compile(LPCWSTR shaderFile)
{
	return helper::compile(shaderFile, "vsMain", "vs_5_0", L"errorVertexShader.txt");
}
