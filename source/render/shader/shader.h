#ifndef SHADER_H_
#define SHADER_H_

#include "bunkcore.h"

#include "render/renderQueue.h"
#include "render/shader/common/shaderHelper.h"

#include "mesh/meshVertex.h"

#include "render/shader/verpixShader.h"




namespace render
{
	render::queue<verpix::shader, std::function<void()>>* findShader(str shaderName);

}

#endif