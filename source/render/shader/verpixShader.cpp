#include "verpixShader.h"

#include <fstream>

#include "render/shader/common/shaderHelper.h"
#include "error/errorLog.h"
#include "D3D.h"

namespace verpix
{

	shader::shader(std::filesystem::path vertex, std::filesystem::path pixel, std::vector<D3D11_INPUT_ELEMENT_DESC> vld)
	{
		HRESULT result;
		auto device = d3d::getDevice();

		auto vsb = ::shader::vertex::compile(vertex.c_str());
		auto psb = ::shader::pixel::compile(pixel.c_str());

		result = device->CreateVertexShader(vsb->GetBufferPointer(), vsb->GetBufferSize(), nullptr, vertexShader.GetAddressOf());
		result = device->CreatePixelShader(psb->GetBufferPointer(), psb->GetBufferSize(), nullptr, pixelShader.GetAddressOf());	

		result = device->CreateInputLayout(vld.data(), (ui32)vld.size(), vsb->GetBufferPointer(), vsb->GetBufferSize(), inputLayout.GetAddressOf());

		// Default sampler desc.
		D3D11_SAMPLER_DESC samplerDesc;
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		device->CreateSamplerState(&samplerDesc, samplerState.GetAddressOf());

		if(psb) psb->Release();
		if(vsb) vsb->Release();
	}

	shader::shader(std::filesystem::path vertexShader, std::filesystem::path pixelShader)
	{
		HRESULT result;
		auto device = d3d::getDevice();

		auto vsb = ::shader::vertex::compile(vertexShader.c_str());
		auto psb = ::shader::pixel::compile(pixelShader.c_str());

		result = device->CreateVertexShader(vsb->GetBufferPointer(), vsb->GetBufferSize(), nullptr, this->vertexShader.GetAddressOf());
		result = device->CreatePixelShader(psb->GetBufferPointer(), psb->GetBufferSize(), nullptr, this->pixelShader.GetAddressOf());

		// Default sampler desc.
		D3D11_SAMPLER_DESC samplerDesc;
		samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 1;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		device->CreateSamplerState(&samplerDesc, samplerState.GetAddressOf());

		if(psb) psb->Release();
		if(vsb) vsb->Release();
	}

	void shader::set()
	{
		auto context = d3d::getDeviceContext();

		context->PSSetShader(pixelShader.Get(), nullptr, 0);
		context->VSSetShader(vertexShader.Get(), nullptr, 0);

		context->PSSetSamplers(0, 1, samplerState.GetAddressOf());
		context->IASetInputLayout(inputLayout.Get());
	}
}
