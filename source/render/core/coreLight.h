#ifndef CORE_RENDER_LIGHT_H_
#define CORE_RENDER_LIGHT_H_

#include "bunkcore.h"

#include "render/light/lightTypes.h"

#include "buffer/constant/bufferConstant.h"
#include "buffer/structured/bufferStructured.h"

#include <memory>

namespace render
{
	namespace light
	{
		static const ui32 maxDirectionals = 2;
		static const ui32 maxPoints = 256;

		struct constants
		{ // Various counts for each light array.
			ui32 directionalSize;
			ui32 pointSize;
			fl32 padding[2];
		};

		void initialize();
		void cleanup();

		void all();
	}

	class lighting
	{
	public:
		lighting(render::light::constants constants);
		~lighting();
	public:
		buffer::constant<light::constants> gpuSizes;
		light::constants cpuSizes;

		buffer::structured::input<render::light::directional> directionals;
		buffer::structured::input<render::light::point> points;
	};

	
}

#endif