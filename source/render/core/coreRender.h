// coreRender.h
// Core functionality of render (initialize, cleanup, and the
// function for general update).
#ifndef CORE_RENDER_H_
#define CORE_RENDER_H_

#include <windows.h>
#include "bunkcore.h"

namespace render
{
	bool isFullscreen();

	// Function performs the set up of the render. It also performs
	// the rendering of all objects.
	void all();

	void initialize(ui32 screenWidth, ui32 screenHeight, HWND hwnd);
	void cleanup();
}


#endif