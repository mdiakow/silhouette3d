#include "render/core/coreLight.h"
#include "render/light/renderLight.h"

#include "render/light/lightTypes.h"

#include "buffer/constant/bufferConstant.h"
#include "buffer/structured/bufferStructured.h"

#include "d3d.h"

namespace render::light
{
	static std::unique_ptr<render::lighting> lightSystem;
	
	//bs::input<::light::directional> directionals;
	//bs::input<::light::point> points;
	
	template <> void add<render::light::point>(render::light::point &light)
	{
		lightSystem->points.set(lightSystem->cpuSizes.pointSize, light);

		lightSystem->cpuSizes.pointSize++;
		lightSystem->gpuSizes.set(lightSystem->cpuSizes);		
	}

	template <> void add<render::light::directional>(render::light::directional &light)
	{
		lightSystem->directionals.set(lightSystem->cpuSizes.directionalSize, light);

		lightSystem->cpuSizes.directionalSize++;
		lightSystem->gpuSizes.set(lightSystem->cpuSizes);
	}

	template <>	void add<render::light::directional>(std::vector<render::light::directional> &lights)
	{
		lightSystem->directionals.set(lights);

		lightSystem->cpuSizes.directionalSize = (ui32)lights.size();
		lightSystem->gpuSizes.set(lightSystem->cpuSizes);
	}

	template <>	void add<render::light::point>(std::vector<render::light::point> &lights)
	{
		lightSystem->points.set(lights);

		lightSystem->cpuSizes.pointSize = (ui32)lights.size();
		lightSystem->gpuSizes.set(lightSystem->cpuSizes);
	}

	void initialize()
	{
		render::light::constants c;
		c.directionalSize = 0;
		c.pointSize = 0;

		lightSystem = std::make_unique<render::lighting>(c);
	}

	void cleanup()
	{

	}

	void all()
	{
		auto context = d3d::getDeviceContext();
		
		ID3D11ShaderResourceView *views[2] = {
			lightSystem->directionals.view.Get(),
			lightSystem->points.view.Get()
		};

		context->PSSetShaderResources(1, 2, views);
		context->PSSetConstantBuffers(0, 1, lightSystem->gpuSizes.data.GetAddressOf());
	}
}

namespace render
{
	render::lighting::lighting(render::light::constants constants) :
		directionals( {render::light::directional(), render::light::maxDirectionals} ),
		points( {render::light::point(), render::light::maxPoints} ),
		cpuSizes( constants ),
		gpuSizes( {constants} )
	{
	}

	lighting::~lighting()
	{
	}
}
