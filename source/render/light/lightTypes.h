#ifndef LIGHT_TYPES_H_
#define LIGHT_TYPES_H_

#include "bunkcore.h"

namespace render
{
	namespace light
	{
		struct point
		{
			v4 colour;
			v3 position;
			fl32 range;
		};

		struct directional
		{
			v4 colour;
			v3 direction;
			fl32 intensity;
		};
	}
}


#endif