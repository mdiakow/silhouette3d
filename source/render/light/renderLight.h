#ifndef RENDER_LIGHT_H_
#define RENDER_LIGHT_H_

#include <vector>

#include "render/light/lightTypes.h"

namespace render
{
	namespace light
	{
		template <typename light_t>
		void add(light_t &light);

		template <typename light_t>
		void add(std::vector<light_t> &lights);
	}
}

#endif