// render.h
// The forward facing interface that allows other modules and objects to
// add their render functions. Also allows access to render matrices.

#ifndef RENDER_H_
#define RENDER_H_

#include <functional>

#include "d3d.h"
#include "camera/camera.h"

#include "mesh/meshVertex.h"

#include "shader/shader.h"

namespace render
{
	//shader::base<vertex::simple, cbuffer::matrix> *colour;

	// Add a function to the list of things to be rendered.
	void add(std::function<void()> fn);
	

	// Pass in the camera you'd like to render with.
	void setMainCamera(camera *cam);
	camera *getMainCamera();
	v3 getCameraPosition();

	v2 getScreenSize();

	// Render matrices
	m4 getWorld();
	m4 getView();
	m4 getPerspective();
	m4 getOrtho();

	HWND getHwnd();
}

#endif