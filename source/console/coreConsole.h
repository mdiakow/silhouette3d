#ifndef CORE_CONSOLE_H_
#define CORE_CONSOLE_H_

namespace console
{
	void update();
	void render();

	void initialize();
	void cleanup();
}

#endif