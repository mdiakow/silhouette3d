#include "console/coreConsole.h"
#include "console/console.h"

#include <map>

#include "bunkcore.h"
#include "userInterface/userInterface.h"

#include "input/input.h"
#include "update/update.h"
#include "render/render.h"

#include "debugger/bunkdebug.h"

namespace console
{
	input::button *openButton;
	input::button *enterButton;

	namespace ui = userInterface;

	std::unique_ptr<ui::text8::input> inputText;
	std::unique_ptr<ui::text8::line> displayText;
	std::unique_ptr<ui::box> backgroundBox;

	// internal function pointers to allow other things to be passed in.
	std::map<str, std::function<void()>> settings;

	bool isOpen;

	// coreConsole.h start
	void update()
	{		
		if (openButton->onPress()) {
			isOpen = !isOpen;
			if (isOpen) {
				input::text::setFocus(inputText.get());
			} else { // it's closed, reset focus.
				input::text::setFocus(nullptr);
			}
		}
		
		if (isOpen) {
			inputText->update();

			if (enterButton->onPress()) {
				str currentBuffer = inputText->getBuffer();
				auto fn = settings.find(currentBuffer);
				if (fn != settings.end()) {
					fn->second();
				} else {
					displayText->setText("Invalid Command: " + currentBuffer);
				}
				
				inputText->clear();
			}
		}

		
	}

	void render()
	{
		if (isOpen) {
			backgroundBox->render();
			displayText->render();
			inputText->render();
		}
	}

	void initialize()
	{
		// input start
		openButton = new input::button(new ui16{input::key::tilde}, 1);
		enterButton = new input::button(new ui16{input::key::enter}, 1);
		// input end

		// ui start
		isOpen = false;
	
		auto uiStyle = std::make_unique<ui::style>(ui::getDefaultFont(), ui::defaultNeutral(), ui::defaultMouseOver(), ui::defaultMousePress(), ui::defaultBorder());
		v2 lowerLeft = ui::screen::topLeft() - v2(0.f, 300.f);
		v2 upperRight = ui::screen::topRight();

		ui::boundingBox bounds(lowerLeft, upperRight);
		backgroundBox = std::make_unique<ui::box>(lowerLeft, upperRight, uiStyle->neutral.background);
		ui::textFormatting format = uiStyle->font->format;
		ui::margin margin = format.margin;

		v2 lowerLeftMargin = v2((fl32)margin.left, (fl32)margin.bottom);
		v2 upperRightMargin = v2((fl32)margin.right, (fl32)margin.top);

		// Calculations to place input at the bottom of the window.
		lowerLeft = bounds.getLowerLeft() + lowerLeftMargin;
		upperRight = bounds.getLowerRight() + v2(-(fl32)margin.right, (fl32)(format.betweenLines + format.fontHeight));
		std::regex validConsoleInput("[^0-9a-zA-Z ,._]");
		inputText = std::make_unique<ui::text8::input>(validConsoleInput, lowerLeft, upperRight, uiStyle.get());
		
		// Calculations to place an element at the top of the window.
		lowerLeft = bounds.getUpperLeft() - v2(-(fl32)margin.left, (fl32)(margin.top + format.fontHeight));
		upperRight = bounds.getUpperRight() - upperRightMargin;
		displayText = std::make_unique<ui::text8::line>("textHere", lowerLeft, upperRight, uiStyle->font);

		render::findShader("ui")->add(console::render);
		update::add(console::update);
	}

	void cleanup()
	{
		
	}
	// coreConsole.h end

	//////////////////////
	// console.h start
	//////////////////////
	void add(str command, std::function<void()> fn)
	{
		settings.insert_or_assign(command, fn);
	}

	void logText(std::stringstream &ss)
	{
		displayText->setText(ss.str());
	}

	static Eigen::IOFormat fmt2d(4, 0, ", ", " ", " ", " ");
	void log(const v2 &vec2)
	{
		std::stringstream ss;
		ss << vec2.format(fmt2d);
		displayText->setText(ss.str());
	}

	void log(const v3 &vec3)
	{
		std::stringstream ss;
		ss << vec3.format(fmt2d);
		displayText->setText(ss.str());
	}
	//console.h end
}