#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <functional>

#include "bunkcore.h"
#include "math/bunkmath.h"

namespace console
{
	void add(str command, std::function<void()> fn);

	void logText(std::stringstream &ss);
	
	void log(const v2 &vec2);
	void log(const v3 &vec3);

	template<typename T>
	void log(T input)
	{
		std::stringstream ss;
		ss << input;
		logText(ss);
	}
}

#endif