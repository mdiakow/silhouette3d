#include "genTree.h"
#include "math/linalg.h"
#include "math/bunkmath.h"

namespace generation::tree
{
	namespace traversal
	{
		// pre-order 
		void createSkeleton(node<stem> &current, node<stem> &parent, std::vector<vertex::line> &vertices, ui32 &indexCount);
		void grow(node<stem> &current, node<stem> &parent, growthPattern &pattern);
	}

	base::base(v3 position, v3 direction) :
		nodeCount{ 1 },
		root{ node<stem>(stem(position, direction)) }
	{
	}

	mesh::line<vertex::line> createSkeleton(base & tree)
	{
		std::vector<vertex::line> vertices;
		ui32 indexCount = 0;

		for (auto &child : tree.root.children) {
			traversal::createSkeleton(child, tree.root, vertices, indexCount);
		}

		std::vector<ui32> indices(indexCount);

		return mesh::line<vertex::line>(vertices.data(), indices.data(), (ui32)vertices.size(), (ui32)indices.size());
	}

	fl32 static pi = math::pi;
	static m3 rightRotation = linalg::rotation::yawPitchRoll(0.f, pi*0.25f, 0.f);
	static m3 leftRotation = linalg::rotation::yawPitchRoll(0.f, -pi*0.25f, 0.f);

	static v3 rightLean = v3(1.f, 1.f, 0.f).normalized();
	static v3 leftLean = v3(-1.f, 1.f, 0.f).normalized();
	void grow(base &tree, growthPattern &pattern)
	{
		// TEMP TEST
		pattern.split = 2;
		pattern.increasedLength = 1.3f;
		pattern.newLength = 0.5f;

		if (tree.root.children.size() > 0) {
			for (auto &child : tree.root.children) {
				traversal::grow(child, tree.root, pattern);
			}
		} else {
			auto &current = tree.root.data;
			v3 rightDirection = (current.direction + rightLean).normalized();
			v3 leftDirection = (current.direction + leftLean).normalized();

			v3 rightPosition = current.position + rightDirection*pattern.newLength;
			v3 leftPosition = current.position + leftDirection*pattern.newLength;

			tree.root.children.push_back( node<stem>({leftPosition, leftDirection}) );
			tree.root.children.push_back( node<stem>({rightPosition, rightDirection}) );
		}

	}

	namespace traversal
	{
		void createSkeleton(node<stem>& current, node<stem>& parent, std::vector<vertex::line>& vertices, ui32 & indexCount)
		{
			vertices.push_back({parent.data.position});
			vertices.push_back({current.data.position});

			indexCount++;
			for (auto & child : current.children) {
				createSkeleton(child, current, vertices, indexCount);
			}

		}

		void grow(node<stem> &current, node<stem> &parent, growthPattern &pattern)
		{
			if (current.children.size() > 0) {
				for (auto &child : current.children) {
					traversal::grow(child, current, pattern);
				}
			} else {
				v3 rightDirection = (current.data.direction + rightLean).normalized();
				v3 leftDirection = (current.data.direction + leftLean).normalized();

				v3 rightPosition = current.data.position + rightDirection*pattern.newLength;
				v3 leftPosition = current.data.position + leftDirection*pattern.newLength;

				current.children.push_back( node<stem>({leftPosition, leftDirection}) );
				current.children.push_back( node<stem>({rightPosition, rightDirection}) );
			}
		}
	}
}




