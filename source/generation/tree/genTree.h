#ifndef GEN_TREE_H_
#define GEN_TREE_H_

#include "bunkcore.h"

#include <vector>

#include "voxel/vpPanel.h"

#include "mesh/meshLine.h"
#include "shaders/cpp/shaderLine.h"
#include "shaders/cpp/shaderInstLine.h"

namespace generation
{
	namespace tree
	{
		struct growthPattern
		{
			ui32 split; // Number of splits per child node.
			fl32 newLength; // New length added to children.
			fl32 increasedLength; // scale 
		};

		template <class t>
		struct node
		{
			node(t data) : data{ data } {};

			t data;
			std::vector<node<t>> children;
		};
				
		struct stem
		{
			stem(v3 position, quat orientation) : position{ position }, orientation{ orientation } {};

			v3 position;
			quat orientation;			
		};

		struct base
		{
		public:
			base(v3 position, quat orientation);
		public:	
			node<stem> root;
		private:
			ui32 nodeCount;
		};

		// Debug tree that shows underlying skeleton.
		mesh::line<vertex::line> createSkeleton(base &tree);

		void grow(base &tree, growthPattern &pattern);
	}
}



#endif