This is a personal project where I have been learning directX. The only added library is
eigen (For matrices, vectors, quaternions).

The general end idea is to have a voxel editing program to be able to build 3D
environment geometry.

So far I have added:

##  Particle Simulation/System (source/animation/particle/):  
*  All computations reside on the GPU:
    *  Compute shader for creation/destruction of particles (uses a ring buffer to manage memory on the GPU) 
    *  Intermediary compute shader manages ring buffer indices.  
    *  final compute shader to simulate each particle while it is alive.  
    *  makes use of instanced indirect drawing so nothing needs to be copied back to the CPU to render properly.  
    *  makes use of depth stencil + alpha blend so I can avoid sorting.
	*  vector fields can be added to create more interesting particle movement.
	*  created an emitter field, which is just a series of position+direction to generate particles on.
	*  Uses a hybrid randomization scheme, I generate some random numbers on the CPU so that each emission on the GPU looks   different. I didn't do any formal tests, but earlier versions of RNG weren't even passing the eyeball test.
*  Current things to improve/add:   
    *  Animated particles/textures  
	*  Colour blending: Maybe I could use the pixel shader for this?
	*  Colour spaces: I need to dig my notes out on this, it's been a little while.
	*  Emission on arbitrary meshes.
	*  Note for myself: Consider some sort of physics system and rewrite the foliage simulation to use the vector field.
   
##  Water Surface Simulation (source/animation/water/):
*  GPU driven:
	*  Intermediary texture holds height map data for waves
	*  Vertex shader generates all geometry
	*  Compute shader to calculate sum of sine wave transforms (speed, direction, amplitude, frequency, etc)
	*  Applied lighting to the surface
	*  Tried a couple of small tests with changing alpha relative to camera view angle and experimented with water  
	colour, but wasn't really satisfied with it.
*  Things to improve/add:
	*  Interactivity (Shouldn't be too challenging by changing the texture)
	*  Foam, better textures, better water colour. I found a nice paper on waves and computer graphics so maybe  
	I can figure out a nice way to do it without textures.
	*  More expensive effects: Fresnel, reflection, etc)

##  Voxel Processing (source/voxel/):  
*  3D convolution masks (smoothing, edge finding)  
*  3D painting (to add voxels to arbitrary points in the voxel image)  
*  Feature extraction (thresholding once things have been convolved)  
*  Marching cubes (for meshing the voxel image)  
*  Pseudo random generation using the above (mixes CPU and GPU steps for the algorithm):  
	*  Apply an edge finding mask to target specific parts of the voxel image (GPU)    
	*  Return a set of these as a list of valid points (GPU*  >CPU)   
	*  Randomly select some subset of those as points to paint (CPU)  
	*  Use that set of points as input to paint functions (CPU*  >GPU)
	*  Run smoothing on voxel image (Currently just a 3D gaussian filter) (GPU)    
	*  Mesh the voxel image using marching cubes (GPU)
	*  I'd like to do: calculate texture coordinates, figure out a way to  
	avoid copying data back from the GPU when I'm creating the mesh using marching  
	cubes. Figure out some texture selection/blending to automatically paint the geometry  
	depending on the surface normal.

##  Mesh processing:  
*  I was trying to find a solution for creating a mesh from a quad built from bezier curves. The code for that is inside the mesh folder.  


##  A foliage simulation:  
*  Uses directX instancing to reduce memory overhead of model (1 model, rerendered
	n times)  
*  A single skeleton that holds a number of important features (spring coefficients
	for individual joints, children and the general layout of the tree for traversal)  
*  A large buffer holds current joint orientations for each instance.  
		*  Wind/force volumes are laid over top and any piece of foliage within any volume(s)
		receives the corresponding force(s).  
		*  It is a relatively simple model, it is not a full rigidbody simulation (no collision)
		and is not simulating on every vertex of the mesh, just the underlying skeleton.
		This means that torque may not behave perfectly but the hope is that a complex model
		like a tree would also work without having to do a huge amount of calculations.  
		*  It uses compute shaders for most calculations since the buffers are large and
		most computations are independent(think instance by instance computation, each 
			blade of grass doesn't need to know what its neighbours are doing)  

##  Custom input:  
*  Custom user input that is designed to be easy to use and change the key binds.  


##  Some debugging tools:  
*  Console to log things to (Great for testing real time input, where a debugger 
	can be more tedious/slow)  
*  File logging (For low level systems that are run before generation of UI)  
*  These aren't replacements for breakpoint and stepping through the code, but
they can be better for certain things (Checking solution to math at runtime
	and user input as stated earlier)  

##  A custom GUI (source/userInterface/):  
*  font rendering from scratch  
*  text output  
*  text input fields (example: set brush size/colour for canvas, change wind
	direction, force and other settings for foliage simulation)  
*  Custom tabs (essentially buttons, but closer to web browser tab implementation)  
*  I would like to add sliders and improve some of the code  
*  This part is being continuously refactored and added to as I add more modules.  

##  A drawing canvas (source/canvas/):  
*  Uses compute shaders to apply an arbitrary n by m brush to a 2D grid of
textures.  
*  It is one of the older pieces of code, and something that I would like to
refactor in the future.  
*  This will eventually be used to quickly prototype environment layouts and
eventually convert from 2D image to 3D voxels.  

##  Image Processing: (I refactored my drawing canvas and a bunch of other code and it broke):  
*  I can apply general n by m kernels to the drawing canvas. (Any of your standard
	edge finding, gaussian smoothing, etc).  
*  This could be made more general to just work on directX11 textures, but I think
it makes sense to refactor later when I approach the drawing canvas again. 